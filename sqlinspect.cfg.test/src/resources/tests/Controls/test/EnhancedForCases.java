package test;

import java.util.ArrayList;
import java.util.List;

public class EnhancedForCases {
	public void testEnhancedFor1() {
		List<Object> l = new ArrayList<>();
		for (Object o : l) {
			System.out.println(o);
		}
	}

	public void testEnhancedForContinue1(Object o2) {
		List<Object> l = new ArrayList<>();
		for (Object o : l) {
			System.out.println(o2);
			if (o.equals(o2)) {
				continue;
			}
			System.out.println(o);
		}
	}

	public void testEnhancedForContinue2(Object o2) {
		List<Object> l = new ArrayList<>();
		for (Object o : l) {
			System.out.println(o2);
			if (o.equals(o2)) {
				continue;
			}
			System.out.println(o);
		}
		System.out.println("Done");
	}

	public void testEnhancedForBreak1(Object o2) {
		List<Object> l = new ArrayList<>();
		for (Object o : l) {
			System.out.println(o2);
			if (o.equals(o2)) {
				break;
			}
			System.out.println(o);
		}
	}

	public void testEnhancedForBreak2(Object o2) {
		List<Object> l = new ArrayList<>();
		for (Object o : l) {
			System.out.println(o2);
			if (o.equals(o2)) {
				break;
			}
			System.out.println(o);
		}
		System.out.println("Done");
	}

	public int testEnhancedForReturn1(List<Integer> l) {
		for (Integer i : l) {
			if (i == 123) {
				return i;
			}
		}
		return 10;
	}

	public int testEnhancedForReturn2(List<Integer> l) {
		for (Integer i : l) {
			return i;
		}
		return 10;
	}
}