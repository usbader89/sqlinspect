package test;

public class ForCases {
	public void testFor1(int x) {
		for (;;)
			;
	}

	public void testFor2(int x) {
		for (int i = 0; i < 5; i++) {
			System.out.println("Iteration: " + i);
		}
	}

	public void testFor3(int x) {
		for (int i = 0, j = 2; i < 5; i++, j++) {
			System.out.println("Iteration: " + i);
		}
	}

	public void testForBreak1(int x) {
		for (int i = 0, j = 2; i < 5; i++, j++) {
			System.out.println("Iteration: " + i);
			if (i == 3) {
				break;
			}
		}
	}

	public void testForBreak2(int x) {
		for (int i = 0, j = 2; i < 5; i++, j++) {
			System.out.println("Iteration: " + i);
			if (i == 3) {
				break;
			}
		}
	}

	public void testForBreak3(int x) {
		for (int i = 0; i < 5; i++) {
			break;
		}
	}

	public void testForBreak4(String[] args) {
		int i = 0;
		for (; i <= 5; i++) {
			System.out.println("Iteration: " + i);
			if (i == 4) {
				i = 5;
				break;
			}
			System.out.println("Iteration end");
		}
		System.out.println("After: " + i);
	}

	public void testForContinue1(int x) {
		for (int i = 0, j = 2; i < 5; i++, j++) {
			System.out.println("Iteration: " + i);
			if (i == 3) {
				continue;
			}
			System.out.println("Iteration end");
		}
	}

	public void testForContinue2(int x) {
		for (int i = 0, j = 2; i < 5; i++, j++) {
			System.out.println("Iteration: " + i);
			if (i == 3) {
				continue;
			}
		}
	}

	public void testForContinue3(int x) {
		for (int i = 0; i < 5; i++) {
			continue;
		}
	}

	public void testForContinue4(String[] args) {
		int i = 0;
		for (; i <= 5; i++) {
			System.out.println("Iteration: " + i);
			if (i == 4) {
				i = 5;
				continue;
			}
			System.out.println("Iteration end");
		}
		System.out.println("After: " + i);
	}

	public int testForReturn1(int x) {
		for (int i = 1; i < 10; i++) {
			if (x * i > 10) {
				return x * i;
			}
		}
		x = x + 10;
		return x;
	}

	public int testForReturn2(int x) {
		for (int i = 1; i < 10; i++) {
			return x + 1;
		}
		x = x + 10;
		return x;
	}

}