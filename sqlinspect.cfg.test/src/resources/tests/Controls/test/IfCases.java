package test;

public class IfCases {
	public void testIf1(int x) {
		if (x > 10) {
			x = x + 1;
		}
	}

	public void testIf2(int x) {
		if (x > 10) {
			x = x + 1;
			return;
		}
		x++;
	}

	public void testIf3(int x) {
		if (x > 10) {
			x = x + 1;
		} else {
			x = x + 2;
		}
		x++;
	}

	public void testIf4(int x) {
		if (x > 10) {
			x = x + 1;
		} else if (x > 20) {
			x = x + 2;
		} else if (x > 30) {
			x = x + 3;
		} else {
			x = x + 4;
		}
		x++;
	}

	public void testIf5(int x) {
		if (x > 10) {
			x = x + 1;
		}
		if (x > 20) {
			x = x + 2;
		}
		if (x > 30) {
			x = x + 3;
		}
		if (x > 40) {
			x = x + 4;
		}
		x++;
	}

	public void testIf6(int x) {
		String s = "A";
		if (x > 10) {
			s += "B";
			if (x > 20) {
				s += "C";
			} else {
				s += "D";
			}
		} else {
			s += "E";
		}
		s += "F";
	}

	public void testIfReturn1(int x) {
		if (x > 10) {
			return;
		}
	}

	public int testIfReturn2(int x) {
		if (x > 10) {
			return x + 10;
		} else {
			return x + 12;
		}
	}

	public int testIfReturn3(int x) {
		if (x > 10) {
			x = x + 10;
			return x;
		}
		x = x + 20;
		return x;
	}

}