package sqlinspect.cfg.test;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.cfg.CFG;
import sqlinspect.cfg.CFGStore;
import sqlinspect.cfg.MethodDeclVisitor;
import sqlinspect.sqlan.SQLDialect;
import sqlinspect.uiplugin.Context;
import sqlinspect.uiplugin.PropertyStore;
import sqlinspect.uiplugin.extractors.AndroidHotspotFinder;
import sqlinspect.uiplugin.extractors.InterQueryResolver;
import sqlinspect.uiplugin.model.Model;
import sqlinspect.uiplugin.model.Project;
import sqlinspect.uiplugin.utils.ASTStore;
import sqlinspect.uiplugin.utils.ASTUtils;
import sqlinspect.uiplugin.utils.EclipseProjectUtil;

public class CFGControlsTest {
	private static final String UTF8 = "utf-8";

	private static final Logger logger = LoggerFactory.getLogger(CFGSimpleTest.class);

	protected static final String TESTS_DIR = System.getenv().get("CFGTEST_RES_DIR") + "/tests";
	protected static final String TEST_REF_DIR = System.getenv().get("CFGTEST_RES_DIR") + "/references";

	private final ASTStore astStore = new ASTStore();
	private final CFGStore cfgStore = new CFGStore();

	private final Map<String, MethodDeclaration> methodMap = new HashMap<>();

	private static final String projectname = "Controls";
	private static final String projectdir = TESTS_DIR + "/" + projectname;
	private static final String cfgSuffix = "-cfg.dump";
	private static final String cfgExportDir = projectname;
	private static final String cfgReferenceDir = TEST_REF_DIR + "/" + projectname;

	private void setProjectProperties(IProject iproj) {
		PropertyStore.setStringProperty(iproj, PropertyStore.PROP_STRING_RESOLVER,
				InterQueryResolver.class.getSimpleName());
		PropertyStore.setStringProperty(iproj, PropertyStore.PROP_HOTSPOT_FINDER,
				AndroidHotspotFinder.class.getSimpleName());
		PropertyStore.setStringProperty(iproj, PropertyStore.PROP_DIALECT, SQLDialect.SQLITE.toString());
		PropertyStore.setBooleanProperty(iproj, PropertyStore.PROP_RUN_SMELL_DETECTORS, false);
		PropertyStore.setBooleanProperty(iproj, PropertyStore.PROP_RUN_SQL_METRICS, false);
		PropertyStore.setBooleanProperty(iproj, PropertyStore.PROP_RUN_TABLE_ACCESS, false);
	}

	protected void createOutputDir(String outputDir) throws IOException {
		File dir = new File(outputDir);
		Path path = dir.toPath();
		if (!Files.exists(path)) {
			Files.createDirectory(path);
		}
	}

	private void analyzeProject(String projectname, String projectdir)
			throws CoreException, JavaModelException, FileNotFoundException, UnsupportedEncodingException, IOException {
		IProject iproj = EclipseProjectUtil.createEclipseProject(projectname, projectdir, null, null);
		Model model = Context.getInstance().getModel();
		Project project = model.createProject(iproj);
		setProjectProperties(iproj);

		Set<MethodDeclaration> methods = new HashSet<>();

		if (iproj.hasNature(JavaCore.NATURE_ID)) {
			IJavaProject javaProject = project.getIJavaProject();
			IPackageFragment[] packages = javaProject.getPackageFragments();
			for (IPackageFragment p : packages) {
				if (p.getKind() == IPackageFragmentRoot.K_SOURCE) {
					for (ICompilationUnit unit : p.getCompilationUnits()) {
						MethodDeclVisitor visitor = new MethodDeclVisitor();
						methods.addAll(visitor.run(astStore.get(unit)));
					}
				}
			}
		} else {
			logger.error("The project is not a Java project!");
		}

		for (MethodDeclaration method : methods) {
			methodMap.put(ASTUtils.getMethodQualifiedName(method), method);
			logger.debug("Method: {}", ASTUtils.getMethodQualifiedName(method));
		}
	}

	private void testMethodCFG(String methodSignature) throws IOException {
		final String cfgExport = cfgExportDir + "/" + methodSignature + cfgSuffix;
		final String cfgReference = cfgReferenceDir + "/" + methodSignature + cfgSuffix;
		logger.debug("Exporting CFG of method {} to {}", methodSignature, cfgExport);
		PrintWriter pw = new PrintWriter(new File(cfgExport), "UTF-8");

		MethodDeclaration md = methodMap.get(methodSignature);

		CFG cfg = cfgStore.get(md);

		cfg.dump(pw);

		pw.close();

		logger.debug("CFG exported to {}", cfgExport);
		assertEquals("The files differ!", FileUtils.readLines(new File(cfgExport), UTF8),
				FileUtils.readLines(new File(cfgReference), UTF8));
	}

	@Before
	public void prepareProject() throws CoreException, IOException {
		analyzeProject(projectname, projectdir);
		createOutputDir(cfgExportDir);
	}

	// Do While

	@Test
	public void testDo1() throws CoreException, IOException {
		testMethodCFG("test.DoCases.testDo1");
	}

	@Test
	public void testDo2() throws CoreException, IOException {
		testMethodCFG("test.DoCases.testDo2");
	}

	@Test
	public void test3() throws CoreException, IOException {
		testMethodCFG("test.DoCases.testDo3");
	}

	@Test
	public void testDoBreak1() throws CoreException, IOException {
		testMethodCFG("test.DoCases.testDoBreak1");
	}

	@Test
	public void testDoContinue1() throws CoreException, IOException {
		testMethodCFG("test.DoCases.testDoContinue1");
	}

	@Test
	public void testDoReturn1() throws CoreException, IOException {
		testMethodCFG("test.DoCases.testDoReturn1");
	}

	@Test
	public void testDoReturn2() throws CoreException, IOException {
		testMethodCFG("test.DoCases.testDoReturn2");
	}

	// Enhanced For Cases

	@Test
	public void testEnhancedFor1() throws CoreException, IOException {
		testMethodCFG("test.EnhancedForCases.testEnhancedFor1");
	}

	@Test
	public void testEnhancedForBreak1() throws CoreException, IOException {
		testMethodCFG("test.EnhancedForCases.testEnhancedForBreak1");
	}

	@Test
	public void testEnhancedForBreak2() throws CoreException, IOException {
		testMethodCFG("test.EnhancedForCases.testEnhancedForBreak2");
	}

	@Test
	public void testEnhancedForContinue1() throws CoreException, IOException {
		testMethodCFG("test.EnhancedForCases.testEnhancedForContinue1");
	}

	@Test
	public void testEnhancedForContinue2() throws CoreException, IOException {
		testMethodCFG("test.EnhancedForCases.testEnhancedForContinue2");
	}

	@Test
	public void testEnhancedForReturn1() throws CoreException, IOException {
		testMethodCFG("test.EnhancedForCases.testEnhancedForReturn1");
	}

	@Test
	public void testEnhancedForReturn2() throws CoreException, IOException {
		testMethodCFG("test.EnhancedForCases.testEnhancedForReturn2");
	}

	// For Cases

	@Test
	public void testFor1() throws CoreException, IOException {
		testMethodCFG("test.ForCases.testFor1");
	}

	@Test
	public void testFor2() throws CoreException, IOException {
		testMethodCFG("test.ForCases.testFor2");
	}

	@Test
	public void testFor3() throws CoreException, IOException {
		testMethodCFG("test.ForCases.testFor3");
	}

	@Test
	public void testForBreak1() throws CoreException, IOException {
		testMethodCFG("test.ForCases.testForBreak1");
	}

	@Test
	public void testForBreak2() throws CoreException, IOException {
		testMethodCFG("test.ForCases.testForBreak2");
	}

	@Test
	public void testForBreak3() throws CoreException, IOException {
		testMethodCFG("test.ForCases.testForBreak3");
	}

	@Test
	public void testForBreak4() throws CoreException, IOException {
		testMethodCFG("test.ForCases.testForBreak4");
	}

	@Test
	public void testForContinue1() throws CoreException, IOException {
		testMethodCFG("test.ForCases.testForContinue1");
	}

	@Test
	public void testForContinue2() throws CoreException, IOException {
		testMethodCFG("test.ForCases.testForContinue2");
	}

	@Test
	public void testForContinue3() throws CoreException, IOException {
		testMethodCFG("test.ForCases.testForContinue3");
	}

	@Test
	public void testForContinue4() throws CoreException, IOException {
		testMethodCFG("test.ForCases.testForContinue4");
	}

	@Test
	public void testForReturn1() throws CoreException, IOException {
		testMethodCFG("test.ForCases.testForReturn1");
	}

	@Test
	public void testForReturn2() throws CoreException, IOException {
		testMethodCFG("test.ForCases.testForReturn2");
	}

	// If Cases

	@Test
	public void testReturnIf1() throws CoreException, IOException {
		testMethodCFG("test.IfCases.testIfReturn1");
	}

	@Test
	public void testReturnIf2() throws CoreException, IOException {
		testMethodCFG("test.IfCases.testIfReturn2");
	}

	@Test
	public void testReturnIf3() throws CoreException, IOException {
		testMethodCFG("test.IfCases.testIfReturn3");
	}

	@Test
	public void testIf1() throws CoreException, IOException {
		testMethodCFG("test.IfCases.testIf1");
	}

	@Test
	public void testIf2() throws CoreException, IOException {
		testMethodCFG("test.IfCases.testIf2");
	}

	@Test
	public void testIf3() throws CoreException, IOException {
		testMethodCFG("test.IfCases.testIf3");
	}

	@Test
	public void testIf4() throws CoreException, IOException {
		testMethodCFG("test.IfCases.testIf4");
	}

	@Test
	public void testIf5() throws CoreException, IOException {
		testMethodCFG("test.IfCases.testIf5");
	}

	@Test
	public void testIf6() throws CoreException, IOException {
		testMethodCFG("test.IfCases.testIf6");
	}

	// Mixed Cases

	@Test
	public void testMixed1() throws CoreException, IOException {
		testMethodCFG("test.MixedCases.testMixed1");
	}

	// Body Cases

	@Test
	public void testBody1() throws CoreException, IOException {
		testMethodCFG("test.BodyCases.testBody1");
	}

	@Test
	public void testBody2() throws CoreException, IOException {
		testMethodCFG("test.BodyCases.testBody2");
	}

	@Test
	public void testBody3() throws CoreException, IOException {
		testMethodCFG("test.BodyCases.testBody3");
	}

	// Switch Cases

	@Test
	public void testSwitch1() throws CoreException, IOException {
		testMethodCFG("test.SwitchCases.testSwitch1");
	}

	@Test
	public void testSwitch2() throws CoreException, IOException {
		testMethodCFG("test.SwitchCases.testSwitch2");
	}

	@Test
	public void testSwitch3() throws CoreException, IOException {
		testMethodCFG("test.SwitchCases.testSwitch3");
	}

	@Test
	public void testSwitch4() throws CoreException, IOException {
		testMethodCFG("test.SwitchCases.testSwitch4");
	}

	@Test
	public void testSwitch5() throws CoreException, IOException {
		testMethodCFG("test.SwitchCases.testSwitch5");
	}

	@Test
	public void testSwitch6() throws CoreException, IOException {
		testMethodCFG("test.SwitchCases.testSwitch6");
	}

	@Test
	public void testSwitch7() throws CoreException, IOException {
		testMethodCFG("test.SwitchCases.testSwitch7");
	}

	@Test
	public void testSwitchReturn1() throws CoreException, IOException {
		testMethodCFG("test.SwitchCases.testSwitchReturn1");
	}

	@Test
	public void testSwitchReturn2() throws CoreException, IOException {
		testMethodCFG("test.SwitchCases.testSwitchReturn2");
	}

	// Try Cases
	@Test
	public void testTry1() throws CoreException, IOException {
		testMethodCFG("test.TryCases.testTry1");
	}

	@Test
	public void testTry2() throws CoreException, IOException {
		testMethodCFG("test.TryCases.testTry2");
	}

	@Test
	public void testTry3() throws CoreException, IOException {
		testMethodCFG("test.TryCases.testTry3");
	}

	@Test
	public void testTry4() throws CoreException, IOException {
		testMethodCFG("test.TryCases.testTry4");
	}

	@Test
	public void testTry5() throws CoreException, IOException {
		testMethodCFG("test.TryCases.testTry5");
	}

	@Test
	public void testTry6() throws CoreException, IOException {
		testMethodCFG("test.TryCases.testTry6");
	}

	@Test
	public void testTry7() throws CoreException, IOException {
		testMethodCFG("test.TryCases.testTry7");
	}

	@Test
	public void testTry8() throws CoreException, IOException {
		testMethodCFG("test.TryCases.testTry8");
	}

	@Test
	public void testTry9() throws CoreException, IOException {
		testMethodCFG("test.TryCases.testTry9");
	}

	@Test
	public void testWhile1() throws CoreException, IOException {
		testMethodCFG("test.WhileCases.testWhile1");
	}

	@Test
	public void testWhile2() throws CoreException, IOException {
		testMethodCFG("test.WhileCases.testWhile2");
	}

	@Test
	public void testWhileBreak1() throws CoreException, IOException {
		testMethodCFG("test.WhileCases.testWhileBreak1");
	}

	@Test
	public void testWhileBreak2() throws CoreException, IOException {
		testMethodCFG("test.WhileCases.testWhileBreak2");
	}

	@Test
	public void testWhileContinue1() throws CoreException, IOException {
		testMethodCFG("test.WhileCases.testWhileContinue1");
	}

	@Test
	public void testWhileContinue2() throws CoreException, IOException {
		testMethodCFG("test.WhileCases.testWhileContinue2");
	}

	@Test
	public void testWhileReturn1() throws CoreException, IOException {
		testMethodCFG("test.WhileCases.testWhileReturn1");
	}

	@Test
	public void testWhileReturn2() throws CoreException, IOException {
		testMethodCFG("test.WhileCases.testWhileReturn2");
	}
}
