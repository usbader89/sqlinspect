package sqlinspect.cfg.test;
import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.cfg.CFGStore;
import sqlinspect.cfg.MethodDeclVisitor;
import sqlinspect.sqlan.SQLDialect;
import sqlinspect.uiplugin.Context;
import sqlinspect.uiplugin.PropertyStore;
import sqlinspect.uiplugin.extractors.AndroidHotspotFinder;
import sqlinspect.uiplugin.extractors.InterQueryResolver;
import sqlinspect.uiplugin.model.Model;
import sqlinspect.uiplugin.model.Project;
import sqlinspect.uiplugin.utils.ASTStore;
import sqlinspect.uiplugin.utils.EclipseProjectUtil;

public class CFGSimpleTest {
	private static final String UTF8 = "utf-8";

	private static final Logger logger = LoggerFactory.getLogger(CFGSimpleTest.class);

	protected static final String TESTS_DIR = System.getenv().get("CFGTEST_RES_DIR") + "/tests";
	protected static final String TEST_REF_DIR = System.getenv().get("CFGTEST_RES_DIR") + "/references";

	private void setProjectProperties(IProject iproj) {
		PropertyStore.setStringProperty(iproj, PropertyStore.PROP_STRING_RESOLVER,
				InterQueryResolver.class.getSimpleName());
		PropertyStore.setStringProperty(iproj, PropertyStore.PROP_HOTSPOT_FINDER,
				AndroidHotspotFinder.class.getSimpleName());
		PropertyStore.setStringProperty(iproj, PropertyStore.PROP_DIALECT, SQLDialect.SQLITE.toString());
		PropertyStore.setBooleanProperty(iproj, PropertyStore.PROP_RUN_SMELL_DETECTORS, false);
		PropertyStore.setBooleanProperty(iproj, PropertyStore.PROP_RUN_SQL_METRICS, false);
		PropertyStore.setBooleanProperty(iproj, PropertyStore.PROP_RUN_TABLE_ACCESS, false);
	}

	private void analyzeProject(String projectname, String projectdir, String cfgExport, String cfgReference)
			throws CoreException, JavaModelException, FileNotFoundException, UnsupportedEncodingException, IOException {
		IProject iproj = EclipseProjectUtil.createEclipseProject(projectname, projectdir, null, null);
		Model model = Context.getInstance().getModel();
		Project project = model.createProject(iproj);
		setProjectProperties(iproj);

		ASTStore astStore = new ASTStore();
		CFGStore cfgStore = new CFGStore();

		Set<MethodDeclaration> methods = new HashSet<>();

		if (iproj.hasNature(JavaCore.NATURE_ID)) {
			IJavaProject javaProject = project.getIJavaProject();
			IPackageFragment[] packages = javaProject.getPackageFragments();
			for (IPackageFragment p : packages) {
				if (p.getKind() == IPackageFragmentRoot.K_SOURCE) {
					for (ICompilationUnit unit : p.getCompilationUnits()) {
						MethodDeclVisitor visitor = new MethodDeclVisitor();
						methods.addAll(visitor.run(astStore.get(unit)));
					}
				}
			}
		} else {
			logger.error("The project is not a Java project!");
		}

		methods.stream().forEach(method -> cfgStore.get(method));

		logger.debug("Exports", cfgExport);
		PrintWriter pw = new PrintWriter(new File(cfgExport), "UTF-8");
		cfgStore.dump(pw);
		pw.close();
		logger.debug("CFGStore exported to {}", cfgExport);
		assertEquals("The files differ!", FileUtils.readLines(new File(cfgExport), UTF8),
				FileUtils.readLines(new File(cfgReference), UTF8));
	}

	
	@Test
	public void testSimple() throws CoreException, IOException {
		String projectname = "Simple";
		String projectdir = TESTS_DIR + "/" + projectname;
		String cfgSuffix = "-cfg.dump";
		String cfgExport = projectname + cfgSuffix;
		String cfgReference = TEST_REF_DIR + "/" + projectname + cfgSuffix;

		analyzeProject(projectname, projectdir, cfgExport, cfgReference);
	}
}
