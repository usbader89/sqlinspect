package sqlinspect.sqlan.smells.util;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.sqlan.asg.base.Base;
import sqlinspect.sqlan.asg.expr.Id;
import sqlinspect.sqlan.asg.schema.Column;
import sqlinspect.sqlan.asg.visitor.Visitor;

public class ColumnRefsVisitor extends Visitor {
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(ColumnRefsVisitor.class);

	private final Set<Column> columns = new HashSet<>();

	private final Set<String> unresolved = new HashSet<>();

	@Override
	public void visit(Id n) {
		Base ref = n.getRefersTo();
		if (ref instanceof Column) {
			columns.add((Column) ref);
		} else if (n.getName() != null) {
			unresolved.add(n.getName());
		}
	}

	public Set<Column> getColumns() {
		return columns;
	}

	public Set<String> getUnResolved() {
		return unresolved;
	}
}
