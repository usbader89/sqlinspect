package sqlinspect.sqlan.smells.detectors;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.sqlan.asg.expr.Asterisk;
import sqlinspect.sqlan.asg.expr.Expression;
import sqlinspect.sqlan.asg.expr.ExpressionList;
import sqlinspect.sqlan.asg.expr.SelectExpression;
import sqlinspect.sqlan.asg.statm.Insert;
import sqlinspect.sqlan.asg.visitor.Visitor;
import sqlinspect.sqlan.smells.common.SQLSmell;
import sqlinspect.sqlan.smells.common.SQLSmellCertKind;
import sqlinspect.sqlan.smells.common.SQLSmellKind;

/*
 * "Although using wildcards and unnamed columns satisfies the goal of
 *  less typing, this habit creates several hazards."
 * 
 *    SELECT * FROM Bugs;
 *    INSERT INTO Bugs VALUES (DEFAULT, CURDATE(), 'New bug' , 'Test T987 fails...' ,
 *       NULL, 123, NULL, NULL, DEFAULT, 'Medium' , NULL);
 *    
 * Implementation:
 *  - if we find a star in the column list of a select exptession, we trigger a warning
 *  - if we find an insert statement withouth a column list, we trigger a warning
 */

public class ImplicitColumns extends Visitor {
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(RandomSelection.class);

	private final Set<SQLSmell> smells = new HashSet<>();

	private static final String MESSAGE = "Name columns explicitly.";

	@Override
	public void visit(SelectExpression n) {
		Expression columns = n.getColumnList();
		if (columns instanceof ExpressionList) {
			ExpressionList columnList = (ExpressionList) columns;
			for (Expression expr : columnList.getExpressions()) {
				if (expr instanceof Asterisk) {
					smells.add(
							new SQLSmell(n, SQLSmellKind.IMPLICIT_COLUMNS, MESSAGE, SQLSmellCertKind.NORMAL_CERTAINTY));
				}
			}
		}
	}

	@Override
	public void visit(Insert n) {
		if (n.getColumnList() == null) {
			smells.add(new SQLSmell(n, SQLSmellKind.IMPLICIT_COLUMNS, MESSAGE, SQLSmellCertKind.NORMAL_CERTAINTY));
		}
	}

	public Set<SQLSmell> getSmells() {
		return smells;
	}
}
