package sqlinspect.sqlan.smells.detectors;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.sqlan.SQLDialect;
import sqlinspect.sqlan.asg.expr.Expression;
import sqlinspect.sqlan.asg.expr.ExpressionList;
import sqlinspect.sqlan.asg.expr.FunctionCall;
import sqlinspect.sqlan.asg.expr.Id;
import sqlinspect.sqlan.asg.expr.InnerFunctionCall;
import sqlinspect.sqlan.asg.expr.Query;
import sqlinspect.sqlan.asg.expr.UserFunctionCall;
import sqlinspect.sqlan.asg.visitor.Visitor;
import sqlinspect.sqlan.smells.common.SQLSmell;
import sqlinspect.sqlan.smells.common.SQLSmellCertKind;
import sqlinspect.sqlan.smells.common.SQLSmellKind;

/*
 * "The most common SQL trick to pick a random row from a query is to
 * sort the query randomly and pick the first row. This technique is easy
 * to understand and easy to implement:
 * 
 *    SELECT * FROM Bugs ORDER BY RAND() LIMIT 1;
 *    
 * Although this is a popular solution, it quickly shows its weakness."
 * 
 * Implementation:
 *  - if we find rand() in the order by clause, it triggers a warning
 */

public class RandomSelection extends Visitor {
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(RandomSelection.class);

	private final Set<SQLSmell> smells = new HashSet<>();

	private static final String MYSQL_RAND = "RAND";
	private static final String IMPALA_RAND = "RAND";
	private static final String SQLITE_RAND = "RANDOM";

	private static final String MESSAGE = "Avoid sorting data randomly to select a random record.";

	private boolean inOrderBy;

	private final SQLDialect dialect;

	public RandomSelection(SQLDialect dialect) {
		super();
		this.dialect = dialect;
	}

	private void visit(FunctionCall n) {
		if (inOrderBy) {
			Expression func = n.getExpression();
			if (func instanceof Id && isRandomFunction(((Id) func).getName())) {
				smells.add(new SQLSmell(n, SQLSmellKind.RANDOM_SELECTION, MESSAGE, SQLSmellCertKind.NORMAL_CERTAINTY));
			}
		}
	}

	@Override
	public void visit(InnerFunctionCall n) {
		visit((FunctionCall) n);
	}

	@Override
	public void visit(UserFunctionCall n) {
		visit((FunctionCall) n);
	}

	@Override
	public void visitEdge_Query_OrderBy(Query node, ExpressionList end) {
		inOrderBy = true;
	}

	@Override
	public void visitEdgeEnd_Query_OrderBy(Query node, ExpressionList end) {
		inOrderBy = false;
	}

	public Set<SQLSmell> getSmells() {
		return smells;
	}

	private boolean isRandomFunction(String functionName) {
		if (dialect == SQLDialect.IMPALA) {
			return IMPALA_RAND.equalsIgnoreCase(functionName);
		} else if (dialect == SQLDialect.MYSQL) {
			return MYSQL_RAND.equalsIgnoreCase(functionName);
		} else if (dialect == SQLDialect.SQLITE) {
			return SQLITE_RAND.equalsIgnoreCase(functionName);
		} else {
			return false;
		}
	}
}
