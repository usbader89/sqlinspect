package sqlinspect.sqlan.smells.detectors;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.sqlan.SQLDialect;
import sqlinspect.sqlan.asg.base.Base;
import sqlinspect.sqlan.asg.common.ASGUtils;
import sqlinspect.sqlan.asg.expr.Alias;
import sqlinspect.sqlan.asg.expr.Expression;
import sqlinspect.sqlan.asg.expr.ExpressionList;
import sqlinspect.sqlan.asg.expr.FunctionCall;
import sqlinspect.sqlan.asg.expr.FunctionParams;
import sqlinspect.sqlan.asg.expr.Id;
import sqlinspect.sqlan.asg.expr.SelectExpression;
import sqlinspect.sqlan.asg.schema.Column;
import sqlinspect.sqlan.asg.traversal.PreOrderTraversal;
import sqlinspect.sqlan.asg.visitor.Visitor;
import sqlinspect.sqlan.smells.common.SQLSmell;
import sqlinspect.sqlan.smells.common.SQLSmellCertKind;
import sqlinspect.sqlan.smells.common.SQLSmellKind;
import sqlinspect.sqlan.smells.util.ColumnRefsVisitor;

/*
 * "Every column in the select-list of a query must have a single value row
 *  per row group. This is called the Single-Value Rule. Columns named in
 *  the GROUP BY clause are guaranteed to be exactly one value per group,
 *  no matter how many rows the group matches.
 *  The MAX ( ) expression is also guaranteed to result in a single value for
 *  each group: the highest value found in the argument of MAX ( ) over all
 *  the rows in the group.
 *  However, the database server can’t be so sure about any other column
 *  named in the select-list. It can’t always guarantee that the same value
 *  occurs on every row in a group for those other columns."
 *
 *  Implementation:
 *
 *  Check if the ids in the column list of a select expression are contained in either an
 *  aggregate function or the GROUP BY clause
 *
 *  MySQL reference:
 *    https://dev.mysql.com/doc/refman/5.7/en/group-by-handling.html
 */

public class AmbiguousGroups extends Visitor {
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(AmbiguousGroups.class);

	private final Set<SQLSmell> smells = new HashSet<>();

	private static final String MESSAGE = "'%s' refers to a non-grouped column '%s'";

	private final Map<SelectExpression, Set<Column>> groupByColumns = new HashMap<>();
	private final Map<SelectExpression, Set<String>> groupByColumnsUnResolved = new HashMap<>();

	private final Deque<SelectExpression> actualColumnList = new ArrayDeque<>();

	private boolean inSetFunctionCall;

	private final Deque<SelectExpression> actualSelectExpression = new ArrayDeque<>();
	private final Deque<SelectExpression> actualGroupBy = new ArrayDeque<>();

	private static final String[] MYSQL_SET_FUNCTIONS = new String[] { "AVG", "COUNT", "MIN", "MAX", "STD", "STD_DEV",
			"BIT_AND", "BIT_OR", "BIT_OR", "BIT_XOR", "VARIANCE", "SUM", "GROUP_CONCAT" };
	private static final String[] IMPALA_SET_FUNCTIONS = new String[] { "AVG", "COUNT", "DENSE_RANK", "FIRST_VALUE",
			"LAG", "LAST_VALUE", "LEAD", "MAX", "MIN", "RANK", "ROW_NUMBER", "SUM" };
	private static final String[] SQLITE_AGGREGATE_FUNCTIONS = new String[] { "AVG", "COUNT", "GROUP_CONCAT", "MAX",
			"MIN", "SUM", "TOTAL" };

	private final SQLDialect dialect;

	public AmbiguousGroups(SQLDialect dialect) {
		super();
		this.dialect = dialect;
	}

	@Override
	public void visit(SelectExpression n) {
		actualSelectExpression.addLast(n);

		ExpressionList groupBy = n.getGroupBy();
		if (groupBy != null) {

			ColumnRefsVisitor visitor = new ColumnRefsVisitor();
			PreOrderTraversal traversal = new PreOrderTraversal(visitor);
			traversal.traverse(groupBy);

			groupByColumns.put(n, visitor.getColumns());
			groupByColumnsUnResolved.put(n, visitor.getUnResolved());
		}
	}

	@Override
	public void visitEnd(SelectExpression n) {
		actualSelectExpression.removeLast();
	}

	@Override
	public void visit(Id n) {
		if (actualSelectExpression.isEmpty()) {
			return;
		}

		if (!actualColumnList.isEmpty() && actualColumnList.getLast() == actualSelectExpression.getLast()) {
			idInSelectList(n);
		}
	}

	@Override
	public void visitEdge_SelectExpression_ColumnList(SelectExpression node, Expression end) {
		actualColumnList.addLast(node);
	}

	@Override
	public void visitEdgeEnd_SelectExpression_ColumnList(SelectExpression node, Expression end) {
		actualColumnList.removeLast();
	}

	@Override
	public void visitEdge_SelectExpression_GroupBy(SelectExpression node, ExpressionList end) {
		actualGroupBy.addLast(node);
	}

	@Override
	public void visitEdgeEnd_SelectExpression_GroupBy(SelectExpression node, ExpressionList end) {
		actualGroupBy.removeLast();
	}

	@Override
	public void visitEdge_FunctionCall_ParamList(FunctionCall node, FunctionParams end) {
		if (isSetFunctionCall(node.getExpression())) {
			inSetFunctionCall = true;
		}
	}

	@Override
	public void visitEdgeEnd_FunctionCall_ParamList(FunctionCall node, FunctionParams end) {
		inSetFunctionCall = false;
	}

	private void idInSelectList(Id n) {
		// if we are in a setfunctioncall ignore this
		if (inSetFunctionCall) {
			return;
		}

		final Set<Column> columnsInGroupBy = groupByColumns.get(actualColumnList.getLast());
		final Set<String> unresolved = groupByColumnsUnResolved.get(actualColumnList.getLast());

		final Alias alias = hasAlias(n);

		final Base ref = n.getRefersTo();
		if (ref instanceof Column) {
			boolean notInGroupBy = columnsInGroupBy != null && !columnsInGroupBy.contains(ref);
			boolean notInUnresolved = n.getName() != null && unresolved != null && !unresolved.contains(n.getName());
			boolean aliasNotInGroupBy = alias != null && unresolved != null && !unresolved.contains(alias.getName());
			if (notInGroupBy && notInUnresolved && (alias == null || aliasNotInGroupBy)) {
				String message = String.format(MESSAGE, n.getName(), ASGUtils.getColumnFullName((Column) ref));

				smells.add(new SQLSmell(n, SQLSmellKind.AMBIGUOUS_GROUPS, message, SQLSmellCertKind.NORMAL_CERTAINTY));
			}
		}
	}

	public Set<SQLSmell> getSmells() {
		return smells;
	}

	private Alias hasAlias(Id id) {
		Base parent = id.getParent();
		while (parent instanceof Expression) {
			if (parent instanceof Alias) {
				return (Alias) parent;
			}
			parent = parent.getParent();
		}
		return null;
	}

	private boolean isSetFunctionCall(Expression callExpression) {
		if (callExpression instanceof Id) {
			Id funcId = (Id) callExpression;
			return isSetFunction(funcId);
		}
		return false;
	}

	private boolean isSetFunction(Id functionId) {
		if (dialect == SQLDialect.IMPALA) {
			return Arrays.asList(IMPALA_SET_FUNCTIONS).contains(functionId.getName().toUpperCase(Locale.ENGLISH));
		} else if (dialect == SQLDialect.MYSQL) {
			return Arrays.asList(MYSQL_SET_FUNCTIONS).contains(functionId.getName().toUpperCase(Locale.ENGLISH));
		} else if (dialect == SQLDialect.SQLITE) {
			return Arrays.asList(SQLITE_AGGREGATE_FUNCTIONS).contains(functionId.getName().toUpperCase(Locale.ENGLISH));
		} else {
			return false;
		}
	}
}
