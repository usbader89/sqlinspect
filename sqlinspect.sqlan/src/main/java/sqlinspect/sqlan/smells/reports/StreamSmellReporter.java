package sqlinspect.sqlan.smells.reports;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.sqlan.smells.common.SQLSmell;

public class StreamSmellReporter implements ISmellReporter {
	private static final Logger logger = LoggerFactory.getLogger(StreamSmellReporter.class);

	private final OutputStream stream;

	public StreamSmellReporter(OutputStream stream) {
		this.stream = stream;
	}

	@Override
	public void writeReport(Collection<SQLSmell> smells) {
		try {
			for (SQLSmell sqlSmell : smells) {
				writeSmell(stream, sqlSmell);
			}
		} catch (IOException e) {
			logger.error("IO error while writing to stream", e);
		}

	}

	public void writeSmell(OutputStream stream, SQLSmell smell) throws IOException {
		StringBuilder sb = new StringBuilder();
		sb.append(smell.getNode().getPath()).append('(').append(smell.getNode().getStartLine()).append("): ")
				.append(smell.getKind().toString()).append('(').append(smell.getCertainty().toString()).append("): ")
				.append(smell.getMessage()).append('\n');

		stream.write(sb.toString().getBytes(Charset.forName("UTF-8")));
	}

}
