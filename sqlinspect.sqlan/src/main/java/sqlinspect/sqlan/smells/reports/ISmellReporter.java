package sqlinspect.sqlan.smells.reports;

import java.io.IOException;
import java.util.Collection;

import sqlinspect.sqlan.smells.common.SQLSmell;

public interface ISmellReporter {
	void writeReport(Collection<SQLSmell> smells) throws IOException;
}
