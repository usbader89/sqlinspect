package sqlinspect.sqlan.smells.reports;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.sqlan.smells.common.SQLSmell;

public class TXTSmellReporter implements ISmellReporter {
	private static final Logger logger = LoggerFactory.getLogger(TXTSmellReporter.class);

	private final String path;

	private static final String UTF8 = "UTF-8";

	public TXTSmellReporter(String path) {
		this.path = path;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see sqlinspect.sqlan.smells.SmellReporter#writeReport(java.util.Collection)
	 */
	@Override
	public void writeReport(Collection<SQLSmell> smells) {
		try (OutputStreamWriter ow = new OutputStreamWriter(Files.newOutputStream(new File(path).toPath()), UTF8);
				BufferedWriter bw = new BufferedWriter(ow);) {

			for (SQLSmell sqlSmell : smells) {
				writeSmell(bw, sqlSmell);
			}
		} catch (IOException e) {
			logger.error("IO error while writing report!", e);
		}
	}

	public void writeSmell(BufferedWriter bw, SQLSmell smell) throws IOException {
		StringBuilder sb = new StringBuilder();
		sb.append(smell.getNode().getPath()).append('(').append(smell.getNode().getStartLine()).append("): ")
				.append(smell.getKind().toString()).append('(').append(smell.getCertainty().toString()).append("): ")
				.append(smell.getMessage()).append('\n');

		bw.write(sb.toString());
	}

}
