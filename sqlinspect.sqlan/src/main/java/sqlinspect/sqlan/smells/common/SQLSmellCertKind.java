package sqlinspect.sqlan.smells.common;

public enum SQLSmellCertKind {
	HIGH_CERTAINTY,
	NORMAL_CERTAINTY,
	LOW_CERTAINTY
}
