package sqlinspect.sqlan.smells.common;

import java.util.Objects;

import sqlinspect.sqlan.asg.base.Base;

public class SQLSmell {

	private Base node;

	private SQLSmellKind kind;

	private String message;

	private SQLSmellCertKind certainty;

	private String file;

	private int line;

	public SQLSmell(Base node, SQLSmellKind kind, String message, SQLSmellCertKind certainty) {
		this.node = node;
		this.kind = kind;
		this.message = message;
		this.certainty = certainty;
		this.file = "";
		this.line = 0;
	}

	public SQLSmell(Base node, SQLSmellKind kind, String message, SQLSmellCertKind certainty, String file, int line) {
		this.node = node;
		this.kind = kind;
		this.message = message;
		this.certainty = certainty;
		this.file = file;
		this.line = line;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		} else if (o instanceof SQLSmell) {
			SQLSmell s = (SQLSmell) o;
			return Objects.equals(s.getNode(), node) && Objects.equals(s.getKind(), kind)
					&& Objects.equals(s.getFile(), file) && Objects.equals(s.getLine(), line)
					&& Objects.equals(s.getMessage(), message) && Objects.equals(s.getCertainty(), certainty);
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return Objects.hash(node, file, line, kind, certainty);
	}

	/**
	 * @return the node
	 */
	public Base getNode() {
		return node;
	}

	/**
	 * @param node
	 *            the node to set
	 */
	public void setNode(Base node) {
		this.node = node;
	}

	/**
	 * @return the kind
	 */
	public SQLSmellKind getKind() {
		return kind;
	}

	/**
	 * @param kind
	 *            the kind to set
	 */
	public void setKind(SQLSmellKind kind) {
		this.kind = kind;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the certainty
	 */
	public SQLSmellCertKind getCertainty() {
		return certainty;
	}

	/**
	 * @param certainty
	 *            the certainty to set
	 */
	public void setCertainty(SQLSmellCertKind certainty) {
		this.certainty = certainty;
	}

	/**
	 * @return the file
	 */
	public String getFile() {
		return file;
	}

	/**
	 * @param file
	 *            the file to set
	 */
	public void setFile(String file) {
		this.file = file;
	}

	/**
	 * @return the line
	 */
	public int getLine() {
		return line;
	}

	/**
	 * @param line
	 *            the line to set
	 */
	public void setLine(int line) {
		this.line = line;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SQLSmell [node=" + node + ", kind=" + kind + " certainty=" + certainty + " message=" + message
				+ " file: " + file + " sline: " + line + "]";
	}

}
