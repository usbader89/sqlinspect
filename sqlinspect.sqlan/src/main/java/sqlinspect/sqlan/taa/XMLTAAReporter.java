package sqlinspect.sqlan.taa;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.Map;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.sqlan.asg.common.ASG;
import sqlinspect.sqlan.asg.common.ASGUtils;
import sqlinspect.sqlan.asg.schema.Column;
import sqlinspect.sqlan.asg.schema.Database;
import sqlinspect.sqlan.asg.schema.Table;
import sqlinspect.sqlan.asg.statm.Statement;

public class XMLTAAReporter implements ITAAReporter {
	private static final Logger logger = LoggerFactory.getLogger(XMLTAAReporter.class);

	private static final String ROOT_NODE = "TableAccesses";
	private static final String DATABASE_NODE = "Database";
	private static final String STATEMENT_NODE = "Statement";
	private static final String STATEMENT_ATTR_ID = "Id";
	private static final String STATEMENT_ATTR_PATH = "Path";
	private static final String STATEMENT_ATTR_LINE = "Line";
	private static final String TABLES_NODE = "Tables";
	private static final String TABLE_NODE = "Table";
	private static final String TABLE_ATTR_NAME = "name";
	private static final String COLUMNS_NODE = "Columns";
	private static final String COLUMN_NODE = "Column";
	private static final String COLUMN_ATTR_NAME = "name";

	private static final String TABC = "  ";
	private static final String NLC = "\n";

	private final String path;

	private final ASG asg;

	private final Map<Statement, Map<Table, Integer>> tableAcc;
	private final Map<Statement, Map<Column, Integer>> columnAcc;

	private XMLStreamWriter writer;
	private int indent;

	public XMLTAAReporter(ASG asg, String path, Map<Statement, Map<Table, Integer>> tableAcc,
			Map<Statement, Map<Column, Integer>> columnAcc) {
		this.asg = asg;
		this.path = path;
		this.tableAcc = tableAcc;
		this.columnAcc = columnAcc;
		indent = 1;
	}

	@Override
	public void writeReport() {
		XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
		OutputStream ostream = null;

		try {
			ostream = Files.newOutputStream(new File(path).toPath());
			writer = outputFactory.createXMLStreamWriter(ostream);
			writer.writeStartDocument();
			nl();

			writer.writeStartElement(ROOT_NODE);
			nl();

			for (Database db : asg.getRoot().getDatabases()) {
				tab(indent++);
				writer.writeStartElement(DATABASE_NODE);
				nl();

				for (Statement stmt : db.getStatements()) {
					writeAccesses(stmt);

				}
				writer.writeEndElement();
				tab(--indent);
				nl();
			}

			writer.writeEndElement();
			nl();
			writer.writeEndDocument();
			writer.flush();
		} catch (FileNotFoundException e) {
			logger.error("Could not open file.", e);
		} catch (XMLStreamException e) {
			logger.error("Could not write XML.", e);
		} catch (IOException e) {
			logger.error("IO error.", e);
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (XMLStreamException e) {
					logger.error("Could not clean XMLStreamWriter!", e);
				}
			}
			if (ostream != null) {
				try {
					ostream.close();
				} catch (IOException e) {
					logger.error("Could not clean OutputStream!", e);
				}
			}
		}
	}

	private void writeAccesses(Statement stmt) throws XMLStreamException {
		tab(indent++);
		writer.writeStartElement(STATEMENT_NODE);
		writer.writeAttribute(STATEMENT_ATTR_ID, Integer.toString(stmt.getId()));
		String lpath = stmt.getPath();
		if (lpath != null) {
			writer.writeAttribute(STATEMENT_ATTR_PATH, lpath);
			writer.writeAttribute(STATEMENT_ATTR_LINE, Integer.toString(stmt.getStartLine()));
		}
		nl();

		writeTableAccesses(stmt);
		writeColumnAccesses(stmt);

		tab(--indent);
		writer.writeEndElement();
		nl();
	}

	private void writeColumnAccesses(Statement stmt) throws XMLStreamException {
		Map<Column, Integer> cac = columnAcc.get(stmt);
		if (cac != null) {
			tab(indent++);
			writer.writeStartElement(COLUMNS_NODE);
			nl();
			for (Column col : cac.keySet()) {
				tab(indent);
				writer.writeStartElement(COLUMN_NODE);
				writer.writeAttribute(COLUMN_ATTR_NAME, ASGUtils.getQualifiedName(col));
				writer.writeEndElement();
				nl();
			}
			tab(--indent);
			writer.writeEndElement();
			nl();
		}
	}

	private void writeTableAccesses(Statement stmt) throws XMLStreamException {
		Map<Table, Integer> tac = tableAcc.get(stmt);
		if (tac != null) {
			tab(indent++);
			writer.writeStartElement(TABLES_NODE);
			nl();
			for (Table tab : tac.keySet()) {
				tab(indent);
				writer.writeStartElement(TABLE_NODE);
				writer.writeAttribute(TABLE_ATTR_NAME, ASGUtils.getQualifiedName(tab));
				writer.writeEndElement();
				nl();
			}
			tab(--indent);
			writer.writeEndElement();
			nl();
		}
	}

	private void nl() throws XMLStreamException {
		writer.writeCharacters(NLC);
	}

	private void tab(int level) throws XMLStreamException {
		for (int i = 0; i < level; i++) {
			writer.writeCharacters(TABC);
		}
	}
}
