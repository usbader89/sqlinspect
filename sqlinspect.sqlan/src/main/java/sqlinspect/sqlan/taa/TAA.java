package sqlinspect.sqlan.taa;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.sqlan.asg.base.Base;
import sqlinspect.sqlan.asg.expr.Alias;
import sqlinspect.sqlan.asg.expr.Asterisk;
import sqlinspect.sqlan.asg.expr.BinaryExpression;
import sqlinspect.sqlan.asg.expr.Expression;
import sqlinspect.sqlan.asg.expr.ExpressionList;
import sqlinspect.sqlan.asg.expr.Id;
import sqlinspect.sqlan.asg.expr.Join;
import sqlinspect.sqlan.asg.expr.SelectExpression;
import sqlinspect.sqlan.asg.kinds.BinaryExpressionKind;
import sqlinspect.sqlan.asg.schema.Column;
import sqlinspect.sqlan.asg.schema.Table;
import sqlinspect.sqlan.asg.statm.Delete;
import sqlinspect.sqlan.asg.statm.Insert;
import sqlinspect.sqlan.asg.statm.Select;
import sqlinspect.sqlan.asg.statm.Statement;
import sqlinspect.sqlan.asg.statm.Update;
import sqlinspect.sqlan.asg.visitor.Visitor;

public class TAA extends Visitor {
	private static final Logger logger = LoggerFactory.getLogger(TAA.class);

	private Optional<Statement> actualStmt = Optional.empty();

	private final Map<Statement, Map<Column, Integer>> columnAcc = new HashMap<>();
	private final Map<Statement, Map<Table, Integer>> tableAcc = new HashMap<>();

	public static class AccessMode {
		public static final int CREATE = 1;
		public static final int READ = 2;
		public static final int UPDATE = 4;
		public static final int DELETE = 8;
		public static final int IMPLICIT = 16;

		private AccessMode() {
		}		
	}

	/**
	 * @return the columnAcc
	 */
	public Map<Statement, Map<Column, Integer>> getColumnAcc() {
		return columnAcc;
	}

	/**
	 * @return the tableAcc
	 */
	public Map<Statement, Map<Table, Integer>> getTableAcc() {
		return tableAcc;
	}

	public void visit(Statement n) {
		actualStmt = Optional.of(n);
	}

	public void visitEnd(Statement n) {
		actualStmt = Optional.empty();
	}

	@Override
	public void visit(Insert n) {
		visit((Statement) n);
		Expression tab = n.getTable();
		if (tab != null) {
			addAllColumsnOfFromElement(tab, AccessMode.CREATE);
		}
	}

	@Override
	public void visitEnd(Insert n) {
		visitEnd((Statement) n);
	}

	@Override
	public void visit(Delete n) {
		visit((Statement) n);
		Expression from = n.getFrom();
		if (from != null) {
			addAllColumsnOfFromElement(from, AccessMode.DELETE);
		}
		Expression using = n.getUsing();
		if (using != null) {
			addAllColumsnOfFromElement(using, AccessMode.DELETE);
		}
	}

	@Override
	public void visitEnd(Delete n) {
		visitEnd((Statement) n);
	}

	@Override
	public void visit(Select n) {
		visit((Statement) n);
	}

	@Override
	public void visitEnd(Select n) {
		visitEnd((Statement) n);
	}

	@Override
	public void visit(Update n) {
		visit((Statement) n);
	}

	@Override
	public void visitEnd(Update n) {
		visitEnd((Statement) n);
	}

	@Override
	public void visit(SelectExpression n) {
		Expression columnList = n.getColumnList();
		Expression from = n.getFrom();
		if (columnList != null) {
			getColumnAccesses(columnList, from);
		}
	}

	@Override
	public void visit(Id n) {
		if (!actualStmt.isPresent()) {
			return;
		}

		Base ref = n.getRefersTo();
		if (ref != null) {
			if (ref instanceof Column) {
				addColumnAccess(actualStmt.get(), (Column) ref, AccessMode.READ);
			} else if (ref instanceof Table) {
				addTableAccess(actualStmt.get(), (Table) ref, AccessMode.READ);
			}
		}
	}

	private void getColumnAccesses(Expression columnList, Expression from) {
		if (columnList instanceof BinaryExpression) {
			getColumnAccesses((BinaryExpression) columnList, AccessMode.READ);
		} else if (columnList instanceof ExpressionList) {
			getColumnAccesses((ExpressionList) columnList, from);
		} else if (columnList instanceof Asterisk) {
			getAllColumnAccesses(from, AccessMode.READ);
		} else if (columnList instanceof Alias || columnList instanceof Id) {
			// we don't need to do anything, we will visit the Id through the preorder visit
		} else if (columnList != null) {
			logger.warn("Unhandled Expression kind in getColumnAccesses : {}", columnList);
		} else {
			// this can happen if the AST is not complete, ignore this case
		}
	}

	private void getColumnAccesses(BinaryExpression expr, int mode) {
		final Expression left = expr.getLeft();
		final Expression right = expr.getRight();
		if (expr.getKind() == BinaryExpressionKind.FIELDSELECTOR && left != null && right != null) {
			if (right instanceof Asterisk) {
				addAllColumsnOfFromElement(left, mode | AccessMode.IMPLICIT);
			}
		} else {
			// we don't have to do anything here
		}
	}

	private void getColumnAccesses(ExpressionList exprList, Expression from) {
		for (Expression expr : exprList.getExpressions()) {
			getColumnAccesses(expr, from);
		}
	}

	private void getAllColumnAccesses(Expression from, int mode) {
		if (from != null) {
			addAllColumsnOfFromElement(from, mode | AccessMode.IMPLICIT);
		}
	}

	private Base resolveAlias(Alias a) {
		Expression expr = a.getExpression();
		if (expr != null) {
			if (expr instanceof Id) {
				Id id = (Id) expr;
				Base ref = id.getRefersTo();
				if (ref instanceof Alias) {
					return resolveAlias((Alias) ref);
				} else {
					return ref;
				}
			} else {
				return expr;
			}
		}
		return null;
	}

	private void addColumnAccess(Statement stmt, Column col, int accessMode) {
		logger.debug("New Column access {} - {} - {}", stmt, col, accessMode);
		Map<Column, Integer> acc = columnAcc.computeIfAbsent(stmt, k->new HashMap<>());
		Integer oldMode = acc.get(col);
		acc.put(col, oldMode == null ? accessMode : accessMode | oldMode);
	}

	private void addTableAccess(Statement stmt, Table tab, int accessMode) {
		logger.debug("New Table access {} - {} - {}", stmt, tab, accessMode);
		Map<Table, Integer> acc = tableAcc.computeIfAbsent(stmt, k-> new HashMap<>());
		Integer oldMode = acc.get(tab);
		acc.put(tab, oldMode == null ? accessMode : accessMode | oldMode);
	}

	private void addAllColumnsOfTable(Table tab, int accessMode) {
		if (actualStmt.isPresent()) {
			for (Column col : tab.getColumns()) {
				addColumnAccess(actualStmt.get(), col, accessMode);
			}
		}
	}

	private void addAllColumsnOfFromElement(Expression expr, int accessMode) {
		if (expr instanceof ExpressionList) {
			addAllColumsnOfFromElement((ExpressionList) expr, accessMode);
		} else if (expr instanceof Id) {
			addAllColumsnOfFromElement((Id) expr, accessMode);
		} else if (expr instanceof Alias) {
			addAllColumsnOfFromElement((Alias) expr, accessMode);
		} else if (expr instanceof Join) {
			addAllColumsnOfFromElement((Join) expr, accessMode);
		} else if (expr instanceof SelectExpression) {
			// no need to do anything, we will add the columns while visiting the node
		} else {
			logger.warn("Unhandled from element: {}", expr);
		}
	}

	private void addAllColumsnOfFromElement(ExpressionList list, int accessMode) {
		for (Expression expr : list.getExpressions()) {
			addAllColumsnOfFromElement(expr, accessMode);
		}
	}

	private void addAllColumsnOfFromElement(Id expr, int accessMode) {
		Base ref = expr.getRefersTo();
		if (ref instanceof Table) {
			addAllColumnsOfTable((Table) ref, accessMode);
		}
	}

	private void addAllColumsnOfFromElement(Alias expr, int accessMode) {
		Base ref = resolveAlias(expr);
		if (ref != null) {
			if (ref instanceof Table) {
				addAllColumnsOfTable((Table) ref, accessMode);
			} else if (ref instanceof Expression) {
				addAllColumsnOfFromElement((Expression) ref, accessMode);
			}
		}
	}

	private void addAllColumsnOfFromElement(Join join, int accessMode) {
		Expression left = join.getLeft();
		if (left != null) {
			addAllColumsnOfFromElement(left, accessMode);
		}

		Expression right = join.getRight();
		if (right != null) {
			addAllColumsnOfFromElement(right, accessMode);
		}
	}

}
