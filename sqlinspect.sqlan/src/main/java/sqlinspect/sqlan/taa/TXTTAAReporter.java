package sqlinspect.sqlan.taa;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.sqlan.asg.common.ASG;
import sqlinspect.sqlan.asg.common.ASGUtils;
import sqlinspect.sqlan.asg.schema.Column;
import sqlinspect.sqlan.asg.schema.Database;
import sqlinspect.sqlan.asg.schema.Table;
import sqlinspect.sqlan.asg.statm.Statement;

public class TXTTAAReporter implements ITAAReporter {
	private static final Logger logger = LoggerFactory.getLogger(TXTTAAReporter.class);

	private final String path;

	private final ASG asg;

	private final Map<Statement, Map<Table, Integer>> tableAcc;
	private final Map<Statement, Map<Column, Integer>> columnAcc;

	private static final String UTF8 = "UTF-8";

	public TXTTAAReporter(ASG asg, String path, Map<Statement, Map<Table, Integer>> tableAcc,
			Map<Statement, Map<Column, Integer>> columnAcc) {
		this.asg = asg;
		this.path = path;
		this.tableAcc = tableAcc;
		this.columnAcc = columnAcc;
	}

	@Override
	public void writeReport() {
		try (OutputStreamWriter ow = new OutputStreamWriter(Files.newOutputStream(new File(path).toPath()), UTF8);
				BufferedWriter bw = new BufferedWriter(ow);) {
			for (Database db : asg.getRoot().getDatabases()) {
				for (Statement stmt : db.getStatements()) {
					writeAccesses(bw, stmt);
				}
			}
		} catch (IOException e) {
			logger.error("IO error while writing report!", e);
		}
	}

	private void writeAccesses(BufferedWriter bw, Statement stmt) throws IOException {
		bw.write("Statement (" + stmt.getId() + ")");
		String lpath = stmt.getPath();
		if (lpath != null) {
			bw.write(" at " + lpath + '(' + stmt.getStartLine() + ")");
		}
		bw.write(" accesses:\n");
		writeTableAccesses(bw, stmt);
		writeColumnAccesses(bw, stmt);
	}

	private void writeColumnAccesses(BufferedWriter bw, Statement stmt) throws IOException {
		Map<Column, Integer> cac = columnAcc.get(stmt);
		bw.write("  Columns:\n");
		if (cac != null) {
			for (Column col : cac.keySet()) {
				bw.write("    " + ASGUtils.getQualifiedName(col) + "\n");
			}
		}
	}

	private void writeTableAccesses(BufferedWriter bw, Statement stmt) throws IOException {
		bw.write("  Tables:\n");
		Map<Table, Integer> tac = tableAcc.get(stmt);
		if (tac != null) {
			for (Table tab : tac.keySet()) {
				bw.write("    " + ASGUtils.getQualifiedName(tab) + "\n");
			}
		}
	}
}
