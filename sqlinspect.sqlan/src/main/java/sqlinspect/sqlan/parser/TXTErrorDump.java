package sqlinspect.sqlan.parser;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Files;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.sqlan.asg.common.ASG;
import sqlinspect.sqlan.asg.schema.Database;
import sqlinspect.sqlan.asg.statm.Statement;
import sqlinspect.sqlan.asg.visitor.Visitor;

public class TXTErrorDump extends Visitor implements IErrorDump {
	private static final Logger logger = LoggerFactory.getLogger(TXTErrorDump.class);

	private final String path;
	private final ASG asg;
	private final boolean writeTrace;

	private static final String UTF8 = "UTF-8";

	public TXTErrorDump(ASG asg, String path, boolean writeTrace) {
		super();
		this.asg = asg;
		this.path = path;
		this.writeTrace = writeTrace;
	}

	@Override
	public void writeDump() {
		try (OutputStreamWriter ow = new OutputStreamWriter(Files.newOutputStream(new File(path).toPath()), UTF8);
				BufferedWriter bw = new BufferedWriter(ow);) {

			for (Database db : asg.getRoot().getDatabases()) {
				for (Statement stmt : db.getStatements()) {
					writeStatement(bw, stmt);
				}
			}
		} catch (IOException e) {
			logger.error("IO error while writing report!", e);
		}
	}

	private void writeStatement(BufferedWriter bw, Statement stmt) throws IOException {
		if (stmt.getError()) {
			writeError(bw, stmt);
		}
	}

	private void writeError(BufferedWriter bw, Statement stmt) throws IOException {
		bw.write("Statement (Id: " + stmt.getId() + ", internalId: " + stmt.getInternalId() + ") ");
		bw.write(" at " + stmt.getPath() + ": " + stmt.getErrorLine() + ", " + stmt.getErrorCol() + ": ");
		bw.write(stmt.getErrorMessage() + "\n");
		if (writeTrace) {
			bw.write("Trace: " + stmt.getErrorTrace());
		}
	}
}
