package sqlinspect.sqlan.parser;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.ConsoleErrorListener;
import org.antlr.v4.runtime.Lexer;

public abstract class AbstractSQLLexer extends Lexer {
	
	public AbstractSQLLexer(CharStream input) {
		super(input);
		removeErrorListener(ConsoleErrorListener.INSTANCE);
		addErrorListener(SQLErrorListener.getInstance());
	}
}
