package sqlinspect.sqlan.parser;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Deque;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.sqlan.asg.base.Base;
import sqlinspect.sqlan.asg.base.Named;
import sqlinspect.sqlan.asg.base.SQLRoot;
import sqlinspect.sqlan.asg.common.ASG;
import sqlinspect.sqlan.asg.common.NodeKind;
import sqlinspect.sqlan.asg.common.SymbolDesc;
import sqlinspect.sqlan.asg.common.SymbolTable;
import sqlinspect.sqlan.asg.expr.Alias;
import sqlinspect.sqlan.asg.expr.Asterisk;
import sqlinspect.sqlan.asg.expr.BinaryExpression;
import sqlinspect.sqlan.asg.expr.BinaryQuery;
import sqlinspect.sqlan.asg.expr.Expression;
import sqlinspect.sqlan.asg.expr.ExpressionList;
import sqlinspect.sqlan.asg.expr.Id;
import sqlinspect.sqlan.asg.expr.Join;
import sqlinspect.sqlan.asg.expr.SelectExpression;
import sqlinspect.sqlan.asg.expr.TableRef;
import sqlinspect.sqlan.asg.expr.UnaryExpression;
import sqlinspect.sqlan.asg.kinds.BinaryExpressionKind;
import sqlinspect.sqlan.asg.kinds.UnaryExpressionKind;
import sqlinspect.sqlan.asg.schema.Column;
import sqlinspect.sqlan.asg.schema.Database;
import sqlinspect.sqlan.asg.schema.Schema;
import sqlinspect.sqlan.asg.schema.Table;
import sqlinspect.sqlan.asg.statm.Delete;
import sqlinspect.sqlan.asg.statm.Insert;
import sqlinspect.sqlan.asg.statm.Update;
import sqlinspect.sqlan.asg.visitor.Visitor;

public final class VisitorExtraEdges extends Visitor {

	private static final Logger logger = LoggerFactory.getLogger(VisitorExtraEdges.class);

	private final ASG fact;
	private final SQLRoot root;

	private final SymbolTable symTab;

	private Optional<Database> actualDatabase = Optional.empty();
	private Optional<Schema> actualSchema = Optional.empty();
	private Optional<Table> actualTable = Optional.empty();

	private final Deque<Set<Base>> visibleScopes = new ArrayDeque<>();

	private int idTotal;
	private final Set<Id> emptyIds = new HashSet<>();
	private final Set<Id> resolvedIds = new HashSet<>();

	private static final NodeKind[] TABLE_KINDS = new NodeKind[] { NodeKind.TABLE, NodeKind.SELECTEXPRESSION,
			NodeKind.BINARYQUERY, NodeKind.ALIAS };

	private static final NodeKind[] COLUMN_KINDS = new NodeKind[] { NodeKind.COLUMN, NodeKind.ALIAS };

	private static final boolean DEBUG = false;

	public VisitorExtraEdges(ASG fact, SymbolTable symTab) {
		super();
		this.fact = fact;
		this.root = fact.getRoot();
		this.symTab = symTab;

		setScopesToDefault();
	}

	private void setScopesToDefault() {
		actualDatabase = Optional.of(root.getDefaultDatabase());
		actualSchema = Optional.of(actualDatabase.get().getDefaultSchema());
		actualTable = Optional.empty();
	}

	private void pushScope(Base n) {
		Set<Base> s = new HashSet<>();
		s.add(n);
		visibleScopes.push(s);
	}

	private void popScope() {
		visibleScopes.pop();
	}

	private void insertToTopScope(Base n) {
		visibleScopes.getFirst().add(n);

	}

	@Override
	public void visit(Database n) {
		super.visit(n);
		actualDatabase = Optional.of(n);
		actualSchema = Optional.of(n.getDefaultSchema());

		symTab.insert(n.getName().toLowerCase(Locale.ENGLISH), new SymbolDesc(n, n.getNodeKind(), fact.getRoot()));
		symTab.insertScope(n, fact.getRoot());

		pushScope(actualDatabase.get());
		pushScope(actualSchema.get());
	}

	@Override
	public void visitEnd(Database n) {
		super.visitEnd(n);
		popScope();
		popScope();
		setScopesToDefault();
	}

	@Override
	public void visit(Schema n) {
		super.visit(n);
		popScope(); // remove default schema
		actualSchema = Optional.of(n);

		if (actualDatabase.isPresent()) {
			symTab.insert(n.getName().toLowerCase(Locale.ENGLISH),
					new SymbolDesc(n, n.getNodeKind(), actualDatabase.get()));
			symTab.insertScope(n, actualDatabase.get());
			pushScope(n);
		} else {
			logger.error("There is on actual database");
		}
	}

	@Override
	public void visitEnd(Schema n) {
		super.visitEnd(n);
		popScope(); // remove default schema
		if (actualDatabase.isPresent()) {
			actualSchema = Optional.of(actualDatabase.get().getDefaultSchema());
			pushScope(actualSchema.get());
		} else {
			logger.error("There is no actual database.");
		}
	}

	@Override
	public void visit(Table n) {
		super.visit(n);
		if (!actualSchema.isPresent()) {
			logger.error("No actual schema);");
			return;
		}

		actualTable = Optional.of(n);
		symTab.insert(n.getName().toLowerCase(Locale.ENGLISH), new SymbolDesc(n, n.getNodeKind(), actualSchema.get()));
		symTab.insertScope(n, actualSchema.get());

	}

	@Override
	public void visitEnd(Table n) {
		super.visit(n);

		actualTable = Optional.empty();
	}

	@Override
	public void visit(Column n) {
		super.visit(n);

		if (actualTable.isPresent()) {
			symTab.insert(n.getName().toLowerCase(Locale.ENGLISH),
					new SymbolDesc(n, n.getNodeKind(), actualTable.get()));
			symTab.insertScope(n, actualTable.get());
		} else {
			logger.debug("Visited column without actual table set");
		}
	}

	@Override
	public void visit(Id n) {
		super.visit(n);

		idTotal++;

		// if the reference is already filled, we don't do anything
		if (n.getRefersTo() != null) {
			return;
		}

		resolveId(n);

		if (n.getRefersTo() == null) {
			emptyIds.add(n);
		}
	}

	@Override
	public void visit(SelectExpression n) {
		super.visit(n);

		resolveSelectExpression(n, false);
	}

	@Override
	public void visitEnd(SelectExpression n) {
		super.visitEnd(n);
		popScope();
	}

	@Override
	public void visit(Insert n) {
		super.visit(n);

		resolveInsert(n, false);
	}

	@Override
	public void visitEnd(Insert n) {
		super.visitEnd(n);
		popScope();
	}

	@Override
	public void visit(Update n) {
		super.visit(n);

		resolveUpdate(n, false);
	}

	@Override
	public void visitEnd(Update n) {
		super.visitEnd(n);
		popScope();
	}

	@Override
	public void visit(Delete n) {
		super.visit(n);

		resolveDelete(n, false);
	}

	@Override
	public void visitEnd(Delete n) {
		super.visitEnd(n);
		popScope();
	}

	// HELPER METHODS

	/**
	 * This method resolves the identifier and sets its refersTo edge. If the edge
	 * is already set, we don't do anything. If the id is on the right hand side of
	 * a field selector, we search the symbol only in the scope specified by the
	 * left hand side. Otherwise, we search for the symbol in the visible scopes
	 * starting from the innermost scope.
	 * 
	 * @param n     The id which should be resolved
	 * @param kinds If the expected NodeKind is known (or must be know), we can
	 *              limit the resolver to search for only the given set of
	 *              NodeKinds.
	 */
	private Base resolveId(Id n, NodeKind... kinds) {
		if (n.getRefersTo() != null) {
			return n.getRefersTo();
		}

		Base parent = n.getParent();
		if (parent.getNodeKind() == NodeKind.BINARYEXPRESSION
				&& ((BinaryExpression) parent).getKind() == BinaryExpressionKind.FIELDSELECTOR) {
			// find the top node of the field selector and resolve it
			Base myParent = parent;
			Base top = myParent;
			while (myParent != null && myParent.getNodeKind() == NodeKind.BINARYEXPRESSION
					&& ((BinaryExpression) myParent).getKind() == BinaryExpressionKind.FIELDSELECTOR) {
				top = myParent;
				myParent = myParent.getParent();
			}

			return resolveFieldSelector((BinaryExpression) top);
		} else {
			return resolveSQLObject(n, kinds);
		}
	}

	/**
	 * Resolve an identifier in the visible scope stack. We start from the innermost
	 * scope and collect SQL objects with the same name. If there is only one object
	 * matching the Id, we return it. Otherwise, we report an error.
	 * 
	 * @param n     The identifier.
	 * @param kinds If the expected NodeKind is known (or must be know), we can
	 *              limit the resolver to search for only the given set of
	 *              NodeKinds.
	 * @return
	 */
	private Base resolveSQLObject(Id n, NodeKind... kinds) {
		boolean found = false;
		Base ret = null;
		String key = n.getName().toLowerCase(Locale.ENGLISH);

		if (DEBUG) {
			logger.trace("Resolve SQL Object: {} ", n);
			if (kinds != null) {
				StringBuilder sb = new StringBuilder();
				for (NodeKind k : kinds) {
					sb.append(k.toString());
					sb.append(' ');
				}
				logger.trace("Kinds: {}", sb);
			} else {
				logger.trace("Kinds: empty");
			}

			printScopeStack();
		}

		for (Iterator<Set<Base>> it = visibleScopes.iterator(); it.hasNext() && !found;) {
			Set<Base> candidates = new HashSet<>();
			Set<Base> scopes = it.next();

			for (Base scope : scopes) {
				if (key != null) {
					Collection<Base> res = symTab.findInInnerMostScope(key, scope, kinds);
					candidates.addAll(res);
				}
			}

			if (!candidates.isEmpty()) {
				found = true;
				if (candidates.size() == 1) {
					ret = (Base) candidates.toArray()[0];
				} else {
					logger.trace("resolveSQLObject could not resolve id because of too many candidates! {} ({})", n,
							key);
					StringBuilder sb = new StringBuilder();
					for (Base c : candidates) {
						if (c instanceof Named) {
							sb.append("key: ").append(((Named) c).getName().toLowerCase(Locale.ENGLISH)).append(' ');
						} else if (c instanceof Alias) {
							sb.append("key: ").append(((Alias) c).getName().toLowerCase(Locale.ENGLISH)).append(' ');
						}
						sb.append(c.toString());
						sb.append(", ");
					}
					logger.trace("Candidates are: {}", sb);
				}
			}

		}

		if (!found) {
			logger.trace("resolveSQLObject could not resolve id because there were no candidates! {} ({})", n, key);
		}

		setIdRefersTo(n, ret);

		return ret;
	}

	private Base resolveSQLObjectInScope(Id id, Base scope, NodeKind... kinds) {
		if (id.getRefersTo() != null) {
			return id.getRefersTo();
		}

		Base ret = null;
		String key = id.getName().toLowerCase(Locale.ENGLISH);

		Collection<Base> candidates = symTab.findInScopeOnly(key, scope, kinds);

		if (candidates.isEmpty()) {
			logger.trace("resolveSQLObjectInScope could not resolve id because there were no candidates! {} ({})", id,
					key);
		} else if (candidates.size() == 1) {
			ret = (Base) candidates.toArray()[0];
		} else {
			logger.trace("resolveSQLObject could not resolve id because of too many candidates! {} ({})", id, key);
			StringBuilder sb = new StringBuilder();
			for (Base c : candidates) {
				if (c instanceof Named) {
					sb.append("key: ").append(((Named) c).getName().toLowerCase(Locale.ENGLISH)).append(' ');
				} else if (c instanceof Alias) {
					sb.append("key: ").append(((Alias) c).getName().toLowerCase(Locale.ENGLISH)).append(' ');
				}
				sb.append(c.toString());
				sb.append(", ");
			}
			logger.trace("Candidates are: {}", sb);
		}

		if (ret != null) {
			setIdRefersTo(id, ret);
		}

		return ret;
	}

	// Possibilities are:
	// - db.schema.table.field
	// - db..table.field
	// - table.field
	// - schema.table.field

	private Base resolveFieldSelector(BinaryExpression n) {
		final Expression left = n.getLeft();
		if (left == null) {
			logger.trace("Could not determine left hand side of field selector! {}", n);
			return null;
		}

		final Expression right = n.getRight();
		if (right == null) {
			logger.trace("Could not determine right hand side of field selector! {}", n);
			return null;
		}

		Base parentScope = null;
		Base child = null;
		if (left.getNodeKind() == NodeKind.ID) {
			NodeKind[] kinds = { NodeKind.TABLE, NodeKind.SCHEMA, NodeKind.DATABASE, NodeKind.ALIAS,
					NodeKind.SELECTEXPRESSION, NodeKind.BINARYQUERY };
			parentScope = resolveSQLObject((Id) left, kinds);
		} else if (left.getNodeKind() == NodeKind.BINARYEXPRESSION) {
			parentScope = resolveFieldSelector((BinaryExpression) left);
		} else if (left.getNodeKind() == NodeKind.JOKER) {
			// we cannot do anything with a joker on the left side :(
		} else {
			logger.trace("Unhandled node kind ({}) at left hand side of field selector! {}", left, n);
		}

		if (right.getNodeKind() == NodeKind.ID) {
			final Id rightId = (Id) right;
			if (parentScope != null) {
				child = resolveSQLObjectInScope(rightId, parentScope);
			} else {
				logger.trace("Left hand side of field selector could not be resolved! {}", n);
			}
		} else if (right.getNodeKind() == NodeKind.ASTERISK) {
			// TODO: put all the columns of the from clause to the scope of the
			// parent select expression. I.e., handle select x,y from (select
			// t.* from t)
		} else {
			logger.trace("Unhandled right hand side of field selector! {}", n);
		}

		return child;
	}

	// An element of a FROM clause can be:
	// - a list of from elements
	// - a from element

	private void resolveFrom(Expression n) {
		if (n.getNodeKind() == NodeKind.EXPRESSIONLIST) {
			final ExpressionList exprList = (ExpressionList) n;
			for (Expression expr : exprList.getExpressions()) {
				resolveFromElement(expr);
			}
		} else {
			resolveFromElement(n);
		}
	}

	private Base resolveFromElement(Expression n) {
		Base ret = null;

		if (n.getNodeKind() == NodeKind.ID) {
			final Id id = (Id) n;
			ret = resolveId(id, TABLE_KINDS);
			if (id.getRefersTo() != null) {
				insertToTopScope(id.getRefersTo());
			}
		} else if (n.getNodeKind() == NodeKind.BINARYEXPRESSION) {
			final BinaryExpression binary = (BinaryExpression) n;
			ret = resolveFieldSelector(binary);
			if (ret != null) {
				insertToTopScope(ret);
			}
		} else if (n.getNodeKind() == NodeKind.UNARYEXPRESSION) {
			final UnaryExpression unary = (UnaryExpression) n;
			if (unary.getKind() == UnaryExpressionKind.BRACKETS) {
				ret = resolveFromElement(unary.getExpression());
			} else {
				logger.error("Unhandled unary kind, or missing subexpression! {}", unary);
			}
		} else if (n.getNodeKind() == NodeKind.SELECTEXPRESSION) {
			final SelectExpression subQuery = (SelectExpression) n;
			ret = resolveSelectExpression(subQuery, true);
			if (ret != null) {
				insertToTopScope(ret);
			}
		} else if (n.getNodeKind() == NodeKind.ALIAS) {
			final Alias alias = (Alias) n;
			final Expression aliasExpr = alias.getExpression();
			final Base parentSelect = getParentSelectExpression(alias);
			if (parentSelect != null) {
				if (aliasExpr != null) {
					ret = resolveFromElement(aliasExpr);
					if (ret != null) {
						String aliasName = alias.getName();
						if (aliasName != null) {
							symTab.insert(alias.getName().toLowerCase(Locale.ENGLISH),
									new SymbolDesc(ret, ret.getNodeKind(), parentSelect));
							insertToTopScope(ret);
						} else {
							logger.trace("Missing name of Alias! {}", alias);
						}
					} else {
						logger.trace("Could not resolve expression of alias: {}", alias);
					}
				} else {
					logger.trace("Missing subexpression of Alias! {}", alias);
				}
			} else {
				logger.trace("Could not determine parent select of alias! {}", alias);
			}
		} else if (n.getNodeKind() == NodeKind.JOIN) {
			final Join join = (Join) n;
			if (join.getLeft() != null) {
				ret = resolveFromElement(join.getLeft());
			} else {
				logger.trace("Missing left side of Join! {}", join);
			}
			if (join.getRight() != null) {
				ret = resolveFromElement(join.getRight());
			} else {
				logger.trace("Missing right side of Join! {}", join);
			}
		} else if (n.getNodeKind() == NodeKind.TABLEREF) {
			final TableRef tableRef = (TableRef) n;
			if (tableRef.getTable() != null) {
				ret = resolveFromElement(tableRef.getTable());
			} else {
				logger.trace("Missing table expression for TableRef! {}", tableRef);
			}
		} else {
			logger.trace("Unhandled From element! {}", n);
		}

		return ret;
	}

	private Base resolveSelectExpression(SelectExpression n, boolean clearscope) {
		if (!actualSchema.isPresent()) {
			logger.error("No actual schema");
			return n;
		}
		Base parentSelect = getParentSelectExpression(n);
		Base parentScope = parentSelect != null ? parentSelect : actualSchema.get();

		// the actual select expression sets up a new scope
		pushScope(n);
		symTab.insertScope(n, parentScope);

		// resolve the from clause before the column list
		Expression from = n.getFrom();
		if (from != null) {
			resolveFrom(from);
		} else {
			logger.trace("Missing From clause of SelectExpression! {}", n);
		}

		final Expression columnList = n.getColumnList();
		if (columnList != null) {
			if (columnList instanceof ExpressionList) {
				final ExpressionList list = (ExpressionList) columnList;
				for (Expression expr : list.getExpressions()) {
					resolveColumn(expr);
				}
			} else {
				resolveColumn(columnList);
			}
		} else {
			logger.trace("Missing ColumnList of SelectExpression! {}", n);
		}

		if (clearscope) {
			popScope();
		}

		return n;
	}

	// Here, we are only interested in named columns, so we can put them into
	// the symboltable.
	// Possibilities are
	// - column name: e.g. select x from ...
	// - aliased column: e.g. select a as x, from ...
	// - aliased expression: e.g. select <expr> as y from ....
	// - expression <-- we simply don't care

	private Base resolveColumn(Expression n) {
		Base ret = null;
		Base parentSelect = getParentSelectExpression(n);
		if (parentSelect == null) {
			logger.error("Cannot determine parent select for Column! {}", n);
			return ret;
		}

		if (n.getNodeKind() == NodeKind.ID) {
			// case 1: column name: e.g. select x from ...
			final Id id = (Id) n;
			ret = resolveId(id, COLUMN_KINDS);
			if (ret != null) {
				symTab.insert(id.getName().toLowerCase(Locale.ENGLISH),
						new SymbolDesc(ret, ret.getNodeKind(), parentSelect));
			}
		} else if (n.getNodeKind() == NodeKind.BINARYEXPRESSION) {
			final BinaryExpression binary = (BinaryExpression) n;
			ret = resolveFieldSelector(binary);
			if (ret instanceof Named) {
				final Named retNamed = (Named) ret;
				symTab.insert(retNamed.getName().toLowerCase(Locale.ENGLISH),
						new SymbolDesc(ret, ret.getNodeKind(), parentSelect));
			}

			if (binary.getRight() != null && binary.getRight().getNodeKind() == NodeKind.ASTERISK) {
				// put all the columns of the left side to the scope of the
				// parent select expression
				// E.g. handle: select x, y from (select t.* from t);

				Set<Base> columns = getAllColumnsOfFrom(binary.getLeft());
				addSymbolsToScope(columns, parentSelect);
			}
		} else if (n.getNodeKind() == NodeKind.ALIAS) {
			final Alias alias = (Alias) n;
			final Expression expr = alias.getExpression();
			if (expr != null) {
				if (alias.getName() != null) {
					if (expr.getNodeKind() == NodeKind.ID) {
						Id id = (Id) expr;
						ret = resolveId(id);
						if (ret != null) {
							symTab.insert(alias.getName(), new SymbolDesc(ret, ret.getNodeKind(), parentSelect));
						}
					} else {
						symTab.insert(alias.getName().toLowerCase(Locale.ENGLISH),
								new SymbolDesc(alias, alias.getNodeKind(), parentSelect));
					}
				} else {
					logger.trace(" Missing name of Alias! {}", alias);
				}
			} else {
				logger.trace(" Missing expression of Alias! {}", alias);
			}
		} else if (n.getNodeKind() == NodeKind.ASTERISK) {
			// put all the columns of the from clause to the scope of the parent
			// select expression
			// E.g. handle: select x, y from (select * from t);
			Set<Base> columns = getColumnsReturnedBySelectExpression((SelectExpression) parentSelect);
			addSymbolsToScope(columns, parentSelect);
		}

		return ret;
	}

	private Base resolveInsert(Insert n, boolean clearscope) {
		if (!actualSchema.isPresent()) {
			logger.error("No actual schema);");
			return n;
		}

		pushScope(n);
		Base parentScope = actualSchema.get();
		symTab.insertScope(n, parentScope);
		Expression into = n.getTable();
		if (into != null) {
			resolveIntoClause(into);
		} else {
			logger.trace("Missing Into clause of Insert! {}", n);
		}

		if (clearscope) {
			popScope();
		}

		return n;
	}

	private Base resolveIntoClause(Expression n) {
		Base ret = null;
		if (n.getNodeKind() == NodeKind.ID) {
			Id id = (Id) n;
			ret = resolveId(id);
			if (id.getRefersTo() != null) {
				insertToTopScope(id.getRefersTo());
			}
		} else if (n.getNodeKind() == NodeKind.BINARYEXPRESSION) {
			BinaryExpression binary = (BinaryExpression) n;
			ret = resolveFieldSelector(binary);
			if (ret != null) {
				insertToTopScope(ret);
			}
		} else {
			logger.trace("Unhandled Into clause element! {}", n);
		}

		return ret;
	}

	private Base resolveUpdate(Update n, boolean clearscope) {
		if (!actualSchema.isPresent()) {
			logger.error("No actual schema");
			return n;
		}

		Base parentScope = actualSchema.get();
		pushScope(n);
		symTab.insertScope(n, parentScope);

		Expression tableList = n.getTableReferenceList();
		if (tableList != null) {
			resolveFrom(tableList);
		} else {
			logger.trace("Missing table references of Update {}", n);
		}

		if (clearscope) {
			popScope();
		}

		return n;
	}

	private Base resolveDelete(Delete n, boolean clearscope) {
		if (!actualSchema.isPresent()) {
			logger.error("No actual schema");
			return n;
		}

		Base parentScope = actualSchema.get();
		pushScope(n);
		symTab.insertScope(n, parentScope);

		if (n.getFrom() != null) {
			// DELETE FROM table_name
			// DELETE FROM tbl_name[.*] [, tbl_name[.*]] ... FROM
			// table_references

			Expression tableList = n.getFrom();
			resolveFrom(tableList);
		} else if (n.getUsing() != null) {
			// DELETE FROM tbl_name[.*] [, tbl_name[.*]] ... USING
			// table_references
			Expression tableList = n.getUsing();
			resolveFrom(tableList);
		} else {
			logger.trace("Missing table references of Delete {}", n);
		}

		if (clearscope) {
			popScope();
		}

		return n;
	}

	private Set<Base> getColumnsReturnedBySelectExpression(SelectExpression n) {
		Set<Base> ret = new HashSet<>();

		Expression colList = n.getColumnList();

		if (colList instanceof ExpressionList) {
			final ExpressionList exprList = (ExpressionList) colList;
			for (Expression expr : exprList.getExpressions()) {
				if (expr instanceof Id) {
					final Id id = (Id) expr;
					if (id.getRefersTo() != null) {
						ret.add(id.getRefersTo());
					}
				} else if (expr instanceof BinaryExpression) {
					final BinaryExpression bin = (BinaryExpression) expr;
					final Expression left = bin.getLeft();
					final Expression right = bin.getRight();
					if (right instanceof Id) {
						final Id id = (Id) right;
						if (id.getRefersTo() != null) {
							ret.add(id.getRefersTo());
						}
					} else if (right instanceof Asterisk && left != null) {
						ret.addAll(getAllColumnsOfFrom(left));
					}
				} else if (expr instanceof Alias) {
					ret.add(expr);
				} else if (expr instanceof Asterisk) {
					ret.addAll(getAllColumnsOfFrom(n.getFrom()));
				} else {
					logger.trace(
							"getColumnsReturnedBySelectExpression(): Unhandled nodekind {} in the column list of select {}",
							n, expr);
				}
			}
		} else if (colList instanceof Asterisk) {
			ret.addAll(getAllColumnsOfFrom(n.getFrom()));
		} else if (colList != null) {
			logger.trace("getColumnsReturnedBySelectExpression(): Unhandled column list expression {} of select {}",
					n.getColumnList(), n);
		} else {
			logger.trace("Select expression without column list! {}", n);
		}

		return ret;
	}

	private Set<Base> getAllColumnsOfFrom(Expression n) {
		Set<Base> columns = new HashSet<>();

		if (n != null) {
			NodeKind kind = n.getNodeKind();
			switch (kind) {
			case ID:
				final Id id = (Id) n;
				final Base ref = id.getRefersTo();
				if (ref != null) {
					if (ref.getNodeKind() == NodeKind.TABLE) {
						columns.addAll(getAllColumnsOfTable(n));
					} else if (ref instanceof Expression) {
						final Expression expr = (Expression) ref;
						if (!expr.equals(n)) {
							columns.addAll(getAllColumnsOfFrom(expr));
						} else {
							logger.warn("Self loop detected for FROM reference! {}", expr);
						}
					} else {
						logger.trace(" getAllColumnsOfFrom unhandled Id reference for id {}", n);
					}
				}
				break;
			case BINARYQUERY:
				final BinaryQuery bin = (BinaryQuery) n;
				if (bin.getLeft() != null) {
					columns.addAll(getAllColumnsOfFrom(bin.getLeft()));
				}
				if (bin.getRight() != null) {
					columns.addAll(getAllColumnsOfFrom(bin.getRight()));
				}
				break;
			case JOIN:
				final Join join = (Join) n;
				if (join.getLeft() != null) {
					columns.addAll(getAllColumnsOfFrom(join.getLeft()));
				}
				if (join.getRight() != null) {
					columns.addAll(getAllColumnsOfFrom(join.getRight()));
				}
				break;
			case ALIAS:
				final Alias alias = (Alias) n;
				final Expression expr = alias.getExpression();
				if (expr != null) {
					columns.addAll(getAllColumnsOfFrom(expr));
				}
				break;
			case SELECTEXPRESSION:
				final SelectExpression selectExpression = (SelectExpression) n;
				columns.addAll(getColumnsReturnedBySelectExpression(selectExpression));
				break;
			default:
				// we do nothing here now
				break;
			}
		}
		return columns;
	}

	private Set<Base> getAllColumnsOfTable(Expression n) {
		Table tab = null;
		Set<Base> ret = new HashSet<>();

		if (n.getNodeKind() == NodeKind.ID) {
			final Id id = (Id) n;
			final Base ref = id.getRefersTo();
			if (ref != null) {
				if (ref.getNodeKind() == NodeKind.TABLE) {
					tab = (Table) ref;
				}
			} else {
				logger.trace("getAllColumnsOfTable(): Expression supposed to point to a table, but it does not {} ", n);
			}
		} else if (n.getNodeKind() == NodeKind.BINARYEXPRESSION) {
			BinaryExpression bin = (BinaryExpression) n;
			if (bin.getRight() != null && bin.getRight().getNodeKind() == NodeKind.ID) {
				Id id = (Id) bin.getRight();
				Base ref = id.getRefersTo();
				if (ref != null && ref.getNodeKind() == NodeKind.TABLE) {
					tab = (Table) ref;
				} else {
					logger.trace(
							"getAllColumnsOfTable(): Right hand side of binary expression supposed to point to a table, but it does not {}",
							n);
				}
			} else {
				logger.trace("getAllColumnsOfTable(): Binary expression without right hand side {}", n);
			}
		}

		if (tab != null) {
			for (Column col : tab.getColumns()) {
				ret.add(col);
			}
		}

		return ret;
	}

	private Base getParentSelectExpression(Base n) {
		Base parent = n.getParent();
		while (parent != null && parent.getNodeKind() != NodeKind.SELECTEXPRESSION) {
			parent = parent.getParent();
		}
		return parent;
	}

	private void setIdRefersTo(Id n, Base ref) {
		if (n.getRefersTo() != null && ref != null && n.getRefersTo().getId() != ref.getId()) {
			logger.trace("Wanted to reset refersTo edge of Id {} (from: {} to {})! Forcing previous value!", n,
					n.getRefersTo(), ref);
			return;
		}

		if (ref != null) {
			n.setRefersTo(ref);
		}

		if (n.getRefersTo() != null) {
			resolvedIds.add(n);
		}
	}

	private void addSymbolsToScope(Set<Base> columns, Base scope) {
		for (Base col : columns) {
			if (col.getNodeKind() == NodeKind.ID) {
				// case 1: column name: e.g. select x from ..
				final Id id = (Id) col;
				Base ret = resolveId(id);
				if (ret != null) {
					symTab.insert(id.getName().toLowerCase(Locale.ENGLISH),
							new SymbolDesc(ret, ret.getNodeKind(), scope));
				}
			} else if (col.getNodeKind() == NodeKind.ALIAS) {
				final Alias alias = (Alias) col;
				final Expression expr = alias.getExpression();
				if (expr != null) {
					if (expr.getNodeKind() == NodeKind.ID) {
						final Id id = (Id) expr;
						Base ret = resolveId(id);
						if (ret != null) {
							symTab.insert(alias.getName().toLowerCase(Locale.ENGLISH),
									new SymbolDesc(ret, ret.getNodeKind(), scope));
						}
					} else {
						symTab.insert(alias.getName().toLowerCase(Locale.ENGLISH),
								new SymbolDesc(alias, alias.getNodeKind(), scope));
					}
				} else {
					logger.trace("Missing expression of Alias! {}", alias);
				}
			} else if (col instanceof Named) {
				final Named named = (Named) col;
				symTab.insert(named.getName().toLowerCase(Locale.ENGLISH),
						new SymbolDesc(named, named.getNodeKind(), scope));
			} else {
				// do nothing
			}
		}
	}

	private void printScopeStack() {
		if (logger.isDebugEnabled()) {
			logger.debug("Visible scope stack: ");
			for (Set<Base> scopes : visibleScopes) {
				StringBuilder sb = new StringBuilder();
				sb.append("( ");
				for (Base n : scopes) {
					sb.append(n.toString()).append(' ');
				}
				sb.append(" )");
				logger.debug(sb.toString());
			}
		}
	}

	public void logStats() {
		if (logger.isDebugEnabled()) {
			logger.debug("Total number of ids: {}", idTotal);
			logger.debug("Resolved ids: {}", resolvedIds.size());
			logger.debug("Empty ids: {}", emptyIds.size());
		}
	}
}
