package sqlinspect.sqlan.parser;

public class ParserConfig {
	
	private static final String DEFAULT_SCHEMA = "SCH";
	private static final String DEFAULT_DB = "DB";
	
	private String defaultSchema = DEFAULT_SCHEMA;
	private String defaultDatabase = DEFAULT_DB;
	
	private boolean interpretSchema = true;
	
	public String getDefaultSchema() {
		return defaultSchema;
	}
	
	public void setDefaultSchema(String defaultSchema) {
		this.defaultSchema = defaultSchema;
	}
	
	public String getDefaultDatabase() {
		return defaultDatabase;
	}
	
	public void setDefaultDatabase(String defaultDatabase) {
		this.defaultDatabase = defaultDatabase;
	}

	public boolean isInterpretSchema() {
		return interpretSchema;
	}

	public void setInterpretSchema(boolean interpretSchema) {
		this.interpretSchema = interpretSchema;
	}
	
}
