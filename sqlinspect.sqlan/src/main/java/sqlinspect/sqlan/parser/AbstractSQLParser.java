package sqlinspect.sqlan.parser;

import java.util.List;

import org.antlr.v4.runtime.ConsoleErrorListener;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.TokenStream;

import sqlinspect.sqlan.asg.common.ASG;
import sqlinspect.sqlan.asg.statm.Statement;

public abstract class AbstractSQLParser extends Parser {
	
	protected ASG fact;
    protected boolean stmtError; // false default
    protected SQLError stmtErrorDetails;
    
	protected static final SQLErrorListener sqlErrorListener = SQLErrorListener.getInstance();
    
    
	public AbstractSQLParser(TokenStream input) {
		super(input);
		removeErrorListener(ConsoleErrorListener.INSTANCE);
		addErrorListener(sqlErrorListener);
	}
    
    public void setFactory(ASG fact) {
        this.fact = fact;
    }

	/**
	 * @param error the error to set
	 */
	public void setStmtError(boolean error) {
		this.stmtError = error;
	}
	
	/**
	 * @param hasError the error to set
	 */
	public boolean isStmtError() {
		return stmtError;
	}
	
	public void setStmtErrorDetails(SQLError error) {
		this.stmtErrorDetails = error;
	}	
	
	public SQLError getStmtErrorDetails() {
		return stmtErrorDetails;
	}

	public void clearDFA() {
		if (_interp != null) {
			_interp.clearDFA();
		}
	}
    
	public abstract void setActions(AbstractParserActions act);

	public abstract List<Statement> parse();
}
