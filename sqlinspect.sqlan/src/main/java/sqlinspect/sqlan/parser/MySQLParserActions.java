package sqlinspect.sqlan.parser;

import java.util.Locale;

import org.antlr.v4.runtime.Token;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.sqlan.SQLDialect;
import sqlinspect.sqlan.asg.builder.AliasBuilder;
import sqlinspect.sqlan.asg.common.ASG;
import sqlinspect.sqlan.asg.common.ASGException;
import sqlinspect.sqlan.asg.expr.Alias;
import sqlinspect.sqlan.asg.expr.Expression;
import sqlinspect.sqlan.asg.expr.Id;
import sqlinspect.sqlan.asg.expr.Joker;
import sqlinspect.sqlan.asg.expr.Literal;
import sqlinspect.sqlan.asg.kinds.AliasKind;
import sqlinspect.sqlan.asg.kinds.BinaryExpressionKind;
import sqlinspect.sqlan.asg.kinds.BinaryQueryKind;
import sqlinspect.sqlan.asg.kinds.ColumnAttributeKind;
import sqlinspect.sqlan.asg.kinds.DateTypeKind;
import sqlinspect.sqlan.asg.kinds.FunctionParamsKind;
import sqlinspect.sqlan.asg.kinds.JoinKind;
import sqlinspect.sqlan.asg.kinds.LiteralKind;
import sqlinspect.sqlan.asg.kinds.NullOrderKind;
import sqlinspect.sqlan.asg.kinds.NumericTypeKind;
import sqlinspect.sqlan.asg.kinds.OrderByElementKind;
import sqlinspect.sqlan.asg.kinds.SetQuantifierKind;
import sqlinspect.sqlan.asg.kinds.StringTypeKind;
import sqlinspect.sqlan.asg.kinds.TimeUnitKind;
import sqlinspect.sqlan.asg.kinds.UnaryExpressionKind;

public final class MySQLParserActions extends AbstractParserActions {
	private static final Logger logger = LoggerFactory.getLogger(MySQLParserActions.class);

	public MySQLParserActions(ASG fact, ParserConfig config) {
		super(fact, config);
		init();
	}

	@Override
	public SQLDialect getDialect() {
		return SQLDialect.MYSQL;
	}

	public Alias buildAlias(Expression left, Expression right, Token t) {
		AliasBuilder b = new AliasBuilder(fact);
		b.setExpression(left);
		b.setKind(toAliasKind(t));

		if (right instanceof Id) {
			Id rId = (Id) right;
			b.setName(rId.getName());
		} else if (right instanceof Literal) {
			Literal rLiteral = (Literal) right;
			b.setName(rLiteral.getValue());
		} else if (right instanceof Joker) {
			Joker jok = (Joker) right;
			b.setName(jok.getName());
		} else {
			logger.debug("Alias without unsupported right side: {} (left: {})", right, left);
		}

		copyAllPosition(b.result(), left);
		copyEndPosition(b.result(), right);

		return b.result();
	}

	@Override
	protected AliasKind toAliasKind(Token tok) {
		if (tok == null) {
			return AliasKind.NONE;
		} else if (tok.getType() == MySqlLexer.AS) {
			return AliasKind.AS;
		} else {
			throw new ASGException("Invalid token in toAliasKind: " + tokenToString(tok));
		}
	}

	@Override
	protected JoinKind toJoinKind(Token tok) {
		if (tok == null) {
			throw new ASGException("Null token in toJoinKind!");
		}

		switch (tok.getType()) {
		case MySqlLexer.COMMA:
			return JoinKind.COMMA;
		case MySqlLexer.INNER_SYM:
		case MySqlLexer.JOIN_SYM:
			return JoinKind.INNER;
		case MySqlLexer.CROSS:
			return JoinKind.CROSS;
		case MySqlLexer.STRAIGHT_JOIN:
			return JoinKind.STRAIGHTJOIN;
		case MySqlLexer.NATURAL:
			return JoinKind.NATURAL;
		case MySqlLexer.LEFT:
			return JoinKind.LEFTOUTER;
		case MySqlLexer.RIGHT:
			return JoinKind.RIGHTOUTER;
		default:
			throw new ASGException("Invalid token in toJoinKind: " + tokenToString(tok));
		}
	}

	@Override
	protected JoinKind toJoinKind(Token tok1, Token tok2) {
		if (tok1 != null && tok1.getType() == MySqlLexer.NATURAL) {
			if (tok2 != null && tok2.getType() == MySqlLexer.LEFT) {
				return JoinKind.NATURALLEFTOUTER;
			} else if (tok2 != null && tok2.getType() == MySqlLexer.RIGHT) {
				return JoinKind.NATURALRIGHTOUTER;
			} else if (tok2 == null) {
				return JoinKind.NATURAL;
			}
		}

		throw new ASGException("Invalid tokens in toJoinKind: " + tokenToString(tok1) + ", " + tokenToString(tok2));
	}

	@Override
	protected UnaryExpressionKind toUnaryExpressionKind(Token tok) {
		if (tok == null) {
			throw new ASGException("Null token in toUnaryExpressionKind!");
		}

		switch (tok.getType()) {
		case MySqlLexer.PLUS:
			return UnaryExpressionKind.PLUS;
		case MySqlLexer.MINUS:
			return UnaryExpressionKind.MINUS;
		case MySqlLexer.NOT2_SYM:
		case MySqlLexer.NOT_SYM:
			return UnaryExpressionKind.NOT;
		case MySqlLexer.ASC:
			return UnaryExpressionKind.ASCENDING;
		case MySqlLexer.DESC:
			return UnaryExpressionKind.DESCENDING;
		case MySqlLexer.LPAREN:
		case MySqlLexer.RPAREN:
			return UnaryExpressionKind.PARENS;
		case MySqlLexer.TILDE:
			return UnaryExpressionKind.BITNOT;
		case MySqlLexer.EXISTS:
			return UnaryExpressionKind.EXISTS;
		case MySqlLexer.BINARY:
			return UnaryExpressionKind.BINARY;
		default:
			throw new ASGException("Invalid token in toUnaryExpressionKind: " + tokenToString(tok));
		}
	}

	@Override
	protected BinaryExpressionKind toBinaryExpressionKind(Token tok) {
		if (tok == null) {
			throw new ASGException("Null token in toBinaryExpressionKind!");
		}

		switch (tok.getType()) {
		case MySqlLexer.PLUS:
			return BinaryExpressionKind.PLUS;
		case MySqlLexer.MINUS:
			return BinaryExpressionKind.MINUS;
		case MySqlLexer.ASTERISK:
			return BinaryExpressionKind.MULTIPLY;
		case MySqlLexer.DIV:
		case MySqlLexer.DIV_SYM:
			return BinaryExpressionKind.DIVIDE;
		case MySqlLexer.MOD:
		case MySqlLexer.MOD_SYM:
			return BinaryExpressionKind.MODULO;
		case MySqlLexer.AND_SYM:
		case MySqlLexer.AND_AND_SYM:
			return BinaryExpressionKind.AND;
		case MySqlLexer.OR_SYM:
		case MySqlLexer.OR_OR_SYM:
			return BinaryExpressionKind.OR;
		case MySqlLexer.BITAND:
			return BinaryExpressionKind.AND;
		case MySqlLexer.BITOR:
			return BinaryExpressionKind.OR;
		case MySqlLexer.XOR:
			return BinaryExpressionKind.XOR;
		case MySqlLexer.BIT_XOR:
			return BinaryExpressionKind.BITXOR;
		case MySqlLexer.EQUAL_SYM:
		case MySqlLexer.EQ:
			return BinaryExpressionKind.EQUALS;
		case MySqlLexer.NE:
			return BinaryExpressionKind.NOTEQUALS;
		case MySqlLexer.GT_SYM:
			return BinaryExpressionKind.GREATERTHAN;
		case MySqlLexer.LT:
			return BinaryExpressionKind.LESSTHAN;
		case MySqlLexer.GE:
			return BinaryExpressionKind.GREATEROREQUALS;
		case MySqlLexer.LE:
			return BinaryExpressionKind.LESSOREQUALS;
		case MySqlLexer.LIKE:
			return BinaryExpressionKind.LIKE;
		case MySqlLexer.IS:
			return BinaryExpressionKind.IS;
		case MySqlLexer.DOT:
			return BinaryExpressionKind.FIELDSELECTOR;
		case MySqlLexer.IN_SYM:
			return BinaryExpressionKind.IN;
		case MySqlLexer.REGEXP:
			return BinaryExpressionKind.REGEXP;
		case MySqlLexer.SOUNDS_SYM:
			return BinaryExpressionKind.SOUNDSLIKE;
		default:
			throw new ASGException("Invalid token in toBinaryExpressionKind: " + tokenToString(tok));
		}
	}

	@Override
	protected BinaryExpressionKind toBinaryExpressionKind(Token t1, Token t2) {
		throw new ASGException(
				"Invalid token in toBinaryExpressionKind: " + tokenToString(t1) + ", " + tokenToString(t2));
	}

	@Override
	protected BinaryExpressionKind toBinaryExpressionKind(Token t1, Token t2, Token t3) {
		throw new ASGException("Invalid token in toBinaryExpressionKind: " + tokenToString(t1) + ", "
				+ tokenToString(t2) + ", " + tokenToString(t3));
	}

	@Override
	protected LiteralKind toLiteralKind(Token tok) {
		if (tok == null) {
			throw new ASGException("Null token in toLiteralKind!");
		}

		switch (tok.getType()) {

		case MySqlLexer.HEX_NUM:
			return LiteralKind.HEXADECIMAL;
		case MySqlLexer.LIT_BINARY:
			return LiteralKind.BINARY;
		case MySqlLexer.LIT_INTEGER:
			return LiteralKind.INTEGER;
		case MySqlLexer.LIT_DECIMAL:
			return LiteralKind.DECIMAL;
		case MySqlLexer.LIT_REAL:
			return LiteralKind.DOUBLE;
		case MySqlLexer.LIT_STRING:
			return LiteralKind.STRING;
		case MySqlLexer.NULL_SYM:
			return LiteralKind.NULL;
		case MySqlLexer.TRUE_SYM:
		case MySqlLexer.FALSE_SYM:
			return LiteralKind.BOOLEAN;
		case MySqlLexer.INTERVAL_SYM:
			return LiteralKind.INTERVAL;
		case MySqlLexer.DATE_SYM:
			return LiteralKind.DATE;
		case MySqlLexer.DATETIME:
			return LiteralKind.DATETIME;
		case MySqlLexer.TIMESTAMP:
			return LiteralKind.TIMESTAMP;
		case MySqlLexer.UNKNOWN_SYM:
			return LiteralKind.UNKNOWN;
		default:
			throw new ASGException("Invalid token in toLiteralKind: " + tokenToString(tok));
		}
	}

	@Override
	public BinaryQueryKind toBinaryQueryKind(Token t1, Token t2) {
		if (t1 != null && t1.getType() == MySqlLexer.UNION_SYM) {
			if (t2 != null) {
				switch (t2.getType()) {
				case MySqlLexer.ALL:
					return BinaryQueryKind.UNIONALL;
				case MySqlLexer.DISTINCT:
					return BinaryQueryKind.UNIONDISTINCT;
				default:
					throw new ASGException(
							"Invalid Union in toBinaryQueryKind: " + tokenToString(t1) + ", " + tokenToString(t2));
				}
			} else {
				return BinaryQueryKind.UNION;
			}
		}
		throw new ASGException("Invalid tokens in toBinaryQueryKind: " + tokenToString(t1) + ", " + tokenToString(t2));
	}

	@Override
	protected SetQuantifierKind toSetQuantifierKind(Token tok) {
		if (tok == null) {
			throw new ASGException("Null token in toSetQuantifierKind!");
		}

		switch (tok.getType()) {
		case MySqlLexer.ALL:
			return SetQuantifierKind.ALL;
		case MySqlLexer.DISTINCT:
			return SetQuantifierKind.DISTINCT;
		default:
			throw new ASGException("Invalid token in toSqtQuantifierKind: " + tokenToString(tok));
		}
	}

	@Override
	protected ColumnAttributeKind toColumnAttributeKind(Token tok1, Token tok2) {
		if (tok1 == null) {
			throw new ASGException("Null token in toColumnAttributeKind!");
		}

		switch (tok1.getType()) {
		case MySqlLexer.NULL_SYM:
			if (tok2 != null && tok2.getType() == MySqlLexer.NOT_SYM) {
				return ColumnAttributeKind.NOTNULL;
			} else {
				return ColumnAttributeKind.NULL;
			}
		case MySqlLexer.DEFAULT:
			return ColumnAttributeKind.DEFAULT;
		case MySqlLexer.AUTO_INC:
			return ColumnAttributeKind.AUTOINCREMENT;
		case MySqlLexer.KEY_SYM:
			return ColumnAttributeKind.PRIMARYKEY;
		case MySqlLexer.UNIQUE_SYM:
			return ColumnAttributeKind.UNIQUE;
		case MySqlLexer.COMMENT_SYM:
			return ColumnAttributeKind.COMMENT;
		case MySqlLexer.SERIAL_SYM:
			return ColumnAttributeKind.SERIAL;
		default:
			throw new ASGException(
					"Invalid token in toColumnAttributeKind: " + tokenToString(tok1) + ", " + tokenToString(tok2));
		}
	}

	public OrderByElementKind toOrderByElementKind1(Token t) {
		return toOrderByElementKind(t);
	}

	@Override
	protected OrderByElementKind toOrderByElementKind(Token t) {
		if (t == null) {
			return OrderByElementKind.NONE;
		} else if (t.getType() == MySqlLexer.ASC) {
			return OrderByElementKind.ASC;
		} else if (t.getType() == MySqlLexer.DESC) {
			return OrderByElementKind.DESC;
		} else {
			throw new ASGException("Invalid token in toOrderByElementKind: " + tokenToString(t));
		}
	}

	@Override
	protected NullOrderKind toNullOrderKind(Token t1, Token t2) {
		throw new ASGException("NullOrderKind is not supported in MySQL!");
	}

	@Override
	protected NumericTypeKind toNumericTypeKind(Token tok) {
		if (tok == null) {
			throw new ASGException("Null token in toNumericTypeKind!");
		}

		return toNumericTypeKind(tok.getText());
	}

	private NumericTypeKind toNumericTypeKind(String name) {
		if (name == null) {
			throw new ASGException("Null name in toDateTypeKind!");
		}

		switch (name.toUpperCase(Locale.ENGLISH)) {
		case "INT":
		case "INTEGER":
			return NumericTypeKind.INT;
		case "TINYINT":
			return NumericTypeKind.TINYINT;
		case "SMALLINT":
			return NumericTypeKind.SMALLINT;
		case "MEDIUMINT":
			return NumericTypeKind.MEDIUMINT;
		case "BIGINT":
			return NumericTypeKind.BIGINT;
		case "REAL":
			return NumericTypeKind.REAL;
		case "DOUBLE":
			return NumericTypeKind.DOUBLE;
		case "BIT":
			return NumericTypeKind.BIT;
		// SERIAL - MySQL maps it to BIGINT UNSIGNED NOT NULL AUTO_INCREMENT
		// UNIQUE.
		case "SERIAL":
			return NumericTypeKind.SERIAL;
		// MySQL mapped types
		case "BOOL":
		case "BOOLEAN":
			return NumericTypeKind.BOOLEAN;
		case "FIXED":
		case "DECIMAL":
			return NumericTypeKind.DECIMAL;
		case "FLOAT":
		case "FLOAT4":
			return NumericTypeKind.FLOAT;
		case "FLOAT8":
			return NumericTypeKind.DOUBLE;
		case "INT1":
			return NumericTypeKind.TINYINT;
		case "INT2":
			return NumericTypeKind.SMALLINT;
		case "INT3":
			return NumericTypeKind.MEDIUMINT;
		case "INT4":
			return NumericTypeKind.INT;
		case "INT8":
			return NumericTypeKind.BIGINT;
		case "MIDDLEINT":
			return NumericTypeKind.MEDIUMINT;
		case "NUMERIC":
			return NumericTypeKind.DECIMAL;
		default:
			throw new ASGException("Invalid name string in toNumericTypeKind: " + name);
		}
	}

	@Override
	protected DateTypeKind toDateTypeKind(Token tok) {
		if (tok == null) {
			throw new ASGException("Null token in toDateTypeKind!");
		}

		switch (tok.getType()) {
		case MySqlLexer.YEAR_SYM:
			return DateTypeKind.YEAR;
		case MySqlLexer.DATE_SYM:
			return DateTypeKind.DATE;
		case MySqlLexer.TIME_SYM:
			return DateTypeKind.TIME;
		case MySqlLexer.TIMESTAMP:
			return DateTypeKind.TIMESTAMP;
		case MySqlLexer.DATETIME:
			return DateTypeKind.DATETIME;
		default:
			throw new ASGException("Invalid token in toDateTypeKind: " + tokenToString(tok));
		}
	}

	@Override
	protected StringTypeKind toStringTypeKind(Token tok) {
		if (tok == null) {
			throw new ASGException("Null token in toStringTypeKind!");
		}

		return toStringTypeKind(tok.getText());
	}

	private StringTypeKind toStringTypeKind(String name) {
		if (name == null) {
			throw new ASGException("Null name in toStringTypeKind!");
		}

		switch (name.toUpperCase(Locale.ENGLISH)) {
		case "CHAR":
			return StringTypeKind.CHAR;
		case "VARCHAR":
			return StringTypeKind.VARCHAR;
		case "NCHAR":
			return StringTypeKind.NCHAR;
		case "NVARCHAR":
			return StringTypeKind.NVARCHAR;
		case "BINARY":
			return StringTypeKind.BINARY;
		case "VARBINARY":
			return StringTypeKind.VARBINARY;
		case "BLOB":
			return StringTypeKind.BLOB;
		case "TEXT":
			return StringTypeKind.TEXT;
		// MySQL mapped types
		case "TINYTEXT":
			return StringTypeKind.TINYTEXT;
		case "LONGTEXT":
			return StringTypeKind.LONGTEXT;
		case "MEDIUMTEXT":
			return StringTypeKind.MEDIUMTEXT;
		case "TINYBLOB":
			return StringTypeKind.TINYBLOB;
		case "LONGBLOB":
			return StringTypeKind.LONGBLOB;
		case "MEDIUMBLOB":
			return StringTypeKind.MEDIUMBLOB;

		default:
			throw new ASGException("Invalid name string in toStringTypeKind: " + name);
		}
	}

	@Override
	protected FunctionParamsKind toFunctionParamsKind(Token t1, Token t2) {
		if (t1 == null && t2 == null) {
			return FunctionParamsKind.NONE;
		} else if (t1 != null) {
			switch (t1.getType()) {
			case MySqlLexer.ALL:
				return FunctionParamsKind.ALL;
			case MySqlLexer.DISTINCT:
				return FunctionParamsKind.DISTINCT;
			default:
				throw new ASGException(
						"Invalid token in toFunctionParamsKind: " + tokenToString(t1) + ", " + tokenToString(t2));
			}
		} else {
			throw new ASGException("Invalid token in toFunctionParamsKind! " + "NULL" + " , " + tokenToString(t2));
		}
	}

	@Override
	protected TimeUnitKind toTimeUnitKind(Token t) {
		switch (t.getType()) {
		case MySqlLexer.DAY_SYM:
			return TimeUnitKind.DAY;
		case MySqlLexer.WEEK_SYM:
			return TimeUnitKind.WEEK;
		case MySqlLexer.HOUR_SYM:
			return TimeUnitKind.HOUR;
		case MySqlLexer.MINUTE_SYM:
			return TimeUnitKind.MINUTE;
		case MySqlLexer.MONTH_SYM:
			return TimeUnitKind.MONTH;
		case MySqlLexer.QUARTER_SYM:
			return TimeUnitKind.QUARTER;
		case MySqlLexer.SECOND_SYM:
			return TimeUnitKind.SECOND;
		case MySqlLexer.MICROSECOND_SYM:
			return TimeUnitKind.MICROSECOND;
		case MySqlLexer.YEAR_SYM:
			return TimeUnitKind.YEAR;
		case MySqlLexer.DAY_HOUR_SYM:
			return TimeUnitKind.DAYHOUR;
		case MySqlLexer.DAY_MICROSECOND_SYM:
			return TimeUnitKind.DAYMICROSECOND;
		case MySqlLexer.DAY_MINUTE_SYM:
			return TimeUnitKind.DAYMINUTE;
		case MySqlLexer.DAY_SECOND_SYM:
			return TimeUnitKind.DAYSECOND;
		case MySqlLexer.HOUR_MICROSECOND_SYM:
			return TimeUnitKind.HOURMICROSECOND;
		case MySqlLexer.HOUR_MINUTE_SYM:
			return TimeUnitKind.HOURMINUTE;
		case MySqlLexer.HOUR_SECOND_SYM:
			return TimeUnitKind.HOURSECOND;
		case MySqlLexer.MINUTE_MICROSECOND_SYM:
			return TimeUnitKind.MINUTEMICROSECOND;
		case MySqlLexer.MINUTE_SECOND_SYM:
			return TimeUnitKind.MINUTESECOND;
		case MySqlLexer.SECOND_MICROSECOND_SYM:
			return TimeUnitKind.SECONDMICROSECOND;
		case MySqlLexer.YEAR_MONTH_SYM:
			return TimeUnitKind.YEARMONTH;
		default:
			throw new ASGException("Invalid token in toTimeUnitKind: " + tokenToString(t));
		}
	}

}
