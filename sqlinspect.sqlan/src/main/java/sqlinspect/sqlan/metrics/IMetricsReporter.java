package sqlinspect.sqlan.metrics;

import java.io.IOException;

public interface IMetricsReporter {
	void writeReport() throws IOException;
}
