package sqlinspect.sqlan.metrics;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.sqlan.asg.common.ASG;
import sqlinspect.sqlan.asg.schema.Database;
import sqlinspect.sqlan.asg.statm.Statement;

public class TXTMetricsReporter implements IMetricsReporter {
	private static final Logger logger = LoggerFactory.getLogger(TXTMetricsReporter.class);

	private final String path;

	private final ASG asg;

	private final Map<Statement, Map<MetricDesc, Integer>> metrics;

	private static final String UTF8 = "UTF-8";

	public TXTMetricsReporter(ASG asg, String path, Map<Statement, Map<MetricDesc, Integer>> metrics) {
		this.asg = asg;
		this.path = path;
		this.metrics = metrics;
	}

	@Override
	public void writeReport() {
		try (OutputStreamWriter ow = new OutputStreamWriter(Files.newOutputStream(new File(path).toPath()), UTF8);
				BufferedWriter bw = new BufferedWriter(ow);) {

			for (Database db : asg.getRoot().getDatabases()) {
				for (Statement stmt : db.getStatements()) {
					writeStatement(bw, stmt);
				}
			}
		} catch (IOException e) {
			logger.error("IO error while writing report!", e);
		}
	}

	private void writeStatement(BufferedWriter bw, Statement stmt) throws IOException {
		bw.write("Statement (" + stmt.getId() + ")");
		String lpath = stmt.getPath();
		if (lpath != null) {
			bw.write(" at " + lpath + '(' + stmt.getStartLine() + ")");
		}
		bw.write(" accesses:\n");
		writeMetrics(bw, stmt);
	}

	private void writeMetrics(BufferedWriter bw, Statement stmt) throws IOException {
		bw.write("  Metrics:\n");
		Map<MetricDesc, Integer> stmtMetrics = metrics.get(stmt);
		if (stmtMetrics != null) {
			for (Map.Entry<MetricDesc, Integer> me : stmtMetrics.entrySet()) {
				MetricDesc md = me.getKey();
				Integer val = me.getValue();
				bw.write("    " + md.getShortName() + ": " + val + "\n");
			}
		}
	}
}
