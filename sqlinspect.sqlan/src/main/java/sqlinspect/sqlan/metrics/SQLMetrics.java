package sqlinspect.sqlan.metrics;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.sqlan.asg.common.ASG;
import sqlinspect.sqlan.asg.expr.Expression;
import sqlinspect.sqlan.asg.expr.ExpressionList;
import sqlinspect.sqlan.asg.expr.SelectExpression;
import sqlinspect.sqlan.asg.statm.Delete;
import sqlinspect.sqlan.asg.statm.Insert;
import sqlinspect.sqlan.asg.statm.Select;
import sqlinspect.sqlan.asg.statm.Statement;
import sqlinspect.sqlan.asg.statm.Update;
import sqlinspect.sqlan.asg.traversal.PreOrderTraversal;
import sqlinspect.sqlan.asg.visitor.Visitor;

public class SQLMetrics extends Visitor {
	private static final Logger logger = LoggerFactory.getLogger(SQLMetrics.class);

	private Optional<Statement> actualStatement = Optional.empty();

	private final Deque<SelectExpression> actualSelectExpression = new ArrayDeque<>();

	private final Map<Statement, Map<MetricDesc, Integer>> metrics = new HashMap<>();

	private final ASG asg;

	public SQLMetrics(ASG asg) {
		super();
		this.asg = asg;
	}

	public void calculate() {
		metrics.clear();
		PreOrderTraversal traversal = new PreOrderTraversal(this);
		traversal.traverse(asg.getRoot());
	}

	public Map<Statement, Map<MetricDesc, Integer>> getMetrics() {
		return metrics;
	}

	@Override
	public void visit(Select n) {
		actualStatement = Optional.of(n);
	}

	@Override
	public void visitEnd(Select n) {
		actualStatement = Optional.empty();
	}

	@Override
	public void visit(Insert n) {
		actualStatement = Optional.of(n);
	}

	@Override
	public void visitEnd(Insert n) {
		actualStatement = Optional.empty();
	}

	@Override
	public void visit(Update n) {
		actualStatement = Optional.of(n);
	}

	@Override
	public void visitEnd(Update n) {
		actualStatement = Optional.empty();
	}

	@Override
	public void visit(Delete n) {
		actualStatement = Optional.of(n);
	}

	@Override
	public void visitEnd(Delete n) {
		actualStatement = Optional.empty();
	}

	@Override
	public void visit(SelectExpression n) {
		if (!actualStatement.isPresent()) {
			return;
		}

		actualSelectExpression.addLast(n);

		if (n.getParent().equals(actualStatement.get())) {
			calcFields(n);
			setNumberOfQueries(0);
			setNestingLevel(0);
		} else {
			incNumberOfQueries();
			incNestingLevel();
		}
	}

	@Override
	public void visitEnd(SelectExpression n) {
		if (n.equals(actualSelectExpression.getLast())) {
			actualSelectExpression.removeLast();
		}
	}

	private Integer getMetricValue(MetricDesc metric, int defaultValue) {
		if (!actualStatement.isPresent()) {
			logger.error("No actual statament is available");
			return 0;
		}

		Map<MetricDesc, Integer> stmtMetrics = metrics.computeIfAbsent(actualStatement.get(), k -> new EnumMap<>(MetricDesc.class));
		return stmtMetrics.computeIfAbsent(metric, k -> Integer.valueOf(defaultValue));
	}

	private void setMetricValue(MetricDesc metric, Integer value) {
		if (!actualStatement.isPresent()) {
			logger.error("No actual statament is available");
			return;
		}

		Map<MetricDesc, Integer> stmtMetrics = metrics.computeIfAbsent(actualStatement.get(), k -> new EnumMap<>(MetricDesc.class));
		stmtMetrics.put(metric, value);
	}

	private void incMetricValue(MetricDesc metric, Integer value) {
		if (!actualStatement.isPresent()) {
			logger.error("No actual statament is available");
			return;
		}
		
		Map<MetricDesc, Integer> stmtMetrics = metrics.computeIfAbsent(actualStatement.get(), k -> new EnumMap<>(MetricDesc.class));
		
		Integer m = stmtMetrics.get(metric);
		if (m == null) {
			stmtMetrics.put(metric, value);
		} else {
			stmtMetrics.put(metric, m + value);
		}
	}

	private void setNestingLevel(int val) {
		setMetricValue(MetricDesc.NestingLevel, val);
	}

	private void incNestingLevel() {
		Integer val = getMetricValue(MetricDesc.NestingLevel, 0);
		int nl = actualSelectExpression.size() - 1;
		if (nl > val) {
			setMetricValue(MetricDesc.NestingLevel, nl);
		}
	}

	private void setNumberOfQueries(int val) {
		setMetricValue(MetricDesc.NumberOfQueries, val);
	}

	private void incNumberOfQueries() {
		incMetricValue(MetricDesc.NumberOfQueries, 1);
	}

	private void calcFields(SelectExpression n) {
		Expression cl = n.getColumnList();
		if (cl instanceof ExpressionList) {
			ExpressionList el = (ExpressionList) cl;
			setMetricValue(MetricDesc.NumberOfResultFields, el.getExpressions().size());
		} else {
			setMetricValue(MetricDesc.NumberOfResultFields, 1);
		}
	}
}
