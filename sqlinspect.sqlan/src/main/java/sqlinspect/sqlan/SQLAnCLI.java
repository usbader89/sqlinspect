package sqlinspect.sqlan;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Command Line Interface for the Analyzer.
 *
 */
public final class SQLAnCLI {

	private static final String XML = "xml";
	private static final String TXT = "txt";
	private static final String DBPASSWORD = "dbpassword";
	private static final String DBUSER = "dbuser";
	private static final String DBURL = "dburl";
	private static final String PARSESCHEMA = "parseschema";
	private static final String ERRORSFORMAT = "errorsformat";
	private static final String DUMPERRORS = "dumperrors";
	private static final String DUMPSYMTAB = "dumpsymtab";
	private static final String EXPORTJSON = "exportjson";
	private static final String METRICSREPORTFORMAT = "metricsreportformat";
	private static final String METRICSREPORT = "metricsreport";
	private static final String RUNMETRICS = "runmetrics";
	private static final String TAAREPORTFORMAT = "taareportformat";
	private static final String TAAREPORT = "taareport";
	private static final String RUNTAA = "runtaa";
	private static final String SMELLREPORTFORMAT = "smellreportformat";
	private static final String SMELLREPORT = "smellreport";
	private static final String RUNSMELLS = "runsmells";
	private static final String DIALECT = "dialect";
	private static final String VERSION = "version";
	private static final String HELP_OPT = "help";

	private static final String[] FORMATS = { TXT, XML };

	private static final Logger logger = LoggerFactory.getLogger(SQLAnCLI.class);

	private static Options options = new Options();

	private static Config conf = new Config();

	private static SQLAn sqlan = new SQLAn();

	// Utility class, we don't want to instantiate it
	private SQLAnCLI() {
	}

	private static void createOptions() {
		final Option help = new Option(HELP_OPT, "print this message");
		final Option version = new Option(VERSION, "print the version information and exit");
		final Option dialect = new Option(DIALECT, true, "sql dialect (MySQL, Impala) default: MySQL");
		final Option runSmells = new Option(RUNSMELLS, "run smell detection");
		final Option smellReportFile = new Option(SMELLREPORT, true,
				"report smells to the given file (default to System.out)");
		final Option smellReportFormat = new Option(SMELLREPORTFORMAT, true,
				"report smells in the given format (txt/xml)");
		final Option runTAA = new Option(RUNTAA, "run table access analysis");
		final Option taaReportFile = new Option(TAAREPORT, true,
				"table access analysis report file (default to System.out)");
		final Option taaReportFormat = new Option(TAAREPORTFORMAT, true,
				"report table access analysis in the given format (txt/xml)");
		final Option runMetrics = new Option(RUNMETRICS, "run metrics calculation");
		final Option metricsReportFile = new Option(METRICSREPORT, true, "metrics report file (default to System.out)");
		final Option metricsReportFormat = new Option(METRICSREPORTFORMAT, true,
				"report metrics in the given format (txt/xml)");
		final Option exportJson = new Option(EXPORTJSON, true, "export AST in JSON format");
		final Option dumpSymTab = new Option(DUMPSYMTAB, true, "dump symbol sable");
		final Option dumpErrors = new Option(DUMPERRORS, true, "dump parsing errors");
		final Option errorsFormat = new Option(ERRORSFORMAT, true, "dump errors in the given format (txt/xml)");
		final Option parseDbSchema = new Option(PARSESCHEMA, "parse schema from database");
		final Option dburl = new Option(DBURL, true, "database URL");
		final Option dbuser = new Option(DBUSER, true, "database user");
		final Option dbpassword = new Option(DBPASSWORD, true, "database password");

		options.addOption(help);
		options.addOption(version);
		options.addOption(dialect);
		options.addOption(runSmells);
		options.addOption(smellReportFile);
		options.addOption(smellReportFormat);
		options.addOption(runTAA);
		options.addOption(taaReportFile);
		options.addOption(taaReportFormat);
		options.addOption(runMetrics);
		options.addOption(metricsReportFile);
		options.addOption(metricsReportFormat);
		options.addOption(exportJson);
		options.addOption(dumpSymTab);
		options.addOption(dumpErrors);
		options.addOption(errorsFormat);
		options.addOption(parseDbSchema);
		options.addOption(dburl);
		options.addOption(dbuser);
		options.addOption(dbpassword);
	}

	private static void welcome() {
		logger.info("Hello dear SQLAnalyzer user!");
	}

	private static void help() {
		// automatically generate the help statement
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp("SQLAn [options] [input1] [input2] ...", options);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		welcome();

		createOptions();

		try {
			// create the parser
			CommandLineParser parser = new DefaultParser();

			// parse the command line arguments
			CommandLine cmdLine = parser.parse(options, args);
			String[] inputs = cmdLine.getArgs();

			if (cmdLine.hasOption(HELP_OPT) || inputs.length < 1) {
				help();
				return;
			}

			if (cmdLine.hasOption(DIALECT)) {
				conf.setDialect(SQLDialect.valueOf(cmdLine.getOptionValue(DIALECT).toUpperCase(Locale.ENGLISH)));
			}

			if (cmdLine.hasOption(DBURL)) {
				conf.setDbUrl(cmdLine.getOptionValue(DBURL));
			}

			if (cmdLine.hasOption(DBUSER)) {
				conf.setDbUser(cmdLine.getOptionValue(DBUSER));
			}

			if (cmdLine.hasOption(DBPASSWORD)) {
				conf.setDbPassword(cmdLine.getOptionValue(DBPASSWORD));
			}

			if (cmdLine.hasOption(PARSESCHEMA)) {
				if (conf.getDbUrl() == null || conf.getDbUrl().isEmpty()) {
					logger.warn(
							"The option parseDBSchema can be set only if a database URL is provided too. This is not the case, so we ignore it.");
					conf.setParseDBSchema(false);
				} else {
					conf.setParseDBSchema(true);
				}
			}

			if (cmdLine.hasOption(EXPORTJSON)) {
				conf.setJsonExport(cmdLine.getOptionValue(EXPORTJSON));
			}

			if (cmdLine.hasOption(DUMPSYMTAB)) {
				conf.setDumpSymTab(cmdLine.getOptionValue(DUMPSYMTAB));
			}

			if (cmdLine.hasOption(DUMPERRORS)) {
				conf.setDumpErrors(cmdLine.getOptionValue(DUMPERRORS));
			}

			if (cmdLine.hasOption(ERRORSFORMAT)) {
				String format = cmdLine.getOptionValue(ERRORSFORMAT);
				List<String> formats = Arrays.asList(FORMATS);

				if (formats.contains(format.toLowerCase(Locale.ENGLISH))) {
					conf.setErrorsFormat(cmdLine.getOptionValue(ERRORSFORMAT));
				} else {
					logger.warn(
							"Smell report Format ({}) is not supported, using default value ({})! Supported formats: {}",
							format, conf.getSmellReportFormat(), formats);
				}
			}

			if (cmdLine.hasOption(SMELLREPORT)) {
				conf.setSmellReportFile(cmdLine.getOptionValue(SMELLREPORT));
			}

			if (cmdLine.hasOption(SMELLREPORTFORMAT)) {
				String format = cmdLine.getOptionValue(SMELLREPORTFORMAT);
				List<String> formats = Arrays.asList(FORMATS);

				if (formats.contains(format.toLowerCase(Locale.ENGLISH))) {
					conf.setSmellReportFormat(cmdLine.getOptionValue(SMELLREPORTFORMAT));
				} else {
					logger.warn(
							"Smell report Format ({}) is not supported, using default value ({})! Supported formats: {}",
							format, conf.getSmellReportFormat(), formats);
				}
			}

			if (cmdLine.hasOption(RUNSMELLS)) {
				conf.setRunSmells(true);
			}

			if (cmdLine.hasOption(TAAREPORT)) {
				conf.setTAAReportFile(cmdLine.getOptionValue(TAAREPORT));
			}

			if (cmdLine.hasOption(TAAREPORTFORMAT)) {
				String format = cmdLine.getOptionValue(TAAREPORTFORMAT);
				List<String> formats = Arrays.asList(FORMATS);

				if (formats.contains(format.toLowerCase(Locale.ENGLISH))) {
					conf.setTAAReportFormat(cmdLine.getOptionValue(TAAREPORTFORMAT));
				} else {
					logger.warn(
							"TAA report Format ({}) is not supported, using default value ({}) ! Supported formats: {}",
							format, conf.getTAAReportFormat(), formats);
				}
			}

			if (cmdLine.hasOption(RUNTAA)) {
				conf.setRunTAA(true);
			}

			if (cmdLine.hasOption(METRICSREPORT)) {
				conf.setSQLMetricsReportFile(cmdLine.getOptionValue(METRICSREPORT));
			}

			if (cmdLine.hasOption(METRICSREPORTFORMAT)) {
				String format = cmdLine.getOptionValue(METRICSREPORTFORMAT);
				List<String> formats = Arrays.asList(FORMATS);

				if (formats.contains(format.toLowerCase(Locale.ENGLISH))) {
					conf.setSQLMetricsReportFormat(cmdLine.getOptionValue(METRICSREPORTFORMAT));
				} else {
					logger.warn(
							"Metrics report Format ({}) is not supported, using default value ({})! Supported formats: {}",
							format, conf.getSQLMetricsReportFormat(), formats);
				}
			}

			if (cmdLine.hasOption(RUNMETRICS)) {
				conf.setRunSQLMetrics(true);
			}

			sqlan.setConf(conf);

			sqlan.analyze(inputs);

		} catch (ParseException exp) {
			logger.error("Parsing failed.  Reason: {}", exp.getMessage());
			logger.error("Try \"--help\" for further details.);");
		}
	}
}
