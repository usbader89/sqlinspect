package sqlinspect.sqlan;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import sqlinspect.sqlan.asg.base.Base;
import sqlinspect.sqlan.asg.base.SQLRoot;
import sqlinspect.sqlan.asg.common.ASG;
import sqlinspect.sqlan.asg.common.SymbolTable;
import sqlinspect.sqlan.asg.schema.Column;
import sqlinspect.sqlan.asg.schema.Database;
import sqlinspect.sqlan.asg.schema.Table;
import sqlinspect.sqlan.asg.statm.Statement;
import sqlinspect.sqlan.asg.traversal.MultiPreOrderTraversal;
import sqlinspect.sqlan.asg.traversal.PreOrderTraversal;
import sqlinspect.sqlan.metrics.IMetricsReporter;
import sqlinspect.sqlan.metrics.MetricDesc;
import sqlinspect.sqlan.metrics.SQLMetrics;
import sqlinspect.sqlan.metrics.TXTMetricsReporter;
import sqlinspect.sqlan.metrics.XMLMetricsReporter;
import sqlinspect.sqlan.parser.AbstractParserActions;
import sqlinspect.sqlan.parser.AbstractSQLLexer;
import sqlinspect.sqlan.parser.AbstractSQLParser;
import sqlinspect.sqlan.parser.IErrorDump;
import sqlinspect.sqlan.parser.ImpalaLexer;
import sqlinspect.sqlan.parser.ImpalaParser;
import sqlinspect.sqlan.parser.ImpalaParserActions;
import sqlinspect.sqlan.parser.MySQLParserActions;
import sqlinspect.sqlan.parser.MySqlLexer;
import sqlinspect.sqlan.parser.MySqlParser;
import sqlinspect.sqlan.parser.ParserConfig;
import sqlinspect.sqlan.parser.ParserException;
import sqlinspect.sqlan.parser.SQLiteLexer;
import sqlinspect.sqlan.parser.SQLiteParser;
import sqlinspect.sqlan.parser.SQLiteParserActions;
import sqlinspect.sqlan.parser.TXTErrorDump;
import sqlinspect.sqlan.parser.VisitorExtraEdges;
import sqlinspect.sqlan.parser.XMLErrorDump;
import sqlinspect.sqlan.smells.common.SQLSmell;
import sqlinspect.sqlan.smells.common.SQLSmellComparator;
import sqlinspect.sqlan.smells.detectors.AmbiguousGroups;
import sqlinspect.sqlan.smells.detectors.FearOfTheUnknown;
import sqlinspect.sqlan.smells.detectors.ImplicitColumns;
import sqlinspect.sqlan.smells.detectors.RandomSelection;
import sqlinspect.sqlan.smells.reports.ISmellReporter;
import sqlinspect.sqlan.smells.reports.StreamSmellReporter;
import sqlinspect.sqlan.smells.reports.TXTSmellReporter;
import sqlinspect.sqlan.smells.reports.XMLSmellReporter;
import sqlinspect.sqlan.taa.ITAAReporter;
import sqlinspect.sqlan.taa.TAA;
import sqlinspect.sqlan.taa.TXTTAAReporter;
import sqlinspect.sqlan.taa.XMLTAAReporter;

public class SQLAn {
	private static final Logger logger = LoggerFactory.getLogger(SQLAn.class);

	private final ParserConfig parserConf = new ParserConfig();

	private final SymbolTable symTab = new SymbolTable();

	private final ASG asg = new ASG();

	private AbstractParserActions act;

	private static final String UNKNOWN_INPUT = "<unknown>";

	private Optional<DBHandler> dbHandler = Optional.empty();

	private Config conf = new Config();

	private static final String UTF8 = "UTF-8";

	private final Set<SQLSmell> smells = new HashSet<>();
	private final Map<Statement, Map<Column, Integer>> columnAcc = new HashMap<>();
	private final Map<Statement, Map<Table, Integer>> tableAcc = new HashMap<>();
	private final Map<Statement, Map<MetricDesc, Integer>> metrics = new HashMap<>();

	private final BenchMark bm = new BenchMark();

	/**
	 * @param conf the conf to set
	 */
	public void setConf(Config conf) {
		this.conf = conf;
	}

	public ASG getASG() {
		return asg;
	}

	private SQLDialect getDialect() {
		return conf.getDialect() == null ? SQLDialect.MYSQL : conf.getDialect();
	}

	private void createParserActions() {
		if (conf.getDialect() == SQLDialect.MYSQL) {
			act = new MySQLParserActions(asg, parserConf);
		} else if (conf.getDialect() == SQLDialect.IMPALA) {
			act = new ImpalaParserActions(asg, parserConf);
		} else if (conf.getDialect() == SQLDialect.SQLITE) {
			act = new SQLiteParserActions(asg, parserConf);
		} else {
			logger.warn("No dialect was specified, using default!");
			act = new MySQLParserActions(asg, parserConf);
		}
	}

	public void reinit() {
		smells.clear();
		symTab.clear();
		asg.clearNodes();
		if (act == null || conf.getDialect() != act.getDialect()) {
			createParserActions();
		}
		act.init();
	}

	public List<Statement> parseFile(String path) {
		ArrayList<Statement> ret = new ArrayList<>();
		try {
			logger.info("Parsing file: {}", path);
			if (act == null) {
				createParserActions();
			}

			act.setInputName(path);

			AbstractSQLLexer lexer;
			AbstractSQLParser parser;
			if (conf.getDialect() == SQLDialect.IMPALA) {
				lexer = new ImpalaLexer(CharStreams.fromFileName(path));
				CommonTokenStream tokens = new CommonTokenStream(lexer);
				parser = new ImpalaParser(tokens);
			} else if (conf.getDialect() == SQLDialect.SQLITE) {
				lexer = new SQLiteLexer(CharStreams.fromFileName(path));
				CommonTokenStream tokens = new CommonTokenStream(lexer);
				parser = new SQLiteParser(tokens);
			} else {
				lexer = new MySqlLexer(CharStreams.fromFileName(path));
				CommonTokenStream tokens = new CommonTokenStream(lexer);
				parser = new MySqlParser(tokens);
			}

			parser.setFactory(asg);
			parser.setActions(act);

			ret.addAll(parser.parse());

			logger.info("Done");
		} catch (ParserException e) {
			logger.error("Error initializing the parser: ", e);
		} catch (FileNotFoundException e) {
			logger.error("Could not open input file: " + path, e);
		} catch (IOException e) {
			logger.error("Could not open input stream.", e);
		}
		return ret;
	}

	public List<Statement> parse(String s, int internalId, boolean clearDFA) {
		logger.debug("Parsing string: {}", s);
		AbstractSQLLexer lexer;
		AbstractSQLParser parser;
		ArrayList<Statement> ret = new ArrayList<>();

		try {
			if (act == null) {
				createParserActions();
			}
			act.setInputName(UNKNOWN_INPUT);
			act.setInternalId(internalId);

			if (conf.getDialect() == SQLDialect.IMPALA) {
				lexer = new ImpalaLexer(CharStreams.fromString(s));
				CommonTokenStream tokens = new CommonTokenStream(lexer);
				parser = new ImpalaParser(tokens);
			} else if (conf.getDialect() == SQLDialect.SQLITE) {
				lexer = new SQLiteLexer(CharStreams.fromString(s));
				CommonTokenStream tokens = new CommonTokenStream(lexer);
				parser = new SQLiteParser(tokens);
			} else {
				lexer = new MySqlLexer(CharStreams.fromString(s));
				CommonTokenStream tokens = new CommonTokenStream(lexer);
				parser = new MySqlParser(tokens);
			}

			parser.setFactory(asg);
			parser.setActions(act);

			try {
				ret.addAll(parser.parse());
				if (clearDFA) {
					parser.clearDFA();
				}
			} catch (Exception e) {
				logger.error("Error occured while parsing strings: ", e);
			}

		} catch (ParserException e) {
			logger.error("Error initializing the parser: ", e);
		}
		return ret;
	}

	public List<Statement> parse(String s, String locationFile, int locationRow, int internalId, boolean clearDFA) {
		logger.debug("Parsing string: {} location: {} ({})", s, locationFile, locationRow);
		AbstractSQLLexer lexer;
		AbstractSQLParser parser;
		ArrayList<Statement> ret = new ArrayList<>();

		try {

			if (act == null) {
				createParserActions();
			}

			act.setInputName(locationFile);
			act.setInitialRow(locationRow);
			act.setInternalId(internalId);

			if (conf.getDialect() == SQLDialect.IMPALA) {
				lexer = new ImpalaLexer(CharStreams.fromString(s));
				CommonTokenStream tokens = new CommonTokenStream(lexer);
				parser = new ImpalaParser(tokens);
			} else if (conf.getDialect() == SQLDialect.SQLITE) {
				lexer = new SQLiteLexer(CharStreams.fromString(s));
				CommonTokenStream tokens = new CommonTokenStream(lexer);
				parser = new SQLiteParser(tokens);
			} else {
				lexer = new MySqlLexer(CharStreams.fromString(s));
				CommonTokenStream tokens = new CommonTokenStream(lexer);
				parser = new MySqlParser(tokens);
			}

			parser.setFactory(asg);
			parser.setActions(act);

			try {
				ret.addAll(parser.parse());

				if (clearDFA) {
					parser.clearDFA();
				}
			} catch (Exception e) {
				logger.error("Error occured while parsing strings: ", e);
			}

		} catch (ParserException e) {
			logger.error("Error initializing the parser: ", e);
		}
		return ret;
	}

	private void parseDBSchema() {
		logger.info("Parsing Schema from database");
		if (!dbHandler.isPresent()) {
			logger.error("No dbhandler is available.!");
			return;
		}
		String schema = dbHandler.get().getDBSchema();
		logger.debug("Schema: {}", schema);
		parse(schema, 0, false);
		logger.info("Done.");
	}

	private void resolveIdentifiers() {
		logger.info("Resolving identifiers.");
		VisitorExtraEdges visitor = new VisitorExtraEdges(asg, symTab);
		PreOrderTraversal traversal = new PreOrderTraversal(visitor);
		traversal.traverse(asg.getRoot());

		visitor.logStats();
		logger.info("Done.");
	}

	public boolean jsonExport(String path) {
		try {
			logger.info("Exporting AST to {}", path);
			ObjectMapper mapper = new ObjectMapper();

			SQLRoot root = asg.getRoot();
			mapper.writerWithDefaultPrettyPrinter().writeValue(new File(path), root);
			logger.info("Done.");
			return true;
		} catch (IOException e) {
			logger.error("Could not export to file " + path, e);
			return false;
		}
	}

	/**
	 * @param path
	 */
	private void dumpSymTab(String path) {
		logger.info("Dump symbol table to {}", path);

		symTab.dump(path);
		logger.info("Done.");
	}

	private void dumpErrors(String dumpFile, String format) {
		logger.info("Dump parser errors: {} ({})", dumpFile, format);

		IErrorDump dump;
		if ("XML".equalsIgnoreCase(format)) {
			dump = new XMLErrorDump(asg, dumpFile, false);
		} else {
			dump = new TXTErrorDump(asg, dumpFile, false);
		}

		dump.writeDump();
		logger.info("Done.");
	}

	private void runSmells() {
		logger.info("Run code smells.");
		bm.startAll("SQLAn.runSmells()");

		FearOfTheUnknown fearVisitor = new FearOfTheUnknown(dbHandler);
		AmbiguousGroups abgVisitor = new AmbiguousGroups(getDialect());
		RandomSelection randomVisitor = new RandomSelection(getDialect());
		ImplicitColumns implicitColsVisitor = new ImplicitColumns();

		MultiPreOrderTraversal traversal = new MultiPreOrderTraversal();
		traversal.addVisitor(fearVisitor);
		traversal.addVisitor(abgVisitor);
		traversal.addVisitor(randomVisitor);
		traversal.addVisitor(implicitColsVisitor);

		traversal.traverse(asg.getRoot());

		smells.addAll(fearVisitor.getSmells());
		smells.addAll(abgVisitor.getSmells());
		smells.addAll(randomVisitor.getSmells());
		smells.addAll(implicitColsVisitor.getSmells());

		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		StreamSmellReporter rep = new StreamSmellReporter(stream);
		rep.writeReport(smells);
		String repStr;
		try {
			repStr = stream.toString(UTF8);
			if (!repStr.isEmpty()) {
				logger.info(repStr);
			}
		} catch (UnsupportedEncodingException e) {
			logger.error("Unsupported encoding", e);
		}

		if (conf.getSmellReportFile() != null && !conf.getSmellReportFile().isEmpty()) {
			reportSmells(conf.getSmellReportFile(), conf.getSmellReportFormat());
		}

		bm.stopAll("SQLAn.runSmells()");
		logger.info("Done");
	}

	/**
	 * Run Table Access Analysis
	 */
	private void runTAA() {
		logger.info("Run TAA.");
		bm.startAll("SQLAn.runTAA()");

		TAA tableAccessVisitor = new TAA();
		PreOrderTraversal traversal = new PreOrderTraversal(tableAccessVisitor);
		traversal.traverse(asg.getRoot());
		tableAcc.putAll(tableAccessVisitor.getTableAcc());
		columnAcc.putAll(tableAccessVisitor.getColumnAcc());

		if (conf.getTAAReportFile() != null && !conf.getTAAReportFile().isEmpty()) {
			reportTAA(conf.getTAAReportFile(), conf.getTAAReportFormat());
		}

		bm.stopAll("SQLAn.runTAA()");
		logger.info("Done.");
	}

	/**
	 * Run SQL Metrics Analysis
	 */
	private void runSQLMetrics() {
		logger.info("Run SQL Metrics.");
		bm.startAll("SQLAn.runSQLMetrics()");

		SQLMetrics sqlMetrics = new SQLMetrics(asg);
		sqlMetrics.calculate();
		metrics.putAll(sqlMetrics.getMetrics());

		if (conf.getSQLMetricsReportFile() != null && !conf.getSQLMetricsReportFile().isEmpty()) {
			reportSQLMetrics(conf.getSQLMetricsReportFile(), conf.getSQLMetricsReportFormat());
		}

		bm.stopAll("SQLAn.runSQLMetrics()");
		logger.info("Done.");
	}

	public Set<SQLSmell> getSmells() {
		return smells;
	}

	public Map<Statement, Map<Column, Integer>> getColumnAcc() {
		return columnAcc;
	}

	public Map<Statement, Map<Table, Integer>> getTableAcc() {
		return tableAcc;
	}

	public Map<Statement, Map<MetricDesc, Integer>> getMetrics() {
		return metrics;
	}

	public boolean reportSmells(String reportFile, String reportFormat) {
		logger.info("Write smell report: {} ({})", reportFile, reportFormat);

		ISmellReporter report;
		if ("XML".equalsIgnoreCase(reportFormat)) {
			report = new XMLSmellReporter(reportFile);
		} else {
			report = new TXTSmellReporter(reportFile);
		}

		Set<SQLSmell> smellsNoDuplicates = new TreeSet<>(new SQLSmellComparator());
		smellsNoDuplicates.addAll(smells);

		try {
			report.writeReport(smellsNoDuplicates);
		} catch (IOException e) {
			logger.error("IO error while writing report: ", e);
		}
		return true;
	}

	public boolean reportTAA(String reportFile, String reportFormat) {
		logger.info("Write TAA report: {} ({})", reportFile, reportFormat);

		ITAAReporter report;
		if ("XML".equalsIgnoreCase(reportFormat)) {
			report = new XMLTAAReporter(asg, reportFile, tableAcc, columnAcc);
		} else {
			report = new TXTTAAReporter(asg, reportFile, tableAcc, columnAcc);
		}

		report.writeReport();
		return true;
	}

	public boolean reportSQLMetrics(String reportFile, String reportFormat) {
		logger.info("Write metrics report: {} ({})", reportFile, reportFormat);

		IMetricsReporter report;
		if ("XML".equalsIgnoreCase(reportFormat)) {
			report = new XMLMetricsReporter(asg, reportFile, metrics);
		} else {
			report = new TXTMetricsReporter(asg, reportFile, metrics);
		}

		try {
			report.writeReport();
		} catch (IOException e) {
			logger.error("IO error writing report: ", e);
		}
		return true;
	}

	public List<Statement> analyze(String... inputs) {
		logger.info("SQLAn analysis started.");
		bm.startAll("SQLAn.analyze()");

		ArrayList<Statement> ret = new ArrayList<>();

		if (conf.getDbUrl() != null && !conf.getDbUrl().isEmpty()) {
			dbHandler = Optional.of(new DBHandler(conf.getDbUrl(), conf.getDbUser(), conf.getDbPassword()));
			dbHandler.get().connect();
		}

		if (conf.isParseDBSchema()) {
			parseDBSchema();
		}

		for (String i : inputs) {
			ret.addAll(parseFile(i));
		}

		resolveIdentifiers();

		stats();

		if (conf.getJsonExport() != null && !conf.getJsonExport().isEmpty()) {
			jsonExport(conf.getJsonExport());
		}

		if (conf.getDumpSymTab() != null && !conf.getDumpSymTab().isEmpty()) {
			dumpSymTab(conf.getDumpSymTab());
		}

		if (conf.getDumpErrors() != null && !conf.getDumpErrors().isEmpty()) {
			dumpErrors(conf.getDumpErrors(), conf.getErrorsFormat());
		}

		if (conf.isRunSmells()) {
			runSmells();
		}

		if (conf.isRunTAA()) {
			runTAA();
		}

		if (conf.isRunSQLMetrics()) {
			runSQLMetrics();
		}

		if (dbHandler.isPresent()) {
			dbHandler.get().close();
			dbHandler = Optional.empty();
		}

		bm.stopAll("SQLAn.analyze()");
		logger.info("Analysis done.");
		return ret;
	}

	public Map<Integer, List<Statement>> analyzeStrings(String... inputs) {
		return analyzeStrings(inputs, null, null);
	}

	public Map<Integer, List<Statement>> analyzeStrings(String[] inputs, String[] locationFiles, int[] locationRows) {
		logger.info("SQLAn analysis started.");
		bm.startAll("SQLAn.analyze()");

		Map<Integer, List<Statement>> ret = new HashMap<>();

		if (conf.getDbUrl() != null && !conf.getDbUrl().isEmpty()) {
			dbHandler = Optional.of(new DBHandler(conf.getDbUrl(), conf.getDbUser(), conf.getDbPassword()));
			dbHandler.get().connect();
		}

		if (conf.isParseDBSchema()) {
			parseDBSchema();
		}

		for (int i = 0; i < inputs.length; i++) {
			int internalId = i + 1;
			if (locationFiles != null && locationRows != null && locationFiles.length > i && locationRows.length > i) {
				if (i < inputs.length - 1) {
					ret.put(i, parse(inputs[i], locationFiles[i], locationRows[i], internalId, false));
				} else {
					ret.put(i, parse(inputs[i], locationFiles[i], locationRows[i], internalId, true));
				}
			} else {
				if (i < inputs.length - 1) {
					ret.put(i, parse(inputs[i], internalId, false));
				} else {
					ret.put(i, parse(inputs[i], internalId, true));
				}
			}
		}

		resolveIdentifiers();

		stats();

		if (conf.getJsonExport() != null && !conf.getJsonExport().isEmpty()) {
			jsonExport(conf.getJsonExport());
		}

		if (conf.getDumpSymTab() != null && !conf.getDumpSymTab().isEmpty()) {
			dumpSymTab(conf.getDumpSymTab());
		}

		if (conf.getDumpErrors() != null && !conf.getDumpErrors().isEmpty()) {
			dumpErrors(conf.getDumpErrors(), conf.getErrorsFormat());
		}

		if (conf.isRunSmells()) {
			runSmells();
		}

		if (conf.isRunTAA()) {
			runTAA();
		}

		if (conf.isRunSQLMetrics()) {
			runSQLMetrics();
		}

		if (dbHandler.isPresent()) {
			dbHandler.get().close();
			dbHandler = Optional.empty();
		}

		bm.stopAll("SQLAn.analyze()");
		logger.info("Analysis done.");
		return ret;
	}

	public List<Statement> searchStatement(String sql, boolean ignoreLiterals, boolean ignoreNames,
			boolean ignoreRefs) {
		logger.debug("Searching for query: {}", sql);
		ArrayList<Statement> ret = new ArrayList<>();

		Map<Integer, List<Statement>> ref = analyzeStrings(sql);

		if (ref == null || ref.size() != 1) {
			logger.error("Error parsing the statement.");
			return ret;
		}

		List<Statement> refStmtList = ref.get(Integer.valueOf(0));
		if (refStmtList == null || refStmtList.size() != 1) {
			logger.error("Only one statement can be specified.");
			return ret;
		}

		Statement refStmt = refStmtList.get(0);
		logger.debug("Searching for statement: {}", refStmt);

		for (Database db : asg.getRoot().getDatabases()) {
			for (Statement stmt : db.getStatements()) {
				if (!stmt.equals(refStmt)) {
					try {
						MethodHandles.Lookup lookup = MethodHandles.lookup();
						MethodHandle mh = lookup.findVirtual(refStmt.getClass(), "matchTree", MethodType
								.methodType(boolean.class, Base.class, boolean.class, boolean.class, boolean.class));
						boolean res = (boolean) mh.invoke(refStmt, stmt, ignoreLiterals, ignoreNames, ignoreRefs);
						if (res) {
							logger.debug("Found matching statement: {}", stmt);
							ret.add(stmt);
						}
					} catch (Throwable e) {
						logger.error("Could not invoke matchTree method: ", e);
					}
				}
			}
		}

		return ret;
	}

	public void stats() {
		logger.info("Successfully parsed {} statements.", act.getStatements());
	}

	public void dumpBM(File f) {
		bm.dumpFile(f);
	}

}
