package sqlinspect.sqlan.asg.common;

import java.util.Arrays;

import sqlinspect.sqlan.asg.base.Base;

public class SymbolDesc {

	private Base symbol;
	private NodeKind kind;
	private Base scope;

	public SymbolDesc(Base symbol, NodeKind kind, Base scope) {
		this.symbol = symbol;
		this.kind = kind;
		this.scope = scope;
	}

	/**
	 * @return the symbol
	 */
	public Base getSymbol() {
		return symbol;
	}

	/**
	 * @param symbol
	 *            the symbol to set
	 */
	public void setSymbol(Base symbol) {
		this.symbol = symbol;
	}

	/**
	 * @return the kind
	 */
	public NodeKind getKind() {
		return kind;
	}

	/**
	 * @param kind
	 *            the kind to set
	 */
	public void setKind(NodeKind kind) {
		this.kind = kind;
	}

	/**
	 * @return the scope
	 */
	public Base getScope() {
		return scope;
	}

	/**
	 * @param scope
	 *            the scope to set
	 */
	public void setScope(Base scope) {
		this.scope = scope;
	}

	@Override
	public int hashCode() {
		int hash = 8;
		hash = 31 * hash + symbol.hashCode();
		hash = 31 * hash + kind.hashCode();
		hash = 31 * hash + scope.hashCode();
		return hash;
	}

	@Override
	public boolean equals(Object other) {
		boolean ret = false;
		if (other instanceof SymbolDesc) {
			SymbolDesc sd = (SymbolDesc) other;
			ret = symbol == sd.getSymbol() && kind == sd.getKind() && scope == sd.getScope();
		}
		return ret;
	}

	public boolean match(SymbolDesc sd) {
		return (symbol == null || symbol == sd.getSymbol()) && (kind == NodeKind.BASE || kind == sd.getKind())
				&& (scope == null || scope == sd.getScope());
	}

	public boolean match(NodeKind... kinds) {
		return kinds == null || Arrays.asList(kinds).contains(this.kind);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SymbolDesc [symbol=" + symbol + ", kind=" + kind + ", scope=" + scope + "]";
	}
}
