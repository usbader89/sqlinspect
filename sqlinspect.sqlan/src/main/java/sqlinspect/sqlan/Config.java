package sqlinspect.sqlan;

public class Config {
	
	private SQLDialect dialect;
	
	private static final String DEFAULT_FORMAT = "xml";
	
	private String dbUrl;
	
	private String dbUser;
	private String dbPassword;
	
	private boolean parseDBSchema;
	
	private boolean runSmells;
	private String smellReportFile;
	private String smellReportFormat = DEFAULT_FORMAT;

	private boolean runTAA;
	private String taaReportFile;
	private String taaReportFormat = DEFAULT_FORMAT;

	private boolean runSQLMetrics;
	private String sqlMetricsReportFile;
	private String sqlMetricsReportFormat = DEFAULT_FORMAT;
	
	private String jsonExport;
	private String dumpSymTab;
	
	private String dumpErrors;
	private String errorsFormat = DEFAULT_FORMAT;
	
	/**
	 * @return the dialect
	 */
	public SQLDialect getDialect() {
		return dialect;
	}

	/**
	 * @param dialect the dialect to set
	 */
	public void setDialect(SQLDialect dialect) {
		this.dialect = dialect;
	}
	
	/**
	 * @return the database url
	 */
	public String getDbUrl() {
		return dbUrl;
	}

	/**
	 * @return the dbUser
	 */
	public String getDbUser() {
		return dbUser;
	}

	/**
	 * @param dbUser the dbUser to set
	 */
	public void setDbUser(String dbUser) {
		this.dbUser = dbUser;
	}

	/**
	 * @return the dbPassword
	 */
	public String getDbPassword() {
		return dbPassword;
	}

	/**
	 * @param dbPassword the dbPassword to set
	 */
	public void setDbPassword(String dbPassword) {
		this.dbPassword = dbPassword;
	}

	/**
	 * @param dburl the database url to set
	 */
	public void setDbUrl(String dbUrl) {
		this.dbUrl = dbUrl;
	}

	/**
	 * @return the parseDBSchema
	 */
	public boolean isParseDBSchema() {
		return parseDBSchema;
	}

	/**
	 * @param parseDBSchema the parseDBSchema to set
	 */
	public void setParseDBSchema(boolean parseDBSchema) {
		this.parseDBSchema = parseDBSchema;
	}

	/**
	 * @return the runSmells
	 */
	public boolean isRunSmells() {
		return runSmells;
	}

	/**
	 * @param runSmells the runSmells to set
	 */
	public void setRunSmells(boolean runSmells) {
		this.runSmells = runSmells;
	}

	/**
	 * @return the smellReportFile
	 */
	public String getSmellReportFile() {
		return smellReportFile;
	}

	/**
	 * @param smellReportFile the smellReportFile to set
	 */
	public void setSmellReportFile(String smellReportFile) {
		this.smellReportFile = smellReportFile;
	}

	/**
	 * @return the smellReportFormat
	 */
	public String getSmellReportFormat() {
		return smellReportFormat;
	}

	/**
	 * @param smellReportFormat the smellReportFormat to set
	 */
	public void setSmellReportFormat(String smellReportFormat) {
		this.smellReportFormat = smellReportFormat;
	}

	/**
	 * @return the jsonExport
	 */
	public String getJsonExport() {
		return jsonExport;
	}

	/**
	 * @param jsonExport the jsonExport to set
	 */
	public void setJsonExport(String jsonExport) {
		this.jsonExport = jsonExport;
	}

	/**
	 * @return the dumpSymTab
	 */
	public String getDumpSymTab() {
		return dumpSymTab;
	}

	/**
	 * @param dumpSymTab the dumpSymTab to set
	 */
	public void setDumpSymTab(String dumpSymTab) {
		this.dumpSymTab = dumpSymTab;
	}

	/**
	 * @return the runTAA
	 */
	public boolean isRunTAA() {
		return runTAA;
	}

	/**
	 * @param runSmells the runSmells to set
	 */
	public void setRunTAA(boolean runTAA) {
		this.runTAA = runTAA;
	}

	/**
	 * @return the smellReportFile
	 */
	public String getTAAReportFile() {
		return taaReportFile;
	}

	/**
	 * @param smellReportFile the smellReportFile to set
	 */
	public void setTAAReportFile(String taaReportFile) {
		this.taaReportFile = taaReportFile;
	}

	/**
	 * @return the smellReportFormat
	 */
	public String getTAAReportFormat() {
		return taaReportFormat;
	}

	/**
	 * @param smellReportFormat the smellReportFormat to set
	 */
	public void setTAAReportFormat(String taaReportFormat) {
		this.taaReportFormat = taaReportFormat;
	}

	/**
	 * @return the runSQLMetrics
	 */
	public boolean isRunSQLMetrics() {
		return runSQLMetrics;
	}

	/**
	 * @param runSQLMetrics the runSQLMetrics to set
	 */
	public void setRunSQLMetrics(boolean runSQLMetrics) {
		this.runSQLMetrics = runSQLMetrics;
	}

	/**
	 * @return the sqlMetricsReportFile
	 */
	public String getSQLMetricsReportFile() {
		return sqlMetricsReportFile;
	}

	/**
	 * @param sqlMetricsReportFile the sqlMetricsReportFile to set
	 */
	public void setSQLMetricsReportFile(String sqlMetricsReportFile) {
		this.sqlMetricsReportFile = sqlMetricsReportFile;
	}

	/**
	 * @return the sqlMetricsReportFormat
	 */
	public String getSQLMetricsReportFormat() {
		return sqlMetricsReportFormat;
	}

	/**
	 * @param sqlMetricsReportFormat the sqlMetricsReportFormat to set
	 */
	public void setSQLMetricsReportFormat(String sqlMetricsReportFormat) {
		this.sqlMetricsReportFormat = sqlMetricsReportFormat;
	}

	/**
	 * @return the dumpErrors
	 */
	public String getDumpErrors() {
		return dumpErrors;
	}

	/**
	 * @param dumpErrors the dumpErrors to set
	 */
	public void setDumpErrors(String dumpErrors) {
		this.dumpErrors = dumpErrors;
	}

	/**
	 * @return the errorsFormat
	 */
	public String getErrorsFormat() {
		return errorsFormat;
	}

	/**
	 * @param errorsFormat the errorsFormat to set
	 */
	public void setErrorsFormat(String errorsFormat) {
		this.errorsFormat = errorsFormat;
	}

}
