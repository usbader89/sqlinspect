package sqlinspect.sqlan;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BenchMark {
	private static final Logger logger = LoggerFactory.getLogger(BenchMark.class);

	private static final long MEGABYTE = 1024L * 1024L;

	private final Runtime runtime = Runtime.getRuntime();

	private final Map<String, Long> startMemoryMap = new HashMap<>();
	private final Map<String, Long> startTimeMap = new HashMap<>();

	private final Map<String, Stat> memoryStats = new HashMap<>();
	private final Map<String, Stat> timeStats = new HashMap<>();

	private static final String CHARACTERSET = "UTF-8";

	private static final String NL = "\n";

	private static class Stat {
		private final long startVal;
		private final long stopVal;

		public Stat(long startVal, long stopVal) {
			this.startVal = startVal;
			this.stopVal = stopVal;
		}

		public long getStartVal() {
			return startVal;
		}

		public long getStopVal() {
			return stopVal;
		}

	}

	public static long bytesToMegabytes(long bytes) {
		return bytes / MEGABYTE;
	}

	public void startRuntime(String par) {
		startTimeMap.put(par, System.currentTimeMillis());
	}

	public void stopRuntime(String par) {
		Long startTime = startTimeMap.get(par);
		if (startTime != null) {
			long stopTime = System.currentTimeMillis();
			timeStats.put(par, new Stat(startTime, stopTime));
			if (logger.isDebugEnabled()) {
				logger.debug("{} took {} ms", par, stopTime - startTime);
			}
		} else {
			throw new IllegalArgumentException("No startRuntime time was called for " + par);
		}
	}

	public void startMemory(String par) {
		runtime.gc();
		startMemoryMap.put(par, runtime.totalMemory() - runtime.freeMemory());
	}

	public void stopMemory(String par) {
		Long startMemory = startMemoryMap.get(par);
		if (startMemory != null) {
			runtime.gc();
			Long stopMemory = runtime.totalMemory() - runtime.freeMemory();
			memoryStats.put(par, new Stat(startMemory, stopMemory));
			if (logger.isDebugEnabled()) {
				logger.debug("{} used {} MB memory", par, bytesToMegabytes(stopMemory - startMemory));
			}
		} else {
			throw new IllegalArgumentException("No startMemory was called for " + par);
		}
	}

	public void startAll(String par) {
		startMemory(par);
		startRuntime(par);
	}

	public void stopAll(String par) {
		stopRuntime(par);
		stopMemory(par);
	}

	public void dumpFile(File f) {

		try (OutputStreamWriter ow = new OutputStreamWriter(Files.newOutputStream(f.toPath()), CHARACTERSET);
				BufferedWriter bw = new BufferedWriter(ow);) {
			bw.write("Kind,Key,Start,Stop,Diff" + NL);
			for (Map.Entry<String, Stat> e : timeStats.entrySet()) {
				bw.write("Time," + e.getKey() + "," + e.getValue().getStartVal() + "," + e.getValue().getStopVal() + ","
						+ (e.getValue().getStopVal() - e.getValue().getStartVal()) + NL);
			}
			for (Map.Entry<String, Stat> e : memoryStats.entrySet()) {
				bw.write("Memory," + e.getKey() + "," + e.getValue().getStartVal() + "," + e.getValue().getStopVal()
						+ "," + (e.getValue().getStopVal() - e.getValue().getStartVal()) + NL);
			}
		} catch (IOException e) {
			logger.debug("IO exception", e);
		}
	}
}
