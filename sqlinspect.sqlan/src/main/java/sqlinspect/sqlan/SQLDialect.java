package sqlinspect.sqlan;

public enum SQLDialect {
	MYSQL("MySQL"), IMPALA("Apache Impala"), SQLITE("SQLite");

	private final String text;

	SQLDialect(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return text;
	}

	public static SQLDialect getSQLDialect(String value) {
		for (SQLDialect d : values()) {
			if (d.toString().equalsIgnoreCase(value)) {
				return d;
			}
		}
		throw new IllegalArgumentException();
	}
}
