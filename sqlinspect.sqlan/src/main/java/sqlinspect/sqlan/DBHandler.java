package sqlinspect.sqlan;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Optional;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DBHandler {
	private static final Logger logger = LoggerFactory.getLogger(DBHandler.class);

	private final String dburl;
	private final String username;
	private final String password;

	private Optional<Connection> conn = Optional.empty();

	public DBHandler(String dburl, String username, String password) {
		this.dburl = dburl;
		this.username = username;
		this.password = password;
	}

	public void connect(boolean readonly) {

		Properties connectionProps = new Properties();
		connectionProps.put("user", username);
		connectionProps.put("password", password);

		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = Optional.of(DriverManager.getConnection(dburl, connectionProps));
		} catch (SQLException e) {
			logger.error("Could not connect to database.", e);
		} catch (ClassNotFoundException e) {
			logger.error("Could not load database driver.", e);
		}

		if (!conn.isPresent()) {
			logger.error("Could not open connection to database!");
			return;
		}

		if (readonly) {
			try {
				conn.get().setReadOnly(true);
			} catch (SQLException e) {
				logger.error("Could not set readonly mode.", e);
			}
		}

		logger.debug("Connected to database: {}", dburl);
	}

	public void connect() {
		connect(true);
	}

	public void close() {
		try {
			if (conn.isPresent()) {
				conn.get().close();
				conn = Optional.empty();
			}
			logger.debug("Closed DB connection.");
		} catch (SQLException e) {
			logger.error("Could not close connection.", e);
		}
	}

	// TODO: it works for MySQL, we should have a specific DBHandler for
	// different databases
	public String getDBSchema() {
		if (!conn.isPresent()) {
			logger.error("Not yet connected to the database.");
			return "";
		}

		StringBuilder sb = new StringBuilder();

		try (Statement stmt = conn.get().createStatement();) {
			try (ResultSet r = stmt
					.executeQuery("SELECT table_name FROM information_schema.tables WHERE table_schema=database();");) {
				while (r.next()) {
					sb.append(getTableDescription(r.getString(1)));
				}
			}
		} catch (SQLException e) {
			logger.error("Error sending the statement to the database", e);
		}

		return sb.toString();
	}

	public String getTableDescription(String table) {
		if (!conn.isPresent()) {
			logger.error("Not yet connected to the database.");
			return "";
		}

		StringBuilder sb = new StringBuilder();
		try (Statement stmt = conn.get().createStatement();) {
			try (ResultSet r = stmt.executeQuery("SHOW CREATE TABLE " + table)) {
				// result 1: table name
				// result 2: sql code
				while (r.next()) {
					sb.append(r.getString(2));
					sb.append(';');
				}
			}
		} catch (SQLException e) {
			logger.error("Erorr sending the statement to the database", e);
		}
		return sb.toString();
	}

	public int getNullableRecords(String table, String column) {
		if (!conn.isPresent()) {
			logger.error("Not yet connected to the database.");
			return -1;
		}

		PreparedStatement pstmt = null;
		ResultSet r = null;
		try {
			pstmt = conn.get().prepareStatement("SELECT COUNT(*) FROM ? WHERE ? IS NULL;");
			pstmt.setString(1, table);
			pstmt.setString(2, column);
			r = pstmt.executeQuery();
			if (!r.next()) {
				logger.warn("No result for count query.");
				return -1;
			}
			return r.getInt(1);
		} catch (SQLException e) {
			logger.error("Erorr sending the statement to the database", e);
		} finally {
			if (r != null) {
				try {
					r.close();
				} catch (SQLException e) {
					logger.error("Could not close resultset", e);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
					logger.error("Could not close statement", e);
				}
			}
		}

		return -1;
	}

	public void runScript(String path, String charset) {
		if (!conn.isPresent()) {
			logger.error("Not yet connected to the database.");
			return;
		}

		try (InputStreamReader ir = new InputStreamReader(Files.newInputStream(new File(path).toPath()), charset);
				BufferedReader br = new BufferedReader(ir);) {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();
			while (line != null) {
				if (line.length() > 0 && line.charAt(0) == '-' || line.length() == 0) {
					continue;
				}
				sb.append(' ');
				sb.append(line);
				if (sb.charAt(sb.length() - 1) == ';') {
					try (Statement stmt = conn.get().createStatement();) {
						stmt.execute(sb.toString());
					} catch (SQLException e) {
						logger.error("Error sending query to the database: ", e);
					}
					sb.setLength(0);
				}
				line = br.readLine();
			}
		} catch (IOException e) {
			logger.error("Error running the script: ", e);
		}
	}
}
