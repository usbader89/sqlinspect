package {{ mypackage }};

{% for i in imports %}
import {{ i }};
{% endfor %}

public class Visitor extends AbstractVisitor {
  public Visitor() {
    super();
  }
  
  {% for cl in classes if not cl.abstract %}
  @Override
  public void visit({{ cl | getClassFullName }} node) { }
  
  @Override
  public void visitEnd({{cl | getClassFullName }} node) { }
  {% endfor %}

  {% for cl in classes %}
  {% for e in cl.edges if e is treeEdge %}
  @Override
  public void visitEdge_{{ e | getEdgeVisitMethodName }}({{ cl.name }} node, {{ e.type | getType }} end) { }
  
  @Override
  public void visitEdgeEnd_{{ e | getEdgeVisitMethodName }}({{ cl.name }} node, {{ e.type | getType }} end) { }
  {% endfor %}
  {% endfor %}
}
