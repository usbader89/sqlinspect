package {{ mypackage }};

import sqlinspect.sqlan.asg.visitor.*;
import sqlinspect.sqlan.asg.base.Base;

public interface ITraversal {
  
  public void traverse(Base node);
}
