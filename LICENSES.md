Additional license information:

  * 'Database icon' icon set from http://www.fasticon.com under Linkware license
  * Open Iconic - www.useiconic.com/open under MIT and Open Font Licence
  * https://www.iconsdb.com/icon-sets/web-2-blue-icons/
