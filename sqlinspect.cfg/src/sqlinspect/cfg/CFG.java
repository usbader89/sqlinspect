package sqlinspect.cfg;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class CFG {
	private static final Logger logger = LoggerFactory.getLogger(CFG.class);

	private final BasicBlock entry;
	private final BasicBlock exit;
	private final MethodDeclaration method;

	private final List<BasicBlock> basicblocks = new ArrayList<>();

	public CFG(MethodDeclaration method) {
		this.method = method;
		entry = createBasicBlock();
		exit = createBasicBlock();
	}

	public BasicBlock createBasicBlock() {
		int i = basicblocks.size();
		BasicBlock ret = new BasicBlock(i);
		basicblocks.add(ret);
		return ret;
	}

	public BasicBlock getEntry() {
		return entry;
	}

	public BasicBlock getExit() {
		return exit;
	}

	public MethodDeclaration getMethod() {
		return method;
	}

	public List<BasicBlock> basicblocks() {
		return basicblocks;
	}

	public void dump() {
		if (logger.isDebugEnabled()) {
			logger.debug("CFG of method: {}", method.resolveBinding());
			logger.debug("Entry: {}", entry.getId());
			logger.debug("Exit: {}", exit.getId());
			logger.debug("Basic blocks:");
			basicblocks.stream().forEach(BasicBlock::dump);
		}
	}

	public void dump(PrintWriter pw) {
		pw.format("CFG of method: %s%n", method.resolveBinding());
		pw.format("Entry: %s%n", entry.getId());
		pw.format("Exit: %s%n", exit.getId());
		pw.println("Basicblocks:");
		basicblocks.stream().sorted((bb1, bb2) -> Integer.compare(bb1.getId(), bb2.getId())).forEach(bb -> bb.dump(pw));
	}
}
