package sqlinspect.cfg;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.MethodDeclaration;

public class MethodDeclVisitor extends ASTVisitor {

	private final Set<MethodDeclaration> methods = new HashSet<>();

	@Override
	public boolean visit(MethodDeclaration node) {
		methods.add(node);
		return false;
	}

	public Set<MethodDeclaration> run(CompilationUnit unit) {
		methods.clear();
		unit.accept(this);
		return methods;
	}
}
