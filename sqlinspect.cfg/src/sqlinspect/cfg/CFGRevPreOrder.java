package sqlinspect.cfg;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.Map;

public abstract class CFGRevPreOrder {

	private static class PreOrderElement {
		private final BasicBlock element;
		private final int depth;

		public PreOrderElement(BasicBlock element, int depth) {
			this.element = element;
			this.depth = depth;
		}

		public int getDepth() {
			return depth;
		}

		public BasicBlock getElement() {
			return element;
		}
	}

	public void run(BasicBlock startBB, int maxDepth) {
		Deque<PreOrderElement> toVisit = new ArrayDeque<>();
		Map<BasicBlock, Integer> visited = new HashMap<>();

		toVisit.add(new PreOrderElement(startBB, 0));
		toVisit.add(new PreOrderElement(startBB, 0));
		while (!toVisit.isEmpty()) {
			PreOrderElement actualElement = toVisit.pop();
			BasicBlock actualBB = actualElement.getElement();
			Integer count = visited.computeIfAbsent(actualBB, k -> Integer.valueOf(0));
			if (count % 2 == 0) {
				// visit
				visited.put(actualBB, count + 1);

				if (visit(actualBB) && (actualElement.getDepth() < maxDepth || maxDepth == 0)) {
					actualBB.prevs().stream().filter(bb -> visited.getOrDefault(bb, 0) % 2 == 0).forEach(bb -> {
						toVisit.addFirst(new PreOrderElement(bb, actualElement.getDepth() + 1));
						toVisit.addFirst(new PreOrderElement(bb, actualElement.getDepth() + 1));
					});
				}
			} else {
				// visitEnd
				visitEnd(actualBB);
				visited.put(actualBB, count + 1);
			}
		}
	}

	protected abstract boolean visit(BasicBlock bb);

	protected abstract void visitEnd(BasicBlock bb);

}
