# SQLInspect #

SQLInspect is a static SQL analyzer with plug-in support for Eclipse to inspect database usage in Java applications. It statically extracts SQL queries embedded in Java and performs various analyses on them. It aims to provide support for developers to accomplish SQL-related maintenance tasks in the code, where it meets data.

## Goals ##

The primary aim is to help developers dealing with SQL code embedded in Java. SQLInspect integrates into Eclipse as a plug-in that can extract SQL queries from Java code through static string analysis. It parses the extracted queries and performs various analyses on them. As a result, one can easily explore the source code which accesses a given part of the database, or which is responsible for the construction of a given SQL query. SQL-related metrics and common coding mistakes are also used to spot out inefficiently or defectively performing SQL statements and to identify poorly designed classes. For instance, those that construct many queries through complex control-flow paths. SQLInspect is a novel tool relying on recent query extraction approaches. It currently supports Java applications working with JDBC and SQL code.

The purpose was to combine _state-of-the-art_ software analysis techniques in SQLInspect and provide a compelling, working solution for developers to assess the Java code using embedded SQL. This is helpful for data-centric applications where higher level APIs are avoided in favor of efficiency or because of limited resources, e.g., distributed database applications or Android development.

For this purpose,

 * SQLInspect implements a _static control-flow based SQL extraction_ technique, that is configurable and able to deal with Java applications having hundreds of thousands of lines of code.
 * An _internal parser_ then analyzes the queries extracted which can _tolerate unresolved code fragments_, i.e., parts of string operations that cannot be statically resolved due to the dynamicity problem.
 * It _resolves identifiers of database objects_ to show the slice of the source code working with given schema objects.
 * SQL quality _metrics_ are provided for database accesses so one can assess methods and classes that pester the database too much or complex queries which deal with many schema elements.
 * SQL _bad smells_ are also detected, e.g., common coding mistakes within the extracted queries.
 * A _tree-matching algorithm_ enables the developer to perform a configurable _search for a given query_ within the entire source code. In case a query shows up in an error report, one can search for its location in the source code. This search can be configured so that some parts of the query are ignored, e.g., literal values or identifier names. In the following subsections, we elaborate on the main use cases.

## Getting Started ##

### Install requirements ###

  * At least Java 1.8 with JavaFX is required
  * Eclipse Oxygen or newer
  * E(fx)clipse (can be installed from Eclipse Marketplace)

### Install ###

  * Go to the *Help/Install New Software* menu of Eclipse
  * *Add* a new site:
    - Name: SQLInspect
    - Location: select `sqlinspect-x.y.z.zip` as an `Archive`
  * SQLInspect plug-ins will appear in the list, select them and follow the next steps of the wizard

### Usage Summary ###

To explore the SQL code embedded in your project:

  * Set configuration parameters for the analysis in the property page (SQLInspect) under your *project settings*
  * Start an analysis of your project in the project menu *SQLInspect>Analyze current project*
  * Open *Window > Show View > SQLInspect* and explore the results in the *Queries/Hotspots/Schema/Metrics views*
  * Look for SQL smells under the *Problems view*
  * Have fun! ;-)

## Analyses ##

### Query Extraction ###

SQLInspect implements a query extraction technique inspired by Meurice et al. [3]. It has two main steps: (1) the identification of _hotspots_ where SQL statements are sent to the database, followed by (2) the extraction of SQL strings sent to the database through the hotspots.

#### Hotspot Identification ####

Hotspot identification is implemented for _JDBC_ by the `JDBCHotspotFinder` and for _Android_ by the `AndroidHotspotFinder`.

It looks for the following hotspots in an application using _JDBC_:

  * _Statement execute_ signatures: where a query is sent to the database directly through a string parameter of the call (e.g. `Statement.execute()`).
  * _PreparedStatement execute_ signatures: where the query is prepared initially as a `PreparedStatement` object, and then the `execute()` call sends the query to the DB (e.g. `PreparedStatement.execute()`). For these hotspots, JDBCHotspotFinder walks through the backward slice and collects the definitions of the prepared statements looking for method calls defined as _Prepare statement_ signature, e.g. `Connection.prepareStatement(java.lang.String)`.
  
It looks for the following hotspots in an application using _Android_:

  * _SQLiteDatabase.execSQL_ signatures: where a query is sent to the database directly through a string parameter of the call (e.g. `SQLiteDatabase.execSQL()`).
  * _SQLiteStatement.execute_ signatures: the execution of prepared statements (e.g. `SQLiteStatement.execute()`). The query extractor walks through the backward slice and collects the definitions of the prepared statements looking for method calls defined as _SQLiteDatabase.compileStatement_ signatures, e.g. `SQLiteDatabase.compileStatement()`.
  * _SQLiteStatement.rawQuery_ signatures: cursor calls, they are handled the same way as execSQL calls, e.g. `android.database.sqlite.SQLiteDatabase.rawQuery()`. 

Project-specific method signatures for hotspots can be defined under properties of a project. For example, `java.sql.Statement.executeQuery(java.lang.String)/1` describes a method call for a statement execution, where the first parameter will be extracted later by the SQL String resolution.

#### SQL String Resolution ####

The SQL String resolution is the process where the `QueryResolver` resolves the possible String values of the String parameters of the hotspots identified before.
The resolver tries to find out the string values of a string expression by resolving each variable involved in it.

The actual `QueryResolver` implementation is the `InterQueryResolver` an inter-procedural implementation of Meurice et al. [1]. It walks through the backward slice of a hotspot but supports control-flow branches. Since branches in the control flow, can exponentially increase the number of possible paths reaching a hotspot, thresholds can be specified under the project's property page. If it reaches one of these thresholds for a subexpression, that part of the query will remain unresolved as an `UnresolvedQueryPart`.

The configuration parameters are the followings:
  * _Maximum Nesting Level_: maximum nesting level of conditional branches,
  * _Maximum Def-use Level_: maximum redefinitions of a variable in the query slice,
  * _Maximum Call Level_: maximum number of calls in the call chain of a slice.

### SQL Parser ###

SQLInspect has its internal SQL parser(s). The currently supported SQL dialects are:

  * MySQL 5.7
  * Apache Impala 2.7.0
  * SQLite 3.24

After parsing, a name resolver will determine the references of identifiers in the SQL statements. This resolution is precise only if the schema is provided (see the properties of the project).

The AST of the parsed SQL statements can be exported to `.json` for further analyses.

#### Unrecognized Code Fragments ####

In many cases, it can happen that a specific part of the SQL string cannot be resolved. This part of the SQL is represented by `{{na}}` in the query string. The parser builds a special node, called `Joker` node in the AST for these. Similarly, `?` markers in prepared statements are represented by `{{question}}` in the query string and `Joker` nodes in the AST.

### Table/Column Access Analysis ###

The _Table/Column Access Analysis_ determines which tables/columns are accessed from the queries. It is also provided whether the column is explicitly written in the query (e.g., for `select * from t where x>10` x is explicitly accessed while all the other columns are implicitly accessed).

The results of this analysis can be observed from the _Schema view_ or can be exported to `.xml`.

## Query Search ##

The execution point of a concrete query can be searched from the _Search_ menu of Eclipse.

SQLInspect searches the AST for queries with similar structures even if they contain _Unrecognized Code Fragments_. If a SQL appears in a dynamic trace or server log with concrete, literal values (e.g., `select x+15 from t where i>10`), it can be easily located in the source code. Even if it was constructed through a prepared statement (e.g., `select x+? from t where i>?`). Besides, the query search can be configured to ignore identifier names or any literal value.

## Metrics ##

### Java-SQL Metrics ###

For a JavaElement, we calculate these metrics by summing up from lower elements at lower levels to their parents. The lowest level is a CompilationUnit, i.e., a method.

  * *NumberOfHotspots*: The total number of hotspots in the JavaElement.
  * *NumberOfQueries*: The total number of queries in the JavaElement.
  * *NumberOfColumnAccesses*: The total number of (unique) columns accessed through the JavaElement. When aggregated to the parents, the union of the accessed column sets will be calculated.
  * *NumberOfTableAccesses*: The total number of (unique) tables accessed through the JavaElement. When aggregated to the parents, the union of the accessed column sets will be calculated.

### SQL Metrics ###

For each SQL query the following metrics are calculated:

  * *NumberOfResultFields*: The number of returned fields by the query
  * *NumberOfTables*: The number of tables used in the query
  * *NumberOfQueries*: The number of subqueries
  * *NestingLevel*: The nesting level of subqueries

## SQL Smells ##

The following smells [1,2] were inspired by the *SQL Antipattern* catalog of *Bill Karwin* [4]. As he says, _"SQL Antipatterns describes the most frequently made missteps I've seen people naively make while using SQL."_ The book categorizes antipatterns into Logical Database Design, Physical Database Design, Query, and Application Development categories. Our purpose was to support working with queries embedded in Java code. Hence, we implemented a prototype tool for the identification of Query antipatterns. We refer to these as *SQL code smells* since they are specific to SQL queries and indicate 'smelly' code, i.e., there might be a bug or an issue nearby.

### Fear of the Unknown ###

#### Problem ####

The reason behind this antipattern is the special meaning of `NULL` in relational databases that can easily confuse developers. `NULL` is a special marker to indicate that data does not exist in the database, i.e., it is `unknown`. `NULL` is not the same as zero. A number ten greater than an `unknown` is still `unknown`.

`NULL` is not the same as false, either. Hence, a boolean expression with `AND`, `OR`, and `NOT` can easily produce a result that someone may find confusing. Also, `NULL` is not the same as a string with zero length.

A string combined with `NULL` returns `NULL` in standard SQL, but to make things more complicated, different RDBMSs handle it differently, e.g., in Oracle and Sybase the concatenation of `NULL` to a string will result in the original string.

```sql
SELECT * FROM rentals WHERE customer_id = 456;
-- will not return records where customer_id is null
SELECT * FROM rentals WHERE NOT (customer_id = 456);
```

Both the first and the second queries are syntactically and semantically correct. It is tempting to assume that the result of the second query is the complement of the first query. However, this is not always the case.
For records in the table with a `NULL` value for `customer_id`, the expression in the `WHERE` clause evaluates to `unknown,' which do not satisfy the search criteria, so these records will not show up in the output.

```sql
-- will return NULL if age is NULL
SELECT age + 10 FROM customers;
-- will return NULL if e.g. middle_initial is NULL
SELECT first_name || ' ' || middle_initial || ' '
       || last_name AS full_name FROM customers;

-- should use IS NULL or IS NOT NULL
SELECT * FROM rentals WHERE customer_id = NULL;
SELECT * FROM rentals WHERE customer_id <> NULL;
```

None of the last two queries of the example above will return records where `customer_id` is `NULL`. The proper way to test for `NULL` is to use the `IS NULL` operator in SQL. Both queries are syntactically and semantically correct and will run without errors, however.

#### Solution ####

  * Use the `NOT NULL` constraint on columns where you know that `NULL` values are not allowed.
  * Use the `IS NULL` operator for `NULL` tests.
  * Be careful with expressions where NULL are likely to be involved. Particularly with aggregates like `COUNT` or `AVG`.

### Ambiguous Groups ###

#### Problem ####

A possible source of errors is the handling of columns in the `SELECT` list of a query with a `GROUP BY` clause. The SQL-92 standard is straightforward:
> it does not permit queries for which the select list, HAVING condition, or ORDER BY list refer to nonaggregated columns that are not named in the GROUP BY clause.

Therefore, the reference to the `address` column in the example makes the query invalid according to the standard. Later standards, however, such as SQL-99
> permits such non-aggregates [...] if they are functionally dependent on GROUP BY columns.

So, if `address` functionally depends on `name`, the query is acceptable. This relaxation of the rule leads to various implementations in RDBMSs. For instance, Oracle follows the strict [standard](https://docs.oracle.com/cd/B19306_01/server.102/b14200/statements_10002.htm#i2182483). While the default behavior of [MySQL](https://dev.mysql.com/doc/refman/5.7/en/group-by-handling.html) is that the
 > use of GROUP BY permits the select list, HAVING condition, or ORDER BY list to refer to nonaggregated columns even if the columns are not functionally dependent on GROUP BY columns.

 Moreover,
 > the server is free to choose any value from each group, so unless they are the same, the values chosen are indeterminate, which is probably not what you want.

 [SQLite](https://sqlite.org/lang_select.html), as another popular RDBMS implements a similar behavior:
> if the expression is an aggregate expression, it is evaluated across all rows in the group. Otherwise, it is evaluated against a single *arbitrarily chosen* row from within the group.

Needless to say that this is a reason for queries which sometimes work as the developer wants it, but sometimes may also result in unexpected behavior.

```sql
-- the reference to 'address' causes unpredictable
  -- behavior in MySQL and SQLite
  SELECT name, address, AVG(age) FROM customers
    GROUP BY name;
```

#### Solution ####

  * Eliminate ambiguous columns from the query.

### Random Selection

#### Problem ####

The problem with the example below that it performs a full table scan and an expensive sort operation, which sounds unnecessary for the selection of a single record. The possibilities in SQL are limited, and even popular [StackOverflow posts](https://stackoverflow.com/questions/580639/how-to-randomly-select-rows-in-sql) come to the same solution.

```sql
SELECT * FROM customers ORDER BY RAND() LIMIT 1;
```

#### Solution ####
  * Some database server offer ready solutions, e.g., `TABLESAMPLE` in SQL Server, or `SAMPLE` in Oracle.

### Implicit Columns ###

#### Problem ####

```sql
SELECT * FROM customers c JOIN rentals r
    ON c.customer_id = r.customer_id;

INSERT INTO customers VALUES
    (DEFAULT, 'Brown', 'Peter', NULL, NULL);
```

#### Solution ####

  * Name columns explicitly.
  
## Android Smells ##

### SQLiteDatabase.execSQL() returns data ###

#### Problem ####

As the [Developer Guide](https://developer.android.com/reference/android/database/sqlite/SQLiteDatabase.html#execSQL(java.lang.String)) says, the purpose of execSQL() is to
>execute a single SQL statement that is NOT a SELECT or any other SQL statement that returns data.

#### Solution ####

  * Statements such as `SELECT`, `INSERT`, `UPDATE`, `DELETE` should be  sent to the database through other, specific API calls (e.g., `SQLiteDatabase.insert()` for an `INSERT`).
  
### More than one statement in SQLiteDatabase.execSQL(), SQLiteDatabase.rawQuery() or SQLiteStatement.execute() ###

#### Problem ####

The purpose of these statements is to send a single SQL statement to the database, and not more than one. What is worse, in case of multiple statements the database will recognize only the first one and will not report any error.

#### Solution ####

  * Use multiple execute calls for more than statements.

### SQLiteStatement.executeInsert() should send an INSERT statement ###

#### Problem ####

`executeInsert()` returns the ID of the row inserted. Although there is no checking of the statement sent to the database, as the [Developer Guide](https://developer.android.com/reference/android/database/sqlite/SQLiteStatement.html#executeInsert()) says,
> the SQL statement should be an INSERT for this to be a useful call.

#### Solution ####

  * Statements such as `SELECT`, `UPDATE`, `DELETE` should be sent to the database through other, specific API calls (e.g., `SQLiteDatabase.rawQuery()`, `SQLiteDatabase.update()`).
  
### More than one column in an SQLiteStatement execution ###

#### Problem ####

A statement executed through the `SQLiteStatement` API
> cannot return multiple rows or columns, but single value (1 x 1) result sets are supported.

#### Solution ####

  * Use for example cursors, e.g. `SQLiteDatabase.rawQuery()`, `SQLiteDatabase.query()` to query multiple rows or columns from the database.

## Building ##

### Compilation requirements ###

  * Apache Maven 3.3.9 (or newer)
  * Java 1.8.x
  * Python 3.x (in the PATH)
     - PyYaml (`pip install pyyaml`)
     - Jinja2 (`pip install Jinja2`)
  * Android SDK 26 (or newer, OPTIONAL, only for unit tests)

### How to compile ###

  * Compile without tests: `mvn clean package`
  * Compile with tests:
    - the pom.xml of sqlinspect.plugin.test looks for `${env.ANDROID_SDK_ROOT}/platforms/android-26/android.jar`, so make sure that `ANDROID_SDK_ROOT` is set and/or update the pom.xml to point to `android.jar`
    - run `mvn clean package`
    - run `mvn test`
  * If you get `[ERROR] Failed to execute goal org.apache.maven.plugins:maven-dependency-plugin:3.0.0:copy-dependencies (copy-dependencies) on project sqlinspect.sqlan` -> rerun the compilation without clean
    
### Notes for the maven build ###

  * To generate ANTLR sources:
    - `mvn generate-sources`
  * To generate javadoc:
    - `mvn javadoc:javadoc`
  * To generate project documentation, including reports:
    - `mvn site`
  * Runing a single test:
    - `mvn -Dtest=FearOfTheUnknown test`
  * Package without running tests:
    - `mvn package -DskipTests`
  * Run integration tests:
    - `mvn integration-test`
  * Switch to mvn offline mode and skip "Fetching p2.index"
    - `mvn -o`
  * Update version:
    - `mvn org.eclipse.tycho:tycho-versions-plugin:set-version -DnewVersion=0.3.0-SNAPSHOT`


### Development in Eclipse ###

  * Import the project as a Maven build project to Eclipse
  * For the project configuration of the Eclipse project:
    - Add `target/generated-sources/antlr4` to the build path
    - Toggle `ignore optional compiler warnings:yes`
      (Apt M2E Connector Eclipse plugin should solve this, but it is
      not working anymore in Eclipse.)
  * In some cases eclipse does not recognize the generated source directories.
    If it happens, add the directories under `target/generated-sources/` to
    the build path as a source directory.

## Licensing ##

SQLInspect is released under New BSD License. See [LICENSE.txt](https://bitbucket.org/csnagy/sqlinspect/src/master/LICENSE.txt) for details.

Copyright: Csaba Nagy (University of Namur, Belgium)

## References ##

  1. Csaba Nagy and Anthony Cleve. "SQLInspect: A Static Analyzer to Inspect Database Usage in Java Applications" In Proceedings of the 40th International Conference on Software Engineering (ICSE 2018), May 27 - 3 June 2018, Gothenburg, Sweden
  2. Csaba Nagy and Anthony Cleve. "Static Code Smell Detection in SQL Queries Embedded in Java Code", In Proceedings of the 17th IEEE International Working Conference on Source Code Analysis and Manipulation (SCAM 2017), September 17-18, 2017 - Shanghai, China
  3. Loup Meurice, Csaba Nagy, and Anthony Cleve. "Static Analysis of Dynamic Database Usage in Java Systems", In Proceedings of the 28th International Conference on Advanced Information Systems Engineering (CAiSE 2016), 13-17 June 2016, Ljubljana, Slovenia
  4. Bill Karwin. "SQL Antipatterns: Avoiding the Pitfalls of Database Programming", The Pragmatic Bookshelf, 2010
