package sqlinspect.sqlan.test.smells;

import static org.junit.Assert.assertEquals;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.junit.Before;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import sqlinspect.sqlan.Config;
import sqlinspect.sqlan.SQLAn;
import sqlinspect.sqlan.SQLDialect;
import sqlinspect.sqlan.smells.common.SQLSmell;
import sqlinspect.sqlan.smells.common.SQLSmellCertKind;
import sqlinspect.sqlan.smells.common.SQLSmellKind;
import sqlinspect.sqlan.test.AbstractSQLAnTest;

public abstract class BadSmellTest extends AbstractSQLAnTest {

	private static final String ROOT_NODE = "Smells";
	private static final String SMELL_NODE = "Smell";
	private static final String SMELL_NODE_KIND = "Kind";
	private static final String SMELL_NODE_FILE = "File";
	private static final String SMELL_NODE_LINE = "Line";
	private static final String SMELL_NODE_CERTAINTY = "Certainty";
	private static final String SMELL_NODE_MESSAGE = "Message";

	protected static final String charset = "UTF-8";

	protected final Config conf = new Config();
	protected SQLAn sqlan;
	protected String input_dir;
	protected String output_dir;
	protected String ref_dir;

	protected SQLDialect dialect;

	protected BadSmellTest(SQLDialect dialect, String input_dir, String output_dir, String ref_dir) {
		this.dialect = dialect;
		this.input_dir = input_dir;
		this.output_dir = output_dir;
		this.ref_dir = ref_dir;
	}

	protected void createOutputDir() {
		File dir = new File(output_dir);
		dir.mkdir();
	}

	@Before
	public void setUp() {
		sqlan = new SQLAn();

		conf.setParseDBSchema(false);
		conf.setRunSmells(true);
		conf.setDialect(dialect);

		sqlan.setConf(conf);

		createOutputDir();
	}

	protected void testSmells(String testname) {
		String[] inputs = { input_dir + '/' + testname + ".sql" };
		String output = output_dir + '/' + testname + "-smells.xml";
		String reference = ref_dir + '/' + testname + "-smells.xml";
		String jsonOutput = output_dir + '/' + testname + ".json";

		conf.setJsonExport(jsonOutput);
		conf.setSmellReportFile(output);
		conf.setSmellReportFormat("xml");
		sqlan.setConf(conf);

		sqlan.analyze(inputs);

		Set<SQLSmell> refSmells = loadXMLSmells(reference);
		Set<SQLSmell> outputSmells = loadXMLSmells(output);

		assertEquals(refSmells, outputSmells);
	}

	protected Set<SQLSmell> loadXMLSmells(String path) {
		try {
			File f = new File(path);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(f);
			NodeList rootlist = doc.getElementsByTagName(ROOT_NODE);
			Set<SQLSmell> smells = new HashSet<SQLSmell>();

			for (int i = 0; i < rootlist.getLength(); i++) {
				Node r = rootlist.item(i);
				if (r.getNodeType() == Node.ELEMENT_NODE) {
					Element rootElement = (Element) r;
					NodeList nl = rootElement.getElementsByTagName(SMELL_NODE);
					for (int j = 0; j < nl.getLength(); j++) {
						Node n = nl.item(j);

						if (n.getNodeType() == Node.ELEMENT_NODE) {
							SQLSmell smell = loadXMLSmell((Element) n);
							smells.add(smell);
						}
					}
				}
			}

			return smells;
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private static String getFileNameOnly(String path) {
		return path.substring(path.lastIndexOf(File.separator)+1, path.length());
	}

	private SQLSmell loadXMLSmell(Element e) {
		SQLSmellKind kind = SQLSmellKind.valueOf(e.getElementsByTagName(SMELL_NODE_KIND).item(0).getTextContent());
		String message = e.getElementsByTagName(SMELL_NODE_MESSAGE).item(0).getTextContent();
		SQLSmellCertKind certainty = SQLSmellCertKind
				.valueOf(e.getElementsByTagName(SMELL_NODE_CERTAINTY).item(0).getTextContent());
		String file = getFileNameOnly(e.getElementsByTagName(SMELL_NODE_FILE).item(0).getTextContent());
		int line = Integer.parseInt(e.getElementsByTagName(SMELL_NODE_LINE).item(0).getTextContent());

		SQLSmell s = new SQLSmell(null, kind, message, certainty, file, line);
		return s;
	}

	protected void dumpSmells(Set<SQLSmell> set1, Set<SQLSmell> set2) {
		System.out.println("output: " + set1.size());
		for (SQLSmell s : set1) {
			System.out.println(s.toString());
		}

		System.out.println("ref: " + set2.size());
		for (SQLSmell s : set2) {
			System.out.println(s.toString());
		}

		System.out.println("seteq: " + set2.equals(set1));
		// Iterator<SQLSmell> it1 = set1.iterator();
		// Iterator<SQLSmell> it2 = set2.iterator();
		// SQLSmell s1 = it1.next();
		// SQLSmell s2 = it2.next();
		//
		// System.out.println("smelleq: " + s1.equals(s2));
		// System.out.println("s1-hash: " + s1.hashCode() + " s2-hash: " +
		// s2.hashCode());
	}

}