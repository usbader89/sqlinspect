package sqlinspect.sqlan.test.smells;

import java.io.File;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ch.vorburger.exec.ManagedProcessException;
import ch.vorburger.mariadb4j.DB;
import ch.vorburger.mariadb4j.DBConfigurationBuilder;
import sqlinspect.sqlan.DBHandler;
import sqlinspect.sqlan.SQLDialect;

public class MySQLBadSmellFearOfUnknownTest extends BadSmellTest {

	private static DBConfigurationBuilder dbConfig;
	private static DB db;
	private static String dbName = "sqlantest";

	public MySQLBadSmellFearOfUnknownTest() {
		super(SQLDialect.MYSQL, TEST_INPUT_DIR + "/mysql-smells", "mysql-smells",
				TEST_REF_DIR + "/mysql-smells");
	}

	private static void loadDB() {
		DBHandler dbHandler = new DBHandler(dbConfig.getURL(dbName), "root", "");
		dbHandler.connect(false);
		dbHandler.runScript(TEST_INPUT_DIR + "/mysql-smells/fearoftheunknown-schema.sql", charset);
		dbHandler.close();
	}

	@Override
	@Before
	public void setUp() {
		super.setUp();

		dbConfig = DBConfigurationBuilder.newBuilder();
		new File("target/DB").mkdirs();
		dbConfig.setBaseDir("target/DB");
		dbConfig.setPort(0); // 0 => autom. detect free port
		try {
			db = DB.newEmbeddedDB(dbConfig.build());
			db.start();

			db.createDB(dbName);
		} catch (ManagedProcessException e) {
			e.printStackTrace();
		}

		conf.setDbUrl(dbConfig.getURL(dbName));
		conf.setDbUser("root");
		conf.setDbPassword("");
		conf.setParseDBSchema(true);		
		
		loadDB();
	}

	@After
	public void tearDown() {
		try {
			db.stop();
		} catch (ManagedProcessException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void test1() {
		testSmells("fearoftheunknown");
	}
}
