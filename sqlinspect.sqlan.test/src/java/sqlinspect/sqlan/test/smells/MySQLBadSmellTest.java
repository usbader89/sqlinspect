package sqlinspect.sqlan.test.smells;

import org.junit.Test;

import sqlinspect.sqlan.SQLDialect;

public class MySQLBadSmellTest extends BadSmellTest {

	public MySQLBadSmellTest() {
		super(SQLDialect.MYSQL, TEST_INPUT_DIR + "/mysql-smells", "mysql-smells",
				TEST_REF_DIR + "/mysql-smells");
	}

	@Test
	public void test1() {
		testSmells("fearoftheunknown-nodb");
	}

	@Test
	public void test2() {
		testSmells("ambiguousgroups");
	}

	@Test
	public void test3() {
		testSmells("randomselection");
	}

	@Test
	public void test4() {
		testSmells("implicitcolumns");
	}
}
