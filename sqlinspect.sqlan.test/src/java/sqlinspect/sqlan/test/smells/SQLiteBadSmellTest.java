package sqlinspect.sqlan.test.smells;

import org.junit.Test;

import sqlinspect.sqlan.SQLDialect;

public class SQLiteBadSmellTest extends BadSmellTest {

	public SQLiteBadSmellTest() {
		super(SQLDialect.SQLITE, TEST_INPUT_DIR + "/sqlite-smells", "sqlite-smells",
				TEST_REF_DIR + "/sqlite-smells");
	}

	@Test
	public void test1() {
		testSmells("ambiguousgroups");
	}

	@Test
	public void test2() {
		testSmells("randomselection");
	}

}
