package sqlinspect.sqlan.test.parser;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import sqlinspect.sqlan.SQLDialect;

import org.junit.Test;

public class SQLiteParserTest extends AbstractParserTest {

	public SQLiteParserTest() {
		super(SQLDialect.SQLITE, TEST_INPUT_DIR + "/sqlite/", "sqlite",
				TEST_REF_DIR + "/sqlite/");
	}

	@Test
	public void testAlterTable() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_altertable");
	}

	@Test
	public void testAutoIncrememt() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_autoincrement");
	}

	@Test
	public void testAvg() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_avg");
	}

	@Test
	public void testCase() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_case");
	}

	@Test
	public void testCount() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_count");
	}

	@Test
	public void testCreateView() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_createview");
	}

	@Test
	public void testCrossJoin() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_crossjoin");
	}

	@Test
	public void testDataTypes() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_datatypes");
	}

	@Test
	public void testDelete() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_delete");
	}

	@Test
	public void testDistinct() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_distinct");
	}

	@Test
	public void testDrop() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_drop");
	}

	@Test
	public void testFullOuterJoin() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_fullouterjoin");
	}

	@Test
	public void testGlob() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_glob");
	}

	@Test
	public void testGroupBy() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_groupby");
	}

	@Test
	public void testHaving() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_having");
	}

	@Test
	public void testIn() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_in");
	}

	@Test
	public void testIndex() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_index");
	}

	@Test
	public void testIndexExpression() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_indexexpression");
	}

	@Test
	public void testInnerjoin() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_innerjoin");
	}

	@Test
	public void testInsert() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_insert");
	}

	@Test
	public void testLeftJoin() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_leftjoin");
	}

	@Test
	public void testLike() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_like");
	}

	@Test
	public void testLimit() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_limit");
	}

	@Test
	public void testMax() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_max");
	}

	@Test
	public void testMin() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_min");
	}

	@Test
	public void testOrderBy() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_orderby");
	}

	@Test
	public void testPriKey() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_prikey");
	}

	@Test
	public void testSelect() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_select");
	}

	@Test
	public void testSelfJoin() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_selfjoin");
	}

	@Test
	public void testSubQuery() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_subquery");
	}

	@Test
	public void testSum() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_sum");
	}

	@Test
	public void testTrigger() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_trigger");
	}

	@Test
	public void testUnion() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_union");
	}

	@Test
	public void testUpdate() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_update");
	}

	@Test
	public void testVacuum() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_vacuum");
	}

	@Test
	public void testWhere() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_where");
	}

	@Test
	public void testCreateTable() throws JsonParseException, JsonMappingException, IOException {
		testFile("sqlitetutorial_createtable");
	}

	@Test
	public void testJoins() throws JsonParseException, JsonMappingException, IOException {
		testFile("joins");
	}

}
