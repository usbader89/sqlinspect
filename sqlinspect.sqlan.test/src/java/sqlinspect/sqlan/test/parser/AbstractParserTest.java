package sqlinspect.sqlan.test.parser;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Before;

import sqlinspect.sqlan.asg.base.SQLRoot;
import sqlinspect.sqlan.asg.statm.Statement;
import sqlinspect.sqlan.test.AbstractSQLAnTest;
import sqlinspect.sqlan.Config;
import sqlinspect.sqlan.SQLAn;
import sqlinspect.sqlan.SQLDialect;

public abstract class AbstractParserTest extends AbstractSQLAnTest {
	Config conf = new Config();
	protected SQLAn sqlan;
	protected final String default_output = "test.json";
	protected String input_dir;
	protected String output_dir;
	protected String ref_dir;

	protected SQLDialect dialect;

	protected AbstractParserTest(SQLDialect dialect, String input_dir, String output_dir, String ref_dir) {
		this.dialect = dialect;
		this.input_dir = input_dir;
		this.output_dir = output_dir;
		this.ref_dir = ref_dir;
	}

	protected void createOutputDir() {
		File dir = new File(output_dir);
		dir.mkdir();
	}

	@Before
	public void setUp() {
		sqlan = new SQLAn();

		conf.setParseDBSchema(false);
		conf.setRunSmells(false);
		conf.setDialect(dialect);

		conf.setJsonExport(default_output);

		sqlan.setConf(conf);

		createOutputDir();
	}

	@After
	public void tearDown() {
		conf = null;
		sqlan = null;
	}

	public void testFile(String testname) throws JsonParseException, JsonMappingException, IOException {
		String[] inputs = { input_dir + testname + ".sql" };
		String output = output_dir + '/' + testname + ".json";
		String reference = ref_dir + testname + ".json";

		conf.setJsonExport(output);
		sqlan.setConf(conf);

		sqlan.analyze(inputs);

		SQLRoot root = sqlan.getASG().getRoot();

		ObjectMapper mapper = new ObjectMapper();

		mapper = new ObjectMapper();
		SQLRoot root2 = mapper.readValue(new File(reference), SQLRoot.class);

		assertTrue(root.matchTree(root2, false, false, false));
	}

	public void testString(String s, String testname) throws JsonParseException, JsonMappingException, IOException {
		parseString(s, testname, true);
	}

	public List<Statement> parseString(String s, String testname, boolean assertASG)
			throws JsonParseException, JsonMappingException, IOException {
		String output = output_dir + '/' + testname + ".json";
		String reference = ref_dir + testname + ".json";
		conf.setJsonExport(output);
		sqlan.setConf(conf);

		Map<Integer, List<Statement>> result = sqlan.analyzeStrings(s);

		SQLRoot root = sqlan.getASG().getRoot();

		if (assertASG) {
			ObjectMapper mapper = new ObjectMapper();

			mapper = new ObjectMapper();
			SQLRoot root2 = mapper.readValue(new File(reference), SQLRoot.class);
			assertTrue(root.matchTree(root2, false, false, false));
		}

		if (result == null || result.isEmpty()) {
			return null;
		}

		List<Statement> ret = result.get(new Integer(0));
		return ret;
	}
}
