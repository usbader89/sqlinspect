package sqlinspect.sqlan.test.parser;

import java.io.IOException;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import sqlinspect.sqlan.SQLDialect;

public class ExtraEdgesTest extends AbstractParserTest {

		public ExtraEdgesTest() {
			super(SQLDialect.MYSQL, TEST_INPUT_DIR + "/common/", "common", TEST_REF_DIR + "/common/");
		}

		@Test
		public void extraedges() throws JsonParseException, JsonMappingException, IOException {
			testFile("extraedges");
		}
}
