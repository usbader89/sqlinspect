
package sqlinspect.sqlan.test.parser;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import sqlinspect.sqlan.asg.base.SQLRoot;
import sqlinspect.sqlan.test.AbstractSQLAnTest;
import sqlinspect.sqlan.Config;
import sqlinspect.sqlan.SQLAn;

public class CreateTableTest extends AbstractSQLAnTest {

	Config conf = new Config();
	private SQLAn sqlan;
	private String output = "createtable.json";

	@Before
	public void setUp() {
		sqlan = new SQLAn();

		conf.setParseDBSchema(false);
		conf.setRunSmells(false);

		conf.setJsonExport(output);

		sqlan.setConf(conf);
	}

	@After
	public void tearDown() {
		conf = null;
		sqlan = null;
	}

	@Test
	public void testSimpleCreateTable() {
		String[] inputs = { TEST_INPUT_DIR + "/mysql/createtable.sql" };

		sqlan.analyze(inputs);

		SQLRoot root = sqlan.getASG().getRoot();

		ObjectMapper mapper = new ObjectMapper();

		mapper = new ObjectMapper();
		try {
			SQLRoot root2 = mapper.readValue(new File(TEST_REF_DIR + "/mysql/createtable.json"),
					SQLRoot.class);
			assertTrue(root.matchTree(root2, false, false, false));
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
