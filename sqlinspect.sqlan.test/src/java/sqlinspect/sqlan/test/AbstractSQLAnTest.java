package sqlinspect.sqlan.test;

public abstract class AbstractSQLAnTest {
	protected static final String TEST_INPUT_DIR = System.getenv().get("TEST_RES_DIR") + "/input";
	protected static final String TEST_REF_DIR = System.getenv().get("TEST_RES_DIR") + "/reference";
}
