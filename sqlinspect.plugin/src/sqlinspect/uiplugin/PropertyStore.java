package sqlinspect.uiplugin;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.QualifiedName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.sqlan.SQLDialect;
import sqlinspect.uiplugin.extractors.InterQueryResolver;
import sqlinspect.uiplugin.extractors.JDBCHotspotFinder;
import sqlinspect.uiplugin.extractors.PreparedStatementResolver;

public final class PropertyStore {
	private static final Logger logger = LoggerFactory.getLogger(PropertyStore.class);

	public static final QualifiedName PROP_DIALECT = new QualifiedName(Activator.PLUGIN_ID, "dialect");

	public static final QualifiedName PROP_LOAD_SCHEMA_FROM_DB = new QualifiedName(Activator.PLUGIN_ID,
			"load_schema_from_db");
	public static final QualifiedName PROP_LOAD_SCHEMA_FROM_FILE = new QualifiedName(Activator.PLUGIN_ID,
			"load_schema_from_file");
	public static final QualifiedName PROP_DB_USER = new QualifiedName(Activator.PLUGIN_ID, "db_user");
	public static final QualifiedName PROP_DB_PWD = new QualifiedName(Activator.PLUGIN_ID, "db_pwd");
	public static final QualifiedName PROP_DB_URL = new QualifiedName(Activator.PLUGIN_ID, "db_url");
	public static final QualifiedName PROP_DB_SCHEMA = new QualifiedName(Activator.PLUGIN_ID, "db_schema_file");
	public static final QualifiedName PROP_RUN_SMELL_DETECTORS = new QualifiedName(Activator.PLUGIN_ID,
			"run_smell_detectors");
	public static final QualifiedName PROP_RUN_TABLE_ACCESS = new QualifiedName(Activator.PLUGIN_ID,
			"run_table_access");

	public static final QualifiedName PROP_JDBC_QUERY_HOTSPOTS = new QualifiedName(Activator.PLUGIN_ID,
			"jdbc_query_hotspots");
	public static final QualifiedName PROP_JDBC_PSTMT_HOTSPOTS = new QualifiedName(Activator.PLUGIN_ID,
			"jdbc_pstmt_hotspots");
	public static final QualifiedName PROP_JDBC_PSTMT = new QualifiedName(Activator.PLUGIN_ID, "jdbc_pstmt");

	public static final QualifiedName PROP_ANDR_EXECSQL = new QualifiedName(Activator.PLUGIN_ID, "andr_execsql");
	public static final QualifiedName PROP_ANDR_CURSOR_RAWQUERY = new QualifiedName(Activator.PLUGIN_ID,
			"andr_cursor_rawquery");
	public static final QualifiedName PROP_ANDR_STMT_EXEC = new QualifiedName(Activator.PLUGIN_ID, "andr_stmt_exec");
	public static final QualifiedName PROP_ANDR_COMPILE_STMT = new QualifiedName(Activator.PLUGIN_ID,
			"andr_compile_stmt");

	private static final String ERROR_RETR_PROP = "Error retrieving property";
	private static final String ERROR_SAVE_PROP = "Error saving property";

	public static final String DEFAULT_DIALECT = SQLDialect.MYSQL.toString();
	public static final String DEFAULT_DB_URL = "";
	public static final String DEFAULT_DB_USER = "";
	public static final String DEFAULT_DB_PWD = "";
	public static final String DEFAULT_DB_SCHEMA = "";
	public static final boolean DEFAULT_LOAD_SCHEMA_FROM_DB = false;
	public static final boolean DEFAULT_LOAD_SCHEMA_FROM_FILE = false;
	public static final boolean DEFAULT_RUN_SMELL_DETECTOR = true;
	public static final boolean DEFAULT_RUN_TABLE_ACCESS = true;

	public static final QualifiedName PROP_HOTSPOT_FINDER = new QualifiedName(Activator.PLUGIN_ID, "hotspot_finder");
	public static final String DEFAULT_HOTSPOT_FINDER = JDBCHotspotFinder.class.getSimpleName();

	public static final QualifiedName PROP_STRING_RESOLVER = new QualifiedName(Activator.PLUGIN_ID, "string_resolver");
	public static final String DEFAULT_STRING_RESOLVER = InterQueryResolver.class.getSimpleName();

	public static final QualifiedName PROP_INTERSR_MAXNL = new QualifiedName(Activator.PLUGIN_ID, "intersr_maxnl");
	public static final int DEFAULT_INTERSR_MAXNL = InterQueryResolver.DEFAULT_MAX_CFGDEPTH;

	public static final QualifiedName PROP_INTERSR_DEFDEPTH = new QualifiedName(Activator.PLUGIN_ID,
			"intersr_maxdefdepth");
//	public static final int DEFAULT_INTERSR_DEFDEPTH = InterQueryResolver.DEFAULT_MAX_DEFDEPTH;

	public static final QualifiedName PROP_INTERSR_MAXCALLDEPTH = new QualifiedName(Activator.PLUGIN_ID,
			"intersr_maxcalldepth");
	public static final int DEFAULT_INTERSR_MAXCALLDEPTH = InterQueryResolver.DEFAULT_MAX_CALLDEPTH;

	public static final QualifiedName PROP_INTERSR_LOOPHANDLING = new QualifiedName(Activator.PLUGIN_ID,
			"intersr_loophandling");
	public static final String DEFAULT_INTERSR_LOOPHANDLING = InterQueryResolver.DEFAULT_LOOP_HANDLING.toString();

	public static final QualifiedName PROP_PSTMT_RESOLVER_MAXDEPTH = new QualifiedName(Activator.PLUGIN_ID,
			"pstmt_resolver_maxdepth");
	public static final int DEFAULT_PSTMT_RESOLVER_MAXDEPTH = PreparedStatementResolver.DEFAULT_MAX_DEPTH;
	
	public static final QualifiedName PROP_RUN_SQL_METRICS = new QualifiedName(Activator.PLUGIN_ID, "run_sql_metrics");
	public static final boolean DEFAULT_RUN_SQL_METRICS = true;

	public static final QualifiedName PROP_DUMP_STATISTICS = new QualifiedName(Activator.PLUGIN_ID, "dump_statistics");
	public static final boolean DEFAULT_DUMP_STATISTICS = false;

	private PropertyStore() {
		// Private constructor to prevent instantiation.
	}

	public static String getStringProperty(IProject project, QualifiedName property, String defaultValue) {
		try {
			String value = project.getPersistentProperty(property);

			if (value != null) {
				return value;
			}
		} catch (CoreException e) {
			logger.error(ERROR_RETR_PROP, e);
		}
		return defaultValue;
	}

	public static String[] getStringArrProperty(IProject project, QualifiedName property, String sep,
			String[] defaultValue) {
		try {
			String value = project.getPersistentProperty(property);

			if (value != null) {
				return value.split(sep);
			}
		} catch (CoreException e) {
			logger.error(ERROR_RETR_PROP, e);
		}
		return defaultValue;
	}

	public static boolean getBooleanProperty(IProject project, QualifiedName property, boolean defaultValue) {
		try {
			String value = project.getPersistentProperty(property);

			if (value != null) {
				return Boolean.parseBoolean(value);
			}
		} catch (CoreException e) {
			logger.error(ERROR_RETR_PROP, e);
		}
		return defaultValue;
	}

	public static int getIntProperty(IProject project, QualifiedName property, int defaultValue) {
		try {
			String value = project.getPersistentProperty(property);

			if (value != null) {
				return Integer.parseInt(value);
			}
		} catch (CoreException e) {
			logger.error(ERROR_RETR_PROP, e);
		}
		return defaultValue;
	}

	public static void setStringProperty(IProject project, QualifiedName property, String arg) {
		try {
			project.setPersistentProperty(property, arg);
		} catch (CoreException e) {
			logger.error(ERROR_SAVE_PROP, e);
		}
	}

	public static void setBooleanProperty(IProject project, QualifiedName property, boolean arg) {
		try {
			project.setPersistentProperty(property, Boolean.toString(arg));
		} catch (CoreException e) {
			logger.error(ERROR_SAVE_PROP, e);
		}
	}

	public static void setIntProperty(IProject project, QualifiedName property, int arg) {
		try {
			project.setPersistentProperty(property, Integer.toString(arg));
		} catch (CoreException e) {
			logger.error(ERROR_SAVE_PROP, e);
		}
	}

	public static void setStringArrProperty(IProject project, QualifiedName property, String sep, String[] arg) {
		try {
			project.setPersistentProperty(property, String.join(sep, arg));
		} catch (CoreException e) {
			logger.error(ERROR_SAVE_PROP, e);
		}
	}

}
