package sqlinspect.uiplugin.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.Collection;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.eclipse.core.runtime.IPath;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.uiplugin.model.CallStack;
import sqlinspect.uiplugin.model.Query;

public class XMLQueries {
	private static final Logger logger = LoggerFactory.getLogger(XMLQueries.class);

	private static final String ROOT_NODE = "Queries";
	private static final String QUERY_NODE = "Query";
	private static final String CALLSTACK_NODE = "CallStack";
	private static final String CALL_NODE = "Call";
	private static final String CALL_METHOD = "method";
	private static final String CALL_PATH = "file";
	private static final String CALL_LINE = "line";
	private static final String QUERY_ATTR_ID = "id";
	private static final String QUERY_NODE_VALUE = "Value";
	private static final String QUERY_NODE_EXEC_CLASS = "ExecClass";
	private static final String QUERY_NODE_EXEC_LINE = "ExecLine";
	private static final String QUERY_NODE_EXEC_STRING = "ExecString";

	private static final String TAB_STR = "  ";

	private final String path;

	private XMLStreamWriter writer;
	private int indent;
	private boolean noIds;

	public XMLQueries(String path) {
		this.path = path;
		indent = 1;
		noIds = false;
	}

	public void writeQueries(Collection<Query> queries, boolean noIds) throws IOException {
		File outf = new File(path);
		logger.debug("Dump queries to file {} ", outf.getAbsolutePath());
		XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
		OutputStream ostream = null;
		this.noIds = noIds;

		try {
			ostream = Files.newOutputStream(outf.toPath());
			writer = outputFactory.createXMLStreamWriter(ostream);
			writer.writeStartDocument();
			nl();

			writer.writeStartElement(ROOT_NODE);
			nl();

			int id = 0;
			for (Query q : queries) {
				++id;

				writeQueryNode(q, id);
			}

			writer.writeEndElement();
			nl();
			writer.flush();
		} catch (FileNotFoundException e) {
			logger.error("Could not open file.", e);
		} catch (XMLStreamException e) {
			logger.error("Could not write XML.", e);
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (XMLStreamException e) {
					logger.error("Could not clean XMLStreamWriter!", e);
				}
			}
			if (ostream != null) {
				try {
					ostream.close();
				} catch (IOException e) {
					logger.error("Could not clean OutputStream!", e);
				}
			}
		}
		logger.debug("Dump queries done.");
	}

	private void writeQueryNode(Query q, int id) throws XMLStreamException {
		tab(indent++);
		writer.writeStartElement(QUERY_NODE);
		if (!noIds) {
			writer.writeAttribute(QUERY_ATTR_ID, Integer.toString(id));
		}
		nl();

		tab(indent);
		writer.writeStartElement(QUERY_NODE_VALUE);
		writer.writeCharacters(q.getValue());
		writer.writeEndElement();
		nl();

		tab(indent);
		writer.writeStartElement(QUERY_NODE_EXEC_CLASS);
		writer.writeCharacters(q.getHotspot().getPath().toString());
		writer.writeEndElement();
		nl();

		tab(indent);
		writer.writeStartElement(QUERY_NODE_EXEC_LINE);
		writer.writeCharacters(Integer.toString(ASTUtils.getLineNumber(q.getHotspot().getExec())));
		writer.writeEndElement();
		nl();

		tab(indent);
		writer.writeStartElement(QUERY_NODE_EXEC_STRING);
		writer.writeCharacters(q.getHotspot().getExec().toString());
		writer.writeEndElement();
		nl();

		writeCallStack(q);

		--indent;
		tab(indent);
		writer.writeEndElement();
		nl();
	}

	private void writeCallStack(Query q) throws XMLStreamException {
		tab(indent++);
		writer.writeStartElement(CALLSTACK_NODE);
		nl();

		CallStack stack = CallStack.calculate(q.getRoot());
		for (CallStack.CallStackElement e : stack.stack) {
			writeCall(e);
		}

		--indent;
		tab(indent);
		writer.writeEndElement();
		nl();
	}

	private void writeCall(CallStack.CallStackElement e) throws XMLStreamException {
		tab(indent++);
		writer.writeStartElement(CALL_NODE);

		IMethodBinding mb = e.getMethodDeclBinding();
		writer.writeAttribute(CALL_METHOD, mb == null ? "Unknown" : BindingUtil.qualifiedSignature(mb));

		IPath lpath = e.getPath();
		writer.writeAttribute(CALL_PATH, lpath == null ? "Unknown" : lpath.toString());

		int line = e.getLineNumber();
		writer.writeAttribute(CALL_LINE, Integer.toString(line));

		writer.writeEndElement();
		nl();
		--indent;
	}

	private void nl() throws XMLStreamException {
		writer.writeCharacters("\n");
	}

	private void tab(int level) throws XMLStreamException {
		for (int i = 0; i < level; i++) {
			writer.writeCharacters(TAB_STR);
		}
	}
}
