package sqlinspect.uiplugin.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.compiler.IProblem;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ASTStore {
	private static final Logger logger = LoggerFactory.getLogger(ASTStore.class);

	private final Map<ICompilationUnit, CompilationUnit> store = new HashMap<>();

	@SuppressWarnings("deprecation")
	private static final ASTParser parser = ASTParser.newParser(AST.JLS9);

	private static CompilationUnit parse(ICompilationUnit unit) {
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		parser.setSource(unit); // set source
		parser.setResolveBindings(true); // we need bindings later on
		parser.setBindingsRecovery(true);
		parser.setStatementsRecovery(true);
		Map<String, String> options = JavaCore.getOptions();
		JavaCore.setComplianceOptions(JavaCore.VERSION_1_8, options);
		options.put(JavaCore.COMPILER_SOURCE, JavaCore.VERSION_1_8);
		options.put(JavaCore.COMPILER_CODEGEN_TARGET_PLATFORM, JavaCore.VERSION_1_8);
		parser.setCompilerOptions(options);

		CompilationUnit cu;
		try {
			cu = (CompilationUnit) parser.createAST(new NullProgressMonitor());
			
			dumpParserErrors(cu);
			
			IPath unitPath = unit.getPath();
			IPath absolutePath = unitPath.makeRelativeTo(ResourcesPlugin.getWorkspace().getRoot().getFullPath());
			String source = absolutePath.toOSString();

			cu.setProperty("SOURCE_WSPATH", source);
		} catch (Exception e) {
			logger.error("Unexpected error while parsing the compilation unit.", e);
			return null;
		}
		return cu;
	}

	private static void dumpParserErrors(CompilationUnit ast) {
		IProblem[] problems = ast.getProblems();

		if (problems.length > 0) {
			ArrayList<String> messages = new ArrayList<>();
			int error = 0;
			for (IProblem p : problems) {
				if (p.isError()) {
					++error;
					messages.add(Integer.toString(p.getSourceLineNumber()) + ": " + p.getMessage());
				}
			}
			if (error > 0) {
				logger.warn("There were problems parsing the compilation unit: {}", ast.getJavaElement().getElementName());
				for (String s : messages) {
					logger.warn(s);
				}
			}
		}
	}

	public CompilationUnit get(ICompilationUnit unit) {
		return store.computeIfAbsent(unit, ASTStore::parse);
	}

	public void clearStore() {
		store.clear();
	}

	public void remove(ICompilationUnit unit) {
		store.remove(unit);
	}

	public boolean inStore(ICompilationUnit unit) {
		return store.containsKey(unit);
	}
}
