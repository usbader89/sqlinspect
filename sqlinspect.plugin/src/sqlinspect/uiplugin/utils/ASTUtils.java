package sqlinspect.uiplugin.utils;

import org.eclipse.core.runtime.IPath;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.ISourceRange;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.NodeFinder;
import org.eclipse.jdt.core.dom.Statement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class ASTUtils {

	private static final Logger logger = LoggerFactory.getLogger(ASTUtils.class);

	private ASTUtils() {
		// Empty constructor. Just to prevent instantiation.
	}

	public static int getLineNumber(ASTNode node) {
		CompilationUnit unit = getCompilationUnit(node);
		if (unit != null) {
			return unit.getLineNumber(node.getStartPosition() - 1);
		} else {
			return 0;
		}
	}

	public static IPath getPath(ASTNode node) {
		CompilationUnit unit = getCompilationUnit(node);
		if (unit != null) {
			IJavaElement element = unit.getJavaElement();
			if (element != null) {
				return element.getPath();
			}
		}
		return null;
	}

	public static CompilationUnit getCompilationUnit(ASTNode node) {
		ASTNode root = node.getRoot();
		if (root instanceof CompilationUnit) {
			return (CompilationUnit) root;
		}
		return null;
	}

	public static Block getParentBlock(ASTNode node) {
		ASTNode parent = node.getParent();
		while (parent != null && !(parent instanceof Block)) {
			parent = parent.getParent();
		}
		if (parent instanceof Block) {
			return (Block) parent;
		}
		return null;
	}

	public static Statement getParentStatement(ASTNode node) {
		ASTNode parent = node.getParent();
		while (parent != null && !(parent instanceof Statement)) {
			parent = parent.getParent();
		}
		if (parent instanceof Statement) {
			return (Statement) parent;
		}
		return null;
	}

	/**
	 * Get the statement of the block which contains the given node.
	 * 
	 * @param node
	 * @return
	 */
	public static Statement getBlockParentStatement(ASTNode node) {
		ASTNode parent = node.getParent();
		Statement lastStatement = null;
		while (parent != null && !(parent instanceof Block)) {
			if (parent instanceof Statement) {
				lastStatement = (Statement) parent;
			}
			parent = parent.getParent();
		}
		return lastStatement;
	}

	public static MethodDeclaration getParentMethodDeclaration(ASTNode node) {
		ASTNode parent = node;
		while (parent != null && !(parent instanceof MethodDeclaration)) {
			parent = parent.getParent();
		}
		if (parent instanceof MethodDeclaration) {
			return (MethodDeclaration) parent;
		} else {
			return null;
		}
	}

	public static IJavaElement getRootJavaElement(ASTNode node) {
		ASTNode root = node.getRoot();
		if (root instanceof CompilationUnit) {
			return ((CompilationUnit) root).getJavaElement();
		}
		return null;
	}

	public static ICompilationUnit getICompilationUnit(ASTNode node) {
		ASTNode root = node.getRoot();
		if (root instanceof CompilationUnit) {
			IJavaElement cu = ((CompilationUnit) root).getJavaElement();
			if (cu instanceof ICompilationUnit) {
				return (ICompilationUnit) cu;
			}
		}
		return null;
	}

	public static ASTNode getParent(ASTNode node, Class<? extends ASTNode> parentClass) {
		ASTNode n = node.getParent();
		while (n != null && !parentClass.isInstance(n)) {
			n = n.getParent();
		}
		return n;
	}

	public static ASTNode findASTNodeInICU(final ICompilationUnit icu, final int offset, final int length) {
		@SuppressWarnings("deprecation")
		ASTParser parser = ASTParser.newParser(AST.JLS9);
		parser.setSource(icu);
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		parser.setResolveBindings(true);
		CompilationUnit cu = (CompilationUnit) parser.createAST(null);

		NodeFinder finder = new NodeFinder(cu, offset, length);
		return finder.getCoveringNode();
	}

	public static MethodDeclaration findMethodDeclaration(final CompilationUnit cu, final IMethod method) {
		try {
			ISourceRange range = method.getNameRange();
			if (range == null) {
				logger.debug("Could not get source range of mehotd {}", method);
				return null;
			}

			final ASTNode node = NodeFinder.perform(cu, range);
			return getParentMethodDeclaration(node);
		} catch (JavaModelException e) {
			logger.error("Could not get source range of method {}", method, e);
			return null;
		}
	}

	public static Expression leftMostSide(MethodInvocation mi) {
		Expression expr = mi;
		while (expr instanceof MethodInvocation) {
			expr = ((MethodInvocation) expr).getExpression();
		}
		return expr;
	}

	public static String getMethodName(ASTNode node) {
		if (node instanceof MethodInvocation) {
			MethodInvocation mi = (MethodInvocation) node;

			IMethodBinding binding = mi.resolveMethodBinding();
			if (binding != null) {
				return binding.getName();
			}

		}
		return null;
	}

	public static String getMethodQualifiedName(ASTNode node) {
		IMethodBinding binding = null;
		if (node instanceof MethodInvocation) {
			MethodInvocation mi = (MethodInvocation) node;

			binding = mi.resolveMethodBinding();
		} else if (node instanceof MethodDeclaration) {
			MethodDeclaration md = (MethodDeclaration) node;

			binding = md.resolveBinding();
		} else {
			throw new IllegalArgumentException("Node has to be a MethodInvocation or MethodDeclaration");
		}

		if (binding != null) {
			return BindingUtil.qualifiedName(binding);
		}

		return null;
	}
}
