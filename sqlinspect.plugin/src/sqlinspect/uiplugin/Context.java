package sqlinspect.uiplugin;

import java.util.ArrayList;
import java.util.List;
import sqlinspect.cfg.CFGStore;
import sqlinspect.uiplugin.extractors.AndroidHotspotFinder;
import sqlinspect.uiplugin.extractors.IHotspotFinder;
import sqlinspect.uiplugin.extractors.IQueryResolver;
import sqlinspect.uiplugin.extractors.InterQueryResolver;
import sqlinspect.uiplugin.extractors.JDBCHotspotFinder;
import sqlinspect.uiplugin.model.Model;
import sqlinspect.uiplugin.utils.ASTStore;

public final class Context {
	private static final Context instance = new Context();

	private final Model model = new Model();

	private final ASTStore astStore = new ASTStore();
	private final CFGStore cfgStore = new CFGStore();

	private final List<IHotspotFinder> hotspotFinders = new ArrayList<>();
	private final List<IQueryResolver> stringResolvers = new ArrayList<>();

	private Context() {
	}

	public void loadResolvers() {
		hotspotFinders.add(new JDBCHotspotFinder());
		hotspotFinders.add(new AndroidHotspotFinder());
		stringResolvers.add(new InterQueryResolver());
	}

	public static Context getInstance() {
		return instance;
	}

	public Model getModel() {
		return model;
	}

	public List<IHotspotFinder> getHotspotFinders() {
		return hotspotFinders;
	}

	public IHotspotFinder getHotspotFinder(String name) {
		if (name == null) {
			return null;
		}

		for (IHotspotFinder hs : hotspotFinders) {
			if (name.equals(hs.getName())) {
				return hs;
			}
		}

		return null;
	}

	public IQueryResolver getStringResolver(String name) {
		if (name == null) {
			return null;
		}

		for (IQueryResolver sr : stringResolvers) {
			if (name.equals(sr.getName())) {
				return sr;
			}
		}

		return null;
	}

	public List<IQueryResolver> getStringResolvers() {
		return stringResolvers;
	}

	public ASTStore getASTStore() {
		return astStore;
	}

	public CFGStore getCFGStore() {
		return cfgStore;
	}
}
