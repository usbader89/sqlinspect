package sqlinspect.uiplugin.sqlan;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.sqlan.Config;
import sqlinspect.sqlan.SQLAn;
import sqlinspect.sqlan.SQLDialect;
import sqlinspect.sqlan.asg.common.ASG;
import sqlinspect.sqlan.asg.schema.Column;
import sqlinspect.sqlan.asg.schema.Table;
import sqlinspect.sqlan.asg.statm.Statement;
import sqlinspect.sqlan.metrics.MetricDesc;
import sqlinspect.sqlan.smells.common.SQLSmell;
import sqlinspect.uiplugin.Context;
import sqlinspect.uiplugin.PropertyStore;
import sqlinspect.uiplugin.extractors.AndroidHotspotFinder;
import sqlinspect.uiplugin.extractors.HotspotDesc;
import sqlinspect.uiplugin.extractors.IHotspotFinder;
import sqlinspect.uiplugin.extractors.IQueryResolver;
import sqlinspect.uiplugin.extractors.InterQueryResolver;
import sqlinspect.uiplugin.extractors.JDBCHotspotFinder;
import sqlinspect.uiplugin.extractors.LoopHandling;
import sqlinspect.uiplugin.extractors.PreparedStatementResolver;
import sqlinspect.uiplugin.model.Hotspot;
import sqlinspect.uiplugin.model.Project;
import sqlinspect.uiplugin.model.Query;
import sqlinspect.uiplugin.utils.ASTUtils;

public final class SQLAnWrapper {
	private static final Logger logger = LoggerFactory.getLogger(SQLAnWrapper.class);

	public static final String SQLSMELL_RULENAME = "rulename";

	private static final char SEMI = ';';

	private final Config conf = new Config();

	private final SQLAn sqlan = new SQLAn();

	private final Project project;

	private Optional<IHotspotFinder> hsf = Optional.empty();
	private Optional<IQueryResolver> resolver = Optional.empty();

	public SQLAnWrapper(Project project) {
		this.project = project;
	}

	public void parseFile(String path) {
		sqlan.parseFile(path);
	}

	public boolean exportAST(String exportFile) {
		return sqlan.jsonExport(exportFile);
	}

	public void initAnalyzer() {
		logger.info("Initializing analyzer");
		getProjectConfig();
		sqlan.setConf(conf);
		sqlan.reinit();
		this.hsf = Optional.of(getHotspotFinder(project.getIProject()));
		this.resolver = Optional.of(getQueryResolver(project.getIProject()));
	}

	public void setConf(Config conf) {
		sqlan.setConf(conf);
	}

	public boolean analyze(IJavaElement javaElement, SubMonitor monitor) {
		logger.info("Analyzing project: {}", project.getName());
		IProject iproj = project.getIProject();

		if (iproj == null) {
			logger.error("Project has no IProject!");
			return false;
		}

		try {
			if (iproj.hasNature(JavaCore.NATURE_ID)) {
				IJavaProject javaProject = project.getIJavaProject();
				IPackageFragment[] packages = javaProject.getPackageFragments();
				int numPackages = packages.length;
				monitor.beginTask("Extracting hotspots", numPackages);
				for (IPackageFragment p : packages) {
					if (p.getKind() == IPackageFragmentRoot.K_SOURCE
							&& ((javaElement instanceof IPackageFragment && p.equals(javaElement))
									|| !(javaElement instanceof IPackageFragment))) {
						SubMonitor subMonitor = monitor.split(1);
						subMonitor.beginTask("Analyze package: " + p.getElementName(), p.getCompilationUnits().length);
						for (ICompilationUnit unit : p.getCompilationUnits()) {
							if ((javaElement instanceof ICompilationUnit && unit.equals(javaElement))
									|| !(javaElement instanceof ICompilationUnit)) {
								analyze(unit);
								subMonitor.worked(1);
							}
						}
					} else {
						monitor.worked(1);
					}
				}
				return true;
			} else {
				logger.error("The project is not a Java project!");
				return false;
			}
		} catch (CoreException e) {
			logger.error("Could not get project nature!", e);
			return false;
		}
	}

	public static IHotspotFinder getHotspotFinder(String name) {
		IHotspotFinder rethsf = Context.getInstance()
				.getHotspotFinder(name == null ? PropertyStore.DEFAULT_HOTSPOT_FINDER : name);

		if (rethsf == null) {
			rethsf = new JDBCHotspotFinder();
		}

		if (rethsf instanceof JDBCHotspotFinder) {
			JDBCHotspotFinder jhsf = (JDBCHotspotFinder) rethsf;
			List<HotspotDesc> hsdescs = JDBCHotspotFinder.getDefaultHotspots();
			jhsf.setDescriptors(hsdescs);
			int maxdepth = PropertyStore.DEFAULT_PSTMT_RESOLVER_MAXDEPTH;
			PreparedStatementResolver psResolver = new PreparedStatementResolver(maxdepth);
			psResolver.setDescriptors(JDBCHotspotFinder.getDefaultPsHotspots());
			jhsf.setPSResolver(psResolver);
		}

		if (rethsf instanceof AndroidHotspotFinder) {
			AndroidHotspotFinder jhsf = (AndroidHotspotFinder) rethsf;
			List<HotspotDesc> hsdescs = AndroidHotspotFinder.getDefaultHotspots();
			jhsf.setDescriptors(hsdescs);
			int maxdepth = PropertyStore.DEFAULT_PSTMT_RESOLVER_MAXDEPTH;
			PreparedStatementResolver psResolver = new PreparedStatementResolver(maxdepth);
			psResolver.setDescriptors(JDBCHotspotFinder.getDefaultPsHotspots());
			jhsf.setPSResolver(psResolver);
		}

		return rethsf;
	}

	private static IHotspotFinder getHotspotFinder(IProject iproj) {
		if (iproj == null) {
			throw new IllegalArgumentException("Project cannot be null!");
		}

		String projectHsf = PropertyStore.getStringProperty(iproj, PropertyStore.PROP_HOTSPOT_FINDER,
				PropertyStore.DEFAULT_HOTSPOT_FINDER);
		IHotspotFinder hsf = Context.getInstance().getHotspotFinder(projectHsf);

		if (hsf == null) {
			hsf = new JDBCHotspotFinder();
		}

		if (hsf instanceof JDBCHotspotFinder) {
			JDBCHotspotFinder jhsf = (JDBCHotspotFinder) hsf;
			List<HotspotDesc> hsdescs = JDBCHotspotFinder.getHotspots(iproj);
			jhsf.setDescriptors(hsdescs);
			int maxdepth = PropertyStore.getIntProperty(iproj, PropertyStore.PROP_PSTMT_RESOLVER_MAXDEPTH,
					PropertyStore.DEFAULT_PSTMT_RESOLVER_MAXDEPTH);
			PreparedStatementResolver psResolver = new PreparedStatementResolver(maxdepth);
			psResolver.setDescriptors(JDBCHotspotFinder.getPsHotspots(iproj));
			jhsf.setPSResolver(psResolver);
		}

		if (hsf instanceof AndroidHotspotFinder) {
			AndroidHotspotFinder jhsf = (AndroidHotspotFinder) hsf;
			List<HotspotDesc> hsdescs = AndroidHotspotFinder.getHotspots(iproj);
			jhsf.setDescriptors(hsdescs);
			int maxdepth = PropertyStore.getIntProperty(iproj, PropertyStore.PROP_PSTMT_RESOLVER_MAXDEPTH,
					PropertyStore.DEFAULT_PSTMT_RESOLVER_MAXDEPTH);
			PreparedStatementResolver psResolver = new PreparedStatementResolver(maxdepth);
			psResolver.setDescriptors(AndroidHotspotFinder.getPsHotspots(iproj));
			jhsf.setPSResolver(psResolver);
		}

		return hsf;
	}

	public static IQueryResolver getQueryResolver(String name) {
		IQueryResolver resolver = Context.getInstance()
				.getStringResolver(name == null ? PropertyStore.DEFAULT_STRING_RESOLVER : name);

		if (resolver == null) {
			resolver = new InterQueryResolver();
		}

		if (resolver instanceof InterQueryResolver) {
			InterQueryResolver intersr = (InterQueryResolver) resolver;
			intersr.setLoopHandling(LoopHandling.getLoopHandling(PropertyStore.DEFAULT_INTERSR_LOOPHANDLING));
			intersr.setMaxDepth(PropertyStore.DEFAULT_INTERSR_MAXNL);
			intersr.setMaxCallDepth(PropertyStore.DEFAULT_INTERSR_MAXCALLDEPTH);
		}

		return resolver;
	}

	private static IQueryResolver getQueryResolver(IProject iproj) {
		if (iproj == null) {
			logger.error("Project cannot be null!");
			return null;
		}

		String projectResolver = PropertyStore.getStringProperty(iproj, PropertyStore.PROP_STRING_RESOLVER,
				PropertyStore.DEFAULT_STRING_RESOLVER);
		IQueryResolver resolver = Context.getInstance().getStringResolver(projectResolver);

		if (resolver == null) {
			resolver = new InterQueryResolver();
		}

		if (resolver instanceof InterQueryResolver) {
			InterQueryResolver intersr = (InterQueryResolver) resolver;
			intersr.setLoopHandling(LoopHandling.getLoopHandling(PropertyStore.getStringProperty(iproj,
					PropertyStore.PROP_INTERSR_LOOPHANDLING, PropertyStore.DEFAULT_INTERSR_LOOPHANDLING)));
			intersr.setMaxDepth(PropertyStore.getIntProperty(iproj, PropertyStore.PROP_INTERSR_MAXNL,
					PropertyStore.DEFAULT_INTERSR_MAXNL));
			intersr.setMaxCallDepth(PropertyStore.getIntProperty(iproj, PropertyStore.PROP_INTERSR_MAXCALLDEPTH,
					PropertyStore.DEFAULT_INTERSR_MAXCALLDEPTH));
		}

		return resolver;
	}

	public boolean analyze(ICompilationUnit unit) {
		if (!hsf.isPresent()) {
			throw new IllegalStateException("HotspotFinder cannot be null!");
		}

		if (!resolver.isPresent()) {
			throw new IllegalStateException("Resolver cannot be null!");
		}

		logger.info("Analyzing ({}, {}) ICompilationUnit: {} ", hsf.get().getClass().getSimpleName(),
				resolver.get().getClass().getSimpleName(), unit.getElementName());

		Collection<Hotspot> hotspots = hsf.get().extract(unit, resolver.get());
		project.addHotspots(hotspots);

		return true;

	}

	public boolean analyze(CompilationUnit unit, IPath path, IHotspotFinder hsf, IQueryResolver resolver) {
		logger.info("Analyzing CompilationUnit: {}", path);

		Collection<Hotspot> hotspots = hsf.extract(unit, path, resolver);
		project.addHotspots(hotspots);
		return true;
	}

	public boolean analyze(List<Query> queries) {
		int numQueries = queries.size();
		String[] queryStrings = new String[numQueries];
		String[] queryPaths = new String[numQueries];
		int[] queryRows = new int[numQueries];

		int i = 0;
		for (Query q : queries) {
			String s = q.getValue().trim();
			if (s.charAt(s.length() - 1) != SEMI) {
				queryStrings[i] = q.getValue() + ';';
			} else {
				queryStrings[i] = q.getValue();
			}

			queryPaths[i] = q.getHotspot().getPath().toOSString();
			queryRows[i] = ASTUtils.getLineNumber(q.getHotspot().getExec());
			i++;
		}

		sqlan.analyzeStrings(queryStrings, queryPaths, queryRows);

		return true;
	}

	public void parseSchemaFromFile(String dbSchema) {
		logger.info("Parsing schema file of the project: {}", dbSchema);
		if (dbSchema != null && !dbSchema.isEmpty()) {
			parseFile(dbSchema);
		}
	}

	private void getProjectConfig() {
		IProject iproj = project.getIProject();
		String dbUser = PropertyStore.getStringProperty(iproj, PropertyStore.PROP_DB_USER,
				PropertyStore.DEFAULT_DB_USER);
		String dbPassword = PropertyStore.getStringProperty(iproj, PropertyStore.PROP_DB_PWD,
				PropertyStore.DEFAULT_DB_PWD);
		boolean runSmellDetectors = PropertyStore.getBooleanProperty(iproj, PropertyStore.PROP_RUN_SMELL_DETECTORS,
				PropertyStore.DEFAULT_RUN_SMELL_DETECTOR);
		boolean runTAA = PropertyStore.getBooleanProperty(iproj, PropertyStore.PROP_RUN_TABLE_ACCESS,
				PropertyStore.DEFAULT_RUN_TABLE_ACCESS);
		boolean runSQLMetrics = PropertyStore.getBooleanProperty(iproj, PropertyStore.PROP_RUN_SQL_METRICS,
				PropertyStore.DEFAULT_RUN_SQL_METRICS);
		String dialect = PropertyStore.getStringProperty(iproj, PropertyStore.PROP_DIALECT,
				PropertyStore.DEFAULT_DIALECT);

		conf.setDialect(SQLDialect.getSQLDialect(dialect));

		if (dbUser != null && !dbUser.isEmpty()) {
			conf.setDbUser(dbUser);
		}

		if (dbPassword != null && !dbPassword.isEmpty()) {
			conf.setDbPassword(dbPassword);
		}

		boolean loadSchemaFromDB = PropertyStore.getBooleanProperty(iproj, PropertyStore.PROP_LOAD_SCHEMA_FROM_DB,
				PropertyStore.DEFAULT_LOAD_SCHEMA_FROM_DB);
		if (loadSchemaFromDB) {
			String dbUrl = PropertyStore.getStringProperty(iproj, PropertyStore.PROP_DB_URL,
					PropertyStore.DEFAULT_DB_URL);

			if (dbUrl != null && !dbUrl.isEmpty()) {
				conf.setDbUrl(dbUrl);
				conf.setParseDBSchema(true);
			} else {
				conf.setParseDBSchema(false);
			}
		} else {
			conf.setParseDBSchema(false);
		}

		conf.setRunSmells(runSmellDetectors);
		conf.setRunTAA(runTAA);
		conf.setRunSQLMetrics(runSQLMetrics);
	}

	public boolean analyzeQueries(SubMonitor monitor) {
		monitor.beginTask("Analyze queries", 100);

		getProjectConfig();

		sqlan.setConf(conf);

		List<Query> queries = project.getQueries();
		int numQueries = queries.size();
		String[] queryStrings = new String[numQueries];
		String[] queryPaths = new String[numQueries];
		int[] queryRows = new int[numQueries];

		int i = 0;
		for (Query q : queries) {
			String s = q.getValue().trim();
			int slength = s.length();
			if (slength > 0 && s.charAt(slength - 1) != SEMI) {
				queryStrings[i] = q.getValue() + SEMI;
			} else {
				queryStrings[i] = q.getValue();
			}

			queryPaths[i] = q.getHotspot().getPath().toOSString();
			queryRows[i] = ASTUtils.getLineNumber(q.getHotspot().getExec()) - 1;
			i++;
		}

		Map<Integer, List<Statement>> ret = sqlan.analyzeStrings(queryStrings, queryPaths, queryRows);
		monitor.worked(90);

		for (Map.Entry<Integer, List<Statement>> e : ret.entrySet()) {
			int ind = e.getKey().intValue();
			Query q = queries.get(ind);
			q.setStatements(e.getValue());
			for (Statement stmt : e.getValue()) {
				project.getQueryMap().put(stmt, q);
			}
		}
		monitor.worked(10);

		return true;
	}

	public boolean reportSmells(String reportFile, String reportFormat) {
		return sqlan.reportSmells(reportFile, reportFormat);
	}

	public boolean reportTAA(String exportFile, String exportFormat) {
		return sqlan.reportTAA(exportFile, exportFormat);
	}

	public boolean reportSQLMetrics(String exportFile, String exportFormat) {
		return sqlan.reportSQLMetrics(exportFile, exportFormat);
	}

	public List<Statement> locateQuery(String query, boolean ignoreLiterals, boolean ignoreNames, boolean ignoreRefs) {
		return sqlan.searchStatement(query, ignoreLiterals, ignoreNames, ignoreRefs);
	}

	public ASG getASG() {
		return sqlan.getASG();
	}

	public Set<SQLSmell> getSmells() {
		return sqlan.getSmells();
	}

	public Map<Statement, Map<Column, Integer>> getColumnAcc() {
		return sqlan.getColumnAcc();
	}

	public Map<Statement, Map<Table, Integer>> getTableAcc() {
		return sqlan.getTableAcc();
	}

	public Map<Statement, Map<MetricDesc, Integer>> getMetrics() {
		return sqlan.getMetrics();
	}

	public void dumpBM(File f) {
		sqlan.dumpBM(f);
	}

	public void clear() {
		sqlan.getMetrics().clear();
		sqlan.getSmells().clear();
	}
}
