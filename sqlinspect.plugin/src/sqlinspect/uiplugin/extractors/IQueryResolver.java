package sqlinspect.uiplugin.extractors;

import java.util.List;

import org.eclipse.jdt.core.dom.Expression;

import sqlinspect.uiplugin.model.QueryPart;

public interface IQueryResolver {
	String getName();

	List<QueryPart> resolve(Expression expr);
}
