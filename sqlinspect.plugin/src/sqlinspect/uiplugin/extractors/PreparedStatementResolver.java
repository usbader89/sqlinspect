package sqlinspect.uiplugin.extractors;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.cfg.CFGStore;
import sqlinspect.uiplugin.Context;
import sqlinspect.uiplugin.utils.BindingUtil;

public final class PreparedStatementResolver {
	private static final Logger logger = LoggerFactory.getLogger(PreparedStatementResolver.class);

	private final CFGStore cfgStore = Context.getInstance().getCFGStore();

	private final Map<String, HotspotDesc> hsMap = new HashMap<>();

	public static final int DEFAULT_MAX_DEPTH = 100;

	private final int maxDepth;

	public PreparedStatementResolver(int maxdepth) {
		this.maxDepth = maxdepth;
	}

	public void setDescriptors(Collection<HotspotDesc> hsdescs) {
		hsMap.clear();
		for (HotspotDesc hd : hsdescs) {
			hsMap.put(hd.getSignature(), hd);
		}
	}

	public List<Expression> resolve(MethodInvocation node) {
		// typically we have sg like:
		// PreparedStatement stmt = connection.prepareStatement(query);
		// ....
		// stmt.executeQuery();
		// OR:
		// PreparedStatement stmt = null;
		// if ( ..) {
		// stmt = connection.prepareStatement(query1);
		// } else {
		// stmt = connection.prepareStatement(query2);
		// }
		// ...
		// stmt.executeQuery();

		// so we try to locate the last assignments of 'stmt'

		List<Expression> ret = new ArrayList<>();

		List<Expression> initExprs = getInitializeExpression(node);

		if (initExprs != null) {
			for (Expression ie : initExprs) {
				if (ie instanceof MethodInvocation) {
					MethodInvocation mi = (MethodInvocation) ie;

					IMethodBinding binding = mi.resolveMethodBinding();

					if (binding != null) {
						String qsig = BindingUtil.qualifiedSignature(binding);

						HotspotDesc hd = hsMap.get(qsig);
						if (hd != null) {
							if (hd.getArgnum() < 0) {
								logger.error("Illegel argument number for hotspot descriptor: {}", hd);
								return ret;
							} else {
								ret.add((Expression) mi.arguments().get(hd.getArgnum() - 1));
							}
						}
					}
				}
			}
		}

		return ret;
	}

	public Expression getAssignmentExpression(Statement stmt, SimpleName sn) {
		if (stmt instanceof ExpressionStatement) {
			ExpressionStatement exprStmt = (ExpressionStatement) stmt;
			Expression innerexpr = exprStmt.getExpression();

			if (innerexpr instanceof Assignment) {
				Assignment assign = (Assignment) innerexpr;
				Expression lhs = assign.getLeftHandSide();
				Expression rhs = assign.getRightHandSide();

				if (lhs instanceof SimpleName) {
					SimpleName innersn = (SimpleName) lhs;
					IBinding binding = innersn.resolveBinding();
					if (binding != null && binding.equals(sn.resolveBinding())) {
						return rhs;
					}
				}
			}
		} else if (stmt instanceof VariableDeclarationStatement) {
			VariableDeclarationStatement vardecl = (VariableDeclarationStatement) stmt;

			for (Object fragmentObject : vardecl.fragments()) {
				if (fragmentObject instanceof VariableDeclarationFragment) {
					VariableDeclarationFragment fragment = (VariableDeclarationFragment) fragmentObject;
					IBinding binding = fragment.resolveBinding();
					if (binding != null && binding.equals(sn.resolveBinding())) {
						return fragment.getInitializer();
					}
				}
			}
		}

		return null;
	}

	public List<Expression> getInitializeExpression(MethodInvocation node) {
		Expression expr = node.getExpression();

		if (expr instanceof SimpleName) {
			SimpleName sn = (SimpleName) expr; // we have 'stmt' now

			PreparedStatementVisitor visitor = new PreparedStatementVisitor(cfgStore);
			List<Statement> defStatements = visitor.run(sn, maxDepth);
			return defStatements.stream().map(stmt -> getAssignmentExpression(stmt, sn)).filter(Objects::nonNull)
					.collect(Collectors.toList());
		}

		return new ArrayList<>(0);
	}
}
