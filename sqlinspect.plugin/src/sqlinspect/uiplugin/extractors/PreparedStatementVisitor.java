package sqlinspect.uiplugin.extractors;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.Statement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.cfg.BasicBlock;
import sqlinspect.cfg.CFG;
import sqlinspect.cfg.CFGNodeFinder;
import sqlinspect.cfg.CFGRevBreadthFirst;
import sqlinspect.cfg.CFGStore;
import sqlinspect.uiplugin.utils.ASTUtils;
import sqlinspect.uiplugin.utils.BindingUtil;

public final class PreparedStatementVisitor extends CFGRevBreadthFirst {
	private static final String PREPARED_STATEMENT_SET = "java.sql.PreparedStatement.set"; // setInt(), setString(), ...
	private static final String SQLITE_STATEMENT_BIND = "android.database.sqlite.SQLiteProgram.bind"; // bindLong(),
																										// bindString(),
																										// ...

	private static final Logger logger = LoggerFactory.getLogger(PreparedStatementResolver.class);

	private final CFGStore cfgStore;

	private Optional<ListIterator<ASTNode>> startIt = Optional.empty();

	private final List<Statement> defs = new ArrayList<>();

	private final DefsVisitor defsVisitor = new DefsVisitor();

	private IBinding toFind;

	public PreparedStatementVisitor(CFGStore cfgStore) {
		super();
		this.cfgStore = cfgStore;
	}

	public List<Statement> run(SimpleName sn, int maxDepth) {
		logger.debug("Looking for the last definition(s) of {}", sn);

		defs.clear();

		toFind = sn.resolveBinding();
		if (toFind == null) {
			logger.warn("Could not resolve binding of simple name {}", sn);
			return defs;
		}

		Statement stmt = ASTUtils.getParentStatement(sn);
		if (stmt == null) {
			logger.warn("Could not get parent statement for simple name {}", sn);
			return defs;
		}

		MethodDeclaration method = ASTUtils.getParentMethodDeclaration(stmt);
		CFG cfg = cfgStore.get(method);

		CFGNodeFinder nodeFinder = new CFGNodeFinder(cfg, stmt);
		if (!nodeFinder.findStatement()) {
			logger.error("Could not find the statement in the cfg!");
			return defs;
		}

		BasicBlock startBB = nodeFinder.getBlock();
		startIt = Optional.ofNullable(nodeFinder.getListIterator());
		if (startIt.isPresent()) {
			startIt.get().previous();
			super.run(startBB, maxDepth);
		}

		return defs;
	}

	@Override
	protected boolean visit(BasicBlock bb) {
		ListIterator<ASTNode> it = startIt.isPresent() ? startIt.get() : bb.nodes().listIterator(bb.nodes().size());
		if (startIt.isPresent()) {
			startIt = Optional.empty();
		}
		while (it.hasPrevious()) {
			ASTNode node = it.previous();
			if (node instanceof Statement) {
				Statement stmt = (Statement) node;
				List<IBinding> stmtDefs = defsVisitor.run(stmt).stream().map(SimpleName::resolveBinding)
						.filter(Objects::nonNull).collect(Collectors.toList());

				if (stmtDefs.contains(toFind) && !isPreparedStatementSetter(stmt)) {
					defs.add(stmt);
					return false;
				}
			}
		}
		return true;
	}

	private boolean isPreparedStatementSetter(Statement stmt) {
		if (stmt instanceof ExpressionStatement) {
			Expression expr = ((ExpressionStatement) stmt).getExpression();
			if (expr instanceof MethodInvocation) {
				MethodInvocation mi = (MethodInvocation) expr;
				IMethodBinding binding = mi.resolveMethodBinding();
				if (binding == null) {
					return false;
				}
				String sig = BindingUtil.qualifiedSignature(binding);
				if (sig.startsWith(PREPARED_STATEMENT_SET) || sig.startsWith(SQLITE_STATEMENT_BIND)) {
					return true;
				}
			}
		}
		return false;
	}
}
