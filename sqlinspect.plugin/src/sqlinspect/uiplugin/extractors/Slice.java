package sqlinspect.uiplugin.extractors;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.SimpleName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Slice {
	private static final Logger logger = LoggerFactory.getLogger(Slice.class);

	private final Map<SimpleName, Expression> defsOf = new HashMap<>();

	private final Map<SimpleName, SimpleName> refsTo = new HashMap<>();

	// variables that are still undefined
	private final Set<SimpleName> toFindDefOf = new HashSet<>();
	
	public Slice() {
		// empty slice
	}

	public Slice(Map<SimpleName, Expression> defsOf, Map<SimpleName, SimpleName> refsTo, Set<SimpleName> toFindDefOf) {
		this.defsOf.putAll(defsOf);
		this.refsTo.putAll(refsTo);
		this.toFindDefOf.addAll(toFindDefOf);
	}

	public Slice(Slice other) {
		this.defsOf.putAll(other.getDefsOf());
		this.refsTo.putAll(other.getRefsTo());
		this.toFindDefOf.addAll(other.getToFindDefOf());
	}

	public void combine(Slice other) {
		if (logger.isTraceEnabled()) {
			logger.trace("Combining slices:");
			this.dump();
			other.dump();
		}
		this.defsOf.putAll(other.getDefsOf());
		this.refsTo.putAll(other.getRefsTo());
		this.toFindDefOf.addAll(other.getToFindDefOf());
	}

	public Map<SimpleName, Expression> getDefsOf() {
		return defsOf;
	}

	public Map<SimpleName, SimpleName> getRefsTo() {
		return refsTo;
	}

	public Set<SimpleName> getToFindDefOf() {
		return toFindDefOf;
	}

	public void dump() {
		logger.trace("Definitions: ");
		for (Entry<SimpleName, Expression> e : defsOf.entrySet()) {
			logger.trace(" {} ({})-> {} ({})", e.getKey(), e.getKey().getStartPosition(), e.getValue(),
					e.getValue().getStartPosition());
		}
		logger.trace("References: ");
		for (Entry<SimpleName, SimpleName> e : refsTo.entrySet()) {
			logger.trace(" {} ({})-> {} ({})", e.getKey(), e.getKey().getStartPosition(), e.getValue(),
					e.getValue().getStartPosition());
		}
	}
}
