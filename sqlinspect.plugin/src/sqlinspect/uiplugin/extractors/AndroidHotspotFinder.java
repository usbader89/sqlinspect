package sqlinspect.uiplugin.extractors;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.uiplugin.Context;
import sqlinspect.uiplugin.PropertyStore;
import sqlinspect.uiplugin.model.Hotspot;
import sqlinspect.uiplugin.model.Query;
import sqlinspect.uiplugin.model.QueryPart;
import sqlinspect.uiplugin.model.UnresolvedQueryPart;
import sqlinspect.uiplugin.utils.ASTStore;
import sqlinspect.uiplugin.utils.ASTUtils;
import sqlinspect.uiplugin.utils.BindingUtil;

public class AndroidHotspotFinder extends ASTVisitor implements IHotspotFinder {

	private final ASTStore astStore = Context.getInstance().getASTStore();

	private CompilationUnit unit;

	private IPath path;

	private static final Logger logger = LoggerFactory.getLogger(AndroidHotspotFinder.class);

	private IQueryResolver resolver;
	private PreparedStatementResolver psResolver;

	public static final String[] DEFAULT_ANDR_EXECSQL = {
			"android.database.sqlite.SQLiteDatabase.execSQL(java.lang.String)" + HotspotDesc.PARAM_SEP + "1",
			"android.database.sqlite.SQLiteDatabase.execSQL(java.lang.String,java.lang.Object[])"
					+ HotspotDesc.PARAM_SEP + "1",
			"android.database.DatabaseUtils.longForQuery(android.database.sqlite.SQLiteDatabase,java.lang.String,java.lang.String[])"
					+ HotspotDesc.PARAM_SEP + "2",
			"android.database.DatabaseUtils.stringForQuery(android.database.sqlite.SQLiteDatabase,java.lang.String,java.lang.String[])"
					+ HotspotDesc.PARAM_SEP + "2" };
	public static final String[] DEFAULT_ANDR_STMT_EXEC = {
			"android.database.sqlite.SQLiteStatement.execute()" + HotspotDesc.PARAM_SEP + "0",
			"android.database.sqlite.SQLiteStatement.executeInsert()" + HotspotDesc.PARAM_SEP + "0",
			"android.database.sqlite.SQLiteStatement.executeUpdateDelete()" + HotspotDesc.PARAM_SEP + "0",
			"android.database.sqlite.SQLiteStatement.simpleQueryForBlobFileDescriptor()" + HotspotDesc.PARAM_SEP + "0",
			"android.database.sqlite.SQLiteStatement.simpleQueryForLong()" + HotspotDesc.PARAM_SEP + "0",
			"android.database.sqlite.SQLiteStatement.simpleQueryForString()" + HotspotDesc.PARAM_SEP + "0",
			"android.database.DatabaseUtils.longForQuery(android.database.sqlite.SQLiteStatement,java.lang.String[])"
					+ HotspotDesc.PARAM_SEP + "1",
			"android.database.DatabaseUtils.stringForQuery(android.database.sqlite.SQLiteStatement,java.lang.String[])"
					+ HotspotDesc.PARAM_SEP + "1" };
	public static final String[] DEFAULT_ANDR_COMPILE_STMT = {
			"android.database.sqlite.SQLiteDatabase.compileStatement(java.lang.String)" + HotspotDesc.PARAM_SEP + "1" };
	public static final String[] DEFAULT_ANDR_CURSOR_RAWQERY = {
			"android.database.sqlite.SQLiteDatabase.rawQuery(java.lang.String,java.lang.String[],android.os.CancellationSignal)"
					+ HotspotDesc.PARAM_SEP + "1",
			"android.database.sqlite.SQLiteDatabase.rawQuery(java.lang.String,java.lang.String[])"
					+ HotspotDesc.PARAM_SEP + "1",
			"android.database.sqlite.SQLiteDatabase.rawQueryWithFactory(android.database.sqlite.SQLiteDatabase.CursorFactory,java.lang.String,java.lang.String,java.lang.String[],java.lang.String,android.os.CancellationSignal)"
					+ HotspotDesc.PARAM_SEP + "2",
			"android.database.sqlite.SQLiteDatabase.rawQueryWithFactory(android.database.sqlite.SQLiteDatabase.CursorFactory,java.lang.String,java.lang.String,java.lang.String[],java.lang.String)"
					+ HotspotDesc.PARAM_SEP + "2" };
	public static final String SIGS_SEP = ";";

	private final Map<String, HotspotDesc> hsMap = new HashMap<>();

	private final List<Hotspot> hotspots = new ArrayList<>();

	@Override
	public void setDescriptors(Collection<HotspotDesc> hsdescs) {
		hsMap.clear();
		for (HotspotDesc hd : hsdescs) {
			hsMap.put(hd.getSignature(), hd);
		}
	}

	public void setPSResolver(PreparedStatementResolver psResolver) {
		this.psResolver = psResolver;
	}

	private void processStatement(MethodInvocation node, HotspotDesc hd) {
		logger.debug("Found query hotspot at {} : {}: {}", unit.getJavaElement().getElementName(),
				ASTUtils.getLineNumber(node), node);

		Hotspot hs = new Hotspot(unit, node, path);
		if (node.arguments().size() < hd.getArgnum()) {
			logger.warn("expected at least {} arguments, but got only {}", hd.getArgnum(), node.arguments().size());
		} else {
			Expression arg = (Expression) node.arguments().get(hd.getArgnum() - 1);
			List<QueryPart> rootparts = resolver.resolve(arg);

			// eliminate duplicated queries
			Map<String, QueryPart> queries = rootparts.stream().filter(Objects::nonNull)
					.collect(Collectors.toMap(QueryPart::getValue, q -> q, (q1, q2) -> q1));

			queries.values().forEach(q -> hs.addQuery(new Query(q)));
		}
		hotspots.add(hs);
	}

	private void processPrepareStatement(MethodInvocation node) {
		logger.debug("Found prepare statement hotspot at line {}: {}", ASTUtils.getLineNumber(node), node);

		Hotspot hs = new Hotspot(unit, node, path);

		List<Expression> exprList = psResolver.resolve(node);
		if (exprList.isEmpty()) {
			Query query = new Query(new UnresolvedQueryPart(node));
			logger.debug("Query: {}", query.getValue());
			hs.addQuery(query);
		} else {
			for (Expression expr : exprList) {

				if (expr != null) {
					List<QueryPart> rootparts = resolver.resolve(expr);

					// eliminate duplicated queries
					Map<String, QueryPart> queries = rootparts.stream().filter(Objects::nonNull)
							.collect(Collectors.toMap(QueryPart::getValue, q -> q, (q1, q2) -> q1));

					queries.values().forEach(q -> hs.addQuery(new Query(q)));
				} else {
					QueryPart qp = new UnresolvedQueryPart(node);
					Query query = new Query(qp);
					hs.addQuery(query);
				}
			}
		}

		hotspots.add(hs);
	}

	@Override
	public boolean visit(MethodInvocation node) {
		IMethodBinding binding = node.resolveMethodBinding();
		if (binding == null) {
			return false;
		}

		String qsig = BindingUtil.qualifiedSignature(binding);

		// check if the method signature is in the list of hotspotdescriptions
		HotspotDesc hd = hsMap.get(qsig);
		if (hd == null) {
			return false;
		}

		if (hd.getArgnum() < 0) {
			logger.error("Illegal argument num for hotspot descriptor: {}", hd);
			return false;
		} else if (hd.getArgnum() == 0) {
			processPrepareStatement(node);
		} else {
			processStatement(node, hd);
		}
		return false;
	}

	@Override
	public Collection<Hotspot> extract(CompilationUnit unit, IPath path, IQueryResolver resolver) {
		hotspots.clear();
		this.path = path;
		this.unit = unit;
		this.resolver = resolver;
		this.unit.accept(this);
		return hotspots;
	}

	@Override
	public String getName() {
		return this.getClass().getSimpleName();
	}

	@Override
	public Collection<Hotspot> extract(ICompilationUnit unit, IQueryResolver resolver) {
		CompilationUnit cu = astStore.get(unit);
		if (cu != null) {
			Collection<Hotspot> ret = extract(cu, unit.getPath(), resolver);
			if (ret.isEmpty()) {
				astStore.remove(unit);
			}
			return ret;
		} else {
			logger.warn("Could not get compilation unit for unit: {}", unit);
			return new ArrayList<>(0);
		}
	}

	private static List<HotspotDesc> getDefaultQueryHotspots() {
		List<HotspotDesc> ret = new ArrayList<>();
		for (String s : DEFAULT_ANDR_EXECSQL) {
			ret.add(HotspotDesc.parse(s));
		}
		for (String s : DEFAULT_ANDR_CURSOR_RAWQERY) {
			ret.add(HotspotDesc.parse(s));
		}
		return ret;
	}

	private static List<HotspotDesc> getQueryHotspots(IProject project) {
		List<HotspotDesc> ret = new ArrayList<>();
		String[] sigs1 = PropertyStore.getStringArrProperty(project, PropertyStore.PROP_ANDR_EXECSQL, SIGS_SEP,
				DEFAULT_ANDR_EXECSQL);
		for (String s : sigs1) {
			ret.add(HotspotDesc.parse(s));
		}
		String[] sigs2 = PropertyStore.getStringArrProperty(project, PropertyStore.PROP_ANDR_CURSOR_RAWQUERY, SIGS_SEP,
				DEFAULT_ANDR_CURSOR_RAWQERY);
		for (String s : sigs2) {
			ret.add(HotspotDesc.parse(s));
		}
		return ret;
	}

	private static List<HotspotDesc> getDefaultPstmtHotspots() {
		List<HotspotDesc> ret = new ArrayList<>();
		for (String s : DEFAULT_ANDR_STMT_EXEC) {
			ret.add(HotspotDesc.parse(s));
		}
		return ret;
	}

	private static List<HotspotDesc> getPstmtHotspots(IProject project) {
		List<HotspotDesc> ret = new ArrayList<>();
		String[] sigs = PropertyStore.getStringArrProperty(project, PropertyStore.PROP_ANDR_STMT_EXEC, SIGS_SEP,
				DEFAULT_ANDR_STMT_EXEC);
		for (String s : sigs) {
			ret.add(HotspotDesc.parse(s));
		}
		return ret;
	}

	public static List<HotspotDesc> getDefaultPsHotspots() {
		List<HotspotDesc> ret = new ArrayList<>();
		for (String s : DEFAULT_ANDR_COMPILE_STMT) {
			ret.add(HotspotDesc.parse(s));
		}
		return ret;
	}

	public static List<HotspotDesc> getPsHotspots(IProject project) {
		List<HotspotDesc> ret = new ArrayList<>();
		String[] sigs = PropertyStore.getStringArrProperty(project, PropertyStore.PROP_ANDR_COMPILE_STMT, SIGS_SEP,
				DEFAULT_ANDR_COMPILE_STMT);
		for (String s : sigs) {
			ret.add(HotspotDesc.parse(s));
		}
		return ret;
	}

	public static List<HotspotDesc> getDefaultHotspots() {
		List<HotspotDesc> ret = getDefaultQueryHotspots();
		ret.addAll(getDefaultPstmtHotspots());
		return ret;
	}

	public static List<HotspotDesc> getHotspots(IProject project) {
		List<HotspotDesc> ret = getQueryHotspots(project);
		ret.addAll(getPstmtHotspots(project));
		return ret;
	}
}
