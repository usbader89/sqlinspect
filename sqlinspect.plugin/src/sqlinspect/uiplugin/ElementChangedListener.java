package sqlinspect.uiplugin;

import java.util.Set;

import org.eclipse.jdt.core.ElementChangedEvent;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IElementChangedListener;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaElementDelta;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.cfg.CFGStore;
import sqlinspect.cfg.MethodDeclVisitor;
import sqlinspect.uiplugin.utils.ASTStore;

public class ElementChangedListener implements IElementChangedListener {

	private static final Logger logger = LoggerFactory.getLogger(ElementChangedListener.class);

	private final ASTStore astStore = Context.getInstance().getASTStore();
	private final CFGStore cfgStore = Context.getInstance().getCFGStore();

	@Override
	public void elementChanged(ElementChangedEvent event) {
		IJavaElementDelta delta = event.getDelta();
		if (delta != null) {
			traverse(delta);
		}
	}

	private void traverse(IJavaElementDelta delta) {
		IJavaElement element = delta.getElement();

		if (element instanceof ICompilationUnit) {
			ICompilationUnit unit = (ICompilationUnit) element;
			logger.debug("Compilation Unit changed: {}", unit.getElementName());
			if (astStore.inStore(unit)) {
				Set<MethodDeclaration> methods = new MethodDeclVisitor().run(astStore.get(unit));
				for (MethodDeclaration decl : methods) {
					cfgStore.remove(decl);
					logger.debug("Removed method from CFG store: {}", decl.getName());
				}
				astStore.remove(unit);
				logger.debug("Removed unit from AST store: {}", unit.getElementName());
			}
		}

		IJavaElementDelta[] children = delta.getAffectedChildren();
		for (IJavaElementDelta child : children) {
			traverse(child);
		}
	}

}
