package sqlinspect.uiplugin.model;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.dom.ASTNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.sqlan.asg.base.SQLRoot;
import sqlinspect.sqlan.asg.common.ASG;
import sqlinspect.sqlan.asg.statm.Statement;
import sqlinspect.uiplugin.metrics.JavaMetrics;
import sqlinspect.uiplugin.smells.common.AndroidSmells;
import sqlinspect.uiplugin.sqlan.SQLAnWrapper;
import sqlinspect.uiplugin.utils.ASTUtils;

/**
 * Project is the highest level unit we represent for an analysis. This is
 * typically related to an eclipse IProject, unless the analysis is executed
 * from command line, when we don't have an eclipse project.
 * 
 * A project has its on SQL Analyzer holding the SQL ASG.
 */
public final class Project {
	private static final Logger logger = LoggerFactory.getLogger(Project.class);

	private final List<Hotspot> hotspots = new ArrayList<>();
	private final Map<ICompilationUnit, List<Hotspot>> cuHotspots = new HashMap<>();
	private final String name;
	private final IProject iProject;
	private final SQLAnWrapper sqlan = new SQLAnWrapper(this);
	private final Map<Statement, Query> queryMap = new HashMap<>();
	private final JavaMetrics metrics = new JavaMetrics(this);
	private final AndroidSmells androidSmells = new AndroidSmells(this);
	private boolean analyzed;

	private static final String UTF8 = "UTF-8";
	private static final String NL = "\n";

	public Project(String name) {
		this.name = name;
		this.iProject = null;
	}

	public Project(String name, IProject iproj) {
		this.name = name;
		this.iProject = iproj;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Project)) {
			return false;
		}
		Project other = (Project) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}

	public void addHotspot(Hotspot hotspot) {
		if (hotspot.getProject() == null) {
			hotspot.setProject(this);
			hotspots.add(hotspot);
			ASTNode node = hotspot.getExec();
			if (node != null) {
				ICompilationUnit cu = ASTUtils.getICompilationUnit(node);
				List<Hotspot> cuHs = cuHotspots.computeIfAbsent(cu, k -> new ArrayList<>());
				cuHs.add(hotspot);
			}

			logger.debug("Hotspot added to project! {}", hotspot);
		} else {
			logger.error("Hotspot already has a project! {}", hotspot);
		}
	}

	public void addHotspots(Collection<Hotspot> hotspots) {
		if (hotspots != null) {
			for (Hotspot hs : hotspots) {
				addHotspot(hs);
			}
		}
	}

	/**
	 * @return the hotspots of the project
	 */
	public List<Hotspot> getHotspots() {
		return hotspots;
	}

	public List<Hotspot> getHotspots(ICompilationUnit cu) {
		return cuHotspots.get(cu);
	}

	public List<Hotspot> findHotspots(IPath path, int startLine) {
		List<Hotspot> ret = new ArrayList<>();
		for (Hotspot hs : hotspots) {
			IPath hsPath = hs.getPath();
			if (hsPath.equals(path) && hs.getStartLine() == startLine) {
				ret.add(hs);
			}
		}
		return ret;
	}

	public void clear() {
		cuHotspots.clear();
		hotspots.clear();
		queryMap.clear();
		metrics.clear();
		androidSmells.clear();
		sqlan.clear();
	}

	/**
	 * @return all the queries in all the hotspots of the project
	 */
	public List<Query> getQueries() {
		List<Query> ret = new ArrayList<>();
		for (Hotspot hs : hotspots) {
			ret.addAll(hs.getQueries());
		}
		return ret;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the corresponding IProject, null if not available
	 */
	public IProject getIProject() {
		return iProject;
	}

	/**
	 * @return the corresponding IJavaProject, null if not available
	 */
	public IJavaProject getIJavaProject() {
		return iProject != null ? JavaCore.create(iProject) : null;
	}

	public SQLRoot getSQLRoot() {
		ASG asg = sqlan.getASG();
		return asg == null ? null : asg.getRoot();
	}

	/**
	 * @return the SQLAn wrapper
	 */
	public SQLAnWrapper getSQLAn() {
		return sqlan;
	}

	/**
	 * @return the queryMap
	 */
	public Map<Statement, Query> getQueryMap() {
		return queryMap;
	}

	public void setAnalyzed(boolean analyzed) {
		this.analyzed = analyzed;
	}

	public boolean isAnalyzed() {
		return analyzed;
	}

	public JavaMetrics getJavaMetrics() {
		return metrics;
	}

	public void dumpStats(File f) {
		try (OutputStreamWriter ow = new OutputStreamWriter(new FileOutputStream(f), UTF8);
				BufferedWriter bw = new BufferedWriter(ow);) {
			bw.write("Project,Hotspots,Queries,AVG(Queries),MAX(Queries)" + NL);
			int lhotspots = getHotspots().size();
			int maxQuery = 0;
			int totalQuery = 0;
			for (Hotspot hs : getHotspots()) {
				int qnum = hs.getQueries().size();
				if (qnum > maxQuery) {
					maxQuery = qnum;
				}
				totalQuery += qnum;
			}
			bw.write(getName() + "," + getHotspots().size() + "," + totalQuery + ","
					+ (lhotspots > 0 ? totalQuery / lhotspots : 0) + "," + maxQuery);
		} catch (IOException e) {
			logger.error("Error while dumping statistics: ", e);
		}
	}

	public AndroidSmells getAndroidSmells() {
		return androidSmells;
	}
}
