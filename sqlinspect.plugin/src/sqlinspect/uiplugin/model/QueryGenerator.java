package sqlinspect.uiplugin.model;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

import org.eclipse.jdt.core.dom.ASTNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.uiplugin.Context;

public class QueryGenerator {
	private static final Logger logger = LoggerFactory.getLogger(QueryGenerator.class);
	private final QueryPartFactory qpf = Context.getInstance().getModel().getQPFactory();

	// QueryPart -> { {QP1,QP2,QP3}, {QP1,QP2,QP4}, ... }
	private final List<LinkedList<QueryPart>> queries = new ArrayList<>(qpf.getSize());
	private final List<PathDesc> pathdescs = new ArrayList<>();
	private final QueryPart root;

	private final Deque<QueryPart> nexts = new ArrayDeque<>();

	// QueryPart -> visited
	// this is okay that the size of this array is only qpf.size()
	// newly inserted nodes, will not be visited
	private int[] visited;
	private boolean[] inque;

	private static class PathDesc {
		private final Set<Branch> branches;

		@SuppressWarnings("unchecked")
		public PathDesc(PathDesc p2) {
			this.branches = (HashSet<Branch>) ((HashSet<Branch>) p2.branches).clone();
		}

		public PathDesc() {
			branches = new HashSet<>();
		}

		/*
		 * true if the two paths share at least one common branch condition
		 */
		public static boolean branchIntersect(PathDesc p1, PathDesc p2) {
			for (Branch br2 : p2.branches) {
				for (Branch br1 : p1.branches) {
					if (br1.node.equals(br2.node)) {
						return true;
					}
				}
			}
			return false;
		}

		/*
		 * true if the left side contains all the branches of the right side
		 */
		public static boolean fullBranchIntersect(PathDesc p1, PathDesc p2) {
			return p1.branches.containsAll(p2.branches);
		}
	}

	private class Branch {
		public final ASTNode node;
		public final int branchNo;
		private static final int PRIME = 31;

		public Branch(ASTNode node, int branchNo) {
			super();
			this.node = node;
			this.branchNo = branchNo;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			int result = 1;
			result = PRIME * result + getOuterType().hashCode();
			result = PRIME * result + branchNo;
			result = PRIME * result + ((node == null) ? 0 : node.hashCode());
			return result;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (!(obj instanceof Branch)) {
				return false;
			}
			Branch other = (Branch) obj;
			if (!getOuterType().equals(other.getOuterType())) {
				return false;
			}
			if (branchNo != other.branchNo) {
				return false;
			}
			if (node == null) {
				if (other.node != null) {
					return false;
				}
			} else if (!node.equals(other.node)) {
				return false;
			}
			return true;
		}

		private QueryGenerator getOuterType() {
			return QueryGenerator.this;
		}
	}

	public QueryGenerator(QueryPart root) {
		this.root = root;
	}

	public List<QueryPart> getQueries() {
		return queries.get(root.getId());
	}

	public void generate(QueryPart root) {
		logger.debug("Generate queries for {}: {}", root.getId(), root);

		visited = new int[qpf.getSize()];
		inque = new boolean[qpf.getSize()];

		nexts.add(root);
		nexts.add(root);
		inque[root.getId()] = true;

		// PreOrder algorithm to visit the subtree
		while (!nexts.isEmpty()) {
			QueryPart node = nexts.pollFirst();
			int visit = visited[node.getId()]++;

			if (visit > 1) {
				continue;
			}

			if (logger.isTraceEnabled()) {
				logger.trace("Visit node: {}({}) {}", node.getId(), visit, node);
				StringBuilder buff = new StringBuilder("Nexts: ");
				for (QueryPart q : nexts) {
					buff.append(Integer.toString(q.getId())).append(' ');
				}
				logger.trace(buff.toString());
			}

			// leaf nodes, we visit them once and just add them to their paths
			if (node instanceof SimpleQueryPart || node instanceof UnresolvedQueryPart) {
				if (visit == 0) {
					initPaths(node.getId(), node);
				}
			} else if (node instanceof CompoundQueryPart || node instanceof MultiQueryPart
					|| node instanceof RefQueryPart) {
				if (visit == 0) {
					initPaths(node.getId());
					addChildrenToNexts(node);
				} else {
					generateSubQueries(node);
				}
			} else {
				throw new IllegalArgumentException("Unhandled QueryPart type: " + node.getClass().getName());
			}
		}

		// free resources
		visited = null;
		inque = null;
		nexts.clear();
		pathdescs.clear();
	}

	private void addChildrenToNexts(QueryPart node) {
		List<QueryPart> children = node.getChildren();
		ListIterator<QueryPart> li = children.listIterator(children.size());
		while (li.hasPrevious()) {
			QueryPart child = li.previous();
			if (!inque[child.getId()] && visited[child.getId()] == 0) {
				nexts.addFirst(child);
				nexts.addFirst(child);
				inque[child.getId()] = true;
			}
		}
	}

	private void generateSubQueries(QueryPart node) {
		if (node instanceof MultiQueryPart) {
			generateSubQueries((MultiQueryPart) node);
		} else if (node instanceof CompoundQueryPart) {
			generateSubQueries((CompoundQueryPart) node);
		} else if (node instanceof RefQueryPart) {
			generateSubQueries((RefQueryPart) node);
		} else {
			throw new IllegalArgumentException("Unhandled QueryPart type: " + node.getClass().getName());
		}
	}

	private void generateSubQueries(MultiQueryPart node) {
		// logger.debug("Lift node: " + node.getId() + " " + node.toString());
		LinkedList<QueryPart> subqueries = queries.get(node.getId());
		List<QueryPart> subparts = node.getChildren();
		for (int i = 0; i < subparts.size(); i++) {
			QueryPart child = subparts.get(i);
			for (QueryPart q : queries.get(child.getId())) {
				PathDesc pd = pathdescs.get(q.getId());
				subqueries.add(q);
				pd.branches.add(new Branch(node.getNode(), i));
			}
		}
	}

	private void generateSubQueries(CompoundQueryPart node) {
		// logger.debug("Lift node: " + node.getId() + " " + node.toString());

		ArrayList<LinkedList<QueryPart>> newQueries = new ArrayList<>();
		ArrayList<PathDesc> newPaths = new ArrayList<>();
		for (QueryPart subpart : node.getChildren()) {
			LinkedList<QueryPart> subpartSubqueries = queries.get(subpart.getId());
			ArrayList<LinkedList<QueryPart>> newQueries2 = new ArrayList<>();
			ArrayList<PathDesc> newPaths2 = new ArrayList<>();
			for (QueryPart subpartSubquery : subpartSubqueries) {
				if (newQueries.isEmpty()) {
					LinkedList<QueryPart> newQuery2 = new LinkedList<>();
					newQuery2.add(subpartSubquery);
					newQueries2.add(newQuery2);
					PathDesc subpartSubqueryPD = pathdescs.get(subpartSubquery.getId());
					newPaths2.add(subpartSubqueryPD);
				} else {
					for (int i = 0; i < newQueries.size(); i++) {
						LinkedList<QueryPart> newQuery = newQueries.get(i);
						PathDesc newQueryPath = newPaths.get(i);
						PathDesc subpartSubqueryPD = pathdescs.get(subpartSubquery.getId());
						boolean intersect = PathDesc.branchIntersect(newQueryPath, subpartSubqueryPD);
						if (!intersect || PathDesc.fullBranchIntersect(newQueryPath, subpartSubqueryPD)) {
							LinkedList<QueryPart> newQuery2 = new LinkedList<>(newQuery);
							newQuery2.add(subpartSubquery);
							newQueries2.add(newQuery2);
							PathDesc newPath2 = new PathDesc(newPaths.get(i));
							newPath2.branches.addAll(subpartSubqueryPD.branches);
							newPaths2.add(newPath2);
						}
					}
				}
			}
			newQueries = newQueries2;
			newPaths = newPaths2;
		}

		LinkedList<QueryPart> nodeQueries = queries.get(node.getId());
		for (int i = 0; i < newQueries.size(); i++) {
			LinkedList<QueryPart> newQuery = newQueries.get(i);
			PathDesc newPD = newPaths.get(i);
			QueryPart newnode = qpf.createCompoundQueryPart(node.getNode(), newQuery);
			initPaths(newnode.getId());
			pathdescs.set(newnode.getId(), newPD);
			nodeQueries.add(newnode);
		}
	}

	private void generateSubQueries(RefQueryPart node) {
		// logger.debug("Lift node: " + node.getId() + " " + node.toString());
		QueryPart ref = node.getRefPart();
		LinkedList<QueryPart> refSubqueries = queries.get(ref.getId());
		LinkedList<QueryPart> subqueries = queries.get(node.getId());
		for (QueryPart p : refSubqueries) {
			RefQueryPart newref = qpf.createRefQueryPart(node.getNode(), p);
			PathDesc refpathdesc = pathdescs.get(p.getId());
			initPaths(newref.getId(), newref);
			subqueries.add(newref);
			PathDesc newpath = pathdescs.get(newref.getId());
			newpath.branches.addAll(refpathdesc.branches);
		}
	}

	private void initPaths(int id) {
		initPaths(id, null);
	}

	private void initPaths(int id, QueryPart part) {
		if (queries.size() <= id) {
			while (queries.size() < id + 1) {
				queries.add(null);
			}
		}
		if (pathdescs.size() <= id) {
			while (pathdescs.size() < id + 1) {
				pathdescs.add(null);
			}
		}

		LinkedList<QueryPart> paths = new LinkedList<>();
		if (part != null) {
			paths.add(part);
		}

		queries.set(id, paths);
		pathdescs.set(id, new PathDesc());
	}
}
