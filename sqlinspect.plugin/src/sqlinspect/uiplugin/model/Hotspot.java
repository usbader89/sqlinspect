package sqlinspect.uiplugin.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.core.runtime.IPath;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.uiplugin.utils.ASTUtils;

public class Hotspot {

	private static final Logger logger = LoggerFactory.getLogger(Hotspot.class);

	/**
	 * The Parent project of the hotspot
	 */
	private Project project;

	/**
	 * The Compilation Unit of the hotspot
	 */
	private final CompilationUnit unit;

	/**
	 * The AST node of the hotspot
	 */
	private final ASTNode exec;

	/**
	 * The path of the compilation unit where the execution point was found.
	 */
	private final IPath path;

	private final List<Query> queries = new ArrayList<>();

	public Hotspot(CompilationUnit unit, ASTNode exec, IPath path) {
		this.project = null;
		this.unit = unit;
		this.exec = exec;
		this.path = path;
	}

	public Hotspot(Project project, CompilationUnit unit, ASTNode exec, IPath path) {
		this.project = project;
		this.unit = unit;
		this.exec = exec;
		this.path = path;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((exec == null) ? 0 : exec.hashCode());
		result = prime * result + ((path == null) ? 0 : path.hashCode());
		result = prime * result + ((project == null) ? 0 : project.hashCode());
		result = prime * result + queries.hashCode();
		result = prime * result + ((unit == null) ? 0 : unit.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Hotspot)) {
			return false;
		}
		Hotspot other = (Hotspot) obj;
		if (exec == null) {
			if (other.exec != null) {
				return false;
			}
		} else if (!exec.equals(other.exec)) {
			return false;
		}
		if (path == null) {
			if (other.path != null) {
				return false;
			}
		} else if (!path.equals(other.path)) {
			return false;
		}
		if (project == null) {
			if (other.project != null) {
				return false;
			}
		} else if (!project.equals(other.project)) {
			return false;
		}
		if (!queries.equals(other.queries)) {
			return false;
		}
		if (unit == null) {
			if (other.unit != null) {
				return false;
			}
		} else if (!unit.equals(other.unit)) {
			return false;
		}
		return true;
	}

	/**
	 * @param project the project to set
	 */
	public void setProject(Project project) {
		this.project = project;
	}

	/**
	 * @return the project
	 */
	public Project getProject() {
		return project;
	}

	/**
	 * @return the unit
	 */
	public CompilationUnit getUnit() {
		return unit;
	}

	/**
	 * @return the exec
	 */
	public ASTNode getExec() {
		return exec;
	}

	/**
	 * @return the path
	 */
	public IPath getPath() {
		return path;
	}

	public int getStartLine() {
		if (exec != null) {
			return ASTUtils.getLineNumber(exec);
		} else {
			return 0;
		}
	}

	public void addQuery(Query q) {
		if (q.getHotspot() == null) {
			queries.add(q);
			q.setHotspot(this);
			logger.debug("Query adedd to hotspot! {}", q);
		} else {
			logger.error("Query already has a hotspot! {}", q);
		}
	}

	public void addQueries(Collection<Query> queries) {
		if (queries != null) {
			for (Query q : queries) {
				addQuery(q);
			}
		}
	}

	public List<Query> getQueries() {
		return queries;
	}
}
