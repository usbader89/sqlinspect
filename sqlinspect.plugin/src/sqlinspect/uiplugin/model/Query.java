package sqlinspect.uiplugin.model;

import java.util.List;
import java.util.regex.Pattern;

import sqlinspect.sqlan.asg.statm.Statement;

public class Query {
	
	/**
	 * The Parent project of the query
	 */
	private Hotspot hotspot;
	
	/**
	 * The Query string
	 */
	private String value;

	/**
	 * The root query part
	 */
	private final QueryPart root;
	
	private List<Statement> statements;
	
	private static final String QUESTION = "{{question$1}}";	
	private static final Pattern questionPattern = Pattern.compile("\\?([0-9]*)");

	public Query(Hotspot hotspot, QueryPart root) {
		this.hotspot = hotspot;
		this.root = root;
	}

	public Query(QueryPart root) {
		this.hotspot = null;
		this.root = root;
	}

	public void setHotspot(Hotspot hotspot) {
		this.hotspot = hotspot;
	}
	
	public Hotspot getHotspot() {
		return hotspot;
	}

	/**
	 * @return the parts
	 */
	public QueryPart getRoot() {
		return root;
	}

	public String replaceSpecialChars(String s) {
		return questionPattern.matcher(s).replaceAll(QUESTION);
	}
	
	/**
	 * @return the parts
	 */
	public String getValue() {
		if (value == null) {
			value = replaceSpecialChars(root.getValue());
		}
		return value;
	}

	/**
	 * @return the statements
	 */
	public List<Statement> getStatements() {
		return statements;
	}

	/**
	 * @param statements the statements to set
	 */
	public void setStatements(List<Statement> statements) {
		this.statements = statements;
	}
}
