package sqlinspect.uiplugin.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IProject;

import sqlinspect.sqlan.asg.base.Base;
import sqlinspect.sqlan.asg.base.SQLRoot;
import sqlinspect.sqlan.asg.common.ASGUtils;
import sqlinspect.uiplugin.Context;

public final class Model {
	private final Map<String, Project> projects;
	
	private final QueryPartFactory qpfactory = new QueryPartFactory();

	public Model() {
		projects = new HashMap<>();
	}

	public Project createProject(String name) {
		return projects.computeIfAbsent(name, Project::new);
	}

	public Project createProject(IProject iproj) {
		Project ret = projects.get(iproj.getName());
		if (ret == null) {
			ret = new Project(iproj.getName(), iproj);
			projects.put(iproj.getName(), ret);
		} else if (!ret.getIProject().equals(iproj)) {
			throw new IllegalArgumentException("Project already exists with the same name: " + ret.getName());
		}
		return ret;
	}

	public Project getProject(String name) {
		return projects.get(name);
	}

	public Project getProject(IProject iproj) {
		for (Map.Entry<String, Project> kp : projects.entrySet()) {
			Project p = kp.getValue();
			if (p.getIProject() == iproj) {
				return p;
			}
		}

		return null;
	}

	public Set<Project> getProjects() {
		Set<Project> ret = new HashSet<>();
		for (Map.Entry<String, Project> kp : projects.entrySet()) {
			ret.add(kp.getValue());
		}
		return ret;
	}

	public List<String> getProjectNames() {
		List<String> projectNames = new ArrayList<>();
		for (Project p : Context.getInstance().getModel().getProjects()) {
			projectNames.add(p.getName());
		}
		return projectNames;
	}

	public Project getParentProject(Base node) {
		SQLRoot root = ASGUtils.getRoot(node);

		if (root != null) {
			for (Map.Entry<String, Project> e : projects.entrySet()) {
				Project p = e.getValue();
				if (p.getSQLRoot().equals(root)) {
					return p;
				}
			}
		}
		return null;
	}
	
	public QueryPartFactory getQPFactory() {
		return qpfactory;
	}
}
