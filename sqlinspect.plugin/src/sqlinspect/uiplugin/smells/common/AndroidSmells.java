package sqlinspect.uiplugin.smells.common;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import sqlinspect.uiplugin.model.Project;
import sqlinspect.uiplugin.smells.detectors.ExecSQLReturnsData;
import sqlinspect.uiplugin.smells.detectors.ExecuteInsertShouldSendInsert;
import sqlinspect.uiplugin.smells.detectors.MoreThanOneColumnInSQLiteStatement;
import sqlinspect.uiplugin.smells.detectors.MoreThanOneStatement;

public class AndroidSmells {
	private final Set<AndroidSmell> smells = new HashSet<>();

	private final Project project;

	public AndroidSmells(Project project) {
		this.project = project;
	}

	public void runSmellDetectors() {
		final List<AndroidSmellDetector> toDetect = new ArrayList<>();
		toDetect.add(new MoreThanOneStatement());
		toDetect.add(new ExecSQLReturnsData());
		toDetect.add(new ExecuteInsertShouldSendInsert());
		toDetect.add(new MoreThanOneColumnInSQLiteStatement());

		toDetect.stream().forEach(sd -> {
			Set<AndroidSmell> detectedSmells = sd.execute(project);
			smells.addAll(detectedSmells);
		});
	}

	public Set<AndroidSmell> getSmells() {
		return smells;
	}

	public void clear() {
		smells.clear();
	}

}
