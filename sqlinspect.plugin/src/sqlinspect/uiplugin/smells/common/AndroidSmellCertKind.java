package sqlinspect.uiplugin.smells.common;

public enum AndroidSmellCertKind {
	HIGH_CERTAINTY,
	NORMAL_CERTAINTY,
	LOW_CERTAINTY
}
