package sqlinspect.uiplugin.smells.common;

import java.util.Objects;

import org.eclipse.jdt.core.dom.ASTNode;

public class AndroidSmell {

	private ASTNode node;

	private AndroidSmellKind kind;

	private String message;

	private AndroidSmellCertKind certainty;

	private String file;

	private int line;

	public AndroidSmell(ASTNode node, AndroidSmellKind kind, String message, AndroidSmellCertKind certainty) {
		this.node = node;
		this.kind = kind;
		this.message = message;
		this.certainty = certainty;
		this.file = "";
		this.line = 0;
	}

	public AndroidSmell(ASTNode node, AndroidSmellKind kind, String message, AndroidSmellCertKind certainty, String file, int line) {
		this.node = node;
		this.kind = kind;
		this.message = message;
		this.certainty = certainty;
		this.file = file;
		this.line = line;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		} else if (o instanceof AndroidSmell) {
			AndroidSmell s = (AndroidSmell) o;
			return Objects.equals(s.getNode(), node) && Objects.equals(s.getKind(), kind)
					&& Objects.equals(s.getFile(), file) && Objects.equals(s.getLine(), line)
					&& Objects.equals(s.getMessage(), message) && Objects.equals(s.getCertainty(), certainty);
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return Objects.hash(node, file, line, kind, certainty);
	}

	/**
	 * @return the node
	 */
	public ASTNode getNode() {
		return node;
	}

	/**
	 * @param node
	 *            the node to set
	 */
	public void setNode(ASTNode node) {
		this.node = node;
	}

	/**
	 * @return the kind
	 */
	public AndroidSmellKind getKind() {
		return kind;
	}

	/**
	 * @param kind
	 *            the kind to set
	 */
	public void setKind(AndroidSmellKind kind) {
		this.kind = kind;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the certainty
	 */
	public AndroidSmellCertKind getCertainty() {
		return certainty;
	}

	/**
	 * @param certainty
	 *            the certainty to set
	 */
	public void setCertainty(AndroidSmellCertKind certainty) {
		this.certainty = certainty;
	}

	/**
	 * @return the file
	 */
	public String getFile() {
		return file;
	}

	/**
	 * @param file
	 *            the file to set
	 */
	public void setFile(String file) {
		this.file = file;
	}

	/**
	 * @return the line
	 */
	public int getLine() {
		return line;
	}

	/**
	 * @param line
	 *            the line to set
	 */
	public void setLine(int line) {
		this.line = line;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AndroidSmell [node=" + node + ", kind=" + kind + " certainty=" + certainty + " message=" + message
				+ "file: " + file + "line: " + line + "]";
	}

}
