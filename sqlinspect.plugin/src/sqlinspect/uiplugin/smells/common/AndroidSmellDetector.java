package sqlinspect.uiplugin.smells.common;

import java.util.Set;

import sqlinspect.uiplugin.model.Project;

public interface AndroidSmellDetector {
	Set<AndroidSmell> execute(Project project);
}
