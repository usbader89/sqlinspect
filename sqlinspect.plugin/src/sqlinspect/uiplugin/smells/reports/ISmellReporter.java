package sqlinspect.uiplugin.smells.reports;

import java.util.Collection;

import sqlinspect.uiplugin.smells.common.AndroidSmell;

public interface ISmellReporter {
	void writeReport(Collection<AndroidSmell> smells);
}
