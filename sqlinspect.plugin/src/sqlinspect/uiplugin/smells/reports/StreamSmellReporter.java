package sqlinspect.uiplugin.smells.reports;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.uiplugin.smells.common.AndroidSmell;
import sqlinspect.uiplugin.utils.ASTUtils;

public class StreamSmellReporter implements ISmellReporter {
	private static final Logger logger = LoggerFactory.getLogger(StreamSmellReporter.class);

	private final OutputStream stream;

	public StreamSmellReporter(OutputStream stream) {
		this.stream = stream;
	}

	@Override
	public void writeReport(Collection<AndroidSmell> smells) {
		try {
			for (AndroidSmell smell : smells) {
				writeSmell(stream, smell);
			}
		} catch (IOException e) {
			logger.error("IO error while writing to stream", e);
		}

	}

	public void writeSmell(OutputStream stream, AndroidSmell smell) throws IOException {
		StringBuilder sb = new StringBuilder();
		String path = ASTUtils.getPath(smell.getNode()).toString();
		int lineNo = ASTUtils.getLineNumber(smell.getNode());
		sb.append(path).append('(').append(lineNo).append("): ").append(smell.getKind().toString()).append('(')
				.append(smell.getCertainty().toString()).append("): ").append(smell.getMessage()).append('\n');

		stream.write(sb.toString().getBytes(Charset.forName("UTF-8")));
	}

}
