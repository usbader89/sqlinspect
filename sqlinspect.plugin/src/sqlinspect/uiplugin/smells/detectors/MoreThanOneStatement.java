package sqlinspect.uiplugin.smells.detectors;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.uiplugin.model.Hotspot;
import sqlinspect.uiplugin.model.Project;
import sqlinspect.uiplugin.model.Query;
import sqlinspect.uiplugin.smells.common.AndroidSmell;
import sqlinspect.uiplugin.smells.common.AndroidSmellCertKind;
import sqlinspect.uiplugin.smells.common.AndroidSmellDetector;
import sqlinspect.uiplugin.smells.common.AndroidSmellKind;
import sqlinspect.uiplugin.utils.ASTUtils;

public class MoreThanOneStatement implements AndroidSmellDetector {

	private static final Logger logger = LoggerFactory.getLogger(MoreThanOneStatement.class);

	private final Set<AndroidSmell> smells = new HashSet<>();

	private static final String MESSAGE = "%s() cannot execute more than one SQL statement.";

	private boolean isExecuteOrRawHotspot(Hotspot hs) {
		String methodName = ASTUtils.getMethodName(hs.getExec());
		return "execSQL".equalsIgnoreCase(methodName) || "rawQuery".equalsIgnoreCase(methodName)
				|| "execute".equalsIgnoreCase(methodName);
	}

	@Override
	public Set<AndroidSmell> execute(Project project) {

		logger.debug("Executing Android Smell detector: {}", getClass().getName());

		for (Hotspot hs : project.getHotspots()) {
			if (isExecuteOrRawHotspot(hs)) {
				for (Query q : hs.getQueries()) {
					int count = q.getStatements().size();
					if (count > 1) {
						String message = String.format(MESSAGE, ASTUtils.getMethodName(hs.getExec()));
						
						smells.add(new AndroidSmell(hs.getExec(), AndroidSmellKind.MORE_THAN_ONE_STATEMENT, message,
								AndroidSmellCertKind.HIGH_CERTAINTY));
					}
				}
			}
		}

		return smells;
	}

}
