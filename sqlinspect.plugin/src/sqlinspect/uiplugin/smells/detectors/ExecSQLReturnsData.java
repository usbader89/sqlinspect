package sqlinspect.uiplugin.smells.detectors;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.sqlan.asg.statm.Delete;
import sqlinspect.sqlan.asg.statm.Insert;
import sqlinspect.sqlan.asg.statm.Select;
import sqlinspect.sqlan.asg.statm.Statement;
import sqlinspect.sqlan.asg.statm.Update;
import sqlinspect.uiplugin.model.Hotspot;
import sqlinspect.uiplugin.model.Project;
import sqlinspect.uiplugin.model.Query;
import sqlinspect.uiplugin.smells.common.AndroidSmell;
import sqlinspect.uiplugin.smells.common.AndroidSmellCertKind;
import sqlinspect.uiplugin.smells.common.AndroidSmellDetector;
import sqlinspect.uiplugin.smells.common.AndroidSmellKind;
import sqlinspect.uiplugin.utils.ASTUtils;

public class ExecSQLReturnsData implements AndroidSmellDetector {

	private static final Logger logger = LoggerFactory.getLogger(ExecSQLReturnsData.class);

	private final Set<AndroidSmell> smells = new HashSet<>();

	private static final String MESSAGE = "the usage of %s is discouraged in %s.";

	private boolean isExecuteHotspot(Hotspot hs) {
		String methodName = ASTUtils.getMethodName(hs.getExec());
		return "execSQL".equalsIgnoreCase(methodName) || "execute".equalsIgnoreCase(methodName);
	}

	@Override
	public Set<AndroidSmell> execute(Project project) {

		logger.debug("Executing Android Smell detector: {}", getClass().getName());

		for (Hotspot hs : project.getHotspots()) {
			if (isExecuteHotspot(hs)) {
				for (Query q : hs.getQueries()) {
					for (Statement stmt : q.getStatements()) {
						if (stmt instanceof Insert || stmt instanceof Update || stmt instanceof Select
								|| stmt instanceof Delete) {
							String message = String.format(MESSAGE, stmt.getNodeKind().toString(),
									ASTUtils.getMethodName(hs.getExec()));

							smells.add(new AndroidSmell(hs.getExec(), AndroidSmellKind.EXEC_SQL_RETURNS_DATA, message,
									AndroidSmellCertKind.HIGH_CERTAINTY));
						}
					}
				}
			}
		}

		return smells;
	}

}
