package sqlinspect.uiplugin.smells.detectors;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.sqlan.asg.expr.BinaryQuery;
import sqlinspect.sqlan.asg.expr.Expression;
import sqlinspect.sqlan.asg.expr.ExpressionList;
import sqlinspect.sqlan.asg.expr.SelectExpression;
import sqlinspect.sqlan.asg.expr.Values;
import sqlinspect.sqlan.asg.statm.Select;
import sqlinspect.sqlan.asg.statm.Statement;
import sqlinspect.uiplugin.model.Hotspot;
import sqlinspect.uiplugin.model.Project;
import sqlinspect.uiplugin.model.Query;
import sqlinspect.uiplugin.smells.common.AndroidSmell;
import sqlinspect.uiplugin.smells.common.AndroidSmellCertKind;
import sqlinspect.uiplugin.smells.common.AndroidSmellDetector;
import sqlinspect.uiplugin.smells.common.AndroidSmellKind;
import sqlinspect.uiplugin.utils.ASTUtils;

public class MoreThanOneColumnInSQLiteStatement implements AndroidSmellDetector {

	private static final Logger logger = LoggerFactory.getLogger(MoreThanOneColumnInSQLiteStatement.class);

	private final Set<AndroidSmell> smells = new HashSet<>();

	private static final String MESSAGE = "An SQL statement in SQLIteStatement cannot return multiple rows or columns, but single value (1 x 1) result sets are supported.";

	private boolean isSQLiteStatementCall(Hotspot hs) {
		String qualifiedName = ASTUtils.getMethodQualifiedName(hs.getExec());
		return qualifiedName != null && qualifiedName.startsWith("android.database.sqlite.SQLiteStatement");
	}

	@Override
	public Set<AndroidSmell> execute(Project project) {

		logger.debug("Executing Android Smell detector: {}", getClass().getName());

		for (Hotspot hs : project.getHotspots()) {
			if (isSQLiteStatementCall(hs)) {
				for (Query q : hs.getQueries()) {
					for (Statement stmt : q.getStatements()) {
						int count = countStatementColumns(stmt);
						if (count > 1) {
							smells.add(new AndroidSmell(hs.getExec(),
									AndroidSmellKind.MORE_THAN_ONE_COLUMN_IN_SQLITESTATEMENT, MESSAGE,
									AndroidSmellCertKind.HIGH_CERTAINTY));
						}
					}
				}
			}
		}

		return smells;
	}

	private int countColumns(SelectExpression sel) {
		Expression columnList = sel.getColumnList();
		if (columnList instanceof ExpressionList) {
			return ((ExpressionList) columnList).getExpressions().size();
		}
		return 0;
	}

	private int countColumns(Values val) {
		Expression columnList = val.getList();
		if (columnList instanceof ExpressionList) {
			return ((ExpressionList) columnList).getExpressions().size();
		}
		return 0;
	}

	private int countColumns(sqlinspect.sqlan.asg.expr.Query query) {
		if (query instanceof SelectExpression) {
			return countColumns((SelectExpression) query);
		} else if (query instanceof Values) {
			return countColumns((Values) query);
		} else if (query instanceof BinaryQuery) {
			BinaryQuery bq = (BinaryQuery) query;
			if (bq.getLeft() != null) {
				return countColumns(bq.getLeft());
			} else if (bq.getRight() != null) {
				return countColumns(bq.getRight());
			}
		}
		return 0;
	}

	private int countStatementColumns(Statement stmt) {
		if (stmt instanceof Select) {
			Select select = (Select) stmt;
			return countColumns(select.getQuery());
		}
		return 0;
	}

}
