package sqlinspect.uiplugin.smells.detectors;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.sqlan.asg.statm.Insert;
import sqlinspect.sqlan.asg.statm.ParserError;
import sqlinspect.sqlan.asg.statm.Statement;
import sqlinspect.uiplugin.model.Hotspot;
import sqlinspect.uiplugin.model.Project;
import sqlinspect.uiplugin.model.Query;
import sqlinspect.uiplugin.smells.common.AndroidSmell;
import sqlinspect.uiplugin.smells.common.AndroidSmellCertKind;
import sqlinspect.uiplugin.smells.common.AndroidSmellDetector;
import sqlinspect.uiplugin.smells.common.AndroidSmellKind;
import sqlinspect.uiplugin.utils.ASTUtils;

public class ExecuteInsertShouldSendInsert implements AndroidSmellDetector {

	private static final Logger logger = LoggerFactory.getLogger(ExecuteInsertShouldSendInsert.class);

	private final Set<AndroidSmell> smells = new HashSet<>();

	private static final String MESSAGE = "executeInsert() should send an INSERT to be a useful call.";

	private boolean isExecuteHotspot(Hotspot hs) {
		String methodName = ASTUtils.getMethodName(hs.getExec());
		return "executeInsert".equalsIgnoreCase(methodName);
	}

	@Override
	public Set<AndroidSmell> execute(Project project) {

		logger.debug("Executing Android Smell detector: {}", getClass().getName());

		for (Hotspot hs : project.getHotspots()) {
			if (isExecuteHotspot(hs)) {
				for (Query q : hs.getQueries()) {
					for (Statement stmt : q.getStatements()) {
						if (stmt != null && !(stmt instanceof Insert) && !(stmt instanceof ParserError)) {
							smells.add(
									new AndroidSmell(hs.getExec(), AndroidSmellKind.EXECUTE_INSERT_SHOULD_SEND_INSERT,
											MESSAGE, AndroidSmellCertKind.HIGH_CERTAINTY));
						}
					}
				}
			}
		}

		return smells;
	}

}
