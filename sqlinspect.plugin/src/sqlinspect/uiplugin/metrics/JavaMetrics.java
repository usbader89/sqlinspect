package sqlinspect.uiplugin.metrics;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.jdt.core.IJavaElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.uiplugin.model.Project;

public class JavaMetrics {
	private static final Logger logger = LoggerFactory.getLogger(JavaMetrics.class);

	private final Map<IJavaElement, Map<MetricDesc, Integer>> metrics = new HashMap<>();

	private final Project project;

	public JavaMetrics(Project project) {
		this.project = project;
	}

	public void calculate() {
		logger.info("Calculating metrics for project: {}", project.getName());
		metrics.clear();

		List<AbstractJavaMetric> toCalculate = new ArrayList<>();
		toCalculate.add(new NumberOfHotspots());
		toCalculate.add(new NumberOfQueries());
		toCalculate.add(new NumberOfInserts());
		toCalculate.add(new NumberOfDeletes());
		toCalculate.add(new NumberOfUpdates());
		toCalculate.add(new NumberOfTableAccesses());
		toCalculate.add(new NumberOfColumnAccesses());

		for (AbstractJavaMetric jm : toCalculate) {
			final Map<IJavaElement, Map<MetricDesc, Integer>> results = jm.calculate(project);
			for (Entry<IJavaElement, Map<MetricDesc, Integer>> e : results.entrySet()) {
				Map<MetricDesc, Integer> elemMetrics = metrics.get(e.getKey());
				for (Entry<MetricDesc, Integer> m : e.getValue().entrySet()) {
					if (elemMetrics == null) {
						elemMetrics = new EnumMap<>(MetricDesc.class);
						metrics.put(e.getKey(), elemMetrics);
					}
					elemMetrics.put(m.getKey(), m.getValue());
				}
			}
		}

		logger.info("Calculating metrics done.");
	}

	public Map<IJavaElement, Map<MetricDesc, Integer>> getMetrics() {
		return metrics;
	}

	public Integer getMetric(IJavaElement element, MetricDesc metric) {
		Map<MetricDesc, Integer> mets = metrics.get(element);
		if (mets != null) {
			return mets.get(metric);
		}
		return null;
	}

	public boolean report(String reportFile, String reportFormat) {
		XMLJavaMetricsReporter report;
		if ("XML".equalsIgnoreCase(reportFormat)) {
			report = new XMLJavaMetricsReporter(reportFile, metrics);
			report.writeReport(project.getIJavaProject());
			return true;
		} else {
			return false;
		}
	}

	public void clear() {
		metrics.clear();
	}
}
