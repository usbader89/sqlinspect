package sqlinspect.uiplugin.metrics;

import sqlinspect.sqlan.asg.statm.Select;
import sqlinspect.sqlan.asg.statm.Statement;

public final class NumberOfSelects extends NumberOfStmt {
	public static final MetricDesc desc = MetricDesc.NumberOfSelects;	

	@Override
	protected boolean isOfStmt(Statement stmt) {
		return stmt instanceof Select;
	}

	@Override
	protected MetricDesc getDesc() {
		return desc;
	}
	
}