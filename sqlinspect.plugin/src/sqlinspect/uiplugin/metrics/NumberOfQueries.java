package sqlinspect.uiplugin.metrics;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.dom.ASTNode;

import sqlinspect.uiplugin.model.Hotspot;
import sqlinspect.uiplugin.model.Project;
import sqlinspect.uiplugin.utils.ASTUtils;

public final class NumberOfQueries extends AbstractJavaMetric {
	public static final MetricDesc desc = MetricDesc.NumberOfQueries;

	@Override
	public Map<IJavaElement, Map<MetricDesc, Integer>> calculate(Project project) {
		logger.info("Calculating for {}", getClass().getSimpleName());
		Map<IJavaElement, Map<MetricDesc, Integer>> metrics = new HashMap<>();
		for (Hotspot hs : project.getHotspots()) {
			ASTNode node = hs.getExec();
			if (node != null) {
				ICompilationUnit cu = ASTUtils.getICompilationUnit(node);
				if (cu != null) {
					incMetrics(metrics, cu, desc, hs.getQueries().size());
				}
			}
		}
		return metrics;
	}
}
