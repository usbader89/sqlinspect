package sqlinspect.uiplugin.metrics;

import sqlinspect.sqlan.asg.statm.Statement;
import sqlinspect.sqlan.asg.statm.Update;

public final class NumberOfUpdates extends NumberOfStmt {

	public static final MetricDesc desc = MetricDesc.NumberOfUpdates;
	

	@Override
	protected boolean isOfStmt(Statement stmt) {
		return stmt instanceof Update;
	}


	@Override
	protected MetricDesc getDesc() {
		return desc;
	}
}