package sqlinspect.uiplugin.metrics;

public enum MetricDesc {
	NumberOfHotspots("NOS", "Hotspots", "Number of Database Access Hotspots"),
	NumberOfQueries("NOQ", "Queries", "Number of Queries"), 
	NumberOfInserts("NOI", "Inserts", "Number of Insert Statements"), 
	NumberOfSelects("NOSe", "Selects", "Number of Select Statements"), 
	NumberOfUpdates("NOU", "Updates", "Number of Update Statements"), 
	NumberOfDeletes("NOD", "Deletes", "Number of Delete Statements"), 
	NumberOfTableAccesses("NOT", "Tables","Number of Tables Accessed"),
	NumberOfColumnAccesses("NOC", "Columns", "Number of Columns Accessed");

	private final String id;
	private final String shortName;
	private final String description;

	MetricDesc(String id, String shortName, String description) {
		this.id = id;
		this.shortName = shortName;
		this.description = description;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the shortName
	 */
	public String getShortName() {
		return shortName;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
}