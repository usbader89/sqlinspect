package sqlinspect.uiplugin.metrics;

import sqlinspect.sqlan.asg.statm.Delete;
import sqlinspect.sqlan.asg.statm.Statement;

public final class NumberOfDeletes extends NumberOfStmt {
	public static final MetricDesc desc = MetricDesc.NumberOfDeletes;

	@Override
	protected boolean isOfStmt(Statement stmt) {
		return stmt instanceof Delete;
	}

	@Override
	protected MetricDesc getDesc() {
		return desc;
	}
}