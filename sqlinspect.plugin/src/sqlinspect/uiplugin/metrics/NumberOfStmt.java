package sqlinspect.uiplugin.metrics;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.dom.ASTNode;

import sqlinspect.sqlan.asg.statm.Statement;
import sqlinspect.uiplugin.model.Hotspot;
import sqlinspect.uiplugin.model.Project;
import sqlinspect.uiplugin.model.Query;
import sqlinspect.uiplugin.utils.ASTUtils;

public abstract class NumberOfStmt extends AbstractJavaMetric {

	protected abstract MetricDesc getDesc();

	private int countStmtOfHotspot(Hotspot hs) {
		int ret = 0;
		for (Query q : hs.getQueries()) {
			for (Statement stmt : q.getStatements()) {
				if (isOfStmt(stmt)) {
					++ret;
				}
			}
		}
		return ret;
	}

	protected abstract boolean isOfStmt(Statement stmt);

	@Override
	public Map<IJavaElement, Map<MetricDesc, Integer>> calculate(Project project) {
		logger.info("Calculating for {}", getClass().getSimpleName());
		Map<IJavaElement, Map<MetricDesc, Integer>> metrics = new HashMap<>();
		for (Hotspot hs : project.getHotspots()) {
			ASTNode node = hs.getExec();
			if (node != null) {
				ICompilationUnit cu = ASTUtils.getICompilationUnit(node);
				if (cu != null) {
					incMetrics(metrics, cu, getDesc(), countStmtOfHotspot(hs));
				}
			}
		}
		return metrics;
	}
}