package sqlinspect.uiplugin.metrics;

import sqlinspect.sqlan.asg.statm.Insert;
import sqlinspect.sqlan.asg.statm.Statement;

public final class NumberOfInserts extends NumberOfStmt {
	public static final MetricDesc desc = MetricDesc.NumberOfInserts;

	@Override
	protected boolean isOfStmt(Statement stmt) {
		return stmt instanceof Insert;
	}

	@Override
	protected MetricDesc getDesc() {
		return desc;
	}
}