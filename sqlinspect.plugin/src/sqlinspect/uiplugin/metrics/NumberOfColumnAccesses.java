package sqlinspect.uiplugin.metrics;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.dom.ASTNode;

import sqlinspect.sqlan.asg.schema.Column;
import sqlinspect.sqlan.asg.statm.Statement;
import sqlinspect.uiplugin.model.Project;
import sqlinspect.uiplugin.model.Query;
import sqlinspect.uiplugin.utils.ASTUtils;

public final class NumberOfColumnAccesses extends AbstractJavaMetric {

	public static final MetricDesc desc = MetricDesc.NumberOfColumnAccesses;

	private void addColumns(Map<ICompilationUnit, Set<Column>> cuColumns, Query q, Set<Column> cols) {
		ASTNode node = q.getHotspot().getExec();
		if (node != null) {
			ICompilationUnit cu = ASTUtils.getICompilationUnit(node);
			if (cu != null) {
				Set<Column> hsCols = cuColumns.computeIfAbsent(cu, k -> new HashSet<>());
				hsCols.addAll(cols);
			}
		}
	}

	@Override
	public Map<IJavaElement, Map<MetricDesc, Integer>> calculate(Project project) {
		logger.info("Calculating for {}", getClass().getSimpleName());
		Map<IJavaElement, Map<MetricDesc, Integer>> metrics = new HashMap<>();

		Map<Statement, Map<Column, Integer>> columnAcc = project.getSQLAn().getColumnAcc();
		Map<ICompilationUnit, Set<Column>> cuColumns = new HashMap<>();
		for (Map.Entry<Statement, Map<Column, Integer>> e : columnAcc.entrySet()) {
			Statement stmt = e.getKey();
			if (stmt != null) {
				Set<Column> cols = e.getValue().keySet();
				Query q = project.getQueryMap().get(stmt);
				if (q != null) {
					addColumns(cuColumns, q, cols);
				}
			}
		}

		for (Map.Entry<ICompilationUnit, Set<Column>> he : cuColumns.entrySet()) {
			incMetrics(metrics, he.getKey(), desc, he.getValue().size());
		}
		return metrics;
	}
}
