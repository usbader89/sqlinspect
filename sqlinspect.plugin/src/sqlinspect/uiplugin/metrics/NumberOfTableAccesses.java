package sqlinspect.uiplugin.metrics;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.dom.ASTNode;

import sqlinspect.sqlan.asg.schema.Table;
import sqlinspect.sqlan.asg.statm.Statement;
import sqlinspect.uiplugin.model.Project;
import sqlinspect.uiplugin.model.Query;
import sqlinspect.uiplugin.utils.ASTUtils;

public final class NumberOfTableAccesses extends AbstractJavaMetric {
	public static final MetricDesc desc = MetricDesc.NumberOfTableAccesses;

	private void addTables(Map<ICompilationUnit, Set<Table>> cuTables, Query q, Set<Table> tables) {
		ASTNode node = q.getHotspot().getExec();
		if (node != null) {
			ICompilationUnit cu = ASTUtils.getICompilationUnit(node);
			if (cu != null) {
				Set<Table> hsTabs = cuTables.computeIfAbsent(cu, k -> new HashSet<>());
				hsTabs.addAll(tables);
			}
		}
	}

	@Override
	public Map<IJavaElement, Map<MetricDesc, Integer>> calculate(Project project) {
		logger.info("Calculating for {}", getClass().getSimpleName());
		Map<IJavaElement, Map<MetricDesc, Integer>> metrics = new HashMap<>();

		Map<Statement, Map<Table, Integer>> tableAcc = project.getSQLAn().getTableAcc();
		Map<ICompilationUnit, Set<Table>> cuTables = new HashMap<>();
		for (Map.Entry<Statement, Map<Table, Integer>> e : tableAcc.entrySet()) {
			Statement stmt = e.getKey();
			if (stmt != null) {
				Set<Table> tabs = e.getValue().keySet();
				Query q = project.getQueryMap().get(stmt);
				if (q != null) {
					addTables(cuTables, q, tabs);
				}
			}
		}

		for (Map.Entry<ICompilationUnit, Set<Table>> he : cuTables.entrySet()) {
			incMetrics(metrics, he.getKey(), desc, he.getValue().size());
		}

		return metrics;
	}
}
