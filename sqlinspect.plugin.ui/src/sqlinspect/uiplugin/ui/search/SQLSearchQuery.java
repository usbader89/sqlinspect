package sqlinspect.uiplugin.ui.search;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.search.ui.ISearchQuery;
import org.eclipse.search.ui.ISearchResult;
import org.eclipse.search.ui.text.Match;

import sqlinspect.sqlan.asg.statm.Statement;
import sqlinspect.uiplugin.Activator;
import sqlinspect.uiplugin.Context;
import sqlinspect.uiplugin.model.Project;
import sqlinspect.uiplugin.model.Query;

public class SQLSearchQuery implements ISearchQuery {

	private final QuerySearchResult result = new QuerySearchResult(this);

	private final String sql;

	private final Set<Project> projectScope = new HashSet<>();

	private static final String MESSAGE_SUCCESS = "Search completed successfully";
	private static final String MESSAGE_ERROR_NO_QUERY = "Error, no query was specified!";

	private final boolean ignoreLiterals;
	private final boolean ignoreNames;
	private final boolean ignoreRefs;

	public SQLSearchQuery(String sql, boolean ignoreLiterals, boolean ignoreNames, boolean ignoreRefs) {
		this.sql = sql;
		this.ignoreLiterals = ignoreLiterals;
		this.ignoreNames = ignoreNames;
		this.ignoreRefs = ignoreRefs;
	}

	@Override
	public boolean canRerun() {
		return true;
	}

	@Override
	public boolean canRunInBackground() {
		return true;
	}

	@Override
	public String getLabel() {
		if (sql != null) {
			StringBuilder sb = new StringBuilder();
			sb.append("Matches for '").append(sql).append('\'');
			if (!projectScope.isEmpty()) {
				sb.append(" under '");
				Project[] projects = projectScope.toArray(new Project[projectScope.size()]);
				for (int i = 0; i < projects.length - 1; i++) {
					sb.append(projects[i].getIProject().getName()).append(", ");
				}
				sb.append(projects[projects.length - 1].getIProject().getName()).append('\'');
			}
			return sb.toString();
		} else {
			return "Query search";
		}
	}

	@Override
	public ISearchResult getSearchResult() {
		return result;
	}

	@Override
	public IStatus run(IProgressMonitor monitor) {
		if (sql == null || sql.isEmpty()) {
			String message = MESSAGE_ERROR_NO_QUERY;
			return new Status(IStatus.ERROR, Activator.PLUGIN_ID, 0, message, null);
		}

		Project[] projects = projectScope.isEmpty()
				? Context.getInstance().getModel().getProjects()
						.toArray(new Project[Context.getInstance().getModel().getProjects().size()])
				: projectScope.toArray(new Project[projectScope.size()]);

		for (Project proj : projects) {
			List<Statement> ret = proj.getSQLAn().locateQuery(sql, ignoreLiterals, ignoreNames, ignoreRefs);
			if (ret != null) {
				for (Statement stmt : ret) {
					Query q = proj.getQueryMap().get(stmt);
					if (q != null) {
						result.addMatch(new Match(q, 0, 0));
					}
				}
			}
		}

		return new Status(IStatus.OK, Activator.PLUGIN_ID, 0, MESSAGE_SUCCESS, null);
	}

	public Set<Project> getProjectScope() {
		return projectScope;
	}
}
