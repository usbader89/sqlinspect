package sqlinspect.uiplugin.ui.search;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.dialogs.DialogPage;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.search.ui.ISearchPage;
import org.eclipse.search.ui.ISearchPageContainer;
import org.eclipse.search.ui.NewSearchUI;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkingSet;

import sqlinspect.uiplugin.Context;
import sqlinspect.uiplugin.model.Project;

public class SQLSearchPage extends DialogPage implements ISearchPage {

	private ISearchPageContainer container;

	private static final String DEFAULT_SQL = "SELECT * FROM t;";

	private Text sqlTextField;

	private Button ignoreLiteralsCheck;

	private Button ignoreNamesCheck;

	private Button ignoreRefsCheck;

	private static final int GRID_COLS = 2;

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets.
	 * Composite)
	 */
	@Override
	public void createControl(Composite parent) {
		Composite composite = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		layout.numColumns = GRID_COLS;
		composite.setLayout(layout);

		Label l = new Label(composite, SWT.NONE);
		l.setText("SQL query:");
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = GRID_COLS;
		l.setLayoutData(gd);

		sqlTextField = new Text(composite, SWT.MULTI | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);

		gd = new GridData(GridData.FILL_BOTH);
		gd.widthHint = 150;
		gd.heightHint = 100;
		gd.horizontalSpan = GRID_COLS;
		sqlTextField.setLayoutData(gd);
		sqlTextField.setText(DEFAULT_SQL);

		ignoreLiteralsCheck = new Button(composite, SWT.CHECK);
		ignoreLiteralsCheck.setText("Ignore literal values");

		ignoreNamesCheck = new Button(composite, SWT.CHECK);
		ignoreNamesCheck.setText("Ignore names");

		ignoreRefsCheck = new Button(composite, SWT.CHECK);
		ignoreRefsCheck.setText("Ignore identifier references");

		setControl(composite);
	}

	private void addProject(Set<Project> projects, IProject iproj) {
		if (iproj != null) {
			Project proj = Context.getInstance().getModel().getProject(iproj);
			if (proj != null) {
				projects.add(proj);
			}
		}
	}

	private Set<Project> projectsInScope() {
		Set<Project> ret = new HashSet<>();
		switch (getContainer().getSelectedScope()) {
		case ISearchPageContainer.WORKSPACE_SCOPE:
			// add all projects analyzed before
			ret.addAll(Context.getInstance().getModel().getProjects());
			break;
		case ISearchPageContainer.SELECTION_SCOPE:
			if (getContainer().getActiveEditorInput() != null) {
				IFile file = getContainer().getActiveEditorInput().getAdapter(IFile.class);
				if (file != null && file.exists()) {
					addProject(ret, file.getProject());
				}
			} else {
				ISelection selection = getContainer().getSelection();
				if (selection instanceof IStructuredSelection && !selection.isEmpty()) {
					for (Object element : ((IStructuredSelection) selection).toArray()) {
						if (element instanceof IProject) {
							addProject(ret, (IProject) element);
						} else if (element instanceof IResource) {
							addProject(ret, ((IResource) element).getProject());
						} else if (element instanceof IJavaProject) {
							addProject(ret, ((IJavaProject) element).getProject());
						} else if (element instanceof IJavaElement) {
							IJavaProject jp = ((IJavaElement) element).getJavaProject();
							if (jp != null) {
								addProject(ret, jp.getProject());
							}
						}
					}
				}
			}
			break;
		case ISearchPageContainer.SELECTED_PROJECTS_SCOPE:
			String[] projectNames = getContainer().getSelectedProjectNames();
			if (projectNames != null) {
				IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
				for (String pn : projectNames) {
					addProject(ret, root.getProject(pn));
				}
			}
			break;
		case ISearchPageContainer.WORKING_SET_SCOPE:
			IWorkingSet[] workingSets = getContainer().getSelectedWorkingSets();
			if (workingSets != null && workingSets.length > 0) {
				for (IWorkingSet ws : workingSets) {
					for (IAdaptable adaptable : ws.getElements()) {
						if (adaptable instanceof IResource) {
							addProject(ret, ((IResource) adaptable).getProject());
						}
					}
				}
			}
			break;
		default:
			// do nothing
			break;
		}
		return ret;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.eclipse.search.ui.ISearchPage#performAction()
	 */
	@Override
	public boolean performAction() {
		boolean ignoreLiterals = ignoreLiteralsCheck.getSelection();
		boolean ignoreNames = ignoreNamesCheck.getSelection();
		boolean ignoreRefs = ignoreRefsCheck.getSelection();
		SQLSearchQuery query = new SQLSearchQuery(sqlTextField.getText(), ignoreLiterals, ignoreNames, ignoreRefs);
		query.getProjectScope().addAll(projectsInScope());
		NewSearchUI.runQueryInForeground(container.getRunnableContext(), query);
		return true;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.eclipse.search.ui.ISearchPage#setContainer(org.eclipse.search.ui.
	 * ISearchPageContainer)
	 */
	@Override
	public void setContainer(ISearchPageContainer spc) {
		this.container = spc;
	}

	private ISearchPageContainer getContainer() {
		return container;
	}
}
