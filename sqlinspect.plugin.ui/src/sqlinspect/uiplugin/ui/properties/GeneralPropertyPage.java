package sqlinspect.uiplugin.ui.properties;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.ui.dialogs.PropertyPage;
import org.eclipse.ui.preferences.IWorkbenchPreferenceContainer;

import sqlinspect.uiplugin.Activator;

public class GeneralPropertyPage extends PropertyPage {

	/*
	 * (non-Javadoc)
	 *
	 * @see org.eclipse.jface.preference.PreferencePage#createContents(org.eclipse.
	 * swt.widgets.Composite)
	 */
	@Override
	protected Control createContents(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		composite.setLayout(new GridLayout());

		Label l = new Label(composite, SWT.WRAP);
		GridData data = new GridData(SWT.FILL, SWT.BEGINNING, true, false);
		data.widthHint = 300;
		l.setLayoutData(data);

		l.setText("Project-specific settings of the " + Activator.PLUGIN_ID
				+ " plugin can be configured under the subpages of the property page: ");
		Link link = new Link(composite, SWT.NONE);
		link.setText("  - <a>'SQL Analyzer'</a> - settings of the SQL parser, SQL analyses.");
		link.setLayoutData(data);
		link.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				IWorkbenchPreferenceContainer container = (IWorkbenchPreferenceContainer) getContainer();
				container.openPage(SQLAnPropertyPage.PROPERTY_PAGE_ID, null);
			}
		});

		link = new Link(composite, SWT.NONE);
		link.setText("  - <a>'SQL Extractor'</a> - settings of SQL extractor algorithms.");
		link.setLayoutData(data);
		link.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				IWorkbenchPreferenceContainer container = (IWorkbenchPreferenceContainer) getContainer();
				container.openPage(SQLExtractorPropertyPage.PROPERTY_PAGE_ID, null);
			}
		});

		noDefaultAndApplyButton();
		Dialog.applyDialogFont(composite);
		return composite;
	}

}
