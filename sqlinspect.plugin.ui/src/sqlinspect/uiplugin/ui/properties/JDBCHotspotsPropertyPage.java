package sqlinspect.uiplugin.ui.properties;

import java.util.Arrays;

import org.eclipse.core.resources.IProject;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.PropertyPage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.uiplugin.PropertyStore;
import sqlinspect.uiplugin.extractors.JDBCHotspotFinder;

public class JDBCHotspotsPropertyPage extends PropertyPage {

	private static final Logger logger = LoggerFactory.getLogger(JDBCHotspotsPropertyPage.class);

	private Text queriesHsTextField;
	private Text pstmtHsTextField;
	private Text pstmtTextField;

	/*
	 * (non-Javadoc)
	 *
	 * @see org.eclipse.jface.preference.PreferencePage#createContents(org.eclipse.
	 * swt.widgets.Composite)
	 */
	@Override
	protected Control createContents(Composite parent) {
		final IProject project = getElement().getAdapter(IProject.class);

		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayoutData(new GridData(GridData.FILL_BOTH));
		GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		composite.setLayout(layout);

		Label l = new Label(composite, SWT.NONE);
		l.setText("Statement execute signatures:");

		queriesHsTextField = new Text(composite, SWT.MULTI | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);

		GridData gd = new GridData(GridData.FILL_BOTH);
		gd.minimumWidth = 200;
		gd.minimumHeight = 100;
		queriesHsTextField.setLayoutData(gd);
		queriesHsTextField.setText(
				String.join("\n", PropertyStore.getStringArrProperty(project, PropertyStore.PROP_JDBC_QUERY_HOTSPOTS,
						JDBCHotspotFinder.SIGS_SEP, JDBCHotspotFinder.DEFAULT_JDBC_STMT_EXEC)));

		l = new Label(composite, SWT.NONE);
		l.setText("PreparedStatement execute signatures:");

		pstmtHsTextField = new Text(composite, SWT.MULTI | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);

		gd = new GridData(GridData.FILL_BOTH);
		gd.minimumWidth = 200;
		gd.minimumHeight = 100;
		pstmtHsTextField.setLayoutData(gd);
		pstmtHsTextField.setText(
				String.join("\n", PropertyStore.getStringArrProperty(project, PropertyStore.PROP_JDBC_PSTMT_HOTSPOTS,
						JDBCHotspotFinder.SIGS_SEP, JDBCHotspotFinder.DEFAULT_JDBC_PSTMT_EXEC)));

		l = new Label(composite, SWT.NONE);
		l.setText("Prepare statement signatures:");

		pstmtTextField = new Text(composite, SWT.MULTI | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);

		gd = new GridData(GridData.FILL_BOTH);
		gd.minimumWidth = 200;
		gd.minimumHeight = 100;
		pstmtTextField.setLayoutData(gd);
		pstmtTextField
				.setText(String.join("\n", PropertyStore.getStringArrProperty(project, PropertyStore.PROP_JDBC_PSTMT,
						JDBCHotspotFinder.SIGS_SEP, JDBCHotspotFinder.DEFAULT_JDBC_PSTMT_PREPARE)));

		return null;
	}

	@Override
	public boolean performOk() {
		final IProject project = getElement().getAdapter(IProject.class);

		if (project == null) {
			logger.error("Could not determine project!");
			return super.performCancel();
		}

		PropertyStore.setStringArrProperty(project, PropertyStore.PROP_JDBC_QUERY_HOTSPOTS, JDBCHotspotFinder.SIGS_SEP,
				queriesHsTextField.getText().split("\\n"));
		PropertyStore.setStringArrProperty(project, PropertyStore.PROP_JDBC_PSTMT_HOTSPOTS, JDBCHotspotFinder.SIGS_SEP,
				pstmtHsTextField.getText().split("\\n"));
		PropertyStore.setStringArrProperty(project, PropertyStore.PROP_JDBC_PSTMT, JDBCHotspotFinder.SIGS_SEP,
				pstmtTextField.getText().split("\\n"));

		return super.performOk();
	}

	@Override
	protected void performDefaults() {
		queriesHsTextField.setText(String.join("\n", JDBCHotspotFinder.DEFAULT_JDBC_STMT_EXEC));
		pstmtHsTextField.setText(String.join("\n", JDBCHotspotFinder.DEFAULT_JDBC_PSTMT_EXEC));
		pstmtTextField.setText(String.join("\n", JDBCHotspotFinder.DEFAULT_JDBC_PSTMT_PREPARE));
	}

	public static void addStatementHotspotToProject(IProject project, String hsDesc) {
		String[] hsArr = PropertyStore.getStringArrProperty(project, PropertyStore.PROP_JDBC_QUERY_HOTSPOTS,
				JDBCHotspotFinder.SIGS_SEP, JDBCHotspotFinder.DEFAULT_JDBC_STMT_EXEC);
		int n = hsArr.length;
		hsArr = Arrays.copyOf(hsArr, n + 1);
		hsArr[n] = hsDesc;
		PropertyStore.setStringArrProperty(project, PropertyStore.PROP_JDBC_QUERY_HOTSPOTS, JDBCHotspotFinder.SIGS_SEP,
				hsArr);
	}

	public static void addPreparedStatementHotspotToProject(IProject project, String hsDesc) {
		String[] hsArr = PropertyStore.getStringArrProperty(project, PropertyStore.PROP_JDBC_PSTMT_HOTSPOTS,
				JDBCHotspotFinder.SIGS_SEP, JDBCHotspotFinder.DEFAULT_JDBC_PSTMT_EXEC);
		int n = hsArr.length;
		hsArr = Arrays.copyOf(hsArr, n + 1);
		hsArr[n] = hsDesc;
		PropertyStore.setStringArrProperty(project, PropertyStore.PROP_JDBC_PSTMT_HOTSPOTS, JDBCHotspotFinder.SIGS_SEP,
				hsArr);
	}

	public static void addPrepareHotspotToProject(IProject project, String hsDesc) {
		String[] hsArr = PropertyStore.getStringArrProperty(project, PropertyStore.PROP_JDBC_PSTMT,
				JDBCHotspotFinder.SIGS_SEP, JDBCHotspotFinder.DEFAULT_JDBC_PSTMT_PREPARE);
		int n = hsArr.length;
		hsArr = Arrays.copyOf(hsArr, n + 1);
		hsArr[n] = hsDesc;
		PropertyStore.setStringArrProperty(project, PropertyStore.PROP_JDBC_PSTMT, JDBCHotspotFinder.SIGS_SEP, hsArr);
	}
}
