package sqlinspect.uiplugin.ui.properties;

import java.util.Arrays;

import org.eclipse.core.resources.IProject;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.PropertyPage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.sqlan.SQLDialect;
import sqlinspect.uiplugin.PropertyStore;

public class SQLAnPropertyPage extends PropertyPage {

	private static final Logger logger = LoggerFactory.getLogger(SQLAnPropertyPage.class);

	private Combo combo;
	private Text dbUserTextField;
	private Text dbPasswordTextField;
	private Text dbURLTextField;
	private Text dbSchemaTextField;
	private Button loadSchemaFromDBCheckBox;
	private Button loadSchemaFromFileCheckBox;
	private Button runSmellDetectorsCheckBox;
	private Button runTableAccessCheckBox;
	private Button runSQLMetricsCheckBox;

	private Button dumpStatsCheckBox;

	private static final String[] FILTER_NAMES = { "SQL Files (*.sql)", "All Files (*.*)" };

	// These filter extensions are used to filter which files are displayed.
	private static final String[] FILTER_EXTS = { "*.sql", "*.*" };

	public static final String PROPERTY_PAGE_ID = "sqlinspect.propertypages.SQLAn";

	private static final int GRID_COLS = 3;

	/*
	 * (non-Javadoc)
	 *
	 * @see org.eclipse.jface.preference.PreferencePage#createContents(org.eclipse.
	 * swt.widgets.Composite)
	 */
	@Override
	protected Control createContents(Composite parent) {
		final IProject project = getElement().getAdapter(IProject.class);

		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		GridLayout layout = new GridLayout();
		layout.numColumns = GRID_COLS;
		composite.setLayout(layout);

		createSQLDialectSelector(composite, project);

		createSeparator(composite);

		createDatabaseConnectionContent(composite, project);

		createSeparator(composite);

		createAnalyzerSettings(composite, project);

		return composite;
	}

	private void createSQLDialectSelector(Composite composite, IProject project) {
		Label l = new Label(composite, SWT.NONE);
		l.setText("SQL dialect:");

		combo = new Combo(composite, SWT.READ_ONLY);
		for (SQLDialect d : SQLDialect.values()) {
			combo.add(d.toString());
		}
		combo.select(Arrays.asList(combo.getItems()).indexOf(
				PropertyStore.getStringProperty(project, PropertyStore.PROP_DIALECT, PropertyStore.DEFAULT_DIALECT)));

		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = GRID_COLS - 1;
		gd.minimumWidth = 150;
		combo.setLayoutData(gd);
	}

	private void createSeparator(Composite composite) {
		Label sep = new Label(composite, SWT.SEPARATOR | SWT.SHADOW_OUT | SWT.HORIZONTAL);
		GridData gd = new GridData(SWT.FILL, SWT.CENTER, true, false);
		gd.horizontalSpan = GRID_COLS;
		sep.setLayoutData(gd);
	}

	private void createDatabaseConnectionContent(final Composite composite, IProject project) {
		loadSchemaFromDBCheckBox = new Button(composite, SWT.CHECK);
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = 3;
		loadSchemaFromDBCheckBox.setLayoutData(gd);
		loadSchemaFromDBCheckBox.setText("Load schema from database: ");
		loadSchemaFromDBCheckBox.setSelection(PropertyStore.getBooleanProperty(project,
				PropertyStore.PROP_LOAD_SCHEMA_FROM_DB, PropertyStore.DEFAULT_LOAD_SCHEMA_FROM_DB));
		loadSchemaFromDBCheckBox.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				Button btn = (Button) event.getSource();
				selectSchemaFromDB(btn.getSelection());
			}
		});

		Label l = new Label(composite, SWT.NONE);
		l.setText("DB URL:");
		dbURLTextField = new Text(composite, SWT.BORDER);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = GRID_COLS - 1;
		gd.minimumWidth = 150;
		dbURLTextField.setLayoutData(gd);
		dbURLTextField.setText(
				PropertyStore.getStringProperty(project, PropertyStore.PROP_DB_URL, PropertyStore.DEFAULT_DB_URL));

		l = new Label(composite, SWT.NONE);
		l.setText("DB User:");
		dbUserTextField = new Text(composite, SWT.BORDER);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = GRID_COLS - 1;
		gd.minimumWidth = 150;
		dbUserTextField.setLayoutData(gd);
		dbUserTextField.setText(
				PropertyStore.getStringProperty(project, PropertyStore.PROP_DB_USER, PropertyStore.DEFAULT_DB_USER));

		l = new Label(composite, SWT.NONE);
		l.setText("DB Password:");
		dbPasswordTextField = new Text(composite, SWT.PASSWORD | SWT.BORDER);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = GRID_COLS - 1;
		gd.minimumWidth = 150;
		dbPasswordTextField.setLayoutData(gd);
		dbPasswordTextField.setText(
				PropertyStore.getStringProperty(project, PropertyStore.PROP_DB_PWD, PropertyStore.DEFAULT_DB_PWD));

		loadSchemaFromFileCheckBox = new Button(composite, SWT.CHECK);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = GRID_COLS;
		loadSchemaFromFileCheckBox.setLayoutData(gd);
		loadSchemaFromFileCheckBox.setText("Load schema from file: ");
		loadSchemaFromFileCheckBox.setSelection(PropertyStore.getBooleanProperty(project,
				PropertyStore.PROP_LOAD_SCHEMA_FROM_FILE, PropertyStore.DEFAULT_LOAD_SCHEMA_FROM_FILE));
		loadSchemaFromFileCheckBox.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				Button btn = (Button) event.getSource();
				selectSchemaFromFile(btn.getSelection());
			}
		});

		l = new Label(composite, SWT.NONE);
		l.setText("Schema file:");
		dbSchemaTextField = new Text(composite, SWT.BORDER | SWT.MULTI | SWT.WRAP);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.minimumWidth = 150;
		dbSchemaTextField.setLayoutData(gd);
		dbSchemaTextField.setText(PropertyStore.getStringProperty(project, PropertyStore.PROP_DB_SCHEMA,
				PropertyStore.DEFAULT_DB_SCHEMA));

		Button fileButton = new Button(composite, SWT.PUSH);
		fileButton.setText("Browse");
		fileButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				// User has selected to open a single file
				FileDialog dlg = new FileDialog(composite.getShell(), SWT.OPEN);
				dlg.setFilterNames(FILTER_NAMES);
				dlg.setFilterExtensions(FILTER_EXTS);
				String fn = dlg.open();
				if (fn != null) {
					dbSchemaTextField.setText(fn);
				}
			}
		});

		// select/deselect fields
		selectSchemaFromFile(PropertyStore.getBooleanProperty(project, PropertyStore.PROP_LOAD_SCHEMA_FROM_FILE,
				PropertyStore.DEFAULT_LOAD_SCHEMA_FROM_FILE));
		selectSchemaFromDB(PropertyStore.getBooleanProperty(project, PropertyStore.PROP_LOAD_SCHEMA_FROM_DB,
				PropertyStore.DEFAULT_LOAD_SCHEMA_FROM_DB));
	}

	private void createAnalyzerSettings(Composite composite, IProject project) {
		runSmellDetectorsCheckBox = new Button(composite, SWT.CHECK);
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = GRID_COLS;
		runSmellDetectorsCheckBox.setLayoutData(gd);
		runSmellDetectorsCheckBox.setText("Run smell detectors");
		runSmellDetectorsCheckBox.setSelection(PropertyStore.getBooleanProperty(project,
				PropertyStore.PROP_RUN_SMELL_DETECTORS, PropertyStore.DEFAULT_RUN_SMELL_DETECTOR));

		runTableAccessCheckBox = new Button(composite, SWT.CHECK);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = GRID_COLS;
		runTableAccessCheckBox.setLayoutData(gd);
		runTableAccessCheckBox.setText("Run table/column access analysis");
		runTableAccessCheckBox.setSelection(PropertyStore.getBooleanProperty(project,
				PropertyStore.PROP_RUN_TABLE_ACCESS, PropertyStore.DEFAULT_RUN_TABLE_ACCESS));

		runSQLMetricsCheckBox = new Button(composite, SWT.CHECK);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = GRID_COLS;
		runSQLMetricsCheckBox.setLayoutData(gd);
		runSQLMetricsCheckBox.setText("Calculate query metrics");
		runSQLMetricsCheckBox.setSelection(PropertyStore.getBooleanProperty(project, PropertyStore.PROP_RUN_SQL_METRICS,
				PropertyStore.DEFAULT_RUN_SQL_METRICS));

		dumpStatsCheckBox = new Button(composite, SWT.CHECK);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = GRID_COLS;
		dumpStatsCheckBox.setLayoutData(gd);
		dumpStatsCheckBox.setText("Dump analyzer statistics");
		dumpStatsCheckBox.setSelection(PropertyStore.getBooleanProperty(project, PropertyStore.PROP_DUMP_STATISTICS,
				PropertyStore.DEFAULT_DUMP_STATISTICS));
	}

	private void selectSchemaFromDB(boolean enabled) {
		if (enabled) {
			dbURLTextField.setEnabled(true);
			dbUserTextField.setEnabled(true);
			dbPasswordTextField.setEnabled(true);
			loadSchemaFromFileCheckBox.setSelection(false);
			loadSchemaFromFileCheckBox.setEnabled(false);
		} else {
			dbURLTextField.setEnabled(false);
			dbUserTextField.setEnabled(false);
			dbPasswordTextField.setEnabled(false);
			loadSchemaFromFileCheckBox.setEnabled(true);
		}
	}

	private void selectSchemaFromFile(boolean enabled) {
		if (enabled) {
			dbSchemaTextField.setEnabled(true);
			loadSchemaFromDBCheckBox.setSelection(false);
			loadSchemaFromDBCheckBox.setEnabled(false);
		} else {
			dbSchemaTextField.setEnabled(false);
			loadSchemaFromDBCheckBox.setEnabled(true);
		}
	}

	@Override
	public boolean performOk() {
		final IProject project = getElement().getAdapter(IProject.class);

		if (project == null) {
			logger.error("Could not determine project!");
			return super.performCancel();
		}

		PropertyStore.setStringProperty(project, PropertyStore.PROP_DIALECT, combo.getText());
		PropertyStore.setStringProperty(project, PropertyStore.PROP_DB_SCHEMA, dbSchemaTextField.getText());
		PropertyStore.setStringProperty(project, PropertyStore.PROP_DB_USER, dbUserTextField.getText());
		PropertyStore.setStringProperty(project, PropertyStore.PROP_DB_PWD, dbPasswordTextField.getText());
		PropertyStore.setStringProperty(project, PropertyStore.PROP_DB_URL, dbURLTextField.getText());
		PropertyStore.setBooleanProperty(project, PropertyStore.PROP_LOAD_SCHEMA_FROM_DB,
				loadSchemaFromDBCheckBox.getSelection());
		PropertyStore.setBooleanProperty(project, PropertyStore.PROP_LOAD_SCHEMA_FROM_FILE,
				loadSchemaFromFileCheckBox.getSelection());
		PropertyStore.setBooleanProperty(project, PropertyStore.PROP_RUN_SMELL_DETECTORS,
				runSmellDetectorsCheckBox.getSelection());
		PropertyStore.setBooleanProperty(project, PropertyStore.PROP_RUN_TABLE_ACCESS,
				runTableAccessCheckBox.getSelection());
		PropertyStore.setBooleanProperty(project, PropertyStore.PROP_RUN_SQL_METRICS,
				runSQLMetricsCheckBox.getSelection());
		PropertyStore.setBooleanProperty(project, PropertyStore.PROP_DUMP_STATISTICS, dumpStatsCheckBox.getSelection());

		return super.performOk();
	}

}
