package sqlinspect.uiplugin.ui.properties;

import java.util.Arrays;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.dialogs.PropertyPage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.uiplugin.Context;
import sqlinspect.uiplugin.PropertyStore;
import sqlinspect.uiplugin.extractors.IHotspotFinder;
import sqlinspect.uiplugin.extractors.IQueryResolver;

public class SQLExtractorPropertyPage extends PropertyPage {

	public static final String PROPERTY_PAGE_ID = "sqlinspect.propertypages.SQLExtractor";

	private static final Logger logger = LoggerFactory.getLogger(SQLExtractorPropertyPage.class);

	private static final int GRID_COLS = 3;

	private Combo hsCombo;
	private Combo srCombo;

	/*
	 * (non-Javadoc)
	 *
	 * @see org.eclipse.jface.preference.PreferencePage#createContents(org.eclipse.
	 * swt.widgets.Composite)
	 */
	@Override
	protected Control createContents(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		GridLayout layout = new GridLayout();
		layout.numColumns = GRID_COLS;
		composite.setLayout(layout);

		final IProject project = getElement().getAdapter(IProject.class);

		createHotspotFinder(composite, project);

		createStringResolver(composite, project);

		return composite;
	}

	private void createHotspotFinder(Composite composite, IProject project) {
		Label l = new Label(composite, SWT.NONE);
		l.setText("Hotspot finder: ");

		hsCombo = new Combo(composite, SWT.READ_ONLY);
		List<IHotspotFinder> hsFinders = Context.getInstance().getHotspotFinders();

		for (IHotspotFinder hs : hsFinders) {
			hsCombo.add(hs.getName());
		}
		hsCombo.select(Arrays.asList(hsCombo.getItems()).indexOf(PropertyStore.getStringProperty(project,
				PropertyStore.PROP_HOTSPOT_FINDER, PropertyStore.DEFAULT_HOTSPOT_FINDER)));

		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = GRID_COLS - 1;
		gd.widthHint = 150;
		hsCombo.setLayoutData(gd);

	}

	private void createStringResolver(Composite composite, IProject project) {
		Label l = new Label(composite, SWT.NONE);
		l.setText("Query resolver: ");

		srCombo = new Combo(composite, SWT.READ_ONLY);
		List<IQueryResolver> srs = Context.getInstance().getStringResolvers();

		for (IQueryResolver sr : srs) {
			srCombo.add(sr.getName());
		}
		srCombo.select(Arrays.asList(srCombo.getItems()).indexOf(PropertyStore.getStringProperty(project,
				PropertyStore.PROP_STRING_RESOLVER, PropertyStore.DEFAULT_STRING_RESOLVER)));

		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = GRID_COLS - 1;
		gd.widthHint = 150;
		srCombo.setLayoutData(gd);
	}

	@Override
	public boolean performOk() {
		final IProject project = getElement().getAdapter(IProject.class);
		if (project == null) {
			logger.error("Could not determine project!");
			return super.performCancel();
		}

		PropertyStore.setStringProperty(project, PropertyStore.PROP_STRING_RESOLVER, srCombo.getText());
		PropertyStore.setStringProperty(project, PropertyStore.PROP_HOTSPOT_FINDER, hsCombo.getText());

		return super.performOk();
	}

}
