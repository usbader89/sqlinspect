package sqlinspect.uiplugin.ui.properties;

import java.util.Arrays;

import org.eclipse.core.resources.IProject;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.PropertyPage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.uiplugin.PropertyStore;
import sqlinspect.uiplugin.extractors.LoopHandling;

public class InterSRPropertyPage extends PropertyPage {

	public static final String PROPERTY_PAGE_ID = "sqlinspect.propertypages.InterSR";

	private static final Logger logger = LoggerFactory.getLogger(InterSRPropertyPage.class);

	private static final int GRID_COLS = 2;

	private Text cfgdepthTextField;

	private Text calldepthTextField;

	private Combo combo;

	/*
	 * (non-Javadoc)
	 *
	 * @see org.eclipse.jface.preference.PreferencePage#createContents(org.eclipse.
	 * swt.widgets.Composite)
	 */
	@Override
	protected Control createContents(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		GridLayout layout = new GridLayout();
		layout.numColumns = GRID_COLS;
		composite.setLayout(layout);

		final IProject project = getElement().getAdapter(IProject.class);

		Label l = new Label(composite, SWT.NONE);
		l.setText("Max. CFG depth: ");

		cfgdepthTextField = new Text(composite, SWT.BORDER);
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.widthHint = 150;
		cfgdepthTextField.setLayoutData(gd);
		cfgdepthTextField.setText(Integer.toString(
				PropertyStore.getIntProperty(project, PropertyStore.PROP_INTERSR_MAXNL, PropertyStore.DEFAULT_INTERSR_MAXNL)));

		l = new Label(composite, SWT.NONE);
		l.setText("Max. call depth: ");
		
		calldepthTextField = new Text(composite, SWT.BORDER);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.widthHint = 150;
		calldepthTextField.setLayoutData(gd);
		calldepthTextField.setText(Integer.toString(
				PropertyStore.getIntProperty(project, PropertyStore.PROP_INTERSR_MAXCALLDEPTH, PropertyStore.DEFAULT_INTERSR_MAXCALLDEPTH)));		

		l = new Label(composite, SWT.NONE);
		l.setText("Loop handling: ");

		combo = new Combo(composite, SWT.READ_ONLY);
		for (LoopHandling lh : LoopHandling.values()) {
			combo.add(lh.toString());
		}
		combo.select(Arrays.asList(combo.getItems()).indexOf(PropertyStore.getStringProperty(project,
				PropertyStore.PROP_INTERSR_LOOPHANDLING, PropertyStore.DEFAULT_INTERSR_LOOPHANDLING)));

		return composite;
	}

	@Override
	public boolean performOk() {
		final IProject project = getElement().getAdapter(IProject.class);
		if (project == null) {
			logger.error("Could not determine project!");
			return super.performCancel();
		}

		PropertyStore.setIntProperty(project, PropertyStore.PROP_INTERSR_MAXNL, Integer.parseInt(cfgdepthTextField.getText()));
		PropertyStore.setIntProperty(project, PropertyStore.PROP_INTERSR_MAXCALLDEPTH, Integer.parseInt(calldepthTextField.getText()));
		PropertyStore.setStringProperty(project, PropertyStore.PROP_INTERSR_LOOPHANDLING, combo.getText());

		return super.performOk();
	}

}
