package sqlinspect.uiplugin.ui.views;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.ui.JavaElementComparator;
import org.eclipse.jdt.ui.JavaElementLabelProvider;
import org.eclipse.jdt.ui.StandardJavaElementContentProvider;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.viewers.CellLabelProvider;
import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider;
import org.eclipse.jface.viewers.IBaseLabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.TreeColumn;

import sqlinspect.sqlan.asg.schema.Column;
import sqlinspect.sqlan.asg.schema.Table;
import sqlinspect.sqlan.asg.statm.Statement;
import sqlinspect.uiplugin.Context;
import sqlinspect.uiplugin.metrics.MetricDesc;
import sqlinspect.uiplugin.model.Hotspot;
import sqlinspect.uiplugin.model.Model;
import sqlinspect.uiplugin.model.Project;
import sqlinspect.uiplugin.model.Query;
import sqlinspect.uiplugin.utils.ASTUtils;
import sqlinspect.uiplugin.utils.ImageUtility;

public class MetricsView extends AbstractSQLInspectViewer {

	public static final String ID = "sqlinspect.ui.views.sqlinspect.MetricsView";

	public static final Object[] NO_ELEMENTS = {};

	private final Model model = Context.getInstance().getModel();

	private static final class MetricsViewLabelProvider extends JavaElementLabelProvider {
		public MetricsViewLabelProvider(int style) {
			super(style);
		}

		@Override
		public Image getImage(Object element) {
			if (element instanceof Hotspot) {
				return ImageUtility.getIcon(ImageUtility.DATA_BACKUP);
			} else if (element instanceof IJavaElement) {
				return super.getImage(element);
			} else {
				return null;
			}
		}

		@Override
		public StyledString getStyledText(Object element) {
			if (element instanceof Hotspot) {
				Hotspot hs = (Hotspot) element;
				ASTNode exec = hs.getExec();
				if (exec != null) {
					ICompilationUnit unit = ASTUtils.getICompilationUnit(hs.getExec());
					if (unit != null) {
						return new StyledString(unit.getElementName() + ":" + hs.getStartLine());
					}
				}
				return new StyledString("");
			} else if (element instanceof Query) {
				Query q = (Query) element;
				return new StyledString(q.getValue());
			} else if (element instanceof IJavaElement) {
				return super.getStyledText(element);
			} else {
				return null;
			}
		}
	}

	private static final class ViewContentProvider implements ITreeContentProvider {
		private final StandardJavaElementContentProvider standardProvider = new StandardJavaElementContentProvider();
		private final Model model = Context.getInstance().getModel();

		@Override
		public Object[] getChildren(Object element) {
			if (element instanceof ICompilationUnit) {
				ICompilationUnit unit = (ICompilationUnit) element;
				Project proj = model.getProject(unit.getJavaProject().getProject());
				if (proj != null) {
					List<Hotspot> hs = proj.getHotspots(unit);
					return hs.toArray();
				}
			} else if (element instanceof Hotspot) {
				Hotspot hs = (Hotspot) element;
				List<Query> queries = hs.getQueries();
				if (queries != null) {
					return queries.toArray();
				}
			} else if (element instanceof IJavaElement) {
				List<IJavaElement> ret = new ArrayList<>();
				for (Object retElement : standardProvider.getChildren(element)) {
					if (retElement instanceof IJavaElement) {
						IJavaElement retJavaElement = (IJavaElement) retElement;
						IJavaProject javaProject = retJavaElement.getJavaProject();
						Project proj = model.getProject(javaProject.getProject());

						Object m = proj.getJavaMetrics().getMetrics().get(retJavaElement);
						if (m != null) {
							ret.add(retJavaElement);
						}
					}
				}
				return ret.toArray();
			}
			return new Object[0];
		}

		@Override
		public Object[] getElements(Object input) {
			if (input instanceof Model) {
				Model m = (Model) input;
				List<IJavaElement> ret = new ArrayList<>();
				if (m.getProjects() != null) {
					for (Project proj : m.getProjects()) {
						ret.add(proj.getIJavaProject());
					}
				}
				return ret.toArray();
			} else {
				return new Object[0];
			}
		}

		@Override
		public Object getParent(Object element) {
			if (element instanceof Model) {
				return null;
			} else if (element instanceof Hotspot) {
				Hotspot hs = (Hotspot) element;
				return hs.getProject().getIJavaProject();
			} else if (element instanceof Query) {
				Query q = (Query) element;
				return q.getHotspot();
			} else {
				return standardProvider.getParent(element);
			}
		}

		@Override
		public boolean hasChildren(Object element) {
			if (element instanceof Model) {
				return !(((Model) element).getProjects().isEmpty());
			} else if (element instanceof Hotspot) {
				Hotspot hs = (Hotspot) element;
				return !(hs.getQueries().isEmpty());
			} else if (element instanceof ICompilationUnit) {
				ICompilationUnit cu = (ICompilationUnit) element;
				Project proj = model.getProject(cu.getJavaProject().getProject());
				if (proj != null) {
					return !(proj.getHotspots(cu).isEmpty());
				}
				return false;
			} else if (element instanceof IJavaElement) {
				return standardProvider.hasChildren(element);
			} else {
				return false;
			}
		}

	}

	private static final class JavaMetricLabelProvider extends CellLabelProvider {
		private final Model model = Context.getInstance().getModel();
		private final MetricDesc metric;

		public JavaMetricLabelProvider(MetricDesc metric) {
			super();
			this.metric = metric;
		}

		@Override
		public void update(ViewerCell cell) {
			Integer val = getJavaMetric(cell.getElement());
			if (val != null) {
				cell.setText(val.toString());
			} else {
				cell.setText("");
			}
		}

		public Integer getJavaMetric(Object element) {
			if (element instanceof IJavaElement) {
				IJavaElement javaElement = (IJavaElement) element;
				IJavaProject javaProject = javaElement.getJavaProject();
				Project proj = model.getProject(javaProject.getProject());

				return proj.getJavaMetrics().getMetric(javaElement, metric);
			} else if (element instanceof Hotspot) {
				Hotspot hs = (Hotspot) element;
				if (metric == MetricDesc.NumberOfQueries) {
					return hs.getQueries().size();
				} else if (metric == MetricDesc.NumberOfTableAccesses) {
					return getHSTA(hs);
				} else if (metric == MetricDesc.NumberOfColumnAccesses) {
					return getHSCA(hs);
				}
			} else if (element instanceof Query) {
				Query q = (Query) element;
				if (metric == MetricDesc.NumberOfTableAccesses) {
					return getTA(q);
				} else if (metric == MetricDesc.NumberOfColumnAccesses) {
					return getCA(q);
				}
			}

			return null;
		}

		private static final Integer getHSTA(Hotspot hs) {
			int maxta = -1;
			for (Query q : hs.getQueries()) {
				Integer ta = getTA(q);
				if (ta != null && ta > maxta) {
					maxta = ta;
				}
			}
			if (maxta >= 0) {
				return maxta;
			} else {
				return null;
			}
		}

		private static final Integer getHSCA(Hotspot hs) {
			int maxca = -1;
			for (Query q : hs.getQueries()) {
				Integer ca = getCA(q);
				if (ca != null && ca > maxca) {
					maxca = ca;
				}
			}
			if (maxca >= 0) {
				return maxca;
			} else {
				return null;
			}
		}

		private static final Integer getTA(Query q) {
			List<Statement> stmts = q.getStatements();
			if (stmts.size() != 1) {
				return null;
			}
			Statement stmt = (Statement) (stmts.toArray()[0]);
			Project proj = q.getHotspot().getProject();
			Map<Table, Integer> tac = proj.getSQLAn().getTableAcc().get(stmt);
			if (tac != null) {
				return tac.size();
			} else {
				return null;
			}
		}

		private static final Integer getCA(Query q) {
			List<Statement> stmts = q.getStatements();
			if (stmts.size() != 1) {
				return null;
			}
			Statement stmt = (Statement) (stmts.toArray()[0]);
			Project proj = q.getHotspot().getProject();
			Map<Column, Integer> cac = proj.getSQLAn().getColumnAcc().get(stmt);
			if (cac != null) {
				return cac.size();
			} else {
				return null;
			}
		}

	}

	private static final class SQLMetricLabelProvider extends CellLabelProvider {
		private final sqlinspect.sqlan.metrics.MetricDesc metric;

		public SQLMetricLabelProvider(sqlinspect.sqlan.metrics.MetricDesc metric) {
			super();
			this.metric = metric;
		}

		@Override
		public void update(ViewerCell cell) {
			Object element = cell.getElement();
			if (element instanceof Query) {
				Query q = (Query) element;
				Integer val = getQuerySQLMetric(q, metric);
				if (val != null) {
					cell.setText(val.toString());
				} else {
					cell.setText("");
				}
			}
		}

		public sqlinspect.sqlan.metrics.MetricDesc getMetric() {
			return metric;
		}

		public static Integer getQuerySQLMetric(Query q, sqlinspect.sqlan.metrics.MetricDesc md) {
			List<Statement> stmts = q.getStatements();
			if (stmts.size() != 1) {
				return null;
			}
			Statement stmt = (Statement) (stmts.toArray()[0]);
			Project proj = q.getHotspot().getProject();
			Map<sqlinspect.sqlan.metrics.MetricDesc, Integer> stmtMetrics = proj.getSQLAn().getMetrics().get(stmt);
			if (stmtMetrics != null) {
				return stmtMetrics.get(md);
			} else {
				return null;
			}
		}
	}

	private static final class MetricsViewerComparator extends ViewerComparator {
		private int propertyIndex;
		private static final int DESCENDING = 1;
		private int direction = DESCENDING;
		private final JavaElementComparator javaComparator = new JavaElementComparator();

		public MetricsViewerComparator() {
			super();
			this.propertyIndex = 0;
			direction = DESCENDING;
		}

		public int getDirection() {
			return direction == 1 ? SWT.DOWN : SWT.UP;
		}

		public void setColumn(int column) {
			if (column == this.propertyIndex) {
				// Same column as last sort; toggle the direction
				direction = 1 - direction;
			} else {
				// New column; do an ascending sort
				this.propertyIndex = column;
				direction = DESCENDING;
			}
		}

		@Override
		public int compare(Viewer viewer, Object e1, Object e2) {
			int ret = 0;
			if (propertyIndex == 0) {
				ret = javaComparator.compare(viewer, e1, e2);
			} else {
				if (viewer instanceof TreeViewer) {
					TreeViewer tv = (TreeViewer) viewer;
					IBaseLabelProvider blp = tv.getLabelProvider(propertyIndex);
					if (blp instanceof SQLMetricLabelProvider && e1 instanceof Query && e2 instanceof Query) {
						SQLMetricLabelProvider lp = (SQLMetricLabelProvider) blp;
						Query q1 = (Query) e1;
						Query q2 = (Query) e2;
						Integer m1 = SQLMetricLabelProvider.getQuerySQLMetric(q1, lp.getMetric());
						Integer m2 = SQLMetricLabelProvider.getQuerySQLMetric(q2, lp.getMetric());
						if (m1 != null && m2 != null) {
							ret = Integer.compare(m1, m2);
						}
					} else if (blp instanceof JavaMetricLabelProvider) {
						JavaMetricLabelProvider lp = (JavaMetricLabelProvider) blp;
						Integer m1 = lp.getJavaMetric(e1);
						Integer m2 = lp.getJavaMetric(e2);
						if (m1 != null && m2 != null) {
							ret = Integer.compare(m1, m2);
						}
					}
				}
			}

			if (direction == DESCENDING) {
				ret = -ret;
			}
			return ret;
		}

	}

	@Override
	public Viewer createViewer(Composite parent) {
		TreeViewer viewer = new TreeViewer(parent);
		viewer.setContentProvider(new ViewContentProvider());
		viewer.getTree().setHeaderVisible(true);
		viewer.getTree().setLinesVisible(true);
		viewer.getControl().setLayoutData(new GridData(GridData.FILL_BOTH));

		MetricDesc[] metrics = new MetricDesc[] { MetricDesc.NumberOfHotspots, MetricDesc.NumberOfQueries,
				MetricDesc.NumberOfTableAccesses, MetricDesc.NumberOfColumnAccesses };
		sqlinspect.sqlan.metrics.MetricDesc[] sqlMetrics = new sqlinspect.sqlan.metrics.MetricDesc[] {
				sqlinspect.sqlan.metrics.MetricDesc.NumberOfResultFields,
				sqlinspect.sqlan.metrics.MetricDesc.NumberOfQueries, sqlinspect.sqlan.metrics.MetricDesc.NestingLevel };

		int colNum = 0;
		TreeViewerColumn col = new TreeViewerColumn(viewer, SWT.LEFT);
		col.getColumn().setWidth(600);
		col.getColumn().setText("Name");
		col.setLabelProvider(new DelegatingStyledCellLabelProvider(
				new MetricsViewLabelProvider(JavaElementLabelProvider.SHOW_DEFAULT)));
		col.getColumn().addSelectionListener(getSelectionAdapter(col, colNum++));

		for (MetricDesc m : metrics) {
			createJavaMetricColumn(viewer, m, colNum++);
		}

		for (sqlinspect.sqlan.metrics.MetricDesc m : sqlMetrics) {
			createSQLMetricColumn(viewer, m, colNum++);
		}

		viewer.setInput(model);
		viewer.setComparator(new MetricsViewerComparator());

		createContextMenu(viewer);

		return viewer;
	}

	private void createJavaMetricColumn(TreeViewer viewer, MetricDesc metric, int colNum) {
		TreeViewerColumn col = new TreeViewerColumn(viewer, SWT.NONE);
		col.setLabelProvider(new JavaMetricLabelProvider(metric));

		TreeColumn treeCol = col.getColumn();
		treeCol.setWidth(150);
		treeCol.setText(metric.getShortName());
		treeCol.setToolTipText(metric.getShortName() + ": " + metric.getDescription());
		treeCol.addSelectionListener(getSelectionAdapter(col, colNum));
	}

	private void createSQLMetricColumn(TreeViewer viewer, sqlinspect.sqlan.metrics.MetricDesc metric, int colNum) {
		TreeViewerColumn col = new TreeViewerColumn(viewer, SWT.NONE);
		col.setLabelProvider(new SQLMetricLabelProvider(metric));

		TreeColumn treeCol = col.getColumn();
		treeCol.setWidth(150);
		treeCol.setText(metric.getShortName());
		treeCol.setToolTipText(metric.getShortName() + ": " + metric.getDescription());
		treeCol.addSelectionListener(getSelectionAdapter(col, colNum));
	}

	private void createContextMenu(TreeViewer viewer) {
		MenuManager menuMgr = new MenuManager();
		Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, viewer);
	}

	private SelectionAdapter getSelectionAdapter(final TreeViewerColumn column, final int index) {
		return new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				TreeViewer viewer = (TreeViewer) getViewer();
				MetricsViewerComparator comparator = (MetricsViewerComparator) viewer.getComparator();
				comparator.setColumn(index);
				int dir = comparator.getDirection();
				viewer.getTree().setSortColumn(column.getColumn());
				viewer.getTree().setSortDirection(dir);
				viewer.refresh();
			}
		};
	}

	public void expandAll() {
		TreeViewer viewer = (TreeViewer) getViewer();
		// workaround of mouse pointer flickering
		Runnable task = () -> {
			try {
				viewer.getTree().setRedraw(false);
				viewer.expandAll();
			} finally {
				viewer.getTree().setRedraw(true);
			}
		};
		BusyIndicator.showWhile(Display.getCurrent(), task);
	}

	public void collapseAll() {
		TreeViewer viewer = (TreeViewer) getViewer();
		viewer.collapseAll();
	}
}