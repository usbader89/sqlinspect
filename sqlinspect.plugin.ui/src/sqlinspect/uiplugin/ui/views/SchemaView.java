package sqlinspect.uiplugin.ui.views;

import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider;
import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider.IStyledLabelProvider;
import org.eclipse.jface.viewers.IBaseLabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;

import sqlinspect.sqlan.asg.base.Named;
import sqlinspect.sqlan.asg.base.SQLRoot;
import sqlinspect.sqlan.asg.common.ASGUtils;
import sqlinspect.sqlan.asg.schema.Column;
import sqlinspect.sqlan.asg.schema.Database;
import sqlinspect.sqlan.asg.schema.Schema;
import sqlinspect.sqlan.asg.schema.Table;
import sqlinspect.uiplugin.Context;
import sqlinspect.uiplugin.model.Model;
import sqlinspect.uiplugin.model.Project;
import sqlinspect.uiplugin.utils.ImageUtility;

public class SchemaView extends AbstractSQLInspectViewer {

	public static final String ID = "sqlinspect.ui.views.sqlinspect.SchemaView";

	public static final Object[] NO_ELEMENTS = {};

	private static class ViewContentProvider implements ITreeContentProvider {
		@Override
		public Object[] getElements(Object parent) {
			if (parent instanceof Model) {
				Model model = (Model) parent;
				return model.getProjects().toArray();
			} else {
				return NO_ELEMENTS;
			}
		}

		@Override
		public Object[] getChildren(Object element) {
			if (element instanceof Project) {
				Project proj = (Project) element;
				return proj.getSQLRoot().getDatabases().toArray();
			} else if (element instanceof Database) {
				Database db = (Database) element;
				return db.getSchemas().toArray();
			} else if (element instanceof Schema) {
				Schema sch = (Schema) element;
				return sch.getTables().toArray();
			} else if (element instanceof Table) {
				Table tab = (Table) element;
				return tab.getColumns().toArray();
			}
			return NO_ELEMENTS;
		}

		private Project getDatabaseParent(Database db) {
			SQLRoot dbParent = (SQLRoot) db.getParent();
			for (Project proj : Context.getInstance().getModel().getProjects()) {
				if (proj.getSQLRoot().equals(dbParent)) {
					return proj;
				}
			}
			return null;
		}

		@Override
		public Object getParent(Object element) {
			if (element instanceof Database) {
				Database db = (Database) element;
				return getDatabaseParent(db);
			} else if (element instanceof Schema) {
				Schema sch = (Schema) element;
				return sch.getParent();
			} else if (element instanceof Table) {
				Table tab = (Table) element;
				return tab.getParent();
			}
			return null;
		}

		@Override
		public boolean hasChildren(Object element) {
			if (element instanceof Project) {
				Project proj = (Project) element;
				return !proj.getSQLRoot().getDatabases().isEmpty();
			} else if (element instanceof Database) {
				Database db = (Database) element;
				return !db.getSchemas().isEmpty();
			} else if (element instanceof Schema) {
				Schema sch = (Schema) element;
				return !sch.getTables().isEmpty();
			} else if (element instanceof Table) {
				Table tab = (Table) element;
				return !tab.getColumns().isEmpty();
			}
			return false;
		}
	}

	private static class ViewNameLabelProvider extends LabelProvider implements IStyledLabelProvider {
		@Override
		public Image getImage(Object element) {
			if (element instanceof Project) {
				return ImageUtility.getIcon(ImageUtility.TEXT_FILE);
			} else if (element instanceof Database) {
				return ImageUtility.getIcon(ImageUtility.DATABASE);
			} else if (element instanceof Schema) {
				return ImageUtility.getIcon(ImageUtility.DATABASE5);
			} else if (element instanceof Table) {
				return ImageUtility.getIcon(ImageUtility.LIST_VIEW);
			} else {
				return null;
			}
		}

		@Override
		public StyledString getStyledText(Object element) {
			if (element instanceof Project) {
				Project p = (Project) element;
				StyledString ret = new StyledString(p.getName());
				if (p.getSQLRoot() != null) {
					ret.append(" (" + p.getSQLRoot().getDatabases().size() + ")", StyledString.COUNTER_STYLER);
				}
				return ret;
			} else if (element instanceof Database) {
				Database db = (Database) element;
				StyledString ret = new StyledString(db.getName());
				ret.append(" (" + db.getSchemas().size() + ")", StyledString.COUNTER_STYLER);
				return ret;
			} else if (element instanceof Schema) {
				Schema sch = (Schema) element;
				StyledString ret = new StyledString(sch.getName());
				ret.append(" (" + sch.getTables().size() + ")", StyledString.COUNTER_STYLER);
				return ret;
			} else if (element instanceof Table) {
				Table tab = (Table) element;
				StyledString ret = new StyledString(tab.getName());
				ret.append(" (" + tab.getColumns().size() + ")", StyledString.COUNTER_STYLER);
				return ret;
			} else if (element instanceof Named) {
				Named n = (Named) element;
				return new StyledString(n.getName());
			} else {
				return new StyledString("NA");
			}
		}
	}

	private static class ViewTypeLabelProvider extends LabelProvider implements IStyledLabelProvider {
		@Override
		public Image getImage(Object arg0) {
			return null;
		}

		@Override
		public StyledString getStyledText(Object element) {
			if (element instanceof Column) {
				Column col = (Column) element;
				return new StyledString(ASGUtils.columnTypeToString(col));
			} else {
				return new StyledString("");
			}
		}
	}

	private static final class SchemaViewerComparator extends ViewerComparator {
		private int propertyIndex;
		private static final int DESCENDING = 1;
		private int direction = DESCENDING;

		public SchemaViewerComparator() {
			super();
			this.propertyIndex = 0;
			direction = DESCENDING;
		}

		public int getDirection() {
			return direction == 1 ? SWT.DOWN : SWT.UP;
		}

		public void setColumn(int column) {
			if (column == this.propertyIndex) {
				// Same column as last sort; toggle the direction
				direction = 1 - direction;
			} else {
				// New column; do an ascending sort
				this.propertyIndex = column;
				direction = DESCENDING;
			}
		}

		@Override
		public int compare(Viewer viewer, Object e1, Object e2) {
			int ret = 0;
			if (propertyIndex == 0 && viewer instanceof TreeViewer) {
				TreeViewer tv = (TreeViewer) viewer;
				IBaseLabelProvider blp = tv.getLabelProvider(propertyIndex);
				if (blp instanceof DelegatingStyledCellLabelProvider) {
					DelegatingStyledCellLabelProvider dlp = (DelegatingStyledCellLabelProvider) blp;
					IStyledLabelProvider lp = dlp.getStyledStringProvider();
					StyledString s1 = lp.getStyledText(e1);
					StyledString s2 = lp.getStyledText(e2);
					if (s1 != null && s2 != null) {
						ret = s1.getString().compareTo(s2.getString());
					}
				}
			}

			if (direction == DESCENDING) {
				ret = -ret;
			}
			return ret;
		}
	}

	@Override
	public Viewer createViewer(Composite parent) {
		TreeViewer viewer = new TreeViewer(parent);
		viewer.setContentProvider(new ViewContentProvider());
		viewer.getTree().setHeaderVisible(true);
		viewer.getTree().setLinesVisible(true);
		viewer.getControl().setLayoutData(new GridData(GridData.FILL_BOTH));

		TreeViewerColumn viewerColumn = new TreeViewerColumn(viewer, SWT.LEFT);
		viewerColumn.getColumn().setWidth(600);
		viewerColumn.getColumn().setText("Name");
		viewerColumn.setLabelProvider(new DelegatingStyledCellLabelProvider(new ViewNameLabelProvider()));
		viewerColumn.getColumn().addSelectionListener(getSelectionAdapter(viewerColumn, 0));

		TreeViewerColumn viewerColumn2 = new TreeViewerColumn(viewer, SWT.NONE);
		viewerColumn2.getColumn().setWidth(150);
		viewerColumn2.getColumn().setText("Type");
		viewerColumn2.setLabelProvider(new DelegatingStyledCellLabelProvider(new ViewTypeLabelProvider()));

		viewer.setInput(Context.getInstance().getModel());
		viewer.setComparator(new SchemaViewerComparator());

		createContextMenu(viewer);

		return viewer;
	}

	private void createContextMenu(TreeViewer viewer) {
		MenuManager menuMgr = new MenuManager();
		Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, viewer);
	}

	private SelectionAdapter getSelectionAdapter(final TreeViewerColumn column, final int index) {
		return new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				TreeViewer viewer = (TreeViewer) getViewer();
				SchemaViewerComparator comparator = (SchemaViewerComparator) viewer.getComparator();
				comparator.setColumn(index);
				int dir = comparator.getDirection();
				viewer.getTree().setSortColumn(column.getColumn());
				viewer.getTree().setSortDirection(dir);
				viewer.refresh();
			}
		};
	}

}
