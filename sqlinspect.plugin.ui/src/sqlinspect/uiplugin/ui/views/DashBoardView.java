package sqlinspect.uiplugin.ui.views;

import java.net.URISyntaxException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.osgi.framework.Bundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swt.FXCanvas;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.StackedBarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.util.Callback;
import javafx.util.StringConverter;
import sqlinspect.sqlan.asg.schema.Table;
import sqlinspect.sqlan.asg.statm.Statement;
import sqlinspect.uiplugin.Context;
import sqlinspect.uiplugin.metrics.MetricDesc;
import sqlinspect.uiplugin.model.Model;
import sqlinspect.uiplugin.model.Project;
import sqlinspect.uiplugin.model.Query;
import sqlinspect.uiplugin.ui.UIActivator;
import sqlinspect.uiplugin.utils.ASTUtils;

public class DashBoardView extends AbstractSQLInspectView {

	public static final String ID = "sqlinspect.ui.views.sqlinspect.DashBoardView";

	private static final Logger logger = LoggerFactory.getLogger(DashBoardView.class);
	private static final Model model = Context.getInstance().getModel();

	private final ObservableList<String> projectList = FXCollections.observableArrayList();

	private final ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList();
	private final ObservableList<XYChart.Series<Number, String>> barChartSeries = FXCollections.observableArrayList();
	private final ObservableList<String> barChartYLabels = FXCollections.observableArrayList();
	private final ObservableList<ClassesTableCellData> classesTableData = FXCollections.observableArrayList();
	private final ObservableList<QueriesTableCellData> queriesTableData = FXCollections.observableArrayList();

	private final ObservableList<StatData> stats = FXCollections.observableArrayList();
	private static final String STATS_DAO_CLASSES = "DAO Classes";
	private static final String STATS_TOTAL_QUERIES = "Queries";
	private static final String STATS_TOTAL_HOTSPOTS = "Hotspots";
	private static final String STATS_AVG_HOTSPOTS = "Avg. Hotspots per Class";
	private static final String STATS_AVG_QUERIES = "Avg. Queries per Class";

	private static final int GR_COMBO_ROW = 0;
	private static final int GR_COMBO_COL = 0;
	private static final int GR_PIE_ROW = 1;
	private static final int GR_PIE_COL = 0;
	private static final int GR_STAT_ROW = 1;
	private static final int GR_STAT_COL = 1;
	private static final int GR_BAR_ROW = 3;
	private static final int GR_BAR_COL = 0;
	private static final int GR_TAB_ROW = 4;
	private static final int GR_TAB_COL = 0;
	private static final int GR_TAB2_ROW = 5;
	private static final int GR_TAB2_COL = 0;

	private static final IPath CHARTS_CSS = new Path("$nl$/css/charts.css");

	private ComboBox<String> projectCombo;
	private StackedBarChart<Number, String> barChart;

	private static final Font H1FONT = new Font("Tahoma", 26);
	private static final Font H2FONT = new Font("Tahoma", 24);

	private static class StatData {
		public final SimpleStringProperty key;
		public final SimpleStringProperty value;

		public StatData(String key, String value) {
			this.key = new SimpleStringProperty(key);
			this.value = new SimpleStringProperty(value);
		}

		/**
		 * @return the key
		 */
		public final String getKey() {
			return key.get();
		}

		/**
		 * @param key the key to set
		 */
		@SuppressWarnings("unused")
		public final void setKey(String key) {
			this.key.set(key);
		}

		@SuppressWarnings("unused")
		public StringProperty key() {
			return key;
		}

		/**
		 * @return the value
		 */
		@SuppressWarnings("unused")
		public String getValue() {
			return this.value.get();
		}

		/**
		 * @param value the value to set
		 */
		public void setValue(String value) {
			this.value.set(value);
		}

		public StringProperty value() {
			return this.value;
		}
	}

	public static class ClassesTableCellData {
		private final IJavaElement javaElement;
		private final SimpleStringProperty className;
		private final SimpleIntegerProperty hotspots;
		private final SimpleIntegerProperty queries;

		public ClassesTableCellData(IJavaElement javaElement, String className, int hotspots, int queries) {
			this.javaElement = javaElement;
			this.className = new SimpleStringProperty(className);
			this.hotspots = new SimpleIntegerProperty(hotspots);
			this.queries = new SimpleIntegerProperty(queries);
		}

		/**
		 * @return the className
		 */
		public String getClassName() {
			return className.getValue();
		}

		public IJavaElement getJavaElement() {
			return javaElement;
		}

		public StringProperty className() {
			return className;
		}

		/**
		 * @return the hotspots
		 */
		public int getHotspots() {
			return hotspots.getValue();
		}

		public IntegerProperty hotspots() {
			return hotspots;
		}

		/**
		 * @return the queries
		 */
		public int getQueries() {
			return queries.getValue();
		}

		public IntegerProperty queries() {
			return queries;
		}

	}

	public static class QueriesTableCellData {
		private final Query query;
		private final SimpleStringProperty className;
		private final SimpleIntegerProperty lineNo;
		private final ObservableList<Integer> metrics = FXCollections.observableArrayList();

		protected static final sqlinspect.sqlan.metrics.MetricDesc[] metricNames = {
				sqlinspect.sqlan.metrics.MetricDesc.NumberOfTables,
				sqlinspect.sqlan.metrics.MetricDesc.NumberOfResultFields,
				sqlinspect.sqlan.metrics.MetricDesc.NumberOfQueries, sqlinspect.sqlan.metrics.MetricDesc.NestingLevel };

		public QueriesTableCellData(Query query, String className, int lineNo, List<Integer> metrics) {
			this.query = query;
			this.className = new SimpleStringProperty(className);
			this.lineNo = new SimpleIntegerProperty(lineNo);
			this.metrics.addAll(metrics);
		}

		public StringProperty className() {
			return className;
		}

		public IntegerProperty lineNo() {
			return lineNo;
		}

		public ObservableList<Integer> metrics() {
			return metrics;
		}

		/**
		 * @return the className
		 */
		public String getClassName() {
			return className.getValue();
		}

		/**
		 * @return the lineNo
		 */
		public int getLineNo() {
			return lineNo.getValue();
		}

		/**
		 * @return the metrics
		 */
		public ObservableList<Integer> getMetrics() {
			return metrics;
		}

		public Query getQuery() {
			return query;
		}
	}

	protected static class IntegerStringConverter extends StringConverter<Number> {
		@Override
		public String toString(Number object) {
			return (object.intValue() != object.doubleValue()) ? "" : Integer.toString(object.intValue());
		}

		@Override
		public Number fromString(String s) {
			return Double.valueOf(s).intValue();
		}
	}

	@Override
	public final Control createMain(Composite parent) {
		ScrolledComposite sc = new ScrolledComposite(parent, SWT.H_SCROLL | SWT.V_SCROLL);
		sc.setExpandHorizontal(true);
		sc.setExpandVertical(true);

		FXCanvas fxCanvas = new FXCanvas(sc, SWT.NONE);

		sc.setContent(fxCanvas);

		GridPane gridPane = new GridPane();
		gridPane.setMinSize(400, 200);
		gridPane.setPadding(new Insets(10, 10, 10, 10));
		gridPane.setVgap(5);
		gridPane.setHgap(5);
		gridPane.setAlignment(Pos.TOP_LEFT);

		Scene scene = new Scene(gridPane);

		Bundle bundle = UIActivator.getDefault().getBundle();
		URL chartCSSURL = FileLocator.find(bundle, CHARTS_CSS, null);
		try {
			scene.getStylesheets().add(chartCSSURL.toURI().toString());
		} catch (URISyntaxException e) {
			logger.error("Cannot find css", e);
		}

		createProjectCombo(gridPane);
		createPieChart(gridPane);
		createStatistics(gridPane);
		createBarChart(gridPane);
		createClassesTable(gridPane);
		createQueriesTable(gridPane);

		fxCanvas.setScene(scene);

		sc.setMinSize(fxCanvas.computeSize(SWT.DEFAULT, SWT.DEFAULT));

		refreshProject();

		return sc;
	}

	private void createPieChart(GridPane pane) {

		final PieChart chart = new PieChart(pieChartData);
		chart.setTitle("Queries per Packages");
		chart.setLabelsVisible(false);
		chart.setAnimated(true);
		chart.setStartAngle(45);

		pane.add(chart, GR_PIE_COL, GR_PIE_ROW);

	}

	private void createStatistics(GridPane pane) {
		stats.add(new StatData(STATS_DAO_CLASSES, ""));
		stats.add(new StatData(STATS_TOTAL_HOTSPOTS, ""));
		stats.add(new StatData(STATS_TOTAL_QUERIES, ""));
		stats.add(new StatData(STATS_AVG_HOTSPOTS, ""));
		stats.add(new StatData(STATS_AVG_QUERIES, ""));

		final VBox vbox = new VBox();
		vbox.setSpacing(10);
		vbox.setPadding(new Insets(10, 0, 0, 10));
		vbox.setAlignment(Pos.TOP_CENTER);

		final Label label = new Label("Statistics");
		label.setFont(H2FONT);
		vbox.getChildren().add(label);

		stats.stream().forEach(e -> {
			final Label labelV = new Label();
			labelV.setFont(new Font("Tahoma", 40));
			labelV.setTextFill(Color.web("#4581a6"));
			labelV.textProperty().bind(e.value());

			final Label labelK = new Label(e.getKey());
			labelK.setPadding(new Insets(4, 4, 4, 4));
			final HBox hbox = new HBox();
			hbox.setAlignment(Pos.BOTTOM_LEFT);
			hbox.setSpacing(2);
			hbox.getChildren().addAll(labelV, labelK);
			vbox.getChildren().add(hbox);
		});

		pane.add(vbox, GR_STAT_COL, GR_STAT_ROW);
	}

	private void setMaxBarHeight(double maxBarHeight) {
		if (!barChart.getData().isEmpty()) {
			barChart.setCategoryGap(10);
			int bars = barChart.getData().get(0).getData().size();
			CategoryAxis yAxis = (CategoryAxis) barChart.getYAxis();
			double yHeight = yAxis.getHeight();
			double barHeight = (yHeight - (bars + 1) * barChart.getCategoryGap()) / bars;
			if (barHeight > maxBarHeight) {
				double newBarSpace = (maxBarHeight) * bars;
				double newCategoryGap = (yHeight - newBarSpace) / (bars + 1);
				barChart.setCategoryGap(newCategoryGap);
			}
		}
	}

	private void createBarChart(GridPane pane) {
		final NumberAxis xAxis = new NumberAxis();
		final CategoryAxis yAxis = new CategoryAxis();

		barChart = new StackedBarChart<>(xAxis, yAxis);

		barChart.setTitle("Package Summary");
		barChart.setAnimated(true);

		xAxis.setLabel("Queries");
		xAxis.setTickLabelFormatter(new IntegerStringConverter());
		yAxis.setLabel("Package");

		pane.add(barChart, GR_BAR_COL, GR_BAR_ROW, 2, 1);
	}

	private void createClassesTable(GridPane pane) {
		final Label label = new Label("Top Classes");
		label.setFont(H2FONT);

		final TableView<ClassesTableCellData> table = new TableView<>();

		TableColumn<ClassesTableCellData, String> classNameCol = new TableColumn<>("Class");
		classNameCol.setMinWidth(300);
		classNameCol.setCellValueFactory(new PropertyValueFactory<ClassesTableCellData, String>("className"));
		table.getColumns().add(classNameCol);

		TableColumn<ClassesTableCellData, Integer> hotspotsCol = new TableColumn<>("Hotspots");
		hotspotsCol.setMinWidth(150);
		hotspotsCol.setCellValueFactory(new PropertyValueFactory<ClassesTableCellData, Integer>("hotspots"));
		hotspotsCol.getStyleClass().add("column-right");
		table.getColumns().add(hotspotsCol);

		TableColumn<ClassesTableCellData, Integer> queriesCol = new TableColumn<>("Queries");
		queriesCol.setMinWidth(150);
		queriesCol.setCellValueFactory(new PropertyValueFactory<ClassesTableCellData, Integer>("queries"));
		queriesCol.getStyleClass().add("column-right");
		table.getColumns().add(queriesCol);

		table.setItems(classesTableData);

		ContextMenu contextMenu = new ContextMenu();

		MenuItem menuItem = new MenuItem("Jump to hotspot");
		EventHandler<ActionEvent> handler = event -> {
			IJavaElement javaElement = table.getSelectionModel().getSelectedItem().getJavaElement();
			IWorkbenchPage page = getSite().getPage();
			try {
				HotspotsView hv = (HotspotsView) page.showView(HotspotsView.ID);
				hv.expandToHotspots();
				List<IJavaElement> toSel = new ArrayList<>();
				toSel.add(javaElement);
				hv.select(new ArrayList<>(toSel));
			} catch (PartInitException e) {
				logger.error("Could not open view.", e);
			}
		};
		menuItem.setOnAction(handler);
		contextMenu.getItems().add(menuItem);

		table.setContextMenu(contextMenu);

		final VBox vbox = new VBox();
		vbox.setSpacing(5);
		vbox.setPadding(new Insets(10, 0, 0, 10));
		vbox.getChildren().addAll(label, table);

		pane.add(vbox, GR_TAB_COL, GR_TAB_ROW, 2, 1);
	}

	private void createQueriesTable(GridPane pane) {
		final Label label = new Label("Top Queries");
		label.setFont(H2FONT);

		final TableView<QueriesTableCellData> table = new TableView<>();

		TableColumn<QueriesTableCellData, String> classNameCol = new TableColumn<>("Class");
		classNameCol.setMinWidth(300);
		classNameCol.setCellValueFactory(new PropertyValueFactory<QueriesTableCellData, String>("className"));
		table.getColumns().add(classNameCol);

		TableColumn<QueriesTableCellData, Integer> hotspotsCol = new TableColumn<>("LineNo");
		hotspotsCol.setMinWidth(150);
		hotspotsCol.setCellValueFactory(new PropertyValueFactory<QueriesTableCellData, Integer>("lineNo"));
		table.getColumns().add(hotspotsCol);

		for (int i = 0; i < QueriesTableCellData.metricNames.length; i++) {
			final int colIndex = i;
			String metricName = QueriesTableCellData.metricNames[colIndex].getShortName();
			TableColumn<QueriesTableCellData, Integer> metricCol = new TableColumn<>(metricName);
			metricCol.setMinWidth(150);
			metricCol.getStyleClass().add("column-right");
			Callback<CellDataFeatures<QueriesTableCellData, Integer>, ObservableValue<Integer>> metricColCallBack = cd -> {
				ObservableList<Integer> metrics = cd.getValue().metrics();
				if (metrics != null && metrics.size() > colIndex) {
					Integer val = cd.getValue().metrics().get(colIndex);
					if (val != null) {
						return new ReadOnlyObjectWrapper<>(val);
					}
				}
				return new ReadOnlyObjectWrapper<>();
			};
			metricCol.setCellValueFactory(metricColCallBack);
			table.getColumns().add(metricCol);
		}

		table.setItems(queriesTableData);

		ContextMenu contextMenu = new ContextMenu();

		MenuItem menuItem = new MenuItem("Jump to hotspot");
		EventHandler<ActionEvent> handler = event -> {
			Query query = table.getSelectionModel().getSelectedItem().getQuery();
			IWorkbenchPage page = getSite().getPage();
			try {
				HotspotsView hv = (HotspotsView) page.showView(HotspotsView.ID);
				hv.expandToQueries();
				List<Query> toSel = new ArrayList<>();
				toSel.add(query);
				hv.select(new ArrayList<>(toSel));
			} catch (PartInitException e) {
				logger.error("Could not open view.", e);
			}
		};
		menuItem.setOnAction(handler);

		contextMenu.getItems().add(menuItem);
		table.setContextMenu(contextMenu);

		final VBox vbox = new VBox();
		vbox.setSpacing(5);
		vbox.setPadding(new Insets(10, 0, 0, 10));
		vbox.getChildren().addAll(label, table);

		pane.add(vbox, GR_TAB2_COL, GR_TAB2_ROW, 2, 1);
	}

	private void createProjectCombo(GridPane gridPane) {
		final Label label = new Label("Project Overview");
		label.setFont(H1FONT);

		projectCombo = new ComboBox<>(projectList);

		ChangeListener<String> projectComboListener = (composant, oldValue, newValue) -> refreshCharts();
		projectCombo.valueProperty().addListener(projectComboListener);

		final VBox vbox = new VBox();
		vbox.getChildren().add(projectCombo);
		vbox.setAlignment(Pos.TOP_RIGHT);

		final VBox vboxHead = new VBox();
		vboxHead.setAlignment(Pos.TOP_LEFT);
		vboxHead.setSpacing(2);
		vboxHead.setPadding(new Insets(10, 0, 0, 10));
		vboxHead.getChildren().addAll(label);

		gridPane.add(vboxHead, GR_COMBO_COL, GR_COMBO_ROW);
		gridPane.add(vbox, GR_COMBO_COL + 1, GR_COMBO_ROW);
	}

	public void refreshProject() {
		String selectedProject = projectCombo.getValue();
		projectList.clear();
		List<String> projectNames = model.getProjectNames();
		projectList.addAll(projectNames);
		if (!projectNames.isEmpty()) {
			if (projectNames.contains(selectedProject)) {
				projectCombo.setValue(selectedProject);
			} else {
				projectCombo.setValue(projectNames.get(0));
			}
		}
	}

	private void refreshCharts() {
		refreshPieChart();
		refreshStats();
		refreshBarChart();
		refreshClassesTable();
		refreshQueriesTable();
	}

	private void refreshPieChart() {
		pieChartData.clear();
		String projName = projectCombo.getValue();
		if (projName == null || projName.isEmpty()) {
			return;
		}

		Project proj = model.getProject(projectCombo.getValue());
		if (proj == null) {
			return;
		}

		Map<IJavaElement, Map<MetricDesc, Integer>> metrics = proj.getJavaMetrics().getMetrics();

		Map<String, Integer> queriesData = new HashMap<>();
		for (Entry<IJavaElement, Map<MetricDesc, Integer>> e : metrics.entrySet()) {
			IJavaElement key = e.getKey();
			Map<MetricDesc, Integer> emetrics = e.getValue();

			if (key.getElementType() == IJavaElement.PACKAGE_FRAGMENT) {
				queriesData.put(key.getElementName(), emetrics.get(MetricDesc.NumberOfQueries));
			}
		}

		queriesData.entrySet().stream().forEach(e -> pieChartData.add(new PieChart.Data(e.getKey(), e.getValue())));

		final DecimalFormat df = new DecimalFormat("#");
		pieChartData.stream().forEach(data -> {
			final Tooltip tooltip = new Tooltip();
			tooltip.setText(data.getName() + ": " + df.format(data.getPieValue()));
			Tooltip.install(data.getNode(), tooltip);
			data.pieValueProperty().addListener(
					(observable, oldValue, newValue) -> tooltip.setText(data.getName() + ": " + df.format(newValue)));
		});
	}

	private void loadBarChartData() {
		String projName = projectCombo.getValue();
		if (projName == null || projName.isEmpty()) {
			return;
		}

		Project proj = model.getProject(projectCombo.getValue());
		if (proj == null) {
			return;
		}

		Map<String, Integer> queriesData = new HashMap<>();
		Map<String, Integer> insertsData = new HashMap<>();
		Map<String, Integer> deletesData = new HashMap<>();
		Map<String, Integer> updatesData = new HashMap<>();
		Map<String, Integer> selectsData = new HashMap<>();

		for (Entry<IJavaElement, Map<MetricDesc, Integer>> e : proj.getJavaMetrics().getMetrics().entrySet()) {
			IJavaElement key = e.getKey();
			Map<MetricDesc, Integer> emetrics = e.getValue();

			if (key.getElementType() == IJavaElement.PACKAGE_FRAGMENT) {
				queriesData.put(key.getElementName(), emetrics.get(MetricDesc.NumberOfQueries));
				insertsData.put(key.getElementName(), emetrics.get(MetricDesc.NumberOfInserts));
				deletesData.put(key.getElementName(), emetrics.get(MetricDesc.NumberOfDeletes));
				updatesData.put(key.getElementName(), emetrics.get(MetricDesc.NumberOfUpdates));
				selectsData.put(key.getElementName(), emetrics.get(MetricDesc.NumberOfSelects));
			}
		}

		barChartYLabels.clear();
		barChartYLabels.addAll(queriesData.keySet());

		((CategoryAxis) barChart.getYAxis()).setCategories(barChartYLabels);

		barChartSeries.clear();

		final XYChart.Series<Number, String> serDel = new XYChart.Series<>();
		serDel.setName("Delete");
		final XYChart.Series<Number, String> serIns = new XYChart.Series<>();
		serIns.setName("Insert");
		final XYChart.Series<Number, String> serUp = new XYChart.Series<>();
		serUp.setName("Update");
		final XYChart.Series<Number, String> serSel = new XYChart.Series<>();
		serSel.setName("Select");
		final XYChart.Series<Number, String> serEtc = new XYChart.Series<>();
		serEtc.setName("Other");

		for (Entry<String, Integer> e : queriesData.entrySet()) {
			String pkg = e.getKey();
			Integer selects = selectsData.get(e.getKey());
			Integer inserts = insertsData.get(e.getKey());
			Integer updates = updatesData.get(e.getKey());
			Integer deletes = deletesData.get(e.getKey());
			Integer others = (e.getValue() == null ? 0 : e.getValue()) - (selects == null ? 0 : selects)
					- (inserts == null ? 0 : inserts) - (updates == null ? 0 : updates)
					- (deletes == null ? 0 : deletes);

			serSel.getData().add(new XYChart.Data<Number, String>(selects == null ? Integer.valueOf(0) : selects, pkg));
			serIns.getData().add(new XYChart.Data<Number, String>(inserts == null ? Integer.valueOf(0) : inserts, pkg));
			serDel.getData().add(new XYChart.Data<Number, String>(deletes == null ? Integer.valueOf(0) : deletes, pkg));
			serUp.getData().add(new XYChart.Data<Number, String>(updates == null ? Integer.valueOf(0) : updates, pkg));
			serEtc.getData().add(new XYChart.Data<Number, String>(others, e.getKey()));
		}

		barChartSeries.add(serDel);
		barChartSeries.add(serIns);
		barChartSeries.add(serUp);
		barChartSeries.add(serSel);
		barChartSeries.add(serEtc);

		barChart.getData().addAll(barChartSeries);
	}

	private void refreshBarChart() {
		barChart.getData().clear();

		loadBarChartData();

		setMaxBarHeight(80);

		final DecimalFormat df = new DecimalFormat("#");
		barChart.getData().stream().forEach(seri -> seri.getData().stream().forEach(data -> {
			Tooltip tooltip = new Tooltip();
			tooltip.setText(data.getYValue() + ": " + df.format(data.getXValue()));
			Tooltip.install(data.getNode(), tooltip);
			data.XValueProperty().addListener(
					(observable, oldValue, newValue) -> tooltip.setText(data.getYValue() + ": " + newValue.toString()));
		}));
	}

	private void refreshClassesTable() {
		String projName = projectCombo.getValue();
		if (projName == null || projName.isEmpty()) {
			return;
		}

		Project proj = model.getProject(projectCombo.getValue());
		if (proj == null) {
			return;
		}

		classesTableData.clear();
		for (Entry<IJavaElement, Map<MetricDesc, Integer>> e : proj.getJavaMetrics().getMetrics().entrySet()) {
			IJavaElement key = e.getKey();
			Map<MetricDesc, Integer> emetrics = e.getValue();

			if (key.getElementType() == IJavaElement.COMPILATION_UNIT) {
				ICompilationUnit icu = (ICompilationUnit) key;
				Integer queries = emetrics.getOrDefault(MetricDesc.NumberOfQueries, 0);
				Integer hotspots = emetrics.getOrDefault(MetricDesc.NumberOfHotspots, 0);
				String className = icu.findPrimaryType().getFullyQualifiedName();
				classesTableData.add(new ClassesTableCellData(key, className, hotspots, queries));
			}
		}
	}

	private void refreshQueriesTable() {
		String projName = projectCombo.getValue();
		if (projName == null || projName.isEmpty()) {
			return;
		}

		Project proj = model.getProject(projectCombo.getValue());
		if (proj == null) {
			return;
		}

		queriesTableData.clear();
		for (Query q : proj.getQueries()) {
			for (Statement stmt : q.getStatements()) {
				Map<sqlinspect.sqlan.metrics.MetricDesc, Integer> stmtMetrics = proj.getSQLAn().getMetrics().get(stmt);
				List<Integer> cellMetrics = new ArrayList<>();
				if (stmtMetrics != null) {
					Map<Table, Integer> tac = proj.getSQLAn().getTableAcc().get(stmt);
					if (tac != null) {
						cellMetrics.add(tac.size());
					} else {
						cellMetrics.add(0);
					}
					cellMetrics.add(stmtMetrics.get(sqlinspect.sqlan.metrics.MetricDesc.NumberOfResultFields));
					cellMetrics.add(stmtMetrics.get(sqlinspect.sqlan.metrics.MetricDesc.NumberOfQueries));
					cellMetrics.add(stmtMetrics.get(sqlinspect.sqlan.metrics.MetricDesc.NestingLevel));
					String className = ASTUtils.getICompilationUnit(q.getHotspot().getExec()).findPrimaryType()
							.getFullyQualifiedName();
					int lineNo = ASTUtils.getLineNumber(q.getHotspot().getExec());
					queriesTableData.add(new QueriesTableCellData(q, className, lineNo, cellMetrics));
				}
			}
		}
	}

	private Optional<StatData> getStatData(String key) {
		return stats.stream().filter(x -> key.equals(x.getKey())).findFirst();
	}

	private void refreshStats() {
		String projName = projectCombo.getValue();
		if (projName == null || projName.isEmpty()) {
			return;
		}

		Project proj = model.getProject(projectCombo.getValue());
		if (proj == null) {
			return;
		}

		int daoClasses = countDAOClasses(proj);
		int totalHotspots = countTotalHotspots(proj);
		int totalQueries = countTotalQueries(proj);

		final DecimalFormat df = new DecimalFormat("#.00");

		getStatData(STATS_DAO_CLASSES).ifPresent(x -> x.setValue(Integer.toString(daoClasses)));
		getStatData(STATS_TOTAL_HOTSPOTS).ifPresent(x -> x.setValue(Integer.toString(totalHotspots)));
		getStatData(STATS_TOTAL_QUERIES).ifPresent(x -> x.setValue(Integer.toString(totalQueries)));
		getStatData(STATS_AVG_HOTSPOTS).ifPresent(x -> x.setValue(df.format(1.0 * totalHotspots / daoClasses)));
		getStatData(STATS_AVG_QUERIES).ifPresent(x -> x.setValue(df.format(1.0 * totalQueries / daoClasses)));
	}

	private static int countTotalHotspots(Project proj) {
		return proj.getHotspots().size();
	}

	private static int countTotalQueries(Project proj) {
		return proj.getQueries().size();
	}

	private static int countDAOClasses(Project proj) {
		return Math.toIntExact(proj.getJavaMetrics().getMetrics().entrySet().stream()
				.filter(e -> e.getKey().getElementType() == IJavaElement.COMPILATION_UNIT).count());
	}
}