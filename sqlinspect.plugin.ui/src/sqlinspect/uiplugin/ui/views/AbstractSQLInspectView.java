package sqlinspect.uiplugin.ui.views;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.part.ViewPart;

import sqlinspect.uiplugin.Activator;
import sqlinspect.uiplugin.Context;

public abstract class AbstractSQLInspectView extends ViewPart {

	private Composite parent;
	private Composite welcome;
	private Control main;
	private StackLayout layout;

	private static final char SPEC_SBOLD = '\uFFFA';
	private static final char SPEC_EBOLD = '\uFFFB';

	protected abstract Control createMain(Composite parent);

	private static void replaceBold(StyledText styledText, Font font) {
		int startoffset = styledText.getText().indexOf(SPEC_SBOLD, 0);
		while (startoffset >= 0) {
			styledText.replaceTextRange(startoffset, 1, "");
			int endoffset = styledText.getText().indexOf(SPEC_EBOLD, startoffset);
			if (endoffset > startoffset) {
				styledText.replaceTextRange(endoffset, 1, "");
				StyleRange style = new StyleRange();
				style.start = startoffset;
				style.length = endoffset - startoffset;
				style.font = font;
				styledText.setStyleRange(style);
				startoffset = styledText.getText().indexOf(SPEC_SBOLD, endoffset);
			} else {
				startoffset = -1;
			}
		}
	}

	private void createWelcome(Composite parent) {
		welcome = new Composite(parent, SWT.NONE);
		final GridLayout wlayout = new GridLayout();
		welcome.setLayout(wlayout);

		final String welcomeText = "Welcome to " + Activator.PLUGIN_ID + "!\n";
		final String text = welcomeText + "\nUse " + Activator.PLUGIN_ID
				+ " to explore the SQL code embedded in your project.\n\n"
				+ "  1. Set configuration parameters for the analysis in the property page (" + Activator.PLUGIN_ID + ") under your " + SPEC_SBOLD + "project settings" + SPEC_EBOLD + ".\n"
				+ "  2. Start an analysis of your project in the project menu " + SPEC_SBOLD
				+ Activator.PLUGIN_ID + " > Analyze current project" + SPEC_EBOLD + ".\n"
				+ "  3. Open " + SPEC_SBOLD
				+ "Window > Show View > " + Activator.PLUGIN_ID + SPEC_EBOLD + " and explore the results in the "
				+ SPEC_SBOLD + "Queries/Hotspots/Schema/Metrics views" + SPEC_EBOLD + ".\n"
				+ "  4. Look for SQL smells under the " + SPEC_SBOLD + "Problems view" + SPEC_EBOLD + ".\n"
				+ "  5. Have fun! ;-)";

		StyledText styledText = new StyledText(welcome, SWT.MULTI | SWT.READ_ONLY | SWT.V_SCROLL);
		styledText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		styledText.setText(text);

		FontData data = styledText.getFont().getFontData()[0];
		Font font1 = new Font(styledText.getDisplay(), data.getName(), data.getHeight() + 4, data.getStyle());
		StyleRange welcomeStyle = new StyleRange();
		welcomeStyle.start = 0;
		welcomeStyle.length = welcomeText.length();
		welcomeStyle.font = font1;
		styledText.setStyleRange(welcomeStyle);

		Font font2 = new Font(styledText.getDisplay(), data.getName(), data.getHeight() - 1, data.getStyle());
		Font font2b = new Font(styledText.getDisplay(), data.getName(), data.getHeight() - 1, SWT.BOLD);
		StyleRange mainStyle = new StyleRange();
		mainStyle.start = welcomeText.length();
		mainStyle.length = text.length() - welcomeText.length();
		mainStyle.font = font2;
		styledText.setStyleRange(mainStyle);
		replaceBold(styledText, font2b);
	}

	@Override
	public final void createPartControl(Composite parent) {
		this.parent = parent;

		parent.setLayoutData(new GridData(GridData.FILL_BOTH));
		layout = new StackLayout();
		parent.setLayout(layout);

		createWelcome(parent);
		main = createMain(parent);

		if (Context.getInstance().getModel().getProjects().isEmpty() && welcome != null) {
			layout.topControl = welcome;
		} else {
			layout.topControl = main;
		}
		
		parent.layout();
		
		makeActions();
		contributeToActionBars();
	}

	protected void contributeToActionBars() {
		// normally there is nothing to do here
	}

	protected void makeActions() {
		// normally there is nothing to do here
	}

	protected Composite getParent() {
		return parent;
	}

	protected void showMain() {
		if (layout.topControl != main) {
			layout.topControl = main;
			parent.layout();
		}
	}

	protected void showWelcome() {
		layout.topControl = welcome;
		parent.layout();
	}

	protected boolean isWelcome() {
		return layout != null && layout.topControl == welcome;
	}

	@Override
	public void setFocus() {
		if (main != null) {
			main.setFocus();
		} else {
			welcome.setFocus();
		}
	}

	public void refresh() {
		if (Context.getInstance().getModel().getProjects().isEmpty()) {
			if (!isWelcome()) {
				showWelcome();
			}
		} else {
			showMain();
		}
	}
}
