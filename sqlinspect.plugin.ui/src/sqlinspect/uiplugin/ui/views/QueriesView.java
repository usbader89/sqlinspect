package sqlinspect.uiplugin.ui.views;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.uiplugin.Context;
import sqlinspect.uiplugin.model.Hotspot;
import sqlinspect.uiplugin.model.Model;
import sqlinspect.uiplugin.model.Project;
import sqlinspect.uiplugin.model.Query;
import sqlinspect.uiplugin.utils.ASTUtils;

public class QueriesView extends AbstractSQLInspectViewer {

	public static final String ID = "sqlinspect.ui.views.sqlinspect.QueriesView";

	private static final Logger logger = LoggerFactory.getLogger(QueriesView.class);

	private QueriesViewerComparator comparator;

	private static class ViewContentProvider implements IStructuredContentProvider {
		@Override
		public void inputChanged(Viewer v, Object oldInput, Object newInput) {
			// No implementation needed here.
		}

		@Override
		public void dispose() {
			// No implementation needed here.
		}

		@Override
		public Object[] getElements(Object parent) {
			List<Query> queries = new ArrayList<>();
			Model model = (Model) parent;
			for (Project p : model.getProjects()) {
				queries.addAll(p.getQueries());
			}
			return queries.toArray();
		}
	}

	private static class ViewLabelProvider extends LabelProvider implements ITableLabelProvider {
		@Override
		public String getColumnText(Object obj, int index) {
			Query q = (Query) obj;
			Hotspot hs = q.getHotspot();
			String text = "";
			switch (index) {
			case 0: /* project name */
				if (hs != null && hs.getProject() != null) {
					text = q.getHotspot().getProject().getName();
				} else {
					text = "No Unit";
				}
				break;
			case 1: /* compilation unit name */
				if (hs != null && hs.getUnit() != null) {
					text = ((CompilationUnit) hs.getExec().getRoot()).getJavaElement().getElementName();
				} else {
					text = "No Unit";
				}
				break;
			case 2: /* line number */
				if (hs != null && hs.getExec() != null && hs.getUnit() != null) {
					text = Integer.toString(ASTUtils.getLineNumber(hs.getExec()));
				} else {
					text = "No Line";
				}
				break;
			case 3: /* hotspot statement */
				if (hs != null && hs.getExec() != null) {
					text = hs.getExec().toString();
				} else {
					text = "No Exec";
				}
				break;
			case 4: /* query */
				String val = q.getValue();
				if (!val.isEmpty()) {
					text = val;
				} else {
					text = "{{empty}}";
				}
				break;
			default:
				logger.error("Unhandled column index!");
				break;
			}

			return text;
		}

		@Override
		public Image getColumnImage(Object arg0, int arg1) {
			return null;
		}
	}

	private static class QueriesViewerComparator extends ViewerComparator {
		private int propertyIndex;
		private static final int DESCENDING = 1;
		private int direction = DESCENDING;

		public QueriesViewerComparator() {
			super();
			this.propertyIndex = 0;
			direction = DESCENDING;
		}

		public int getDirection() {
			return direction == 1 ? SWT.DOWN : SWT.UP;
		}

		public void setColumn(int column) {
			if (column == this.propertyIndex) {
				// Same column as last sort; toggle the direction
				direction = 1 - direction;
			} else {
				// New column; do an ascending sort
				this.propertyIndex = column;
				direction = DESCENDING;
			}
		}

		@Override
		public int compare(Viewer viewer, Object e1, Object e2) {
			int ret = 0;
			if (!(e1 instanceof Query) || !(e2 instanceof Query)) {
				return ret;
			}
			Query q1 = (Query) e1;
			Query q2 = (Query) e2;
			Hotspot hs1 = q1.getHotspot();
			Hotspot hs2 = q2.getHotspot();

			if (hs1 == null || hs2 == null) {
				ret = 0;
			} else {

				switch (propertyIndex) {
				case 0: /* project name */
					ret = hs1.getProject().getName().compareTo(hs2.getProject().getName());
					break;
				case 1: /* compilation unit name */
					if (hs1.getExec() == null || hs2.getExec() == null) {
						ret = 0;
					} else {
						CompilationUnit cu1 = (CompilationUnit) hs1.getExec().getRoot();
						CompilationUnit cu2 = (CompilationUnit) hs2.getExec().getRoot();
						ret = cu1.getJavaElement().getElementName().compareTo(cu2.getJavaElement().getElementName());
					}
					break;
				case 2: /* line number */
					ret = Integer.compare(ASTUtils.getLineNumber(hs1.getExec()), ASTUtils.getLineNumber(hs2.getExec()));
					break;
				case 3: /* hotspot statement */
					if (hs1.getExec() != null && hs2.getExec() != null) {
						ret = hs1.getExec().toString().compareTo(hs2.getExec().toString());
					} else {
						ret = 0;
					}
					break;
				case 4: /* query */
					String q1text = q1.getValue();
					String q2text = q2.getValue();
					ret = q1text.compareTo(q2text);
					break;
				default:
					ret = 0;
					break;
				}
			}

			if (direction == DESCENDING) {
				ret = -ret;
			}
			return ret;
		}

	}

	@Override
	protected Viewer createViewer(Composite parent) {
		TableViewer viewer = new TableViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
		viewer.setContentProvider(new ViewContentProvider());
		viewer.setLabelProvider(new ViewLabelProvider());
		viewer.setInput(Context.getInstance().getModel());

		Table table = viewer.getTable();
		table.setLayoutData(new GridData(GridData.FILL_BOTH));

		TableColumn tc = new TableColumn(table, SWT.LEFT);
		int colNum = 0;
		tc.setText("Project");
		tc.setWidth(100);
		tc.addSelectionListener(getSelectionAdapter(tc, colNum));

		tc = new TableColumn(table, SWT.LEFT);
		++colNum;
		tc.setText("Class");
		tc.setWidth(150);
		tc.addSelectionListener(getSelectionAdapter(tc, colNum));

		tc = new TableColumn(table, SWT.LEFT);
		++colNum;
		tc.setText("Line");
		tc.setWidth(60);
		tc.addSelectionListener(getSelectionAdapter(tc, colNum));

		tc = new TableColumn(table, SWT.LEFT);
		++colNum;
		tc.setText("Exec");
		tc.setWidth(200);
		tc.addSelectionListener(getSelectionAdapter(tc, colNum));

		tc = new TableColumn(table, SWT.LEFT);
		++colNum;
		tc.setText("Query");
		tc.setWidth(300);
		tc.addSelectionListener(getSelectionAdapter(tc, colNum));

		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		comparator = new QueriesViewerComparator();
		viewer.setComparator(comparator);

		createContextMenu(viewer);

		return viewer;
	}

	private void createContextMenu(TableViewer viewer) {
		MenuManager menuMgr = new MenuManager();
		Menu menu = menuMgr.createContextMenu(viewer.getTable());
		viewer.getTable().setMenu(menu);
		getSite().registerContextMenu(menuMgr, viewer);
	}

	private SelectionAdapter getSelectionAdapter(final TableColumn column, final int index) {
		return new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				TableViewer viewer = (TableViewer) getViewer();
				comparator.setColumn(index);
				int dir = comparator.getDirection();
				viewer.getTable().setSortDirection(dir);
				viewer.getTable().setSortColumn(column);
				refresh();
			}
		};
	}
}