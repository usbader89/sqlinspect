package sqlinspect.uiplugin.ui.viewsupport;

public final class SQLMarkers {
	public static final String SQLQUERY = "sqlinspect.marker.SQLQuery";
	public static final String SQLSMELL = "sqlinspect.marker.SQLSmell";
	public static final String SQLSMELL_ATTR_RULENAME = "rulename";
	
	private SQLMarkers() {
	}
}
