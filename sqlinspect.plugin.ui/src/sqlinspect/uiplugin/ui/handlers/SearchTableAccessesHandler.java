package sqlinspect.uiplugin.ui.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.search.ui.NewSearchUI;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.sqlan.asg.schema.Column;
import sqlinspect.sqlan.asg.schema.Table;
import sqlinspect.uiplugin.Context;
import sqlinspect.uiplugin.model.Project;
import sqlinspect.uiplugin.ui.search.TAASearchQuery;
import sqlinspect.uiplugin.ui.views.SchemaView;

public class SearchTableAccessesHandler extends AbstractHandler {
	private static final Logger logger = LoggerFactory.getLogger(SearchTableAccessesHandler.class);

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchPart part = HandlerUtil.getActivePart(event);

		if (part instanceof SchemaView) {
			handleViewSelection((SchemaView) part, event);
		} else {
			logger.error("Unhandled part for SearchTableAccessesHandler: {}", part);
		}

		return null;
	}

	private static void handleViewSelection(SchemaView view, ExecutionEvent event) {
		ISelection selection = view.getViewer().getSelection();
		Object obj = ((IStructuredSelection) selection).getFirstElement();

		Table t = null;
		Column c = null;
		if (obj instanceof Table) {
			t = (Table) obj;
		} else if (obj instanceof Column) {
			c = (Column) obj;
			t = (Table) c.getParent();
		} else {
			logger.error("Selection is not a table or column!");
			return;
		}

		if (t != null) {
			Project proj = Context.getInstance().getModel().getParentProject(t);
			TAASearchQuery query = new TAASearchQuery(proj, t, c);
			IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindow(event);
			NewSearchUI.runQueryInForeground(window, query);
			NewSearchUI.activateSearchResultView();
		} else {
			logger.error("Not a table/column was selected!");
		}
	}

}
