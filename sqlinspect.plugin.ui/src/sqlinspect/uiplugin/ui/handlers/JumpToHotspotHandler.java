package sqlinspect.uiplugin.ui.handlers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.part.EditorPart;
import org.eclipse.ui.part.FileEditorInput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.uiplugin.Context;
import sqlinspect.uiplugin.model.Hotspot;
import sqlinspect.uiplugin.model.Project;
import sqlinspect.uiplugin.model.Query;
import sqlinspect.uiplugin.ui.views.AbstractSQLInspectViewer;
import sqlinspect.uiplugin.ui.views.HotspotsView;

public class JumpToHotspotHandler extends AbstractHandler {
	private static final Logger logger = LoggerFactory.getLogger(JumpToHotspotHandler.class);

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchPart part = HandlerUtil.getActivePart(event);

		if (part instanceof EditorPart) {
			handleEditorSelection(event);
		} else if (part instanceof AbstractSQLInspectViewer) {
			handleQueriesViewSelection((AbstractSQLInspectViewer) part, event);
		} else {
			logger.error("Unhandled part for JumpToHotspotHandler: {}", part);
		}

		return null;
	}

	private static void handleQueriesViewSelection(AbstractSQLInspectViewer view, ExecutionEvent event) throws ExecutionException {
		ISelection selection = view.getViewer().getSelection();
		if (selection instanceof StructuredSelection) {
			// query selected in queries view
			StructuredSelection treeSel = (StructuredSelection) selection;
			@SuppressWarnings("unchecked")
			Stream<Query> queries = treeSel.toList().stream().filter(Query.class::isInstance).map(Query.class::cast);
			List<Hotspot> hotspots = queries.map(Query::getHotspot).collect(Collectors.toList());
			jumpToHotspots(event, hotspots);
		}
	}

	private static void handleEditorSelection(ExecutionEvent event) throws ExecutionException {
		ISelection selection = HandlerUtil.getCurrentSelection(event);
		if (selection instanceof ITextSelection) {
			// hotspot selected in text editor
			ITextSelection tsel = (ITextSelection) selection;
			int startline = tsel.getStartLine() + 1;
			IEditorPart editor = HandlerUtil.getActiveEditor(event);
			IEditorInput input = editor.getEditorInput();
			if (input instanceof FileEditorInput) {
				IPath path = ((FileEditorInput) input).getFile().getFullPath();
				jumpToHotspotByLocation(event, path, startline);
			}
		} else {
			logger.error("No hotspot was selected!");
		}
	}

	private static void jumpToHotspotByLocation(ExecutionEvent event, IPath path, int startline)
			throws ExecutionException {
		List<Hotspot> hotspots = new ArrayList<>();
		for (Project p : Context.getInstance().getModel().getProjects()) {
			List<Hotspot> ret = p.findHotspots(path, startline);
			hotspots.addAll(ret);
		}

		if (!hotspots.isEmpty()) {
			IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
			IWorkbenchPage page = window.getActivePage();
			try {
				HotspotsView hv = (HotspotsView) page.showView(HotspotsView.ID);
				hv.expandToHotspots();
				hv.select(hotspots);
			} catch (PartInitException e) {
				logger.error("Could not open view.", e);
			}
		}
	}

	private static void jumpToHotspots(ExecutionEvent event, List<Hotspot> hotspots) throws ExecutionException {
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		IWorkbenchPage page = window.getActivePage();
		try {
			HotspotsView hv = (HotspotsView) page.showView(HotspotsView.ID);
			hv.expandToHotspots();
			hv.select(hotspots);
		} catch (PartInitException e) {
			logger.error("Could not open view.", e);
		}
	}
}
