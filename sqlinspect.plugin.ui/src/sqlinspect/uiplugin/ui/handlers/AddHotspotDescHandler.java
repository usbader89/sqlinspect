package sqlinspect.uiplugin.ui.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.IWorkbenchWindow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.uiplugin.Activator;
import sqlinspect.uiplugin.utils.ASTUtils;
import sqlinspect.uiplugin.utils.BindingUtil;
import sqlinspect.uiplugin.utils.EditorUtility;

public class AddHotspotDescHandler extends AbstractHandler {
	private static final Logger logger = LoggerFactory.getLogger(AddHotspotDescHandler.class);

	protected String getHotspotDescOfMethodInvocation(MethodInvocation call, boolean withStringParam) {
		IMethodBinding mb = call.resolveMethodBinding();
		if (mb == null) {
			logger.error("Could not get method binding for node: {}", call);
			return null;
		}

		String qsig = BindingUtil.qualifiedSignature(mb);
		if (withStringParam) {
			int stringParam = getStringParamNum(mb);

			if (stringParam == 0) {
				logger.error("The method must have at least one parameter! {}", mb);
				return null;
			}
			return qsig + "/" + stringParam;
		} else {
			return qsig;
		}
	}

	protected ASTNode getSelectedNode(IEditorPart editor) {
		ISelectionService service = editor.getEditorSite().getWorkbenchWindow().getSelectionService();
		if (service == null) {
			logger.error("Could not get selection service for current workbench.");
			return null;
		}

		ICompilationUnit icu = EditorUtility.getEditorInputCU(editor.getEditorInput());
		if (icu == null) {
			logger.error("Could not get compilation unit for editor!");
			return null;
		}

		ISelection sel = service.getSelection();
		if (!(sel instanceof ITextSelection)) {
			logger.error("Selection is not a text selection!");
			return null;
		}

		ITextSelection textsel = (ITextSelection) sel;
		return ASTUtils.findASTNodeInICU(icu, textsel.getOffset(), textsel.getLength());
	}

	protected MethodInvocation getSelectedMethodInvocation(IEditorPart editor) {
		ASTNode node = getSelectedNode(editor);
		if (node == null) {
			logger.error("No node selected.");
			return null;
		}

		ASTNode miNode = ASTUtils.getParent(node, MethodInvocation.class);

		if (!(miNode instanceof MethodInvocation)) {
			logger.error("Selection must be a method invocation!");
			return null;
		}

		return (MethodInvocation) miNode;
	}

	protected String getHotspotDescription(IEditorPart editor, boolean withStringParam) {
		MethodInvocation mi = getSelectedMethodInvocation(editor);

		if (mi == null) {
			logger.error("Could not get method invocation!");
			return null;
		}

		return getHotspotDescOfMethodInvocation(mi, withStringParam);
	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		return null;
	}

	protected void showError(final IWorkbenchWindow window, final String message) {
		Runnable task = () -> MessageDialog.openError(window.getShell(), Activator.PLUGIN_ID, message);
		Display.getDefault().asyncExec(task);
	}

	protected void showSuccess(final IWorkbenchWindow window, final String message) {
		Runnable task = () -> MessageDialog.openInformation(window.getShell(), Activator.PLUGIN_ID, message);
		Display.getDefault().asyncExec(task);
	}

	private int getStringParamNum(IMethodBinding binding) {
		ITypeBinding[] parTypes = binding.getParameterTypes();
		for (int i = 0; i < parTypes.length; i++) {
			ITypeBinding pt = parTypes[i];
			if (pt.getQualifiedName().equals("java.lang.String")) {
				return i + 1;
			}
		}

		return 0;
	}
}
