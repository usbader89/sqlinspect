package sqlinspect.uiplugin.ui.handlers;

import java.io.IOException;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.uiplugin.Activator;
import sqlinspect.uiplugin.Context;
import sqlinspect.uiplugin.model.Model;
import sqlinspect.uiplugin.model.Project;
import sqlinspect.uiplugin.model.Query;
import sqlinspect.uiplugin.ui.dialogs.QueriesExportDialog;
import sqlinspect.uiplugin.utils.EditorUtility;
import sqlinspect.uiplugin.utils.XMLQueries;

public class QueriesExportHandler extends AbstractHandler {
	private static final Logger logger = LoggerFactory.getLogger(QueriesExportHandler.class);
	private final Model model = Context.getInstance().getModel();

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		IProject iproj = EditorUtility.getSelectedProject(window);
		if (iproj == null) {
			MessageDialog.openError(window.getShell(), Activator.PLUGIN_ID, "Please, select a project first!");
			return null;
		}

		Project project = model.getProject(iproj);
		if (project == null || !project.isAnalyzed()) {
			MessageDialog.openError(window.getShell(), Activator.PLUGIN_ID,
					"The project has not been analyzed yet. Please, analyze it first!");
			return null;
		}

		QueriesExportDialog ed = new QueriesExportDialog(window.getShell(), project);
		ed.create();

		if (ed.open() == Window.OK) {
			String exportFile = ed.getOutputFile();
			try {
				List<Query> queries = project.getQueries();
				XMLQueries xmlQueries = new XMLQueries(exportFile);
				xmlQueries.writeQueries(queries, false);
			} catch (IOException e) {
				logger.error("IO error while writing queries: ", e);
				MessageDialog.openError(window.getShell(), Activator.PLUGIN_ID,
						"Could not write to file. See log for details.");
			}

			MessageDialog.openInformation(window.getShell(), Activator.PLUGIN_ID,
					"Queries were successfully exported to " + exportFile);
		}

		return null;
	}
}
