package sqlinspect.uiplugin.ui.handlers;

import java.io.File;
import java.util.Optional;
import java.util.Set;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.handlers.HandlerUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.sqlan.BenchMark;
import sqlinspect.sqlan.parser.ParserException;
import sqlinspect.sqlan.smells.common.SQLSmell;
import sqlinspect.sqlan.smells.common.SQLSmellCertKind;
import sqlinspect.uiplugin.Activator;
import sqlinspect.uiplugin.Context;
import sqlinspect.uiplugin.PropertyStore;
import sqlinspect.uiplugin.model.Model;
import sqlinspect.uiplugin.model.Project;
import sqlinspect.uiplugin.model.Query;
import sqlinspect.uiplugin.smells.common.AndroidSmell;
import sqlinspect.uiplugin.smells.common.AndroidSmellCertKind;
import sqlinspect.uiplugin.sqlan.SQLAnWrapper;
import sqlinspect.uiplugin.ui.views.DashBoardView;
import sqlinspect.uiplugin.ui.views.HotspotsView;
import sqlinspect.uiplugin.ui.views.MetricsView;
import sqlinspect.uiplugin.ui.views.QueriesView;
import sqlinspect.uiplugin.ui.views.SchemaView;
import sqlinspect.uiplugin.ui.viewsupport.AndroidMarkers;
import sqlinspect.uiplugin.ui.viewsupport.SQLMarkers;
import sqlinspect.uiplugin.utils.ASTUtils;

public class AnalyzeHandler extends AbstractHandler {
	private static final Logger logger = LoggerFactory.getLogger(AnalyzeHandler.class);
	private static final Model model = Context.getInstance().getModel();

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		final IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);

		ISelectionService service = window.getSelectionService();
		if (service == null) {
			logger.error("Could not get selection service for current workbench.");
			return null;
		}

		final Optional<IProject> selectedProject = getSelectedProject(service);
		if (!selectedProject.isPresent()) {
			MessageDialog.openError(window.getShell(), Activator.PLUGIN_ID, "Please, select a project first!");
			return null;
		}

		try {
			if (!(selectedProject.get().hasNature(JavaCore.NATURE_ID))) {
				MessageDialog.openError(window.getShell(), Activator.PLUGIN_ID,
						"The selected project has to be a Java Project!");
				return null;
			}
		} catch (CoreException e) {
			logger.error("Could not retrieve nature of the project.", e);
			return null;
		}
		
		final Optional<IJavaElement> selectedElement = getSelectedElement(service);		

		final Project project = model.createProject(selectedProject.get());

		if (project.isAnalyzed()) {
			boolean ret = MessageDialog.openConfirm(window.getShell(), Activator.PLUGIN_ID,
					"This project has been analyzed already, the results of the previous analysis will be lost. Are you sure you want to reanalyze it?");
			if (!ret) {
				return null;
			}
		}

		Job job = new Job("Analyzing " + project.getName()) {
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				SubMonitor subMonitor = SubMonitor.convert(monitor, 100);
				subMonitor.setTaskName("Analyzing project " + project.getName());

				SQLAnWrapper sqlanwrapper = project.getSQLAn();
				IJavaProject javaProject = JavaCore.create(project.getIProject());
				BenchMark bm = new BenchMark();
				bm.startAll("AnalyzeProjectHandler.execute(" + javaProject.getElementName() + "):ALL");

				project.clear();
				clearMarkers(javaProject);

				try {
					sqlanwrapper.initAnalyzer();
				} catch (ParserException e) {
					logger.error("SQLAnWrapper init error!", e);
					showError(window, "Could not initialize the analyzer! Please, check the error log for details.");
				}

				boolean loadschema = PropertyStore.getBooleanProperty(project.getIProject(),
						PropertyStore.PROP_LOAD_SCHEMA_FROM_DB, PropertyStore.DEFAULT_LOAD_SCHEMA_FROM_DB);
				String schemaFile = PropertyStore.getStringProperty(project.getIProject(), PropertyStore.PROP_DB_SCHEMA,
						PropertyStore.DEFAULT_DB_SCHEMA);

				if (!loadschema && schemaFile != null && !schemaFile.isEmpty()) {
					subMonitor.setTaskName("Parsing schema: " + schemaFile);
					subMonitor.worked(5);
					sqlanwrapper.parseSchemaFromFile(schemaFile);
				}

				subMonitor.setTaskName("Extracting hotspots.");
				subMonitor.setWorkRemaining(90);

				bm.startAll("AnalyzeProjectHandler.execute(" + javaProject.getElementName() + "):HOTSPOTS");
				boolean ret = sqlanwrapper.analyze(selectedElement.orElse(null), subMonitor.split(30));
				if (!ret) {
					showError(window, "Error occurred while extracting queries from the project "
							+ project.getIProject().getName() + "! See the log file for details!");
				}
				bm.stopAll("AnalyzeProjectHandler.execute(" + javaProject.getElementName() + "):HOTSPOTS");

				subMonitor.setTaskName("Parsing SQL.");

				bm.startAll("AnalyzeProjectHandler.execute(" + javaProject.getElementName() + "):QUERIES");
				ret = sqlanwrapper.analyzeQueries(subMonitor.split(40));
				if (!ret) {
					showError(window, "Error occurred while analyzing queries from the project "
							+ project.getIProject().getName() + "! See the log file for details!");
				}
				bm.stopAll("AnalyzeProjectHandler.execute(" + javaProject.getElementName() + "):QUERIES");

				subMonitor.setTaskName("Calculate metrics.");

				bm.startAll("AnalyzeProjectHandler.execute(" + javaProject.getElementName() + "):JAVAMETRICS");
				project.getJavaMetrics().calculate();
				bm.stopAll("AnalyzeProjectHandler.execute(" + javaProject.getElementName() + "):JAVAMETRICS");

				subMonitor.setTaskName("Run Android smells.");

				bm.startAll("AnalyzeProjectHandler.execute(" + javaProject.getElementName() + "):ANDROIDSMELLS");
				project.getAndroidSmells().runSmellDetectors();
				bm.stopAll("AnalyzeProjectHandler.execute(" + javaProject.getElementName() + "):ANDROIDSMELLS");

				subMonitor.worked(10);
				subMonitor.setTaskName("Update views.");

				project.setAnalyzed(true);

				bm.startAll("AnalyzeProjectHandler.execute(" + javaProject.getElementName() + "):UPDATE");
				updateViews(window);
				updateSQLSmellMarkers(project);
				updateAndroidSmellMarkers(project);
				updateHotspotMarkers(project);
				bm.stopAll("AnalyzeProjectHandler.execute(" + javaProject.getElementName() + "):UPDATE");

				bm.stopAll("AnalyzeProjectHandler.execute(" + javaProject.getElementName() + "):ALL");

				subMonitor.worked(8);
				if (PropertyStore.getBooleanProperty(project.getIProject(), PropertyStore.PROP_DUMP_STATISTICS,
						PropertyStore.DEFAULT_DUMP_STATISTICS)) {
					subMonitor.setTaskName("Dump statistics.");

					bm.dumpFile(new File(project.getIProject().getName() + "-SQLInspect.stats"));
					project.getSQLAn().dumpBM(new File(project.getIProject().getName() + "-SQLAn.stats"));
					project.dumpStats(new File(project.getIProject().getName() + "-Model.stats"));
				}

				subMonitor.worked(2);

				showSuccess(window, project.getIProject().getName() + " project was analyzed successfully!");
				return Status.OK_STATUS;
			}
		};

		job.schedule();

		return null;
	}

	private Optional<IProject> getSelectedProject(ISelectionService service) {
		ISelection sel = service.getSelection();
		if (sel instanceof IStructuredSelection) {
			Object element = ((IStructuredSelection) sel).getFirstElement();
			if (element instanceof IJavaElement) {
				return Optional.of(((IJavaElement) element).getJavaProject().getProject());
			} else if (element instanceof IProject) {
				return Optional.of((IProject) element);
			}
		}
		return Optional.empty();
	}

	private Optional<IJavaElement> getSelectedElement(ISelectionService service) {
		ISelection sel = service.getSelection();
		if (sel instanceof IStructuredSelection) {
			Object element = ((IStructuredSelection) sel).getFirstElement();
			if (element instanceof IPackageFragment || element instanceof ICompilationUnit) {
				return Optional.of((IJavaElement) element);
			}
		}
		return Optional.empty();
	}

	private void showError(final IWorkbenchWindow window, final String message) {
		Runnable task = () -> MessageDialog.openError(window.getShell(), Activator.PLUGIN_ID, message);
		Display.getDefault().asyncExec(task);
	}

	private void showSuccess(final IWorkbenchWindow window, final String message) {
		Runnable task = () -> MessageDialog.openInformation(window.getShell(), Activator.PLUGIN_ID, message);
		Display.getDefault().asyncExec(task);
	}

	private void updateViews(final IWorkbenchWindow window) {
		Runnable task = () -> {
			try {
				IWorkbenchPage wbPage = window.getActivePage();
				QueriesView qv = (QueriesView) wbPage.showView(QueriesView.ID);
				if (qv != null) {
					qv.refresh();
				} else {
					logger.debug("Could not retrieve view: {}", QueriesView.ID);
				}
				HotspotsView hv = (HotspotsView) wbPage.findView(HotspotsView.ID);
				if (hv != null) {
					hv.refresh();
				} else {
					logger.debug("Could not retrieve view: {}", HotspotsView.ID);
				}
				SchemaView schv = (SchemaView) wbPage.findView(SchemaView.ID);
				if (schv != null) {
					schv.refresh();
				} else {
					logger.debug("Could not retrieve view: {}", SchemaView.ID);
				}
				MetricsView mv = (MetricsView) wbPage.findView(MetricsView.ID);
				if (mv != null) {
					mv.refresh();
				} else {
					logger.debug("Could not retrieve view: {}", MetricsView.ID);
				}

				DashBoardView dbv = (DashBoardView) wbPage.findView(DashBoardView.ID);
				if (dbv != null) {
					dbv.refreshProject();
				} else {
					logger.debug("Could not retrieve view: {}", DashBoardView.ID);
				}
			} catch (PartInitException e) {
				logger.error("Could not update views!", e);
			}
		};
		Display.getDefault().asyncExec(task);
	}

	private int toPriority(SQLSmellCertKind severity) {
		switch (severity) {
		case HIGH_CERTAINTY:
			return IMarker.PRIORITY_HIGH;
		case NORMAL_CERTAINTY:
			return IMarker.PRIORITY_NORMAL;
		case LOW_CERTAINTY:
			return IMarker.PRIORITY_NORMAL;
		default:
			return IMarker.PRIORITY_NORMAL;
		}
	}

	private void updateSQLSmellMarkers(Project project) {
		Set<SQLSmell> smells = project.getSQLAn().getSmells();
		IWorkspace workspace = ResourcesPlugin.getWorkspace();

		for (SQLSmell s : smells) {
			String sourcePath = s.getNode().getPath();

			IPath path = Path.fromOSString(sourcePath);
			IFile file = null;
			try {
				file = workspace.getRoot().getFile(path);
			} catch (IllegalArgumentException e) {
				logger.warn("Wrong path: ", e);
			}

			if (file == null) {
				logger.warn("Could not get file for smell (probably outside of workspace): {}", s);
				continue;
			}

			ICompilationUnit compUnit = (ICompilationUnit) JavaCore.create(file);

			if (compUnit == null) {
				logger.warn("Could not get compilation unit for smell: {} in file: {}", s, sourcePath);
				continue;
			}

			logger.debug("New sql smell marker for compilation unit: {}", sourcePath);

			try {
				final IResource resource = compUnit.getCorrespondingResource();

				if (resource == null) {
					logger.error("Could not get resource for compilation unit! {}", path);
					return;
				}

				final String message = s.getMessage();
				final int lineNo = s.getNode().getStartLine();
				final int prio = toPriority(s.getCertainty());
				final String kind = s.getKind().toString();
				final int sev = IMarker.SEVERITY_WARNING;

				IMarker[] prevmarkers = resource.findMarkers(SQLMarkers.SQLSMELL, false, IResource.DEPTH_ZERO);
				boolean alreadyset = false;
				for (IMarker m : prevmarkers) {
					final String pmessage = m.getAttribute(IMarker.MESSAGE, null);
					final int plineNo = m.getAttribute(IMarker.LINE_NUMBER, 0);
					final int pprio = m.getAttribute(IMarker.PRIORITY, 0);
					final int psev = m.getAttribute(IMarker.SEVERITY, 0);
					final String pkind = m.getAttribute(SQLMarkers.SQLSMELL_ATTR_RULENAME, null);
					if (lineNo == plineNo && message.equals(pmessage) && prio == pprio && kind.equals(pkind)
							&& sev == psev) {
						alreadyset = true;
					}
				}

				if (!alreadyset) {
					final IMarker marker = resource.createMarker(SQLMarkers.SQLSMELL);
					marker.setAttribute(IMarker.MESSAGE, message);
					marker.setAttribute(IMarker.PRIORITY, prio);
					marker.setAttribute(IMarker.LINE_NUMBER, lineNo);
					marker.setAttribute(IMarker.SEVERITY, sev);
					marker.setAttribute(SQLMarkers.SQLSMELL_ATTR_RULENAME, kind);
				}
			} catch (CoreException e) {
				logger.error("Error creating marker!", e);
			}
		}
	}

	private void updateAndroidSmellMarkers(Project project) {
		Set<AndroidSmell> smells = project.getAndroidSmells().getSmells();
		IWorkspace workspace = ResourcesPlugin.getWorkspace();

		for (AndroidSmell s : smells) {
			IPath path = ASTUtils.getPath(s.getNode());
			IFile file = null;
			try {
				file = workspace.getRoot().getFile(path);
			} catch (IllegalArgumentException e) {
				logger.warn("Wrong path: ", e);
			}

			if (file == null) {
				logger.warn("Could not get file for smell (probably outside of workspace): {}", s);
				continue;
			}

			ICompilationUnit compUnit = (ICompilationUnit) JavaCore.create(file);

			if (compUnit == null) {
				logger.warn("Could not get compilation unit for smell: {} in file: {}", s, path.toOSString());
				continue;
			}

			logger.debug("New android smell marker for compilation unit: {}", path.toOSString());

			try {
				final IResource resource = compUnit.getCorrespondingResource();

				if (resource == null) {
					logger.error("Could not get resource for compilation unit! {}", path.toOSString());
					return;
				}

				final String message = s.getMessage();
				final int lineNo = ASTUtils.getLineNumber(s.getNode());
				final int prio = toPriority(s.getCertainty());
				final String kind = s.getKind().toString();
				final int sev = IMarker.SEVERITY_WARNING;

				IMarker[] prevmarkers = resource.findMarkers(AndroidMarkers.ANDROIDSMELL, false, IResource.DEPTH_ZERO);
				boolean alreadyset = false;
				for (IMarker m : prevmarkers) {
					final String pmessage = m.getAttribute(IMarker.MESSAGE, null);
					final int plineNo = m.getAttribute(IMarker.LINE_NUMBER, 0);
					final int pprio = m.getAttribute(IMarker.PRIORITY, 0);
					final int psev = m.getAttribute(IMarker.SEVERITY, 0);
					final String pkind = m.getAttribute(AndroidMarkers.SMELL_ATTR_RULENAME, null);
					if (lineNo == plineNo && message.equals(pmessage) && prio == pprio && kind.equals(pkind)
							&& sev == psev) {
						alreadyset = true;
					}
				}

				if (!alreadyset) {
					final IMarker marker = resource.createMarker(AndroidMarkers.ANDROIDSMELL);
					marker.setAttribute(IMarker.MESSAGE, message);
					marker.setAttribute(IMarker.PRIORITY, prio);
					marker.setAttribute(IMarker.LINE_NUMBER, lineNo);
					marker.setAttribute(IMarker.SEVERITY, sev);
					marker.setAttribute(AndroidMarkers.SMELL_ATTR_RULENAME, kind);
				}
			} catch (CoreException e) {
				logger.error("Error creating marker!", e);
			}
		}
	}

	private int toPriority(AndroidSmellCertKind certainty) {
		switch (certainty) {
		case HIGH_CERTAINTY:
			return IMarker.PRIORITY_HIGH;
		case NORMAL_CERTAINTY:
			return IMarker.PRIORITY_NORMAL;
		case LOW_CERTAINTY:
			return IMarker.PRIORITY_NORMAL;
		default:
			return IMarker.PRIORITY_NORMAL;
		}
	}

	private void clearMarkers(IJavaProject pr) {
		logger.debug("Deleting markers for project: {}", pr.getElementName());

		try {
			IResource resource = pr.getCorrespondingResource();
			clearMarkers(resource, IResource.DEPTH_INFINITE);
		} catch (JavaModelException e) {
			logger.error("Could not delete markers! Could not get resource for project!", e);
		}
	}

	private void clearMarkers(IResource resource, int depth) {
		try {
			resource.deleteMarkers(SQLMarkers.SQLSMELL, true, depth);
			resource.deleteMarkers(SQLMarkers.SQLQUERY, true, depth);
			resource.deleteMarkers(AndroidMarkers.ANDROIDSMELL, true, depth);
		} catch (CoreException e) {
			logger.error("Could not delete markers for resource!", e);
		}
	}

	private void updateHotspotMarkers(Project project) {
		IWorkspace workspace = ResourcesPlugin.getWorkspace();

		for (Query q : project.getQueries()) {
			CompilationUnit cu = q.getHotspot().getUnit();
			String sourcePath = (String) cu.getProperty("SOURCE_WSPATH");
			IPath path = Path.fromOSString(sourcePath);
			IFile file = workspace.getRoot().getFile(path);
			ICompilationUnit compUnit = (ICompilationUnit) JavaCore.create(file);

			if (compUnit == null) {
				logger.error("Could not get compilation unit for query: {}", q);
				continue;
			}

			logger.debug("New query marker for compilation unit: {}", sourcePath);

			try {
				IResource resource = compUnit.getCorrespondingResource();

				if (resource == null) {
					logger.error("Could not get resource for compilation unit! {}", path.toOSString());
					return;
				}

				IMarker marker = resource.createMarker(SQLMarkers.SQLQUERY);
				marker.setAttribute(IMarker.MESSAGE, "Query: " + q.getValue());
				marker.setAttribute(IMarker.PRIORITY, IMarker.PRIORITY_LOW);
				marker.setAttribute(IMarker.LINE_NUMBER, ASTUtils.getLineNumber(q.getHotspot().getExec()));
				marker.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_INFO);
			} catch (CoreException e) {
				logger.error("Error creating query marker!", e);
			}
		}
	}
}
