package sqlinspect.uiplugin.ui.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.handlers.HandlerUtil;

import sqlinspect.uiplugin.ui.views.HotspotsView;
import sqlinspect.uiplugin.ui.views.MetricsView;

public class CollapseAllHandler extends AbstractHandler {
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchPart part = HandlerUtil.getActivePart(event);

		if (part instanceof HotspotsView) {
			HotspotsView hsv = (HotspotsView) part;
			hsv.collapseAll();
		} else if (part instanceof MetricsView) {
			MetricsView mv = (MetricsView) part;
			mv.collapseAll();
		}

		return null;
	}
}
