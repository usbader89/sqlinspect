package sqlinspect.uiplugin.ui.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import sqlinspect.uiplugin.Activator;
import sqlinspect.uiplugin.Context;
import sqlinspect.uiplugin.model.Model;
import sqlinspect.uiplugin.model.Project;
import sqlinspect.uiplugin.ui.dialogs.SQLMetricsExportDialog;
import sqlinspect.uiplugin.utils.EditorUtility;

public class SQLMetricsExportHandler extends AbstractHandler {
	private final Model model = Context.getInstance().getModel();

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);

		IProject iproj = EditorUtility.getSelectedProject(window);
		if (iproj == null) {
			MessageDialog.openError(window.getShell(), Activator.PLUGIN_ID, "Please, select a project first!");
			return null;
		}

		Project project = model.getProject(iproj);
		if (project == null || !project.isAnalyzed()) {
			MessageDialog.openError(window.getShell(), Activator.PLUGIN_ID,
					"The project has not been analyzed yet. Please, analyze it first!");
			return null;
		}

		SQLMetricsExportDialog ed = new SQLMetricsExportDialog(window.getShell(), project);
		ed.create();

		if (ed.open() == Window.OK) {
			String exportFile = ed.getOutputFile();
			if (exportFile == null || exportFile.isEmpty()) {
				return null;
			}

			String exportFormat = "";

			int i = exportFile.lastIndexOf('.');
			if (i > 0) {
				exportFormat = exportFile.substring(i + 1);
			}

			boolean ret = project.getSQLAn().reportSQLMetrics(exportFile, exportFormat);

			MessageDialog.openInformation(window.getShell(), Activator.PLUGIN_ID,
					"SQL metrics were successfully exported to " + exportFile);

			return ret;
		}

		return null;
	}
}
