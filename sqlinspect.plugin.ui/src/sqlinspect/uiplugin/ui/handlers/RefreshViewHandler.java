package sqlinspect.uiplugin.ui.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.handlers.HandlerUtil;

import sqlinspect.uiplugin.ui.views.AbstractSQLInspectViewer;

public class RefreshViewHandler extends AbstractHandler {
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchPart part = HandlerUtil.getActivePart(event);

		if (part instanceof AbstractSQLInspectViewer) {
			AbstractSQLInspectViewer view = (AbstractSQLInspectViewer) part;
			view.refresh();
		}

		return null;
	}
}
