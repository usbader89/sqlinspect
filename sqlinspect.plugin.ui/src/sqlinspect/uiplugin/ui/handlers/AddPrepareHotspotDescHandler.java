package sqlinspect.uiplugin.ui.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.uiplugin.ui.properties.JDBCHotspotsPropertyPage;
import sqlinspect.uiplugin.utils.EditorUtility;

public class AddPrepareHotspotDescHandler extends AddHotspotDescHandler {
	private static final Logger logger = LoggerFactory.getLogger(AddPrepareHotspotDescHandler.class);

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IEditorPart editor = HandlerUtil.getActiveEditor(event);
		String hsDesc = getHotspotDescription(editor, true);
		if (hsDesc == null) {
			logger.error(
					"Error retrieving the signature of the selected coude range. Is this a method invocation? Please check the log for details.");
			return null;
		}

		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindow(event);		
		ICompilationUnit icu = EditorUtility.getEditorInputCU(editor.getEditorInput());
		IProject proj = icu.getJavaProject().getProject();
		JDBCHotspotsPropertyPage.addPrepareHotspotToProject(proj, hsDesc);
		showSuccess(window, "'" + hsDesc + "' added to project '" + proj.getName() + "' as Prepare Hotspot");
		return null;
	}

}
