package sqlinspect.uiplugin.ui.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.texteditor.ITextEditor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.uiplugin.model.Query;
import sqlinspect.uiplugin.ui.views.AbstractSQLInspectViewer;
import sqlinspect.uiplugin.utils.EditorUtility;

public class ShowQueryHandler extends AbstractHandler {
	private static final Logger logger = LoggerFactory.getLogger(ShowQueryHandler.class);

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchPart part = HandlerUtil.getActivePart(event);

		if (part instanceof AbstractSQLInspectViewer) {
			handleViewSelection((AbstractSQLInspectViewer) part);
		} else {
			logger.error("Unhandled part for ShowQueryHandler: {}", part);
		}

		return null;
	}

	private static void handleViewSelection(AbstractSQLInspectViewer view) {
		ISelection selection = view.getViewer().getSelection();
		Object obj = ((IStructuredSelection) selection).getFirstElement();

		if (obj instanceof Query) {
			Query q = (Query) obj;
			ASTNode node = q.getHotspot().getExec();
			if (node != null) {
				int offset = node.getStartPosition();
				int length = node.getLength();
				CompilationUnit cu = q.getHotspot().getUnit();
				String sourcePath = (String) cu.getProperty("SOURCE_WSPATH");
				IPath path = Path.fromOSString(sourcePath);
				IEditorPart editorPart = EditorUtility.openEditor(path);
				if (editorPart instanceof ITextEditor) {
					ITextEditor editor = (ITextEditor) editorPart;
					EditorUtility.selectInEditor(editor, offset, length);
				}
			}

		} else {
			logger.error("Selection must be a query!");
		}
	}

}
