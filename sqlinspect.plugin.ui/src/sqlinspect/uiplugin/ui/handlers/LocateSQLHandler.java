package sqlinspect.uiplugin.ui.handlers;

import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import sqlinspect.sqlan.asg.statm.Statement;
import sqlinspect.uiplugin.Activator;
import sqlinspect.uiplugin.Context;
import sqlinspect.uiplugin.model.Model;
import sqlinspect.uiplugin.model.Project;
import sqlinspect.uiplugin.ui.dialogs.LocateQueryDialog;
import sqlinspect.uiplugin.utils.EditorUtility;

public class LocateSQLHandler extends AbstractHandler {
	private final Model model = Context.getInstance().getModel();

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);

		IProject iproj = EditorUtility.getSelectedProject(window);
		if (iproj == null) {
			MessageDialog.openError(window.getShell(), Activator.PLUGIN_ID, "Please, select a project first!");
			return null;
		}

		Project project = model.getProject(iproj);
		if (project == null || !project.isAnalyzed()) {
			MessageDialog.openError(window.getShell(), Activator.PLUGIN_ID,
					"The project has not been analyzed yet. Please, analyze it first!");
			return null;
		}

		LocateQueryDialog ed = new LocateQueryDialog(window.getShell(), project);
		ed.create();

		if (ed.open() == Window.OK) {
			String query = ed.getSQLQuery();
			if (query == null || query.isEmpty()) {
				return null;
			}

			List<Statement> ret = project.getSQLAn().locateQuery(query, false, false, false);

			if (ret != null) {
				StringBuilder sb = new StringBuilder();
				for (Statement i : ret) {
					sb.append(i).append(' ');
				}

				MessageDialog.openInformation(window.getShell(), Activator.PLUGIN_ID,
						"Matches: " + Integer.toString(ret.size()) + " (" + sb.toString() + ")");
			} else {
				MessageDialog.openInformation(window.getShell(), Activator.PLUGIN_ID,
						"An error occurred, please see the logfile for details. Is your SQL syntactically correct?");
			}
		}

		return null;
	}
}
