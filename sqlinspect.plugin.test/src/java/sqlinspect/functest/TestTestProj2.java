package sqlinspect.functest;

import static org.junit.Assert.assertFalse;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmlunit.builder.DiffBuilder;
import org.xmlunit.builder.Input;
import org.xmlunit.builder.Input.Builder;
import org.xmlunit.diff.DefaultNodeMatcher;
import org.xmlunit.diff.Diff;
import org.xmlunit.diff.DifferenceEvaluators;
import org.xmlunit.diff.ElementSelector;
import org.xmlunit.diff.ElementSelectors;

import sqlinspect.sqlan.SQLDialect;
import sqlinspect.uiplugin.Context;
import sqlinspect.uiplugin.PropertyStore;
import sqlinspect.uiplugin.extractors.InterQueryResolver;
import sqlinspect.uiplugin.extractors.JDBCHotspotFinder;
import sqlinspect.uiplugin.model.Model;
import sqlinspect.uiplugin.model.Project;
import sqlinspect.uiplugin.model.Query;
import sqlinspect.uiplugin.sqlan.SQLAnWrapper;
import sqlinspect.uiplugin.utils.EclipseProjectUtil;
import sqlinspect.uiplugin.utils.XMLQueries;

public class TestTestProj2 extends AbstractProjectTest {
	private static final Logger logger = LoggerFactory.getLogger(TestTestProj2.class);

	private void setProjectProperties(IProject iproj) {
		PropertyStore.setStringProperty(iproj, PropertyStore.PROP_STRING_RESOLVER,
				InterQueryResolver.class.getSimpleName());
		PropertyStore.setStringProperty(iproj, PropertyStore.PROP_HOTSPOT_FINDER,
				JDBCHotspotFinder.class.getSimpleName());
		PropertyStore.setStringProperty(iproj, PropertyStore.PROP_DIALECT, SQLDialect.MYSQL.toString());
		PropertyStore.setBooleanProperty(iproj, PropertyStore.PROP_RUN_SMELL_DETECTORS, false);
		PropertyStore.setBooleanProperty(iproj, PropertyStore.PROP_RUN_SQL_METRICS, false);
		PropertyStore.setBooleanProperty(iproj, PropertyStore.PROP_RUN_TABLE_ACCESS, false);
	}

	@BeforeClass
	public static void init() {
		Context.getInstance().loadResolvers();
	}

	@Test
	public void test() throws IOException, CoreException {
		String projectname = "Testproj2";
		String projectdir = TESTS_DIR + "/" + projectname;
		String queriesSuff = "-queries.xml";
		String queriesexp = projectname + queriesSuff;
		String queriesref = TEST_REF_DIR + "/" + projectname + queriesSuff;

		IProject iproj = EclipseProjectUtil.createEclipseProject(projectname, projectdir, null, "src");
		Model model = Context.getInstance().getModel();
		Project project = model.createProject(iproj);
		setProjectProperties(iproj);

		SQLAnWrapper sqlanwrapper = project.getSQLAn();
		sqlanwrapper.initAnalyzer();

		SubMonitor submonitor = SubMonitor.convert(new NullProgressMonitor());
		sqlanwrapper.analyze(null, submonitor);

		List<Query> queries = project.getQueries();
		XMLQueries xmlQueries = new XMLQueries(queriesexp);
		xmlQueries.writeQueries(queries, true);

		Builder out = Input.from(new File(queriesexp));
		Builder ref = Input.from(new File(queriesref));

		ElementSelector selector = ElementSelectors.conditionalBuilder().whenElementIsNamed("Query")
				.thenUse(ElementSelectors.and(ElementSelectors.byXPath("./Value", ElementSelectors.byNameAndText),
						ElementSelectors.byXPath("./ExecClass", ElementSelectors.byNameAndText),
						ElementSelectors.byXPath("./ExecLine", ElementSelectors.byNameAndText)))
				.elseUse(ElementSelectors.byName).build();

		Diff myDiff = DiffBuilder.compare(out).withTest(ref).withNodeMatcher(new DefaultNodeMatcher(selector))
				.withDifferenceEvaluator(DifferenceEvaluators.Default).checkForSimilar().build();

		if (myDiff.hasDifferences()) {
			logger.error("Diff: {}", myDiff);
		}

		assertFalse(myDiff.hasDifferences());
	}

}
