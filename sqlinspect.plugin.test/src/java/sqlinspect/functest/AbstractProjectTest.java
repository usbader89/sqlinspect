package sqlinspect.functest;

public abstract class AbstractProjectTest {

	protected static final String TESTS_DIR = System.getenv().get("TEST_RES_DIR") + "/tests";
	protected static final String TEST_REF_DIR = System.getenv().get("TEST_RES_DIR") + "/references";
}
