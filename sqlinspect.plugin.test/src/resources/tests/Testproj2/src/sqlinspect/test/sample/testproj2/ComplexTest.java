package sqlinspect.test.sample.testproj2;

import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;

public class ComplexTest {
	
	public void test_cfgdepth(int x, Connection conn) {
		Statement stmt;
		try {
			String asd = "0";
			if (x > 10) {
				asd += "1";
				if (x > 20) {
					asd += "2";
					if (x > 30) {
						asd += "3";
						if (x > 40) {
							asd += "4";
							if (x > 50) {
								asd += "5";
								if (x > 60) {
									asd += "6";
									if (x > 70) {
										asd += "7";
										if (x > 80) {
											asd += "8";
											if (x > 90) {
												asd += "9";
												if (x > 100) {
													asd += "A";
													if (x > 100) {
														asd += "B";
														if (x > 100) {
															asd += "C";
															if (x > 100) {
																asd += "D";
																if (x > 100) {
																	asd += "E";
																	if (x > 100) {
																		asd += "F";
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
			stmt = conn.createStatement();
			stmt.executeUpdate(asd);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
