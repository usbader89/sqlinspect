package sqlinspect.test.sample.testproj2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class InterExtractorTest {
	private static final String DB_URL = "jdbc:mysql://localhost/jdbcdemo";
	private static final String DB_USER = "demo";
	private static final String DB_PASSWORD = "demo";

	private static final String TABLE_CUSTOMERS = "customers";

	private Connection conn = null;

	/**
	 * Initialize the connection to the MySQL server.
	 */
	public void init() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(DB_URL + "?user=" + DB_USER + "&password=" + DB_PASSWORD);
		} catch (ClassNotFoundException | SQLException e) {
			System.err.println("Could not connect to the database!");
			e.printStackTrace();
		}
	}

	/**
	 * Execute example queries.
	 */
	public void run() throws Exception {
		init();

		// test1(1999);

		close();
	}

	public void foo(Connection c) {
		try {
			test1(c, "year > 2000");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void bar(Connection c) {
		try {
			test1(c, "year <= 2000");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void test1(Connection c, String condition) throws SQLException {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		String sql = "SELECT * FROM t WHERE " + condition;

		stmt = conn.prepareStatement(sql);
		rs = stmt.executeQuery();
	}

	public void testrecur1(Connection c, String condition) throws SQLException {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		String sql = "SELECT * FROM t WHERE " + condition;

		if (condition != null) {
			testrecur1(c, condition);
		}

		stmt = conn.prepareStatement(sql);
		rs = stmt.executeQuery();
	}

	/**
	 * Close connection.
	 */
	private void close() {
		try {
			if (conn != null) {
				conn.close();
			}
		} catch (Exception e) {
			System.err.println("Could not close the database connection!");
			e.printStackTrace();
		}
	}

}
