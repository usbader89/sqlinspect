package sqlinspect.test.sample.testproj2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class LoopTest {
	private static final String DB_URL = "jdbc:mysql://localhost/jdbcdemo";
	private static final String DB_USER = "demo";
	private static final String DB_PASSWORD = "demo";

	private static final String TABLE_CUSTOMERS = "customers";

	private Connection conn = null;

	/**
	 * Initialize the connection to the MySQL server.
	 */
	public void init() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(DB_URL + "?user=" + DB_USER + "&password=" + DB_PASSWORD);
		} catch (ClassNotFoundException | SQLException e) {
			System.err.println("Could not connect to the database!");
			e.printStackTrace();
		}
	}

	/**
	 * Execute example queries.
	 */
	public void run() throws Exception {
		init();

		// test1(1999);

		close();
	}

	@SuppressWarnings("unused")
	private void testWhile(int year) throws SQLException {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		String sql = "SELECT * FROM " + TABLE_CUSTOMERS;
		
		while (year < 2000) {
			sql = sql + " WHERE year < " + year;
			++year;
		}

		stmt = conn.prepareStatement(sql);
		stmt.setInt(1, year);
		rs = stmt.executeQuery();
	}

	@SuppressWarnings("unused")
	private void testDo(int year) throws SQLException {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		String sql = "SELECT * FROM " + TABLE_CUSTOMERS;
		
		do {
			sql = sql + " WHERE year < " + year;
			++year;
		} while (year < 2000);

		stmt = conn.prepareStatement(sql);
		stmt.setInt(1, year);
		rs = stmt.executeQuery();
	}
	
	@SuppressWarnings("unused")
	private void testFor(int year) throws SQLException {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		String sql = "SELECT * FROM " + TABLE_CUSTOMERS + " WHERE year > 2000";
		
		for (year = 1995; year < 2000; year++) {
			sql = sql + " OR year = " + year; 
		}

		stmt = conn.prepareStatement(sql);
		stmt.setInt(1, year);
		rs = stmt.executeQuery();
	}
	
	@SuppressWarnings("unused")
	private void testFor2(int year) throws SQLException {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		String sql = "SELECT * FROM " + TABLE_CUSTOMERS + " WHERE year > 2000";
		
		for (year = 1995; year < 2000; year++) {
			String asd = sql + " OR year = " + year; 
		}

		stmt = conn.prepareStatement(sql);
		stmt.setInt(1, year);
		rs = stmt.executeQuery();
	}	
	
	/**
	 * Close connection.
	 */
	private void close() {
		try {
			if (conn != null) {
				conn.close();
			}
		} catch (Exception e) {
			System.err.println("Could not close the database connection!");
			e.printStackTrace();
		}
	}

}
