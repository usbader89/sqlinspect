package sqlinspect.com.androidproject.datasets;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import java.util.ArrayList;
import java.util.List;

import sqlinspect.com.androidproject.model.Test;
import sqlinspect.com.androidproject.util.SqlUtils;

public class SmellyTable {
    private static final String SMELLS_TABLE = "tbl_smells";
    private static final String SMELLS_TABLE2 = "tbl_smells2";
    private static final String SMELLS_TABLE3 = "tbl_smells3";

    protected static void createTables(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + SMELLS_TABLE + " ("
                + " smell_id INTEGER DEFAULT 0,"
                + " name TEXT,"
                + " PRIMARY KEY (smell_id))");
    }

    protected static void smellyCreate(SQLiteDatabase db) {
        final String create1="CREATE TABLE " + SMELLS_TABLE2 + " ("
                + " smell_id INTEGER DEFAULT 0,"
                + " name TEXT,"
                + " PRIMARY KEY (smell_id));";
        final String create2="CREATE TABLE " + SMELLS_TABLE3 + " ("
                + " smell_id INTEGER DEFAULT 0,"
                + " name TEXT,"
                + " PRIMARY KEY (smell_id));";
        db.execSQL(create1+create2); // the second create will not be executed here
        db.execSQL(create2); // it will be executed here!
    }


    protected static void dropTables(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + SMELLS_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + SMELLS_TABLE2);
        db.execSQL("DROP TABLE IF EXISTS " + SMELLS_TABLE3);
    }

    protected static void reset(SQLiteDatabase db) {
        dropTables(db);
        createTables(db);
    }

    public static void insertTable2Data(SQLiteDatabase db, int sid, String name) {
        db.execSQL("INSERT INTO "+SMELLS_TABLE2 + " (smell_id,  name) values("+sid+",'"+name+"')");
    }
    public static void insertTable3Data(SQLiteDatabase db, int sid, String name) {
        db.execSQL("INSERT INTO "+SMELLS_TABLE3 + " (smell_id, name) values("+sid+",'"+name+"')");
    }

    public static List<String> smellySelect(SQLiteDatabase db) {
        List<String> ret = new ArrayList<>();
        Cursor c1 = db.rawQuery("SELECT name FROM " + SMELLS_TABLE2, null);
        while (c1.moveToNext()) {
            ret.add(c1.getString(0));
        }
        Cursor c2 = db.rawQuery("SELECT * FROM " + SMELLS_TABLE3, null);
        while (c2.moveToNext()) {
            ret.add(c2.getString(0));
        }
        return ret;
    }

//    public void smellyMethod1(SQLiteDatabase db) {
//        db.execSQL("SELECT * FROM " + SMELLS_TABLE + ";SELECT * FROM " + SMELLS_TABLE);
//        db.execSQL("PRAGMA index_info('idx12');SELECT * FROM " + SMELLS_TABLE);
//    }

    public static void smellyMethod2(SQLiteDatabase db, int _id) {
        db.execSQL("UPDATE " + SMELLS_TABLE + " SET name='ASD' WHERE smell_id=" +_id);
        db.execSQL("DELETE FROM " + SMELLS_TABLE + " WHERE smell_id=" +_id);
        db.execSQL("INSERT INTO " + SMELLS_TABLE + " (smell_id, name) VALUES ("+_id+", 'ASD')");
    }

    public static void notSmellyMethod2(SQLiteDatabase db, int _id) {
        db.execSQL("ALTER TABLE " + SMELLS_TABLE + " ADD COLUMN new_col integer");
    }

    public static void smellyExecuteInsert(SQLiteDatabase db, String s, int _id) {
        SQLiteStatement stmt = db.compileStatement("UPDATE " + SMELLS_TABLE + " set name='?' WHERE smell_id=?");
        stmt.bindString(1,s);
        stmt.bindLong(2,_id);
        stmt.executeInsert();

        SQLiteStatement stmt2 = db.compileStatement("DELETE FROM " + SMELLS_TABLE + " WHERE smell_id=?");
        stmt2.bindLong(1,_id);
        stmt2.execute();

        SQLiteStatement stmt3 = db.compileStatement("SELECT * FROM " + SMELLS_TABLE + " WHERE smell_id=?");
        stmt3.bindLong(1,_id);
        stmt3.execute();

        SQLiteStatement stmt4 = db.compileStatement("SELECT smell_id, name FROM " + SMELLS_TABLE + " WHERE smell_id=?");
        stmt4.bindLong(1,_id);
        stmt4.execute();

        SQLiteStatement stmt5 = db.compileStatement(s);
        stmt5.executeInsert();
    }

}
