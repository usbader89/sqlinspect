package sqlinspect.com.androidproject.datasets;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import sqlinspect.com.androidproject.MainActivity;

public class MyDatabase extends SQLiteOpenHelper {
    protected static final String DB_NAME = "my.db";
    private static final int DB_VERSION = 135;

    private static MyDatabase myDb;
    private static final Object DB_LOCK = new Object();

    public static MyDatabase getDatabase() {
        if (myDb == null) {
            synchronized (DB_LOCK) {
                if (myDb == null) {
                    myDb = new MyDatabase(MainActivity.getContext());
                    myDb.getWritableDatabase();
                }
            }
        }
        return myDb;
    }

    public static SQLiteDatabase getReadableDb() {
        return getDatabase().getReadableDatabase();
    }

    public static SQLiteDatabase getWritableDb() {
        return getDatabase().getWritableDatabase();
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        // copyDatabase(db);
        // getDatabase().reset(db);
    }

    public static void reset() {
        SQLiteDatabase db = getWritableDb();
        getDatabase().reset(db);
    }

    public MyDatabase(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createAllTables(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        reset(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        reset(db);
    }

    private void createAllTables(SQLiteDatabase db) {
        TestTable.createTables(db);
        SmellyTable.createTables(db);
        SmellyTable.smellyCreate(db);
    }

    private void dropAllTables(SQLiteDatabase db) {
        TestTable.dropTables(db);
        SmellyTable.dropTables(db);
    }

    private void reset(SQLiteDatabase db) {
        db.beginTransaction();
        try {
            dropAllTables(db);
            createAllTables(db);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    private static void purge() {
        SQLiteDatabase db = getWritableDb();
        db.beginTransaction();
        try {
            int numPostsDeleted = TestTable.purge(db);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    public static void purgeAsync() {
        new Thread() {
            @Override
            public void run() {
                purge();
            }
        }.start();
    }

    private void copyDatabase(SQLiteDatabase db) {
        String copyFrom = db.getPath();
        String copyTo = MainActivity.getContext().getExternalFilesDir(null).getAbsolutePath() + "/" + DB_NAME;

        try {
            InputStream input = new FileInputStream(copyFrom);
            OutputStream output = new FileOutputStream(copyTo);

            byte[] buffer = new byte[1024];
            int length;
            while ((length = input.read(buffer)) > 0) {
                output.write(buffer, 0, length);
            }

            output.flush();
            output.close();
            input.close();
        } catch (IOException e) {
        }
    }
}
