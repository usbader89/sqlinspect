package sqlinspect.app;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.eclipse.jdt.core.IJavaProject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.sqlan.BenchMark;
import sqlinspect.sqlan.SQLDialect;
import sqlinspect.sqlan.parser.ParserException;
import sqlinspect.uiplugin.Context;
import sqlinspect.uiplugin.PropertyStore;
import sqlinspect.uiplugin.extractors.AndroidHotspotFinder;
import sqlinspect.uiplugin.extractors.InterQueryResolver;
import sqlinspect.uiplugin.extractors.JDBCHotspotFinder;
import sqlinspect.uiplugin.model.Model;
import sqlinspect.uiplugin.model.Project;
import sqlinspect.uiplugin.model.Query;
import sqlinspect.uiplugin.smells.common.AndroidSmell;
import sqlinspect.uiplugin.smells.reports.XMLSmellReporter;
import sqlinspect.uiplugin.sqlan.SQLAnWrapper;
import sqlinspect.uiplugin.utils.EclipseProjectUtil;
import sqlinspect.uiplugin.utils.XMLQueries;

public class SQLInspectApp implements IApplication {
	private static final Logger logger = LoggerFactory.getLogger(SQLInspectApp.class);

	private static final Context context = Context.getInstance();
	private static final Model model = context.getModel();

	private static final String OPT_HOTSPOTFINDER = "hotspotfinder";
	private static final String OPT_DIALECT = "dialect";
	private static final String OPT_PROJECTNAME = "projectname";
	private static final String OPT_PROJECTDIR = "projectdir";
	private static final String OPT_SRCDIR = "srcdir";
	private static final String OPT_PROJECTCLASSPATH = "projectcp";
	private static final String OPT_MAXCALLDEPTH = "maxcalldepth";
	private static final String OPT_MAXCFGDEPTH = "maxcfgdepth";

	private void setProjectProperties(IProject iproj, SQLDialect dialect, String hsfName, int maxCFGDepth,
			int maxCallDepth) {
		PropertyStore.setStringProperty(iproj, PropertyStore.PROP_STRING_RESOLVER,
				InterQueryResolver.class.getSimpleName());
		PropertyStore.setStringProperty(iproj, PropertyStore.PROP_HOTSPOT_FINDER, hsfName);
		PropertyStore.setStringProperty(iproj, PropertyStore.PROP_DIALECT, dialect.toString());
		PropertyStore.setBooleanProperty(iproj, PropertyStore.PROP_RUN_SMELL_DETECTORS, true);
		PropertyStore.setBooleanProperty(iproj, PropertyStore.PROP_RUN_SQL_METRICS, true);
		PropertyStore.setBooleanProperty(iproj, PropertyStore.PROP_RUN_TABLE_ACCESS, false);
		PropertyStore.setIntProperty(iproj, PropertyStore.PROP_INTERSR_MAXCALLDEPTH, maxCallDepth);
		PropertyStore.setIntProperty(iproj, PropertyStore.PROP_INTERSR_MAXNL, maxCFGDepth);
	}

	private void analyzeProject(Project project) {
		SQLAnWrapper sqlanwrapper = project.getSQLAn();
		IJavaProject javaProject = project.getIJavaProject();
		BenchMark bm = new BenchMark();
		bm.startAll("AnalyzeProjectHandler.execute(" + javaProject.getElementName() + "):ALL");

		project.clear();

		try {
			sqlanwrapper.initAnalyzer();
		} catch (ParserException e) {
			logger.error("SQLAnWrapper init error!", e);
		}

		bm.startAll("AnalyzeProjectHandler.execute(" + javaProject.getElementName() + "):HOTSPOTS");
		boolean ret = sqlanwrapper.analyze(null, SubMonitor.convert(new NullProgressMonitor()));
		if (!ret) {
			logger.error("Error occurred while extracting queries from the project " + project.getName()
					+ "! See the log file for details!");
		}
		bm.stopAll("AnalyzeProjectHandler.execute(" + javaProject.getElementName() + "):HOTSPOTS");

		bm.startAll("AnalyzeProjectHandler.execute(" + javaProject.getElementName() + "):QUERIES");
		ret = sqlanwrapper.analyzeQueries(SubMonitor.convert(new NullProgressMonitor()));
		if (!ret) {
			logger.error("Error occurred while analyzing queries from the project " + project.getName()
					+ "! See the log file for details!");
		}
		bm.stopAll("AnalyzeProjectHandler.execute(" + javaProject.getElementName() + "):QUERIES");

		bm.startAll("AnalyzeProjectHandler.execute(" + javaProject.getElementName() + "):JAVAMETRICS");
		project.getJavaMetrics().calculate();
		bm.stopAll("AnalyzeProjectHandler.execute(" + javaProject.getElementName() + "):JAVAMETRICS");

		bm.startAll("AnalyzeProjectHandler.execute(" + javaProject.getElementName() + "):ANDROIDSMELLS");
		project.getAndroidSmells().runSmellDetectors();
		bm.stopAll("AnalyzeProjectHandler.execute(" + javaProject.getElementName() + "):ANDROIDSMELLS");

		project.setAnalyzed(true);

		bm.stopAll("AnalyzeProjectHandler.execute(" + javaProject.getElementName() + "):ALL");

		bm.dumpFile(new File(project.getName() + "-SQLInspect.stats"));
		project.dumpStats(new File(project.getName() + "-Model.stats"));
	}

	private void exportQueries(Project project, String exportFile) {
		try {
			List<Query> queries = project.getQueries();
			XMLQueries xmlQueries = new XMLQueries(exportFile);
			xmlQueries.writeQueries(queries, false);
		} catch (IOException e) {
			logger.error("IO error while writing queries: ", e);
		}
	}

	private void exportAndroidSmells(Project project, String exportFile) {
		Set<AndroidSmell> smells = project.getAndroidSmells().getSmells();
		XMLSmellReporter xmlSmells = new XMLSmellReporter(exportFile);
		xmlSmells.writeReport(smells);
	}

	private void exportSQLSmells(Project project, String exportFile) {
		project.getSQLAn().reportSmells(exportFile, "xml");
	}

	private void exportSQLMetrics(Project project, String exportFile) {
		project.getSQLAn().reportSQLMetrics(exportFile, "xml");
	}

	private void exportJavaMetrics(Project project, String exportFile) {
		project.getJavaMetrics().report(exportFile, "xml");
	}

	private void analyzeDir(String projectName, String projectDir, String projectCP, String srcDir, SQLDialect dialect,
			String hsfName, int maxCFGDepth, int maxCallDepth) throws CoreException {
		logger.info("Analyze project '{}' under '{}' with source '{}'", projectName, projectDir, srcDir);

		try {
			IProject iproj = EclipseProjectUtil.createEclipseProject(projectName, projectDir, projectCP, srcDir);
			setProjectProperties(iproj, dialect, hsfName, maxCFGDepth, maxCallDepth);

			Project project = model.createProject(iproj);

			analyzeProject(project);

			exportQueries(project, project.getName() + "-queries.xml");
			exportAndroidSmells(project, project.getName() + "-androidsmells.xml");
			exportSQLSmells(project, project.getName() + "-sqlsmells.xml");
			exportSQLMetrics(project, project.getName() + "-sqlmetrics.xml");
			exportJavaMetrics(project, project.getName() + "-javametrics.xml");
		} catch (Exception e) {
			logger.error("Exception occurred: ", e);
		}

		logger.info("Parse directory done.");
	}

	private static void help() {
		System.out.format("SQLInspectApp [params]\n");
		System.out.format("  -%s <hsf> Hotspot finder. E.g., AndroidHotspotFinder\n", OPT_HOTSPOTFINDER);
		System.out.format("  -%s <dialect>   Dialect. E.g., SQLite\n", OPT_DIALECT);
		System.out.format("  -%s <maxCallDepth>   Maximum CALL depth, e.g., 5\n", OPT_MAXCALLDEPTH);
		System.out.format("  -%s <dialect>   Maximum CFG depth, e.g., 100\n", OPT_MAXCFGDEPTH);
		System.out.format("  -%s <name>  Name of the project. E.g., Test\n", OPT_PROJECTNAME);
		System.out.format("  -%s <dir>    Directory of the project. E.g., projects/Test\n", OPT_PROJECTDIR);
		System.out.format(
				"  -%s <dir>        Source dir under project dir (if not specified will be autodetected). E.g., src/\n",
				OPT_SRCDIR);
	}

	private Map<String, String> processArgs(String[] args) {
		if (args.length < 2 && args.length % 2 != 0) {
			throw new IllegalArgumentException("Invalid arguments.");
		}

		Map<String, String> conf = new HashMap<>();
		for (int i = 0; i < args.length; i++) {
			final String pkey = args[i];
			if (!pkey.isEmpty() && pkey.charAt(0) == '-') {
				conf.put(pkey.substring(1), args[i + 1]);
			}
		}

		return conf;
	}

	@Override
	public Object start(IApplicationContext context) throws Exception {
		String[] args = (String[]) context.getArguments().get("application.args");

		Map<String, String> conf;
		try {
			conf = processArgs(args);
		} catch (IllegalArgumentException e) {
			help();
			return IApplication.EXIT_OK;
		}

		String hsfName = conf.get(OPT_HOTSPOTFINDER);
		if (hsfName == null) {
			hsfName = JDBCHotspotFinder.class.getSimpleName();
		}

		String dialectName = conf.get(OPT_DIALECT);
		if (dialectName == null) {
			dialectName = SQLDialect.MYSQL.toString();
		}

		String projectName = conf.get(OPT_PROJECTNAME);
		String projectDir = conf.get(OPT_PROJECTDIR);

		if (projectDir == null || projectDir.isEmpty() || projectName == null || projectName.isEmpty()) {
			help();
			return IApplication.EXIT_OK;
		}

		SQLInspectApp.context.loadResolvers();

		String[] hsfs = new String[] { JDBCHotspotFinder.class.getSimpleName(),
				AndroidHotspotFinder.class.getSimpleName() };
		boolean foundHsf = false;
		for (String hsfn : hsfs) {
			if (hsfn.equals(hsfName)) {
				foundHsf = true;
				break;
			}
		}
		if (!foundHsf) {
			StringBuilder sb = new StringBuilder();
			for (String s : hsfs) {
				sb.append(s).append(' ');

			}
			logger.error("Invalid hotspot finder, options are: {}", sb);
			help();
			return IApplication.EXIT_OK;
		}

		SQLDialect dialect;
		try {
			dialect = SQLDialect.valueOf(dialectName.toUpperCase());
		} catch (IllegalArgumentException e) {
			StringBuilder sb = new StringBuilder();
			for (SQLDialect d : SQLDialect.values()) {
				sb.append(d.toString() + ' ');

			}
			logger.error("Invalid dialect, options are: {}", sb);
			help();
			return IApplication.EXIT_OK;
		}

		int maxCFGDepth = InterQueryResolver.DEFAULT_MAX_CFGDEPTH;
		int maxCallDepth = InterQueryResolver.DEFAULT_MAX_CALLDEPTH;

		if (conf.get(OPT_MAXCALLDEPTH) != null) {
			maxCallDepth = Integer.parseInt(conf.get(OPT_MAXCALLDEPTH));
		}

		if (conf.get(OPT_MAXCFGDEPTH) != null) {
			maxCFGDepth = Integer.parseInt(conf.get(OPT_MAXCFGDEPTH));
		}

		String srcDir = conf.get(OPT_SRCDIR);
		String projectCP = conf.get(OPT_PROJECTCLASSPATH);

		analyzeDir(projectName, projectDir, projectCP, srcDir, dialect, hsfName, maxCFGDepth, maxCallDepth);

		return IApplication.EXIT_OK;
	}

	@Override
	public void stop() {
		// nothing to do here
	}
}