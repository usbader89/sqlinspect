package sqlinspect.cfgvisualizer.graph;

import org.eclipse.gef.graph.Edge;
import org.eclipse.gef.graph.Edge.Builder;
import org.eclipse.gef.graph.Graph;
import org.eclipse.gef.graph.Node;
import org.eclipse.gef.zest.fx.ZestFxModule;
import org.eclipse.gef.zest.fx.ZestProperties;

import com.google.inject.Module;

public abstract class AbstractGraph {
	private static int id;
	protected static final String ID = ZestProperties.CSS_ID__NE;
	protected static final String LABEL = ZestProperties.LABEL__NE;
	protected static final String CSS_CLASS = ZestProperties.CSS_CLASS__NE;
	protected static final String LAYOUT_IRRELEVANT = ZestProperties.LAYOUT_IRRELEVANT__NE;

	protected abstract Graph createGraph();

	protected static String genId() {
		return Integer.toString(id++);
	}

	protected static Edge e(Node n, Node m, Object... attr) {
		String label = (String) n.attributesProperty().get(LABEL) + (String) m.attributesProperty().get(LABEL);
		Builder builder = new Edge.Builder(n, m).attr(LABEL, label).attr(ID, genId());
		for (int i = 0; i < attr.length; i += 2) {
			builder.attr(attr[i].toString(), attr[i + 1]);
		}
		return builder.buildEdge();
	}

	protected static Edge e(Graph graph, Node n, Node m, Object... attr) {
		Edge edge = e(n, m, attr);
		graph.getEdges().add(edge);
		return edge;
	}

	protected static Node n(String id, Object... attr) {
		Node.Builder builder = new Node.Builder();
		builder.attr(ID, id).attr(LABEL, id);
		for (int i = 0; i < attr.length; i += 2) {
			builder.attr(attr[i].toString(), attr[i + 1]);
		}
		return builder.buildNode();
	}

	protected static Node n(Graph graph, String id, Object... attr) {
		Node node = n(id, attr);
		graph.getNodes().add(node);
		return node;
	}

	protected Module createModule() {
		return new ZestFxModule();
	}
}
