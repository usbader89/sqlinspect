package sqlinspect.cfgvisualizer.ui.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.handlers.HandlerUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.cfg.CFG;
import sqlinspect.cfg.CFGBuilder;
import sqlinspect.cfgvisualizer.ui.views.CFGView;
import sqlinspect.uiplugin.Context;
import sqlinspect.uiplugin.utils.ASTUtils;

public class ShowLooplessCFGHandler extends AbstractHandler {
	private static final Logger logger = LoggerFactory.getLogger(ShowLooplessCFGHandler.class);

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		final IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		final ISelectionService service = window.getSelectionService();
		ISelection sel = service.getSelection();

		if (sel instanceof IStructuredSelection) {
			Object element = ((IStructuredSelection) sel).getFirstElement();
			if (element instanceof IMethod) {
				IMethod method = (IMethod) element;
				try {
					logger.debug("Method: {}", method.getSignature());
					ICompilationUnit unit = method.getCompilationUnit();
					CompilationUnit cu = Context.getInstance().getASTStore().get(unit);
					MethodDeclaration md = ASTUtils.findMethodDeclaration(cu, method);
					CFG cfg = CFGBuilder.build(md, true);
					cfg.dump();
					IWorkbenchPage wbPage = window.getActivePage();
					CFGView cv = (CFGView) wbPage.showView(CFGView.ID);
					cv.setGraph(cfg);
				} catch (JavaModelException | PartInitException e) {
					logger.error("Error:", e);
				}
			}
		}

		return null;
	}

}
