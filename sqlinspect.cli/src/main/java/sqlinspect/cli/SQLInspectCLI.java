package sqlinspect.cli;

import java.io.File;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sqlinspect.sqlan.Config;
import sqlinspect.sqlan.parser.ParserException;
import sqlinspect.sqlan.smells.common.SQLSmell;
import sqlinspect.sqlan.smells.reports.XMLSmellReporter;
import sqlinspect.uiplugin.Context;
import sqlinspect.uiplugin.extractors.AndroidHotspotFinder;
import sqlinspect.uiplugin.extractors.IHotspotFinder;
import sqlinspect.uiplugin.extractors.IQueryResolver;
import sqlinspect.uiplugin.extractors.InterQueryResolver;
import sqlinspect.uiplugin.extractors.JDBCHotspotFinder;
import sqlinspect.uiplugin.model.Model;
import sqlinspect.uiplugin.model.Project;
import sqlinspect.uiplugin.model.Query;
import sqlinspect.uiplugin.sqlan.SQLAnWrapper;
import sqlinspect.uiplugin.utils.XMLQueries;

public final class SQLInspectCLI {
	private static final String OPT_SMELLREPORTFORMAT = "smellreportformat";
	private static final String OPT_DBPASSWORD = "dbpassword";
	private static final String OPT_DBUSER = "dbuser";
	private static final String OPT_DBURL = "dburl";
	private static final String OPT_PARSESCHEMA = "parseschema";
	private static final String OPT_SMELLREPORT = "smellreport";
	private static final String OPT_RUNSMELLS = "runsmells";
	private static final String OPT_DUMPSYMTAB = "dumpsymtab";
	private static final String OPT_ERRORSFORMAT = "errorsformat";
	private static final String OPT_DUMPERRORS = "dumperrors";
	private static final String OPT_EXPORTJSON = "exportjson";
	private static final String OPT_CLASSPATH = "classpath";
	private static final String OPT_DUMPQUERIES = "dumpqueries";
	private static final String OPT_VERSION = "version";
	private static final String OPT_HELP = "help";
	private static final String OPT_HOTSPOTFINDER = "hotspotfinder";
	private static final String OPT_STRINGRESOLVER = "stringresolver";

	private static final String XML = "xml";
	private static final String TXT = "txt";
	private static final String[] FORMATS = { TXT, XML };

	private static final Logger logger = LoggerFactory.getLogger(SQLInspectCLI.class);

	private static final Options options = new Options();

	private static final Properties properties = new Properties();

	private static final Config conf = new Config();

	private static final Context context = Context.getInstance();
	private static final Model model = context.getModel();
	private static final String DEFAULT_PROJECT = "DEFAULT";
	private static final Project project = model.createProject(DEFAULT_PROJECT);
	private static final SQLAnWrapper sqlanwrapper = new SQLAnWrapper(project);

	private static Optional<IHotspotFinder> hsf;
	private static Optional<IQueryResolver> resolver;

	private SQLInspectCLI() {
		// private constructor to prevent instantiation
	}

	// use ASTParse to parse the file
	public static CompilationUnit parseFile(String filePath) {
		logger.debug("Parse file: {}", filePath);

		CompilationUnit cu = null;

		String fileContent;
		try {
			fileContent = FileUtils.readFileToString(new File(filePath), "UTF-8");
			ASTParser parser = ASTParser.newParser(AST.JLS9);
			parser.setSource(fileContent.toCharArray());
			parser.setUnitName(filePath);
			parser.setKind(ASTParser.K_COMPILATION_UNIT);
			parser.setResolveBindings(true);
			parser.setBindingsRecovery(true);

			Map<String, String> options = JavaCore.getOptions();
			parser.setCompilerOptions(options);

			String[] sources = properties.getProperty("srcdir", ".").split(":");
			String[] classpath = properties.getProperty(OPT_CLASSPATH, ".").split(":");

			parser.setEnvironment(classpath, sources, new String[] { "UTF-8" }, true);

			cu = (CompilationUnit) parser.createAST(null);

			boolean bindingsResolution = cu.getAST().hasResolvedBindings();
			logger.debug("Binding resolution: {}", bindingsResolution ? "OK" : "NO");

			boolean bindingRecovery = cu.getAST().hasBindingsRecovery();
			logger.debug("Binding recovery: {}", bindingRecovery ? "OK" : "NO");
		} catch (IllegalArgumentException | IOException e) {
			logger.error("Error parsing the file: " + filePath, e);
		}

		logger.debug("Parse file done.");
		return cu;
	}

	public static void analyzeDir(String dir, boolean recursive) {
		logger.info("Parse directory: {}", dir);

		if (!hsf.isPresent()) {
			logger.error("There is no hotspot finder specified");
			return;
		}

		if (!resolver.isPresent()) {
			logger.error("There is no query resolver specified");
			return;
		}

		File root = new File(dir);
		Deque<File> dirs = new ArrayDeque<>();
		dirs.add(root);

		while (!dirs.isEmpty()) {
			File d = dirs.pop();
			File[] files = d.listFiles();

			if (files != null) {
				for (File f : files) {
					IPath filePath = new Path(f.getAbsolutePath());
					if (f.isFile() && f.getName().endsWith(".java")) {
						CompilationUnit cu = parseFile(filePath.toOSString());
						sqlanwrapper.analyze(cu, filePath, hsf.get(), resolver.get());
					} else if (f.isDirectory() && recursive) {
						dirs.add(f);
					}
				}
			}
		}

		logger.info("Parse directory done.");
	}

	public static void printStats() {

		List<Query> queries = project.getQueries();
		Set<ASTNode> execSet = new HashSet<>();
		for (Query q : queries) {
			execSet.add(q.getHotspot().getExec());
		}
		logger.info("Identified {} queries for {} execution points.", queries.size(), execSet.size());
	}

	public static void dumpQueries(String path) {
		try {
			List<Query> queries = project.getQueries();
			XMLQueries xmlQueries = new XMLQueries(path);
			xmlQueries.writeQueries(queries, false);
		} catch (IOException e) {
			logger.error("IO error while writing queries: ", e);
		}

	}

	public static void dumpBadSmells(String path) {
		try {
			Set<SQLSmell> smells = project.getSQLAn().getSmells();
			XMLSmellReporter smellReporter = new XMLSmellReporter(path);
			smellReporter.writeReport(smells);
		} catch (IOException e) {
			logger.error("IO error while writing smells: ", e);
		}

	}

	private static void createOptions() {
		final Option help = new Option(OPT_HELP, "print this message");
		final Option version = new Option(OPT_VERSION, "version information");
		final Option dumpQueries = new Option(OPT_DUMPQUERIES, true, "dump queries to XML");
		final Option dumpErrors = new Option(OPT_DUMPERRORS, true, "dump parsing errors");
		final Option errorsFormat = new Option(OPT_ERRORSFORMAT, true, "dump errors in the given format (txt/xml)");
		final Option classPath = new Option(OPT_CLASSPATH, true, "class path");

		final Option exportJson = new Option(OPT_EXPORTJSON, true, "export AST in JSON format");
		final Option dumpSymTab = new Option(OPT_DUMPSYMTAB, true, "dump Symbol Table");
		final Option runSmells = new Option(OPT_RUNSMELLS, "run smell detection");
		final Option smellReportFile = new Option(OPT_SMELLREPORT, true,
				"report smells to the given file (default to System.out)");
		final Option smellReportFormat = new Option(OPT_SMELLREPORTFORMAT, true,
				"report smells in the given format (txt/xml)");
		final Option parseDbSchema = new Option(OPT_PARSESCHEMA, "parse schema from database");
		final Option dburl = new Option(OPT_DBURL, true, "database URL");
		final Option dbuser = new Option(OPT_DBUSER, true, "database user");
		final Option dbpassword = new Option(OPT_DBPASSWORD, true, "database password");
		final Option hsfinder = new Option(OPT_HOTSPOTFINDER, true, "hotspot finder");
		final Option stringresolver = new Option(OPT_STRINGRESOLVER, true, "string resolver");

		options.addOption(help);
		options.addOption(version);
		options.addOption(dumpQueries);
		options.addOption(dumpErrors);
		options.addOption(errorsFormat);
		options.addOption(classPath);

		options.addOption(exportJson);
		options.addOption(dumpSymTab);
		options.addOption(runSmells);
		options.addOption(smellReportFile);
		options.addOption(smellReportFormat);
		options.addOption(parseDbSchema);
		options.addOption(dburl);
		options.addOption(dbuser);
		options.addOption(dbpassword);
		options.addOption(hsfinder);
		options.addOption(stringresolver);

	}

	private static void welcome() {
		logger.info("Hello dear SQLInspect user!");
	}

	private static void help() {
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp("SQLInspectCLI [options] dir1 [dir2]...", options);
	}

	public static void main(String[] args) {
		welcome();

		createOptions();

		context.loadResolvers();

		try {
			// create the parser
			CommandLineParser parser = new DefaultParser();

			// parse the command line arguments
			CommandLine cmdLine = parser.parse(options, args);
			String[] inputs = cmdLine.getArgs();

			if (cmdLine.hasOption(OPT_HELP) || inputs.length < 1) {
				help();
				return;
			}

			if (cmdLine.hasOption(OPT_CLASSPATH)) {
				properties.setProperty(OPT_CLASSPATH, cmdLine.getOptionValue(OPT_CLASSPATH));
			}

			if (cmdLine.hasOption(OPT_DUMPQUERIES)) {
				properties.setProperty(OPT_DUMPQUERIES, cmdLine.getOptionValue(OPT_DUMPQUERIES));
			}

			if (cmdLine.hasOption(OPT_DUMPERRORS)) {
				conf.setDumpErrors(cmdLine.getOptionValue(OPT_DUMPERRORS));
			}

			if (cmdLine.hasOption(OPT_ERRORSFORMAT)) {
				String format = cmdLine.getOptionValue(OPT_ERRORSFORMAT);
				List<String> formats = Arrays.asList(FORMATS);

				if (formats.contains(format.toLowerCase(Locale.ENGLISH))) {
					conf.setErrorsFormat(cmdLine.getOptionValue(OPT_ERRORSFORMAT));
				} else {
					logger.warn(
							"Smell report Format ({}) is not supported, using default value ({})! Supported formats: {}",
							format, conf.getSmellReportFormat(), formats);
				}
			}

			hsf = Optional.empty();
			String[] hsfs = new String[] { JDBCHotspotFinder.class.getSimpleName(),
					AndroidHotspotFinder.class.getSimpleName() };
			if (cmdLine.hasOption(OPT_HOTSPOTFINDER)) {
				String hsfName = cmdLine.getOptionValue(OPT_HOTSPOTFINDER);
				for (String hsfn : hsfs) {
					if (hsfn.equals(hsfName)) {
						hsf = Optional.of(SQLAnWrapper.getHotspotFinder(hsfn));
						break;
					}
				}
			}

			if (!hsf.isPresent()) {
				SQLAnWrapper.getHotspotFinder(JDBCHotspotFinder.class.getSimpleName());
			}

			resolver = Optional.of(SQLAnWrapper.getQueryResolver(InterQueryResolver.class.getName()));

			properties.setProperty("srcdir", String.join(":", inputs));

			sqlanwrapper.initAnalyzer();

			for (String i : inputs) {
				analyzeDir(i, true);
			}

			printStats();

			String dumpQueriesCmd = properties.getProperty(OPT_DUMPQUERIES);
			if (dumpQueriesCmd != null) {
				dumpQueries(dumpQueriesCmd);
			}

			// SQLAN config

			if (cmdLine.hasOption(OPT_EXPORTJSON)) {
				conf.setJsonExport(cmdLine.getOptionValue(OPT_EXPORTJSON));
			}

			if (cmdLine.hasOption(OPT_DUMPSYMTAB)) {
				conf.setDumpSymTab(cmdLine.getOptionValue(OPT_DUMPSYMTAB));
			}

			if (cmdLine.hasOption(OPT_DBURL)) {
				conf.setDbUrl(cmdLine.getOptionValue(OPT_DBURL));
			}

			if (cmdLine.hasOption(OPT_DBUSER)) {
				conf.setDbUser(cmdLine.getOptionValue(OPT_DBUSER));
			}

			if (cmdLine.hasOption(OPT_DBPASSWORD)) {
				conf.setDbPassword(cmdLine.getOptionValue(OPT_DBPASSWORD));
			}

			if (cmdLine.hasOption(OPT_PARSESCHEMA)) {
				if (conf.getDbUrl() == null || conf.getDbUrl().isEmpty()) {
					logger.warn(
							"The option parseDBSchema can be set only if a database URL is provided. This is not the case, so we ignore it.");
					conf.setParseDBSchema(false);
				} else {
					conf.setParseDBSchema(true);
				}
			}

			if (cmdLine.hasOption(OPT_SMELLREPORT)) {
				conf.setSmellReportFile(cmdLine.getOptionValue(OPT_SMELLREPORT));
			}

			if (cmdLine.hasOption(OPT_SMELLREPORTFORMAT)) {
				String format = cmdLine.getOptionValue(OPT_SMELLREPORTFORMAT);
				List<String> formats = Arrays.asList(FORMATS);

				if (formats.contains(format.toLowerCase(Locale.ENGLISH))) {
					conf.setSmellReportFormat(cmdLine.getOptionValue(OPT_SMELLREPORTFORMAT));
				} else {
					logger.warn(
							"Smell report Format ({}) is not supported, using default value ({})! Supported formats: {}",
							format, conf.getSmellReportFormat(), formats);
				}
			}

			if (cmdLine.hasOption(OPT_RUNSMELLS)) {
				conf.setRunSmells(true);
			}

			sqlanwrapper.setConf(conf);

			List<Query> queries = project.getQueries();
			sqlanwrapper.analyze(queries);
		} catch (ParserException e) {
			logger.error("SQLAnWrapper init error!", e);
		} catch (ParseException exp) {
			logger.error("Parsing failed. ", exp);
			logger.error("Try \"--help\" for further details.);");
		}
	}
}
